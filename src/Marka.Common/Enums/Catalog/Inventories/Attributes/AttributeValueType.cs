﻿namespace Marka.Common.Enums.Catalog.Inventories.Attributes
{
    public enum AttributeValueType
    {
        Attribute = 0,
        RelatedInventory = 1
    }
}