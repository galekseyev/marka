﻿namespace Marka.Common.Enums.Catalog.Inventories.Attributes
{
    public enum AttributeControlType
    {
        Links = 0,
        DropDownList = 1,
        RadioButtonList = 2
    }
}