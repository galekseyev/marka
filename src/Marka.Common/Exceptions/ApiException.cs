﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace Marka.Common.Exceptions
{
    public class ApiException : Exception
    {
        public HttpStatusCode Code { get; }

        public ApiException(string message, HttpStatusCode code)
            : this(message, code, null) { }

        public ApiException(string message, HttpStatusCode code, Exception innerException)
            : base(message, innerException)
        {
            Code = code;
        }
    }
}
