﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Marka.Common.ResponseModels.Languages
{
    public class LanguageModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string LanguageCode { get; set; }
    }
}