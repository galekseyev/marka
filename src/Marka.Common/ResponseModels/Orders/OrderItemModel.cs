﻿using Marka.Common.ResponseModels.Catalog.Inventories;
using System;
using System.Collections.Generic;
using System.Text;

namespace Marka.Common.ResponseModels.Orders
{
    public class OrderItemModel
    {
        public int Id { get; set; }

        public decimal Price { get; set; }

        public int Quantity { get; set; }

        public ICollection<OrderItemAttributesValuesXrefModel> OrderItemAttributesValuesXref { get; set; }
    }
}