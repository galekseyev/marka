﻿namespace Marka.Common.ResponseModels.Orders
{
    public class OrderStatusModel
    {
        public int Id { get; set; }
        public string StatusCode { get; set; }
    }
}