﻿using Marka.Common.ResponseModels.Catalog.Inventories;
using System;
using System.Collections.Generic;
using System.Text;

namespace Marka.Common.ResponseModels.Orders
{
    public class TranslatedOrderItemModel
    {
        public int Id { get; set; }
        public int Quantity { get; set; }

        public string Title { get; set; }
        public string Description { get; set; }
        public decimal OriginalPrice { get; set; }
        public decimal Price { get; set; }

        public InventoryPictureModel Picture { get; set; }

        public ICollection<TranslatedOrderItemAttributesValuesXrefModel> OrderItemAttributesValuesXref { get; set; }
    }
}
