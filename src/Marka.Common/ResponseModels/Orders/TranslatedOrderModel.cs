﻿using Marka.Common.ResponseModels.Payments;
using Marka.Common.ResponseModels.Shipment;
using System;
using System.Collections.Generic;

namespace Marka.Common.ResponseModels.Orders
{
    public class TranslatedOrderModel
    {
        public int Id { get; set; }

        public string OrderNumber { get; set; }

        public DateTime CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }

        public PaymentTypeModel PaymentType { get; set; }
        public PaymentStatusModel PaymentStatus { get; set; }

        public OrderStatusModel OrderStatus { get; set; }

        public decimal SubTotalPrice { get; set; }
        public decimal ShippingPrice { get; set; }
        public decimal TotalPrice { get; set; }

        public ShipmentModel Shipment { get; set; }
        public ICollection<TranslatedOrderItemModel> OrderItems { get; set; }
    }
}