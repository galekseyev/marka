﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Marka.Common.ResponseModels.Orders
{
    public class OrderItemAttributesValuesXrefModel
    {
        public int? Id { get; set; }
        public int InventoryAttributeId { get; set; }
        public int InventoryAttributeValueId { get; set; }
    }
}