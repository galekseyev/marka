﻿using Marka.Common.ResponseModels.Shared;
using System;

namespace Marka.Common.ResponseModels.Carts
{
    public class CartShippingModel : AddressModel
    {
        public int? Id { get; set; }
        public string Email { get; set; }
        public string FullName { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
    }
}