﻿using Marka.Common.ResponseModels.Catalog.Inventories;
using System.Collections.Generic;

namespace Marka.Common.ResponseModels.Carts
{
    public class TranslatedCartItemModel
    {
        public int Id { get; set; }
        public int Quantity { get; set; }
        public int InventoryQuantity { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public decimal OriginalPrice { get; set; }
        public decimal Price { get; set; }

        public InventoryPictureModel Picture { get; set; }

        public ICollection<TranslatedCartItemAttributesValuesXrefModel> CartItemAttributesValuesXref { get; set; }
    }


}