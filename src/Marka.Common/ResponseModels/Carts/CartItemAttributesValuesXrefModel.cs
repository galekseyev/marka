﻿namespace Marka.Common.ResponseModels.Carts
{
    public class CartItemAttributesValuesXrefModel
    {
        public int? Id { get; set; }
        public int InventoryAttributeId { get; set; }
        public int InventoryAttributeValueId { get; set; }
    }
}