﻿using System;
using System.Collections.Generic;

namespace Marka.Common.ResponseModels.Carts
{
    public class CartModel
    {
        public string Key { get; set; }

        public DateTime CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }

        public decimal SubTotalPrice { get; set; }
        public decimal ShippingPrice { get; set; }
        public decimal TotalPrice { get; set; }

        public ICollection<CartItemModel> CartItems { get; set; }
    }
}