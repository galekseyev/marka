﻿using System.Collections.Generic;

namespace Marka.Common.ResponseModels.Carts
{
    public class CartItemModel
    {
        public int Id { get; set; }
        public int Quantity { get; set; }

        ICollection<CartItemAttributesValuesXrefModel> CartItemAttributesValuesXref { get; set; }
    }
}