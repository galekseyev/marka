﻿namespace Marka.Common.ResponseModels.Account
{
    public class UserModel
    {
        public string Email { get; set; }
        public string FullName { get; set; }
    }
}