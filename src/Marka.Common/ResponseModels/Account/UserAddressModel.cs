﻿using Marka.Common.ResponseModels.Shared;

namespace Marka.Common.ResponseModels.Account
{
    public class UserAddressModel : AddressModel
    {
        public int? Id { get; set; }
    }
}