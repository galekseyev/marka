﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Marka.Common.ResponseModels.Account
{
    public class UserProfileModel : UserModel
    {
        public UserAddressModel Address { get; set; }
    }
}