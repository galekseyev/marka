﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Marka.Common.ResponseModels.Countries
{
    public class CountryModel
    {
        public int Id { get; set; }

        public string CountryCode { get; set; }
        public string CountryName { get; set; }
        public string NationalIdMask { get; set; }
        public string NationalIdNumber { get; set; }
        public string NationalIdRegEx { get; set; }
        public string PostalCodeRegEx { get; set; }

        public ICollection<CountryStateModel> CountryStates { get; set; }
    }
}