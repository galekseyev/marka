﻿namespace Marka.Common.ResponseModels.Countries
{
    public class CountryStateModel
    {
        public int Id { get; set; }

        public string StateCode { get; set; }
        public string StateName { get; set; }
    }
}
