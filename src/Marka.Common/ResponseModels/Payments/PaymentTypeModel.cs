﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Marka.Common.ResponseModels.Payments
{
    public class PaymentTypeModel
    {
        public int Id { get; set; }
        public string TypeCode { get; set; }
    }
}