﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Marka.Common.ResponseModels.Payments
{
    public class PaymentStatusModel
    {
        public int Id { get; set; }
        public string StatusCode { get; set; }
    }
}
