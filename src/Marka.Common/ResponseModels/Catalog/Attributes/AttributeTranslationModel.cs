﻿using System.ComponentModel.DataAnnotations;

namespace Marka.Common.ResponseModels.Catalog.Attributes
{
    public class AttributeTranslationModel
    {
        public int? Id { get; set; }

        public int LanguageId { get; set; }

        [Required]
        [StringLength(255, MinimumLength = 1)]
        public string Title { get; set; }
        public string Description { get; set; }
    }
}
