﻿using System.Collections.Generic;

namespace Marka.Common.ResponseModels.Catalog.Attributes
{
    public class AttributeModel
    {
        public int Id { get; set; }

        public bool Disabled { get; set; }

        public IEnumerable<AttributeTranslationModel> Translations { get; set; }
    }
}