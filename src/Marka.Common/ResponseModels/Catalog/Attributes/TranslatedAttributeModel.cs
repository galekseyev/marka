﻿namespace Marka.Common.ResponseModels.Catalog.Attributes
{
    public class TranslatedAttributeModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
    }
}