﻿using Marka.Common.Models.Catalog.Inventories;

namespace Marka.Common.ResponseModels.Catalog.InventoryGroups
{
    public class TranslatedInventoryGroupInventoryModel
    {
        public int Id { get; set; }
        public int InventoryId { get; set; }
        public string Title { get; set; }

        public InventoryPicture Picture { get; set; }

        public bool Disabled { get; set; }
        public int DisplayOrder { get; set; }
    }
}