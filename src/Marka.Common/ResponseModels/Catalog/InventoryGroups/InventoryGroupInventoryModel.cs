﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Marka.Common.ResponseModels.Catalog.InventoryGroups
{
    public class InventoryGroupInventoryModel
    {
        public int Id { get; set; }
        public bool Disabled { get; set; }
        public int DisplayOrder { get; set; }

        public int InventoryId { get; set; }
    }
}
