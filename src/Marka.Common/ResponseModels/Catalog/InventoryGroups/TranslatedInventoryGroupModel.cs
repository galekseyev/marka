﻿using Marka.Common.Models.Catalog.InventoryGroups;
using System;
using System.Collections.Generic;
using System.Text;

namespace Marka.Common.ResponseModels.Catalog.InventoryGroups
{
    public class TranslatedInventoryGroupModel
    {
        public int Id { get; set; }
        public bool Disabled { get; set; }
        public int DisplayOrder { get; set; }

        public virtual InventoryGroupPicture Picture { get; set; }
        public virtual ICollection<TranslatedInventoryGroupInventory> Inventories { get; set; }
    }
}
