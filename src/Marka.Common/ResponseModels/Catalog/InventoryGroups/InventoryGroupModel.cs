﻿using System.Collections.Generic;

namespace Marka.Common.ResponseModels.Catalog.InventoryGroups
{
    public class InventoryGroupModel
    {
        public int Id { get; set; }
        public bool Disabled { get; set; }
        public int DisplayOrder { get; set; }

        public InventoryGroupPictureModel Picture { get; set; }
        public ICollection<InventoryGroupInventoryModel> Inventories { get; set; }
    }
}