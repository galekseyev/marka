﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Marka.Common.ResponseModels.Catalog.Specifications
{
    public class SpecificationValueModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public int DisplayOrder { get; set; }


        public IEnumerable<SpecificationValueTranslationModel> Translations { get; set; }
    }
}