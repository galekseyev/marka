﻿using System.Collections.Generic;

namespace Marka.Common.ResponseModels.Catalog.Specifications
{
    public class SpecificationModel
    {
        public int Id { get; set; }

        public bool Disabled { get; set; }

        public IEnumerable<SpecificationTranslationModel> Translations { get; set; }
    }
}