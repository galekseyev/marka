﻿using System.Collections;
using System.Collections.Generic;

namespace Marka.Common.ResponseModels.Catalog.Specifications
{
    public class TranslatedSpecificationModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }

        public ICollection<TranslatedSpecificationValueModel> Values { get; set; }
    }
}