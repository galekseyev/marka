﻿using System.ComponentModel.DataAnnotations;

namespace Marka.Common.ResponseModels.Catalog.Specifications
{
    public class SpecificationValueTranslationModel
    {
        public int? Id { get; set; }

        public int LanguageId { get; set; }

        [Required]
        [StringLength(255, MinimumLength = 1)]
        public string Title { get; set; }
    }
}