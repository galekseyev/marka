﻿using System.Collections.Generic;

namespace Marka.Common.ResponseModels.Catalog.Inventories
{
    public class InventoryAttributeValueModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public int DisplayOrder { get; set; }

        public InventoryPictureModel Picture { get; set; }

        public IEnumerable<InventoryAttributeValueTranslationModel> Translations { get; set; }
    }
}