﻿using System.ComponentModel.DataAnnotations;

namespace Marka.Common.ResponseModels.Catalog.Inventories
{
    public class InventoryAttributeValueTranslationModel
    {
        public int? Id { get; set; }

        public int LanguageId { get; set; }

        [Required]
        [StringLength(255, MinimumLength = 1)]
        public string Title { get; set; }
    }
}