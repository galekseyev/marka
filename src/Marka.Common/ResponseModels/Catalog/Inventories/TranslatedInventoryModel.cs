﻿using System.Collections.Generic;

namespace Marka.Common.ResponseModels.Catalog.Inventories
{
    public class TranslatedInventoryModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }

        public decimal OriginalPrice { get; set; }
        public decimal Price { get; set; }

        public int Quantity { get; set; }

        public ICollection<InventoryPictureModel> Pictures { get; set; }
        public ICollection<TranslatedInventoryAttributeModel> Attributes { get; set; }
        public ICollection<TranslatedInventorySpecificationModel> Specifications { get; set; }
    }
}