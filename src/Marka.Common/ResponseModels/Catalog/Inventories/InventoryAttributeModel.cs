﻿using Marka.Common.Enums.Catalog.Inventories.Attributes;
using Marka.Common.ResponseModels.Catalog.Attributes;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Collections.Generic;

namespace Marka.Common.ResponseModels.Catalog.Inventories
{
    public class InventoryAttributeModel
    {
        public int Id { get; set; }
        public bool IsRequired { get; set; }
        public bool Disabled { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public AttributeControlType ControlType { get; set; }

        public int DisplayOrder { get; set; }
        public int AttributeId { get; set; }


        //public ICollection<InventoryAttributeValueModel> Values { get; set; }
    }
}