﻿using System.Collections.Generic;

namespace Marka.Common.ResponseModels.Catalog.Inventories
{
    public class InventoryModel
    {
        public int Id { get; set; }

        public bool Disabled { get; set; }

        public decimal OriginalPrice { get; set; }
        public decimal Price { get; set; }

        public int Quantity { get; set; }

        public int CategoryId { get; set; }

        public ICollection<InventoryTranslationModel> Translations { get; set; }
    }
}