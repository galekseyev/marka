﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Marka.Common.ResponseModels.Catalog.Inventories
{
    public class TranslatedInventoryAttributeValueModel
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public bool Disabled { get; set; }
        public int DisplayOrder { get; set; }

        public InventoryPictureModel Picture { get; set; }
    }
}
