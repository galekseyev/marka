﻿using Marka.Common.ResponseModels.Catalog.Specifications;

namespace Marka.Common.ResponseModels.Catalog.Inventories
{
    public class TranslatedInventorySpecificationModel 
    {
        public int Id { get; set; }

        public bool Disabled { get; set; }
        public bool Hidden { get; set; }

        public int DisplayOrder { get; set; }

        public TranslatedSpecificationModel Specification { get; set; }

        public TranslatedSpecificationValueModel SpecificationValue { get; set; }
    }
}