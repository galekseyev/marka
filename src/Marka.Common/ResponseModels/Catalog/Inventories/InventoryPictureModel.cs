﻿namespace Marka.Common.ResponseModels.Catalog.Inventories
{
    public class InventoryPictureModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Alt { get; set; }
        public string Thumbnail { get; set; }
        public string Picture { get; set; }
    }
}