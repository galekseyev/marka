﻿namespace Marka.Common.ResponseModels.Catalog.Inventories
{
    public class InventorySpecificationModel
    {
        public int Id { get; set; }

        public bool Disabled { get; set; }
        public bool Hidden { get; set; }

        public int DisplayOrder { get; set; }

        public int SpecificationId { get; set; }

        public int SpecificationValueId { get; set; }
    }
}