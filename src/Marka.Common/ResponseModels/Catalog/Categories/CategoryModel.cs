﻿using System.Collections.Generic;

namespace Marka.Common.ResponseModels.Catalog.Categories
{
    public class CategoryModel
    {
        public int Id { get; set; }

        public bool Disabled { get; set; }

        public IEnumerable<CategoryTranslationModel> Translations { get; set; }
    }
}