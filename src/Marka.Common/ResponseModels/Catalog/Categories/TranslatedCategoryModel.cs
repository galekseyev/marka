﻿namespace Marka.Common.ResponseModels.Catalog.Categories
{
    public class TranslatedCategoryModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
    }
}
