﻿using System;

namespace Marka.Common.ResponseModels.Blog
{
    public class TranslatedArticleModel
    {
        public int Id { get; set; }

        public string Title { get; set; }
        public string Description { get; set; }

        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }

        public bool Disabled { get; set; }

        public ArticlePictureModel Picture { get; set; }
    }
}
