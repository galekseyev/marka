﻿using System.ComponentModel.DataAnnotations;

namespace Marka.Common.ResponseModels.Blog
{
    public class ArticleTranslationModel
    {
        public int? Id { get; set; }

        [Required]
        [StringLength(255, MinimumLength = 1)]
        public string Title { get; set; }

        [Required]
        public string Description { get; set; }

        public int LanguageId { get; set; }
    }
}