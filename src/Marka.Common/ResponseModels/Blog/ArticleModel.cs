﻿using System;
using System.Collections.Generic;

namespace Marka.Common.ResponseModels.Blog
{
    public class ArticleModel
    {
        public int Id { get; set; }

        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }

        public bool Disabled { get; set; }

        public ArticlePictureModel Picture { get; set; }

        public ICollection<ArticleTranslationModel> Translations { get; set; }
    }
}
