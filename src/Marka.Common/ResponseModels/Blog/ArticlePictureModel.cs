﻿namespace Marka.Common.ResponseModels.Blog
{
    public class ArticlePictureModel
    {
        public int Id { get; set; }

        public string Title { get; set; }
        public string Alt { get; set; }

        public string Thumbnail { get; set; }

        public string Picture { get; set; }
    }
}