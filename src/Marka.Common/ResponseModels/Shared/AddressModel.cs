﻿using Marka.Common.ResponseModels.Countries;

namespace Marka.Common.ResponseModels.Shared
{
    public class AddressModel
    {
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public CountryModel Country { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
    }
}