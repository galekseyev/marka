﻿namespace Marka.Common.Constants
{
    public class PaymentStatusCode
    {
        public const string Pending = "PNDG";
        public const string Paid = "PD";
        public const string Canceled = "CNLD";
        public const string Refunded = "RFND";
        public const string PartiallyRefunded = "PRFND";
    }
}