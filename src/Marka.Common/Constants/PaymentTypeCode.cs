﻿namespace Marka.Common.Constants
{
    public class PaymentTypeCode
    {
        public const string Cash = "CSH";
        public const string BankTransfer = "BNTNF";
        public const string CreditDebitCard = "CDCRD";
        public const string Paypal = "PPL";
    }
}