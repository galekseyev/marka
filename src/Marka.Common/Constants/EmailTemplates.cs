﻿namespace Marka.Common.Constants
{
    public class EmailTemplateType
    {
        public const string Account = "AccountEmailTemplates";
        public const string Contact = "ContactEmailTemplates";
        public const string Order = "OrderEmailTemplates";
    }
}