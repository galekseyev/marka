﻿namespace Marka.Common.Constants
{
    public class OrderStatusCode
    {
        public const string Pending = "PNDG";
        public const string Shipped = "SHPD";
        public const string Delivered = "DLVRD";
        public const string Canceled = "CNLD";
        public const string Refunded = "RFND";
        public const string PartiallyRefunded = "PRFND";
    }
}