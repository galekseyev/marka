﻿namespace Marka.Common.Constants
{
    public class RoleName
    {
        public const string User = "User";
        public const string Admin = "Admin";
    }
}