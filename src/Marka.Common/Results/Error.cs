﻿namespace Marka.Common.Results
{
    public class Error
    {
        private const string UnknownError = "Unknown Error";

        public string Message { get; }

        public Error(string message)
        {
            Message = message ?? UnknownError;
        }
    }

    public static class Errors
    {
        public static Error NotFound { get; } = new Error("Not found.");
    }
}
