﻿using System;
using System.Collections.Generic;
using System.Net;

namespace Marka.Common.Results.Interfaces
{
    //public interface IResultFactory<TSource> : IResultFactory
    //{

    //}

    public interface IResultFactory
    {
        IResult Create(HttpStatusCode code);
        IResult Create(HttpStatusCode code, Exception ex);
        IResult Create(HttpStatusCode code, IEnumerable<Error> errors);
        IResult<TValue> Create<TValue>(HttpStatusCode code, TValue value);
        IResult<TValue> Create<TValue>(HttpStatusCode code, Exception ex);
        IResult<TValue> Create<TValue>(HttpStatusCode code, IEnumerable<Error> errors);
    }
}
