﻿using System;
using System.Collections.Generic;
using System.Net;

namespace Marka.Common.Results.Interfaces
{
    public interface IResult<out TValue> : IResult
    {
        TValue Value { get; }
    }

    public interface IResult
    {
        HttpStatusCode Code { get; }
        Exception Exception { get; }
        IReadOnlyCollection<Error> Errors { get; }
    }
}