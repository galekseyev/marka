﻿using Marka.Common.Results.Interfaces;
using System;
using System.Collections.Generic;
using System.Net;

namespace Marka.Common.Results
{
    public static class ResultFactoryExtension
    {
        public static IResult Success(this IResultFactory factory) => factory.Create(HttpStatusCode.OK);
        public static IResult<TValue> Success<TValue>(this IResultFactory factory, TValue value) => factory.Create(HttpStatusCode.OK, value);

        public static IResult Created(this IResultFactory factory) => factory.Create(HttpStatusCode.Created);
        public static IResult<TValue> Created<TValue>(this IResultFactory factory, TValue value) => factory.Create(HttpStatusCode.Created, value);

        public static IResult NotFound(this IResultFactory factory) => factory.Error(HttpStatusCode.NotFound, Errors.NotFound);
        public static IResult<TValue> NotFound<TValue>(this IResultFactory factory) => factory.Error<TValue>(HttpStatusCode.NotFound, Errors.NotFound);



        public static IResult Error(this IResultFactory factory, string message) => factory.Error(HttpStatusCode.InternalServerError, new Error(message));

        public static IResult Error(this IResultFactory factory, Error error)
        {
            if (error == null)
            {
                throw new ArgumentNullException(nameof(error));
            }

            return factory.Error(HttpStatusCode.InternalServerError, error);
        }

        public static IResult Error(this IResultFactory factory, HttpStatusCode code, Error error) => factory.Error(code, new[] { error });

        public static IResult Error(this IResultFactory factory, HttpStatusCode code, IEnumerable<Error> errors) => factory.Create(code, errors);

        public static IResult<TValue> Error<TValue>(this IResultFactory factory, string message) => factory.Error<TValue>(HttpStatusCode.InternalServerError, new Error(message));

        public static IResult<TValue> Error<TValue>(this IResultFactory factory, Error error)
        {
            if (error == null)
                throw new ArgumentNullException(nameof(error));

            return factory.Error<TValue>(HttpStatusCode.InternalServerError, error);
        }

        public static IResult<TValue> Error<TValue>(this IResultFactory factory, HttpStatusCode code, Error error) => factory.Create<TValue>(code, new[] { error });

        public static IResult<TValue> Error<TValue>(this IResultFactory factory, HttpStatusCode code, IEnumerable<Error> errors) => factory.Create<TValue>(code, errors);



        public static IResult Exception(this IResultFactory factory, Exception ex)
        {
            return factory.Create(HttpStatusCode.InternalServerError, ex);
        }

        public static IResult<TValue> Exception<TValue>(this IResultFactory factory, Exception ex)
        {
            return factory.Create<TValue>(HttpStatusCode.InternalServerError, ex);
        }
    }
}
