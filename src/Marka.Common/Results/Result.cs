﻿using Marka.Common.Results.Interfaces;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;

namespace Marka.Common.Results
{
    public class Result<TValue> : Result, IResult<TValue>
    {
        public TValue Value { get; }

        public Result(HttpStatusCode code, TValue value) : base(code)
        {
            Value = value;
        }

        public Result(HttpStatusCode code, IEnumerable<Error> errors) : base(code, errors) { }

        public Result(HttpStatusCode code, Exception ex) : base(code, ex) { }
    }

    public class Result : IResult
    {
        private const string InvalidCodeMessage = "Status code must be a non-error code when not supplying an error or exception.";

        public HttpStatusCode Code { get; }

        public Exception Exception { get; }

        public IReadOnlyCollection<Error> Errors { get; }
        public Result(HttpStatusCode code) : this(code, null, null) { }


        public Result(HttpStatusCode code, IEnumerable<Error> errors) : this(code, errors, null)
        {
            if (errors == null)
                throw new ArgumentNullException(nameof(errors));
        }

        public Result(HttpStatusCode code, Exception ex) : this(code, null, ex)
        {
            if (ex == null)
                throw new ArgumentNullException(nameof(ex));
        }

        public Result(HttpStatusCode code, IEnumerable<Error> errors, Exception ex)
        {
            Code = code;
            Exception = ex;

            if (errors == null || !errors.Any())
                Errors = new ReadOnlyCollection<Error>(new List<Error>());
            else
                Errors = errors.ToList();

            if (ex == null && !Errors.Any())
            {
                // Ensure that the status code indicates success since we have no errors
                if (code >= HttpStatusCode.BadRequest)
                {
                    throw new ArgumentException(InvalidCodeMessage, nameof(code));
                }
            }
        }
    }
}
