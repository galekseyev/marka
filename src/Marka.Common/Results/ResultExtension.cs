﻿using Marka.Common.Results.Interfaces;
using System.Linq;

namespace Marka.Common.Results
{
    public static class ResultExtension
    {
        public static bool IsSuccessful(this IResult result)
        {
            return result.Exception == null &&
                   (result.Errors == null || !result.Errors.Any()) &&
                   (int)result.Code < 400;
        }
    }
}