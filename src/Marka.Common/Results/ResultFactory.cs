﻿using Marka.Common.Results.Interfaces;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace Marka.Common.Results
{
    public class ResultFactory : IResultFactory/*<TSource> : IResultFactory<TSource>*/
    {
        public IResult Create(HttpStatusCode code) => new Result(code);
        public IResult<TValue> Create<TValue>(HttpStatusCode code, TValue value) => new Result<TValue>(code, value);

        public IResult Create(HttpStatusCode code, Exception ex)
        {
            if (ex == null)
                throw new ArgumentNullException(nameof(ex));

            return new Result(code, ex);
        }

        public IResult Create(HttpStatusCode code, IEnumerable<Error> errors)
        {
            if (errors == null)
                throw new ArgumentNullException(nameof(errors));

            return new Result(code, errors);
        }

        public IResult<TValue> Create<TValue>(HttpStatusCode code, Exception ex)
        {
            if (ex == null)
                throw new ArgumentNullException(nameof(ex));

            return new Result<TValue>(code, ex);
        }

        public IResult<TValue> Create<TValue>(HttpStatusCode code, IEnumerable<Error> errors)
        {
            if (errors == null)
                throw new ArgumentNullException(nameof(errors));

            return new Result<TValue>(code, errors);
        }
    }
}
