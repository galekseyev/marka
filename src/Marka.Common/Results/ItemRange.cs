﻿using System.Collections.Generic;

namespace Marka.Common.Results
{
    public class ItemRange<T>
    {
        public ItemRange()
        {
            Items = new List<T>();
            Total = 0;
        }
        public IEnumerable<T> Items { get; set; }
        public long Total { get; set; }
    }
}