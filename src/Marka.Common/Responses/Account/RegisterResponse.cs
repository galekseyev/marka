﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace Marka.Common.Responses.Account
{
    public class RegisterResponse : ApiResponse
    {
        public RegisterResponse()
            : base()
        { }

        public RegisterResponse(HttpStatusCode resultCode)
            : base(resultCode)
        { }

        public RegisterResponse(string message, HttpStatusCode resultCode)
            : base(message, resultCode)
        { }

        /// <summary>
        /// Returns JWT token.
        /// </summary>
        public string Token { get; set; }

        /// <summary>
        /// Returns JWT token expiration ticks.
        /// </summary>
        public long? Expiration { get; set; }
    }
}