﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Marka.Common.Responses.Account
{
    public class UserProfileRequest 
    {
        [Required]
        public string FullName { get; set; }

        public int? AddressId { get; set; }

        [Required]
        public string Address1 { get; set; }

        [Required]
        public string CountryId { get; set; }

        [Required]
        public string City { get; set; }
        public string State { get; set; }

        [Required]
        public string PostalCode { get; set; }
    }
}
