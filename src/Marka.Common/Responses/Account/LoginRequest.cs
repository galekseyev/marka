﻿using System.ComponentModel.DataAnnotations;

namespace Marka.Common.Responses.Account
{
    public class LoginRequest
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        //public bool Persistant { get; set; }
    }
}