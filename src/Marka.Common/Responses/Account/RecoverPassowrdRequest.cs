﻿using System.ComponentModel.DataAnnotations;

namespace Marka.Common.Responses.Account
{
    public class RecoverPassowrdRequest
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}