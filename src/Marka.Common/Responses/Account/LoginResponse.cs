﻿using System.Net;

namespace Marka.Common.Responses.Account
{
    public class LoginResponse : ApiResponse
    {
        public LoginResponse()
            : base()
        { }

        public LoginResponse(HttpStatusCode resultCode)
            : base(resultCode)
        { }

        public LoginResponse(string message, HttpStatusCode resultCode)
            : base(message, resultCode)
        { }

        /// <summary>
        /// Returns JWT token.
        /// </summary>
        public string Token { get; set; }

        /// <summary>
        /// Returns JWT token expiration ticks.
        /// </summary>
        //public long? Expiration { get; set; }

        /// <summary>
        ///  Returns a flag indication whether the sign-in was successful.
        /// </summary>
        public bool Succeeded { get; protected set; }

        /// <summary>
        /// Returns a flag indication whether the user attempting to sign-in is locked out.
        /// </summary>
        public bool IsLockedOut { get; protected set; }

        /// <summary>
        ///  Returns a flag indication whether the user attempting to sign-in is not allowed to sign-in.
        /// </summary>
        public bool IsNotAllowed { get; protected set; }

        /// <summary>
        /// Returns a flag indication whether the user attempting to sign-in requires two factor authentication.
        /// </summary>
        public bool RequiresTwoFactor { get; protected set; }
    }
}