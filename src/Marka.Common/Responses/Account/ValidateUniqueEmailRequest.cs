﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Marka.Common.Responses.Account
{
    public class ValidateUniqueEmailRequest
    {
        public int? UserId { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}