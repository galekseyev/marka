﻿using Marka.Common.ResponseModels.Account;
using System.Net;

namespace Marka.Common.Responses.Account
{
    public class UserProfileResponse : EmbeddedApiResponse<UserProfileModel>
    {
        public UserProfileResponse()
             : base()
        { }

        public UserProfileResponse(HttpStatusCode resultCode)
            : base(resultCode)
        { }

        public UserProfileResponse(string message, HttpStatusCode resultCode)
            : base(message, resultCode)
        { }
    }
}