﻿using Marka.Common.ResponseModels.Account;
using System.Net;

namespace Marka.Common.Responses.Account
{
    public class UserResponse : EmbeddedApiResponse<UserModel>
    {
        public UserResponse()
             : base()
        { }

        public UserResponse(HttpStatusCode resultCode)
            : base(resultCode)
        { }

        public UserResponse(string message, HttpStatusCode resultCode)
            : base(message, resultCode)
        { }
    }
}