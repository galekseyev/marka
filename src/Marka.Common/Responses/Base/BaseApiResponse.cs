﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace Marka.Common.Responses.Base
{
    public abstract class BaseApiResponse<TCode, TEmbedded> : HalResource<TEmbedded>
        where TCode : struct
    {
        protected BaseApiResponse()
            : this(null, default(TCode))
        { }

        protected BaseApiResponse(TCode resultCode)
            :this(resultCode.ToString(), resultCode)
        { }

        protected BaseApiResponse(string message, TCode resultCode)
        {
            Message = message;
            Code = resultCode;
        }

        [JsonProperty("code")]
        public TCode Code { get; set; }

        [JsonProperty("message", NullValueHandling = NullValueHandling.Ignore)]
        public string Message { get; set; }

        [JsonProperty("errors", NullValueHandling = NullValueHandling.Ignore)]
        public List<string> Errors { get; set; }

        public void AddError(string error)
        {
            if (Errors == null)
                Errors = new List<string>();

            Errors.Add(error);
        }
    }
}