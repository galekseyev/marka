﻿using Marka.Common.Responses.Base;
using Marka.Common.Responses.Interfaces;
using Newtonsoft.Json;

namespace Marka.Common.Responses
{
    public abstract class HalResource<TEmbedded> : IHalResource
    {
        [JsonProperty("links", NullValueHandling = NullValueHandling.Ignore)]
        public HalLinks Links { get; set; }
        
        [JsonProperty("embedded", NullValueHandling = NullValueHandling.Ignore)]
        public TEmbedded Embedded { get; set; }
    }
}
