﻿using Newtonsoft.Json;

namespace Marka.Common.Responses.Base
{
    public abstract class PagedApiResponse<TCode, TEmbedded> : BaseApiResponse<TCode, TEmbedded>
        where TCode : struct
    {
        protected PagedApiResponse() { }

        protected PagedApiResponse(TCode resultCode)
            :base (resultCode)
        { }

        protected PagedApiResponse(string message, TCode resultCode)
            :base (message, resultCode)
        { }

        [JsonProperty("total", DefaultValueHandling = DefaultValueHandling.Ignore)]
        public long? Total { get; set; }

        [JsonProperty("count")]
        public long Count { get; set; }
    }
}