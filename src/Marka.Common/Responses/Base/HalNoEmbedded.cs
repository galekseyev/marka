﻿namespace Marka.Common.Responses.Base
{
    /// <summary>
    /// Empty class used to indicate that a <see cref="HalResource{TEmbedded}"/> has no embedded resources.
    /// </summary>
    public sealed class HalNoEmbedded
    {
        private HalNoEmbedded()
        {
        }
    }
}