﻿using Marka.Common.Responses.Base;

namespace Marka.Common.Responses.Interfaces
{
    public interface IHalResource
    {
        HalLinks Links { get; set; }
    }
}