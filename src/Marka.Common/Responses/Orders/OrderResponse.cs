﻿using Marka.Common.Models.Orders;
using Marka.Common.ResponseModels.Orders;
using System.Net;

namespace Marka.Common.Responses.Orders
{
    public class OrderResponse : EmbeddedApiResponse<OrderModel>
    {
        public OrderResponse()
            : base()
        { }

        public OrderResponse(HttpStatusCode resultCode)
            : base(resultCode)
        { }

        public OrderResponse(string message, HttpStatusCode resultCode)
            : base(message, resultCode)
        { }
    }
}
