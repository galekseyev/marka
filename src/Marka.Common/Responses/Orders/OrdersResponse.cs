﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Marka.Common.Responses.Orders
{
    public class OrdersResponse : EmbeddedPagedApiResponse<IEnumerable<OrderResponse>>
    { }
}
