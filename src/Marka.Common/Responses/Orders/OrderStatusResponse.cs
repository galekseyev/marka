﻿using Marka.Common.ResponseModels.Orders;
using System.Net;

namespace Marka.Common.Responses.Orders
{
    public class OrderStatusResponse : EmbeddedApiResponse<OrderStatusModel>
    {
        public OrderStatusResponse()
            : base()
        { }

        public OrderStatusResponse(HttpStatusCode resultCode)
            : base(resultCode)
        { }

        public OrderStatusResponse(string message, HttpStatusCode resultCode)
            : base(message, resultCode)
        { }
    }
}