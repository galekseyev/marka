﻿using System.ComponentModel.DataAnnotations;

namespace Marka.Common.Responses.Orders
{
    public class OrderRequest
    {
        public string PaymentTypeCode { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        public string FullName { get; set; }
        public string PhoneNumber { get; set; }

        [Required]
        public string Address1 { get; set; }

        [Required]
        public string CountryId { get; set; }

        [Required]
        public string City { get; set; }
        public string State { get; set; }

        [Required]
        public string PostalCode { get; set; }
    }
}
