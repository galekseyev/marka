﻿using Marka.Common.ResponseModels.Orders;
using System.Net;

namespace Marka.Common.Responses.Orders
{
    public class TranslatedOrderResponse : EmbeddedApiResponse<TranslatedOrderModel>
    {
        public TranslatedOrderResponse()
            : base()
        { }

        public TranslatedOrderResponse(HttpStatusCode resultCode)
            : base(resultCode)
        { }

        public TranslatedOrderResponse(string message, HttpStatusCode resultCode)
            : base(message, resultCode)
        { }
    }
}
