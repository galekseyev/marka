﻿using System.Collections.Generic;

namespace Marka.Common.Responses.Orders
{
    public class OrderStatusesResponse : EmbeddedPagedApiResponse<IEnumerable<OrderStatusResponse>>
    { }
}
