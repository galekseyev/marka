﻿using Marka.Common.Responses.Base;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace Marka.Common.Responses
{
    public class EmbeddedApiResponse<TEmbedded> : BaseApiResponse<HttpStatusCode, TEmbedded>
    {
        public EmbeddedApiResponse()
            :this(HttpStatusCode.OK.ToString(), HttpStatusCode.OK)
        { }

        public EmbeddedApiResponse(HttpStatusCode resultCode)
            :base(resultCode)
        { }

        public EmbeddedApiResponse(string message, HttpStatusCode resultCode)
            :base(message, resultCode)
        { }
    }
}
