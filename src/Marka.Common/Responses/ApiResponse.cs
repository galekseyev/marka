﻿using Marka.Common.Responses.Base;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace Marka.Common.Responses
{
    /// <summary>
    /// Generic API response containing a message and result code, and no embedded
    /// </summary>
    public class ApiResponse : BaseApiResponse<HttpStatusCode, HalNoEmbedded>
    {
        public ApiResponse()
            : this(HttpStatusCode.OK.ToString(), HttpStatusCode.OK)
        { }

        public ApiResponse(HttpStatusCode resultCode)
            : base(resultCode)
        { }

        public ApiResponse(string message, HttpStatusCode resultCode)
            : base(message, resultCode)
        { }
    }
}
