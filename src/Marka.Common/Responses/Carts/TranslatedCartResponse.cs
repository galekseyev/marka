﻿using Marka.Common.ResponseModels.Carts;
using System.Net;

namespace Marka.Common.Responses.Carts
{
    public class TranslatedCartResponse : EmbeddedApiResponse<TranslatedCartModel>
    {
        public TranslatedCartResponse()
            : base()
        { }

        public TranslatedCartResponse(HttpStatusCode resultCode)
            : base(resultCode)
        { }

        public TranslatedCartResponse(string message, HttpStatusCode resultCode)
            : base(message, resultCode)
        { }
    }
}
