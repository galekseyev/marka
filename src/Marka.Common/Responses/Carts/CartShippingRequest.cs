﻿using Marka.Common.Models.Shared;
using System;

namespace Marka.Common.Responses.Carts
{
    public class CartShippingRequest : Address
    { 
        public string Email { get; set; }
        public string FullName { get; set; }

        public string PhoneNumber { get; set; }

        public DateTime CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
    }
}