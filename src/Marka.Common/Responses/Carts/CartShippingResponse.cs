﻿using Marka.Common.ResponseModels.Carts;
using System.Net;

namespace Marka.Common.Responses.Carts
{
    public class CartShippingResponse : EmbeddedApiResponse<CartShippingModel>
    {
        public CartShippingResponse()
            : base()
        { }

        public CartShippingResponse(HttpStatusCode resultCode)
            : base(resultCode)
        { }

        public CartShippingResponse(string message, HttpStatusCode resultCode)
            : base(message, resultCode)
        { }
    }
}
