﻿using Marka.Common.ResponseModels.Carts;
using System.Net;

namespace Marka.Common.Responses.Carts
{
    public class CartResponse : EmbeddedApiResponse<CartModel>
    {
        public CartResponse()
            : base()
        { }

        public CartResponse(HttpStatusCode resultCode)
            : base(resultCode)
        { }

        public CartResponse(string message, HttpStatusCode resultCode)
            : base(message, resultCode)
        { }
    }
}