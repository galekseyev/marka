﻿using Marka.Common.ResponseModels.Carts;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Marka.Common.Responses.Carts
{
    public class CartItemRequest
    {
        [Required]
        public int InventoryId { get; set; }

        [Required]
        public int Quantity { get; set; }

        public IEnumerable<CartItemAttributesValuesXrefModel> CartItemAttributesValuesXref { get; set; }
    }
}