﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Marka.Common.Responses
{
    public class FilterRequest
    {
        public int? take { get; set; }
        public int? skip { get; set; }
    }
}
