﻿using System.Collections.Generic;

namespace Marka.Common.Responses.Shippings
{
    public class ShippingRatesResponse : EmbeddedPagedApiResponse<IEnumerable<ShippingRateResponse>>
    {
    }
}