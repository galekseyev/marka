﻿using System.Collections.Generic;

namespace Marka.Common.Responses.Shippings
{
    public class ShippingMethodsResponse : EmbeddedPagedApiResponse<IEnumerable<ShippingMethodResponse>>
    { }
}