﻿using Marka.Common.Models.Shippings;
using System.Net;

namespace Marka.Common.Responses.Shippings
{
    public class ShippingMethodResponse : EmbeddedApiResponse<ShippingMethod>
    {
        public ShippingMethodResponse()
            : base()
        { }

        public ShippingMethodResponse(HttpStatusCode resultCode)
            : base(resultCode)
        { }

        public ShippingMethodResponse(string message, HttpStatusCode resultCode)
            : base(message, resultCode)
        { }
    }
}