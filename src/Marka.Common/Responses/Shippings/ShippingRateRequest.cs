﻿using System.ComponentModel.DataAnnotations;

namespace Marka.Common.Responses.Shippings
{
    public class ShippingRateRequest
    {
        [Required]
        public decimal Threshold { get; set; }

        public decimal? Price { get; set; }
    }
}