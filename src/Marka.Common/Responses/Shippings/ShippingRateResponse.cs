﻿using Marka.Common.Models.Shippings;
using System.Net;

namespace Marka.Common.Responses.Shippings
{
    public class ShippingRateResponse : EmbeddedApiResponse<ShippingRate>
    {
        public ShippingRateResponse()
            : base()
        { }

        public ShippingRateResponse(HttpStatusCode resultCode)
            : base(resultCode)
        { }

        public ShippingRateResponse(string message, HttpStatusCode resultCode)
            : base(message, resultCode)
        { }
    }
}