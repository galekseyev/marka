﻿using System.ComponentModel.DataAnnotations;

namespace Marka.Common.Responses.Shippings
{
    public class ShippingMethodRequest
    {
        [Required]
        [StringLength(255, MinimumLength = 1)]
        public string Title { get; set; }

        public string Description { get; set; }
        public bool Disabled { get; set; }
    }
}