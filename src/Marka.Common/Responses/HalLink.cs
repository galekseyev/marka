﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Marka.Common.Responses
{
    public class HalLink
    {
        /// <summary>
        /// The relative or absolute URI being linked to.
        /// </summary>
        [JsonProperty("href")]
        public string Href { get; set; }

        /// <summary>
        /// Optional alternate key for identifying the link.
        /// </summary>
        [JsonProperty("name", NullValueHandling = NullValueHandling.Ignore)]
        public string Name { get; set; }

        /// <summary>
        /// Optional human readable label for the link.
        /// </summary>
        [JsonProperty("title", NullValueHandling = NullValueHandling.Ignore)]
        public string Title { get; set; }

        /// <summary>
        /// Optional hint indicating the expected media type when dereferencing <see cref="Href"/>.
        /// </summary>
        [JsonProperty("type", NullValueHandling = NullValueHandling.Ignore)]
        public string Type { get; set; }

        /// <summary>
        /// URI pointing to information about the deprecation of this link.  Leave null if not deprecated.
        /// </summary>
        [JsonProperty("deprecation", NullValueHandling = NullValueHandling.Ignore)]
        public string Deprecation { get; set; }

        /// <summary>
        /// If true, indicates that the <see cref="Href"/> is templated.
        /// </summary>
        [JsonProperty("templated", DefaultValueHandling = DefaultValueHandling.Ignore)]
        public bool Templated { get; set; }

        public HalLink() { }

        public HalLink(string href)
        {
            Href = href;
        }
    }
}
