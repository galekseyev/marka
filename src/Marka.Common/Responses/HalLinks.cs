﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace Marka.Common.Responses.Base
{
    public class HalLinks : Dictionary<string, HalLink>
    {
        [JsonIgnore]
        public HalLink Self
        {
            get { return Get(); }
            set { Set(value); }
        }

        [JsonIgnore]
        public HalLink Prev
        {
            get { return Get(); }
            set { Set(value); }
        }

        [JsonIgnore]
        public HalLink Next
        {
            get { return Get(); }
            set { Set(value); }
        }

        public HalLinks() { }

        public HalLinks(int capacity) : base(capacity) { }

        public HalLinks(IDictionary<string, HalLink> dictionary) : base(dictionary) { }

        private HalLink Get([CallerMemberName] string caller = null)
        {
            if (caller == null)
                return null;

            var key = caller.ToLowerInvariant();

            if (!TryGetValue(key, out HalLink result))
                return null;

            return result;
        }

        private void Set(HalLink value, [CallerMemberName] string caller = null)
        {
            if (caller == null)
                return;

            var key = caller.ToLowerInvariant();

            if (value == null)
                Remove(key);
            else
                this[key] = value;
        }
    }
}