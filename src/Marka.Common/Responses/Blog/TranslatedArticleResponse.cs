﻿using Marka.Common.ResponseModels.Blog;
using System.Net;

namespace Marka.Common.Responses.Blog
{
    public class TranslatedArticleResponse : EmbeddedApiResponse<TranslatedArticleModel>
    {
        public TranslatedArticleResponse()
         : base()
        { }

        public TranslatedArticleResponse(HttpStatusCode resultCode)
            : base(resultCode)
        { }

        public TranslatedArticleResponse(string message, HttpStatusCode resultCode)
            : base(message, resultCode)
        { }
    }
}