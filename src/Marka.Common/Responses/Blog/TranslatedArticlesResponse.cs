﻿using System.Collections.Generic;

namespace Marka.Common.Responses.Blog
{
    public class TranslatedArticlesResponse : EmbeddedPagedApiResponse<IEnumerable<TranslatedArticleResponse>>
    {
    }
}