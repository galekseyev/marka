﻿using System.Collections.Generic;

namespace Marka.Common.Responses.Blog
{
    public class ArticlesResponse : EmbeddedPagedApiResponse<IEnumerable<ArticleResponse>>
    {
    }
}