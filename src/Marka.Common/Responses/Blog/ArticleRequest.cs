﻿using Marka.Common.ResponseModels.Blog;
using System;
using System.Collections.Generic;

namespace Marka.Common.Responses.Blog
{
    public class ArticleRequest
    {
        public DateTime? CreateDate { get; set; }

        public bool Disabled { get; set; }
  
        public ICollection<ArticleTranslationModel> Translations { get; set; }
    }
}