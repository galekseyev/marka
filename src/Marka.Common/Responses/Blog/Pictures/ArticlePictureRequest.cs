﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Marka.Common.Responses.Blog.Pictures
{
    public class ArticlePictureRequest
    {
        [Required]
        public string Title { get; set; }

        [Required]
        public string Alt { get; set; }

        [Required]
        public string Thumbnail { get; set; }

        [Required]
        public string Picture { get; set; }
    }
}
