﻿using Marka.Common.ResponseModels.Blog;
using System.Net;

namespace Marka.Common.Responses.Blog.Pictures
{
    public class ArticlePictureResponse : EmbeddedApiResponse<ArticlePictureModel>
    {
        public ArticlePictureResponse()
          : base()
        { }

        public ArticlePictureResponse(HttpStatusCode resultCode)
            : base(resultCode)
        { }

        public ArticlePictureResponse(string message, HttpStatusCode resultCode)
            : base(message, resultCode)
        { }
    }
}