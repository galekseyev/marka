﻿using Marka.Common.ResponseModels.Blog;
using System.Net;

namespace Marka.Common.Responses.Blog
{
    public class ArticleResponse : EmbeddedApiResponse<ArticleModel>
    {
        public ArticleResponse()
           : base()
        { }

        public ArticleResponse(HttpStatusCode resultCode)
            : base(resultCode)
        { }

        public ArticleResponse(string message, HttpStatusCode resultCode)
            : base(message, resultCode)
        { }
    }
}