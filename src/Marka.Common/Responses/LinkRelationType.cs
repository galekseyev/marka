﻿namespace Marka.Common.Responses
{
    public static class LinkRelationType
    {
        public const string First = "first";
        public const string Item = "item";
        public const string Last = "last";
        public const string Next = "next";
        public const string Previous = "prev";
        public const string Related = "related";
        public const string Self = "self";
    }
}