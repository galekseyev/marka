﻿using Marka.Common.Responses.Base;
using System.Net;

namespace Marka.Common.Responses
{
    public class EmbeddedPagedApiResponse<TEmbedded> : PagedApiResponse<HttpStatusCode, TEmbedded>
    {
        public EmbeddedPagedApiResponse()
           : this(HttpStatusCode.OK.ToString(), HttpStatusCode.OK)
        { }

        public EmbeddedPagedApiResponse(HttpStatusCode resultCode)
            : base(resultCode)
        { }

        public EmbeddedPagedApiResponse(string message, HttpStatusCode resultCode)
            : base(message, resultCode)
        { }
    }
}