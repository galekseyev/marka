﻿using System;

namespace Marka.Common.Responses
{
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false)]
    public class ResponseMessageAttribute : Attribute
    {
        public string Message { get; set; }

        public ResponseMessageAttribute(string message)
        {
            Message = message;
        }
    }
}
