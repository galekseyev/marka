﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Marka.Common.Responses.Filters
{
    public class InventoryFilterRequest : FilterRequest
    {
        public int? CategoryId { get; set; }
    }
}
