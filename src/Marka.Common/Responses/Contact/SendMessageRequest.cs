﻿using System.ComponentModel.DataAnnotations;

namespace Marka.Common.Responses.Contact
{
    public class SendMessageRequest
    {
        [Required]
        public string Name { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        public string Message { get; set; }
    }
}