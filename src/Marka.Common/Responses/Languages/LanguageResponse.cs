﻿using Marka.Common.ResponseModels.Languages;
using System.Net;

namespace Marka.Common.Responses.Languages
{
    public class LanguageResponse : EmbeddedApiResponse<LanguageModel>
    {
        public LanguageResponse()
            : base()
        { }

        public LanguageResponse(HttpStatusCode resultCode)
            : base(resultCode)
        { }

        public LanguageResponse(string message, HttpStatusCode resultCode)
            : base(message, resultCode)
        { }
    }
}