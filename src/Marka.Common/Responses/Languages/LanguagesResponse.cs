﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Marka.Common.Responses.Languages
{
    public class LanguagesResponse : EmbeddedPagedApiResponse<IEnumerable<LanguageResponse>>
    {
    }
}
