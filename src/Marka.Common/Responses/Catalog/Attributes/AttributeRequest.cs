﻿using Marka.Common.ResponseModels.Catalog.Attributes;
using System.Collections.Generic;

namespace Marka.Common.Responses.Catalog.Attributes
{
    public class AttributeRequest
    {
        public bool Disabled { get; set; }

        public IEnumerable<AttributeTranslationModel> Translations { get; set; }
    }
}