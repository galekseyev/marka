﻿using Marka.Common.Responses.Catalog.Categories;
using System.Collections.Generic;

namespace Marka.Common.Responses.Catalog.Attributes
{
    public class AttributesResponse : EmbeddedPagedApiResponse<IEnumerable<AttributeResponse>>
    {
    }
}