﻿using System.Collections.Generic;

namespace Marka.Common.Responses.Catalog.Attributes
{
    public class TranslatedAttributesResponse : EmbeddedPagedApiResponse<IEnumerable<TranslatedAttributeResponse>>
    {
    }
}