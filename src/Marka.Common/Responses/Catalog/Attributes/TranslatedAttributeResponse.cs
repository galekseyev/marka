﻿using Marka.Common.ResponseModels.Catalog.Attributes;
using System.Net;

namespace Marka.Common.Responses.Catalog.Attributes
{
    public class TranslatedAttributeResponse : EmbeddedApiResponse<TranslatedAttributeModel>
    {
        public TranslatedAttributeResponse()
           : base()
        { }

        public TranslatedAttributeResponse(HttpStatusCode resultCode)
            : base(resultCode)
        { }

        public TranslatedAttributeResponse(string message, HttpStatusCode resultCode)
            : base(message, resultCode)
        { }
    }
}