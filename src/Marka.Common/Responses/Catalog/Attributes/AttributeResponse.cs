﻿using Marka.Common.ResponseModels.Catalog.Attributes;
using System.Net;

namespace Marka.Common.Responses.Catalog.Attributes
{
    public class AttributeResponse : EmbeddedApiResponse<AttributeModel>
    {
        public AttributeResponse()
          : base()
        { }

        public AttributeResponse(HttpStatusCode resultCode)
            : base(resultCode)
        { }

        public AttributeResponse(string message, HttpStatusCode resultCode)
            : base(message, resultCode)
        { }
    }
}