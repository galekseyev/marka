﻿using System.Collections.Generic;

namespace Marka.Common.Responses.Catalog.Specifications
{
    public class TranslatedSpecificationsResponse : EmbeddedPagedApiResponse<IEnumerable<TranslatedSpecificationResponse>>
    {
    }
}