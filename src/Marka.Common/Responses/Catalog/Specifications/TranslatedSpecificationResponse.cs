﻿using Marka.Common.ResponseModels.Catalog.Specifications;
using System.Net;

namespace Marka.Common.Responses.Catalog.Specifications
{
    public class TranslatedSpecificationResponse : EmbeddedApiResponse<TranslatedSpecificationModel>
    {
        public TranslatedSpecificationResponse()
           : base()
        { }

        public TranslatedSpecificationResponse(HttpStatusCode resultCode)
            : base(resultCode)
        { }

        public TranslatedSpecificationResponse(string message, HttpStatusCode resultCode)
            : base(message, resultCode)
        { }
    }
}