﻿using System.Collections.Generic;

namespace Marka.Common.Responses.Catalog.Specifications
{
    public class SpecificationValuesResponse : EmbeddedPagedApiResponse<IEnumerable<SpecificationValueResponse>>
    { }
}