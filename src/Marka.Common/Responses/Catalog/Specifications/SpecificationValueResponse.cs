﻿using Marka.Common.ResponseModels.Catalog.Specifications;
using System.Net;

namespace Marka.Common.Responses.Catalog.Specifications
{
    public class SpecificationValueResponse : EmbeddedApiResponse<SpecificationValueModel>
    {
        public SpecificationValueResponse()
        : base()
        { }

        public SpecificationValueResponse(HttpStatusCode resultCode)
            : base(resultCode)
        { }

        public SpecificationValueResponse(string message, HttpStatusCode resultCode)
            : base(message, resultCode)
        { }
    }
}
