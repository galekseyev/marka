﻿using Marka.Common.ResponseModels.Catalog.Specifications;
using System.Net;

namespace Marka.Common.Responses.Catalog.Specifications
{
    public class TranslatedSpecificationValueResponse : EmbeddedApiResponse<TranslatedSpecificationValueModel>
    {
        public TranslatedSpecificationValueResponse()
      : base()
        { }

        public TranslatedSpecificationValueResponse(HttpStatusCode resultCode)
            : base(resultCode)
        { }

        public TranslatedSpecificationValueResponse(string message, HttpStatusCode resultCode)
            : base(message, resultCode)
        { }
    }
}