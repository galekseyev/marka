﻿using Marka.Common.ResponseModels.Catalog.Specifications;
using System.Collections.Generic;

namespace Marka.Common.Responses.Catalog.Specifications
{
    public class SpecificationRequest
    {
        public bool Disabled { get; set; }

        public IEnumerable<SpecificationTranslationModel> Translations { get; set; }
    }
}