﻿using Marka.Common.ResponseModels.Catalog.Specifications;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace Marka.Common.Responses.Catalog.Specifications
{
    public class SpecificationResponse : EmbeddedApiResponse<SpecificationModel>
    {
        public SpecificationResponse()
          : base()
        { }

        public SpecificationResponse(HttpStatusCode resultCode)
            : base(resultCode)
        { }

        public SpecificationResponse(string message, HttpStatusCode resultCode)
            : base(message, resultCode)
        { }
    }
}