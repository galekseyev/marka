﻿using Marka.Common.ResponseModels.Catalog.Specifications;
using System;
using System.Collections.Generic;
using System.Text;

namespace Marka.Common.Responses.Catalog.Specifications
{
    public class SpecificationValueRequest
    {
        public int DisplayOrder { get; set; }

        public long? PictureId { get; set; }

        public IEnumerable<SpecificationValueTranslationModel> Translations { get; set; }
    }
}