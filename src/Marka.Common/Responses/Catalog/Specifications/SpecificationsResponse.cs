﻿using System.Collections.Generic;

namespace Marka.Common.Responses.Catalog.Specifications
{
    public class SpecificationsResponse : EmbeddedPagedApiResponse<IEnumerable<SpecificationResponse>>
    {
    }
}