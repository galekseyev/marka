﻿using System.Collections.Generic;

namespace Marka.Common.Responses.Catalog.Specifications
{
    public class TranslatedSpecificationValuesResponse : EmbeddedPagedApiResponse<IEnumerable<TranslatedSpecificationValueResponse>>
    {
    }
}