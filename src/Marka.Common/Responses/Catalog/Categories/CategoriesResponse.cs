﻿using System.Collections.Generic;

namespace Marka.Common.Responses.Catalog.Categories
{
    public class CategoriesResponse : EmbeddedPagedApiResponse<IEnumerable<CategoryResponse>>
    { }
}