﻿using Marka.Common.ResponseModels.Catalog.Categories;
using System.Collections.Generic;

namespace Marka.Common.Responses.Catalog.Categories
{
    public class CategoryRequest
    {
        public bool Disabled { get; set; }

        public IEnumerable<CategoryTranslationModel> Translations { get; set; }
    }
}