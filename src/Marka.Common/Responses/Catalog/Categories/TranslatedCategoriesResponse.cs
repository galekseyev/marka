﻿using System.Collections.Generic;

namespace Marka.Common.Responses.Catalog.Categories
{
    public class TranslatedCategoriesResponse : EmbeddedPagedApiResponse<IEnumerable<TranslatedCategoryResponse>>
    {
    }
}