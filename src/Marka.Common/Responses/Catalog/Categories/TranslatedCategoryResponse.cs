﻿using Marka.Common.ResponseModels.Catalog.Categories;
using System.Net;

namespace Marka.Common.Responses.Catalog.Categories
{
    public class TranslatedCategoryResponse : EmbeddedApiResponse<TranslatedCategoryModel>
    {
        public TranslatedCategoryResponse()
           : base()
        { }

        public TranslatedCategoryResponse(HttpStatusCode resultCode)
            : base(resultCode)
        { }

        public TranslatedCategoryResponse(string message, HttpStatusCode resultCode)
            : base(message, resultCode)
        { }
    }
}