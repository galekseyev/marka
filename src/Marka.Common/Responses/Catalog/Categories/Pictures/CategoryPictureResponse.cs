﻿using Marka.Common.ResponseModels.Catalog.Categories;
using System.Net;

namespace Marka.Common.Responses.Catalog.Categories.Pictures
{
    public class CategoryPictureResponse : EmbeddedApiResponse<CategoryPictureModel>
    {
        public CategoryPictureResponse()
           : base()
        { }

        public CategoryPictureResponse(HttpStatusCode resultCode)
            : base(resultCode)
        { }

        public CategoryPictureResponse(string message, HttpStatusCode resultCode)
            : base(message, resultCode)
        { }
    }
}
