﻿using Marka.Common.ResponseModels.Catalog.Categories;
using System.Net;

namespace Marka.Common.Responses.Catalog.Categories
{
    public class CategoryResponse : EmbeddedApiResponse<CategoryModel>
    {
        public CategoryResponse()
            : base()
        { }

        public CategoryResponse(HttpStatusCode resultCode)
            : base(resultCode)
        { }

        public CategoryResponse(string message, HttpStatusCode resultCode)
            : base(message, resultCode)
        { }
    }
}