﻿using System.Collections.Generic;

namespace Marka.Common.Responses.Catalog.Inventories
{
    public class InventoriesResponse : EmbeddedPagedApiResponse<IEnumerable<InventoryResponse>>
    { }
}