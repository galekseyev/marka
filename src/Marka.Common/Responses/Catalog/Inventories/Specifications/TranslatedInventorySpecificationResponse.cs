﻿using Marka.Common.ResponseModels.Catalog.Inventories;
using System.Net;

namespace Marka.Common.Responses.Catalog.Inventories.Specifications
{
    public class TranslatedInventorySpecificationResponse : EmbeddedApiResponse<TranslatedInventorySpecificationModel>
    {
        public TranslatedInventorySpecificationResponse()
         : base()
        { }

        public TranslatedInventorySpecificationResponse(HttpStatusCode resultCode)
            : base(resultCode)
        { }

        public TranslatedInventorySpecificationResponse(string message, HttpStatusCode resultCode)
            : base(message, resultCode)
        { }
    }
}
