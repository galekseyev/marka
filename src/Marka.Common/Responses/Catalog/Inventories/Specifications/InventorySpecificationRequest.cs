﻿namespace Marka.Common.Responses.Catalog.Inventories.Specifications
{
    public class InventorySpecificationRequest
    {
        public bool Disabled { get; set; }
        public bool Hidden { get; set; }

        public int DisplayOrder { get; set; }

        public int SpecificationId { get; set; }

        public int SpecificationValueId { get; set; }
    }
}