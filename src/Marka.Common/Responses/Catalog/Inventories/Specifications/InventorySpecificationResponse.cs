﻿using Marka.Common.ResponseModels.Catalog.Inventories;
using System.Net;

namespace Marka.Common.Responses.Catalog.Inventories.Specifications
{
    public class InventorySpecificationResponse : EmbeddedApiResponse<InventorySpecificationModel>
    {
        public InventorySpecificationResponse()
         : base()
        { }

        public InventorySpecificationResponse(HttpStatusCode resultCode)
            : base(resultCode)
        { }

        public InventorySpecificationResponse(string message, HttpStatusCode resultCode)
            : base(message, resultCode)
        { }
    }
}