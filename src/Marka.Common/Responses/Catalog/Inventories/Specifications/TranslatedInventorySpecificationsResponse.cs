﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Marka.Common.Responses.Catalog.Inventories.Specifications
{
    public class TranslatedInventorySpecificationsResponse : EmbeddedPagedApiResponse<IEnumerable<TranslatedInventorySpecificationResponse>>
    { }
}
