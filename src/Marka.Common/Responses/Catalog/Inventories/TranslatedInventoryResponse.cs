﻿using Marka.Common.ResponseModels.Catalog.Inventories;
using System.Net;

namespace Marka.Common.Responses.Catalog.Inventories
{
    public class TranslatedInventoryResponse : EmbeddedApiResponse<TranslatedInventoryModel>
    {
        public TranslatedInventoryResponse()
             : base()
        { }

        public TranslatedInventoryResponse(HttpStatusCode resultCode)
            : base(resultCode)
        { }

        public TranslatedInventoryResponse(string message, HttpStatusCode resultCode)
            : base(message, resultCode)
        { }
    }
}