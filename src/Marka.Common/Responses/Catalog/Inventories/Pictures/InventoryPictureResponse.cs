﻿using Marka.Common.ResponseModels.Catalog.Inventories;
using System.Net;

namespace Marka.Common.Responses.Catalog.Inventories.Pictures
{
    public class InventoryPictureResponse : EmbeddedApiResponse<InventoryPictureModel>
    {
        public InventoryPictureResponse()
           : base()
        { }

        public InventoryPictureResponse(HttpStatusCode resultCode)
            : base(resultCode)
        { }

        public InventoryPictureResponse(string message, HttpStatusCode resultCode)
            : base(message, resultCode)
        { }
    }
}