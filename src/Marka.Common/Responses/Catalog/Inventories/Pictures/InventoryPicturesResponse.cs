﻿using System.Collections.Generic;

namespace Marka.Common.Responses.Catalog.Inventories.Pictures
{

    public class InventoryPicturesResponse : EmbeddedPagedApiResponse<IEnumerable<InventoryPictureResponse>>
    { }
}
