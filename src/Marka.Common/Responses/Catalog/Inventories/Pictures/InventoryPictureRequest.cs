﻿using System.ComponentModel.DataAnnotations;

namespace Marka.Common.Responses.Catalog.Inventories.Pictures
{
    public class InventoryPictureRequest
    {
        [Required]
        public string Title { get; set; }

        [Required]
        public string Alt { get; set; }

        [Required]
        public string Thumbnail { get; set; }

        [Required]
        public string Picture { get; set; }
    }
}