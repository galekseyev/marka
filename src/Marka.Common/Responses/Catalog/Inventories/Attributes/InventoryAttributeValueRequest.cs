﻿using Marka.Common.ResponseModels.Catalog.Inventories;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Marka.Common.Responses.Catalog.Inventories.Attributes
{
    public class InventoryAttributeValueRequest
    {
        public int DisplayOrder { get; set; }

        public long? PictureId { get; set; }

        public IEnumerable<InventoryAttributeValueTranslationModel> Translations { get; set; }
    }
}