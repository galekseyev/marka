﻿using Marka.Common.ResponseModels.Catalog.Inventories;
using System.Net;

namespace Marka.Common.Responses.Catalog.Inventories.Attributes
{
    public class TranslatedInventoryAttributeResponse : EmbeddedApiResponse<TranslatedInventoryAttributeModel>
    {
        public TranslatedInventoryAttributeResponse()
        : base()
        { }

        public TranslatedInventoryAttributeResponse(HttpStatusCode resultCode)
            : base(resultCode)
        { }

        public TranslatedInventoryAttributeResponse(string message, HttpStatusCode resultCode)
            : base(message, resultCode)
        { }
    }
}