﻿using Marka.Common.ResponseModels.Catalog.Inventories;
using System.Net;

namespace Marka.Common.Responses.Catalog.Inventories.Attributes
{
    public class TranslatedInventoryAttributeValueResponse : EmbeddedApiResponse<TranslatedInventoryAttributeValueModel>
    {
        public TranslatedInventoryAttributeValueResponse()
       : base()
        { }

        public TranslatedInventoryAttributeValueResponse(HttpStatusCode resultCode)
            : base(resultCode)
        { }

        public TranslatedInventoryAttributeValueResponse(string message, HttpStatusCode resultCode)
            : base(message, resultCode)
        { }
    }
}
