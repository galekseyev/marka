﻿using Marka.Common.ResponseModels.Catalog.Inventories;
using System.Net;

namespace Marka.Common.Responses.Catalog.Inventories.Attributes
{
    public class InventoryAttributeValueResponse : EmbeddedApiResponse<InventoryAttributeValueModel>
    {
        public InventoryAttributeValueResponse()
         : base()
        { }

        public InventoryAttributeValueResponse(HttpStatusCode resultCode)
            : base(resultCode)
        { }

        public InventoryAttributeValueResponse(string message, HttpStatusCode resultCode)
            : base(message, resultCode)
        { }
    }
}