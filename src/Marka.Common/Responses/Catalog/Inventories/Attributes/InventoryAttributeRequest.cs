﻿using Marka.Common.Enums.Catalog.Inventories.Attributes;

namespace Marka.Common.Responses.Catalog.Inventories.Attributes
{
    public class InventoryAttributeRequest
    {
        public bool IsRequired { get; set; }

        public AttributeControlType ControlType { get; set; }

        public int DisplayOrder { get; set; }

        public int AttributeId { get; set; }
    }
}