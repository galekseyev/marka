﻿using System.Net;
using Marka.Common.ResponseModels.Catalog.Inventories;

namespace Marka.Common.Responses.Catalog.Inventories.Attributes
{
    public class InventoryAttributeResponse : EmbeddedApiResponse<InventoryAttributeModel>
    {
        public InventoryAttributeResponse()
          : base()
        { }

        public InventoryAttributeResponse(HttpStatusCode resultCode)
            : base(resultCode)
        { }

        public InventoryAttributeResponse(string message, HttpStatusCode resultCode)
            : base(message, resultCode)
        { }
    }
}