﻿using System.Collections.Generic;

namespace Marka.Common.Responses.Catalog.Inventories.Attributes
{
    public class InventoryAttributeValuesResponse : EmbeddedPagedApiResponse<IEnumerable<InventoryAttributeValueResponse>>
    { }
}
