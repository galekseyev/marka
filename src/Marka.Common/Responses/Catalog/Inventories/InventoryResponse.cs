﻿using Marka.Common.ResponseModels.Catalog.Inventories;
using System.Net;

namespace Marka.Common.Responses.Catalog.Inventories
{
    public class InventoryResponse : EmbeddedApiResponse<InventoryModel>
    {
        public InventoryResponse()
            : base()
        { }

        public InventoryResponse(HttpStatusCode resultCode)
            : base(resultCode)
        { }

        public InventoryResponse(string message, HttpStatusCode resultCode)
            : base(message, resultCode)
        { }
    }
}