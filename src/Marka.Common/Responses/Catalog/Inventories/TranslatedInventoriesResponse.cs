﻿using System.Collections.Generic;

namespace Marka.Common.Responses.Catalog.Inventories
{
    public class TranslatedInventoriesResponse : EmbeddedPagedApiResponse<IEnumerable<TranslatedInventoryResponse>>
    {
    }
}