﻿using Marka.Common.ResponseModels.Catalog.Inventories;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Marka.Common.Responses.Catalog.Inventories
{
    public class InventoryRequest
    {
        public bool Disabled { get; set; }

        public decimal Price { get; set; }
        public int Quantity { get; set; }

        [Required]
        public int CategoryId { get; set; }


        public IEnumerable<InventoryTranslationModel> Translations { get; set; }
    }
}