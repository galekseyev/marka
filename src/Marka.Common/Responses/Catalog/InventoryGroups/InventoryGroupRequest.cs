﻿namespace Marka.Common.Responses.Catalog.InventoryGroups
{
    public class InventoryGroupRequest
    {
        public bool Disabled { get; set; }
        public int DisplayOrder { get; set; }
    }
}