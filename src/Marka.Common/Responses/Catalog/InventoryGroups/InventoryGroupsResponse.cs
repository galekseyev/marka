﻿using System.Collections.Generic;

namespace Marka.Common.Responses.Catalog.InventoryGroups
{
    public class InventoryGroupsResponse : EmbeddedPagedApiResponse<IEnumerable<InventoryGroupResponse>>
    {
    }
}