﻿using Marka.Common.ResponseModels.Catalog.InventoryGroups;

namespace Marka.Common.Responses.Catalog.InventoryGroups.Pictures
{
    public class InventoryGroupPictureResponse : EmbeddedApiResponse<InventoryGroupPictureModel>
    {
    }
}