﻿using System.ComponentModel.DataAnnotations;

namespace Marka.Common.Responses.Catalog.InventoryGroups.Pictures
{
    public class InventoryGroupPictureRequest
    {
        [Required]
        public string Title { get; set; }

        [Required]
        public string Alt { get; set; }

        [Required]
        public string Thumbnail { get; set; }

        [Required]
        public string Picture { get; set; }
    }
}