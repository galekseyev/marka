﻿using Marka.Common.ResponseModels.Catalog.InventoryGroups;
using System.Net;

namespace Marka.Common.Responses.Catalog.InventoryGroups
{
    public class InventoryGroupResponse : EmbeddedApiResponse<InventoryGroupModel>
    {
        public InventoryGroupResponse()
          : base()
        { }

        public InventoryGroupResponse(HttpStatusCode resultCode)
            : base(resultCode)
        { }

        public InventoryGroupResponse(string message, HttpStatusCode resultCode)
            : base(message, resultCode)
        { }
    }
}