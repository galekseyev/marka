﻿using Marka.Common.ResponseModels.Catalog.InventoryGroups;
using System.Net;

namespace Marka.Common.Responses.Catalog.InventoryGroups.Inventories
{
    public class InventoryGroupInventoryResponse : EmbeddedApiResponse<InventoryGroupInventoryModel>
    {
        public InventoryGroupInventoryResponse()
           : base()
        { }

        public InventoryGroupInventoryResponse(HttpStatusCode resultCode)
            : base(resultCode)
        { }

        public InventoryGroupInventoryResponse(string message, HttpStatusCode resultCode)
            : base(message, resultCode)
        { }
    }
}