﻿using System.Collections.Generic;

namespace Marka.Common.Responses.Catalog.InventoryGroups.Inventories
{
    public class InventoryGroupInventoriesResponse : EmbeddedPagedApiResponse<IEnumerable<InventoryGroupInventoryResponse>>
    {
    }
}