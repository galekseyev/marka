﻿using System.Collections.Generic;

namespace Marka.Common.Responses.Catalog.InventoryGroups.Inventories
{
    public class TranslatedInventoryGroupInventoriesResponse : EmbeddedPagedApiResponse<IEnumerable<TranslatedInventoryGroupInventoryResponse>>
    {
    }
}