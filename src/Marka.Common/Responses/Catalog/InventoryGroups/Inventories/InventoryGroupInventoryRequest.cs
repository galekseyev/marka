﻿namespace Marka.Common.Responses.Catalog.InventoryGroups.Inventories
{
    public class InventoryGroupInventoryRequest
    {
        public bool Disabled { get; set; }
        public int DisplayOrder { get; set; }

        public int InventoryId { get; set; }
    }
}