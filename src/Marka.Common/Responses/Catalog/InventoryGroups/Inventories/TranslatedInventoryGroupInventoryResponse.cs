﻿using Marka.Common.ResponseModels.Catalog.InventoryGroups;
using System.Net;

namespace Marka.Common.Responses.Catalog.InventoryGroups.Inventories
{
    public class TranslatedInventoryGroupInventoryResponse : EmbeddedApiResponse<TranslatedInventoryGroupInventoryModel>
    {
        public TranslatedInventoryGroupInventoryResponse()
           : base()
        { }

        public TranslatedInventoryGroupInventoryResponse(HttpStatusCode resultCode)
            : base(resultCode)
        { }

        public TranslatedInventoryGroupInventoryResponse(string message, HttpStatusCode resultCode)
            : base(message, resultCode)
        { }
    }
}
