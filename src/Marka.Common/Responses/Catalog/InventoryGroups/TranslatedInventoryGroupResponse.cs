﻿using Marka.Common.ResponseModels.Catalog.InventoryGroups;
using System.Net;

namespace Marka.Common.Responses.Catalog.InventoryGroups
{
    public class TranslatedInventoryGroupResponse : EmbeddedApiResponse<TranslatedInventoryGroupModel>
    {
        public TranslatedInventoryGroupResponse()
             : base()
        { }

        public TranslatedInventoryGroupResponse(HttpStatusCode resultCode)
            : base(resultCode)
        { }

        public TranslatedInventoryGroupResponse(string message, HttpStatusCode resultCode)
            : base(message, resultCode)
        { }
    }
}
