﻿using Marka.Common.ResponseModels.Countries;
using System.Net;

namespace Marka.Common.Responses.Countries
{
    public class CountryResponse : EmbeddedApiResponse<CountryModel>
    {
        public CountryResponse()
            : base()
        { }

        public CountryResponse(HttpStatusCode resultCode)
            : base(resultCode)
        { }

        public CountryResponse(string message, HttpStatusCode resultCode)
            : base(message, resultCode)
        { }
    }
}