﻿using System.Collections.Generic;

namespace Marka.Common.Responses.Countries
{
    public class CountriesResponse : EmbeddedPagedApiResponse<IEnumerable<CountryResponse>>
    {
    }
}