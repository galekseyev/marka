﻿using System.Collections.Generic;

namespace Marka.Common.Responses.Payments
{
    public class PaymentTypesResponse : EmbeddedPagedApiResponse<IEnumerable<PaymentTypeResponse>>
    { }
}