﻿using Marka.Common.ResponseModels.Payments;
using System.Net;

namespace Marka.Common.Responses.Payments
{
    public class PaymentStatusResponse : EmbeddedApiResponse<PaymentStatusModel>
    {
        public PaymentStatusResponse()
            : base()
        { }

        public PaymentStatusResponse(HttpStatusCode resultCode)
            : base(resultCode)
        { }

        public PaymentStatusResponse(string message, HttpStatusCode resultCode)
            : base(message, resultCode)
        { }
    }
}
