﻿using Marka.Common.ResponseModels.Payments;
using System.Net;

namespace Marka.Common.Responses.Payments
{
    public class PaymentTypeResponse : EmbeddedApiResponse<PaymentTypeModel>
    {
        public PaymentTypeResponse()
            : base()
        { }

        public PaymentTypeResponse(HttpStatusCode resultCode)
            : base(resultCode)
        { }

        public PaymentTypeResponse(string message, HttpStatusCode resultCode)
            : base(message, resultCode)
        { }
    }
}
