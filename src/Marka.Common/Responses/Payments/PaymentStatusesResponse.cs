﻿using System.Collections.Generic;

namespace Marka.Common.Responses.Payments
{
    public class PaymentStatusesResponse : EmbeddedPagedApiResponse<IEnumerable<PaymentStatusResponse>>
    { }
}