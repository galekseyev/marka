﻿using Microsoft.AspNetCore.Identity;

namespace Marka.Common.Models.Account.Roles
{
    public class Role : IdentityRole<int>
    {
        public Role() : base() { }
        public Role(string roleName) : base(roleName) { }
    }
}