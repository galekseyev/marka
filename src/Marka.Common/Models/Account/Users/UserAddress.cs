﻿using Marka.Common.Models.Shared;
using System.ComponentModel.DataAnnotations;

namespace Marka.Common.Models.Account.Users
{
    public class UserAddress : Address
    {
        [Key]
        public int Id { get; set; }

        public virtual User User { get; set; }
        public int UserId { get; set; }
    }
}