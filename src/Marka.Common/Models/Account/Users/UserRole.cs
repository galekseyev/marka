﻿using Marka.Common.Models.Account.Roles;
using Microsoft.AspNetCore.Identity;

namespace Marka.Common.Models.Account.Users
{
    public class UserRole : IdentityUserRole<int>
    {
        public User User { get; set; }
        public Role Role { get; set; }
    }
}