﻿using System;
using System.Collections.Generic;
using Marka.Common.Models.Carts;
using Marka.Common.Models.Shared;
using Microsoft.AspNetCore.Identity;

namespace Marka.Common.Models.Account.Users
{
    public class User : IdentityUser<int>
    {
        public User() : base() { }
        public User(string userName) : base(userName) { }

        public string FullName { get; set; }

        public DateTime CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }

        public bool Disabled { get; set; }

        /// <summary>
        /// Roles Navigation Property
        /// </summary>
        public virtual ICollection<IdentityUserRole<int>> Roles { get; } = new List<IdentityUserRole<int>>();

        /// <summary>
        /// Claims Navigation Property
        /// </summary>
        public virtual ICollection<IdentityUserClaim<int>> Claims { get; } = new List<IdentityUserClaim<int>>();

        /// <summary>
        /// Logins Navigation Property
        /// </summary>
        public virtual ICollection<IdentityUserLogin<int>> Logins { get; } = new List<IdentityUserLogin<int>>();
    }
}