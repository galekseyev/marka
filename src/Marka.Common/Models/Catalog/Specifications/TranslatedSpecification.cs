﻿using System.Collections.Generic;

namespace Marka.Common.Models.Catalog.Specifications
{
    public class TranslatedSpecification
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }

        public ICollection<TranslatedSpecificationValue> Values { get; set; }
    }
}