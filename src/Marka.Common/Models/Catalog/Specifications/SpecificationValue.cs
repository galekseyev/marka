﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Marka.Common.Models.Catalog.Specifications
{
    public class SpecificationValue
    {
        [Key]
        public int Id { get; set; }

        public bool Disabled { get; set; }
        public int DisplayOrder { get; set; }

        public int SpecificationId { get; set; }

        [JsonIgnore]
        public virtual Specification Specification { get; set; }

        public virtual ICollection<SpecificationValueTranslation> Translations { get; set; }
    }
}