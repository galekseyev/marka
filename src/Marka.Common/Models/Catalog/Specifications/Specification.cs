﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Marka.Common.Models.Catalog.Specifications
{
    public class Specification
    {
        [Key]
        public int Id { get; set; }

        public bool Disabled { get; set; }

        public int DisplayOrder { get; set; }

        public virtual ICollection<SpecificationTranslation> Translations { get; set; }

        public virtual ICollection<SpecificationValue> Values { get; set; }
    }
}