﻿using Marka.Common.Models.Languages;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Marka.Common.Models.Catalog.Specifications
{
    public class SpecificationValueTranslation
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(255, MinimumLength = 1)]
        public string Title { get; set; }


        public int LanguageId { get; set; }

        [JsonIgnore]
        public virtual Language Language { get; set; }


        public int SpecificationValueId { get; set; }

        [JsonIgnore]
        public virtual SpecificationValue SpecificationValue { get; set; }
    }
}