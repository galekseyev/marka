﻿namespace Marka.Common.Models.Catalog.Attributes
{
    public class TranslatedAttribute
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
    }
}