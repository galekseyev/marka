﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Marka.Common.Models.Catalog.Attributes
{
    public class Attribute
    {
        [Key]
        public int Id { get; set; }

        public bool Disabled { get; set; }

        public int DisplayOrder { get; set; }

        public virtual ICollection<AttributeTranslation> Translations { get; set; }
    }
}