﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Marka.Common.Models.Catalog.Categories
{
    public class Category
    {
        [Key]
        public int Id { get; set; }

        public bool Disabled { get; set; }
        public int DisplayOrder { get; set; }

        public int? PictureId { get; set; }
        public virtual CategoryPicture Picture { get; set; }
        public virtual ICollection<CategoryTranslation> Translations { get; set; }
    }
}