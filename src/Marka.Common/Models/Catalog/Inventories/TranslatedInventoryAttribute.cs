﻿using Marka.Common.Enums.Catalog.Inventories.Attributes;
using Marka.Common.Models.Catalog.Attributes;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Collections.Generic;

namespace Marka.Common.Models.Catalog.Inventories
{
    public class TranslatedInventoryAttribute
    {
        public int Id { get; set; }

        public bool IsRequired { get; set; }
        public bool Disabled { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public AttributeControlType ControlType { get; set; }

        public int DisplayOrder { get; set; }

        public TranslatedAttribute Attribute { get; set; }

        public ICollection<TranslatedInventoryAttributeValue> Values { get; set; }
        public ICollection<TranslatedInventorySpecification> Specifications { get; set; }
    }
}
