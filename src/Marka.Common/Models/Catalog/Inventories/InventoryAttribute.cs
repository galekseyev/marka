﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using Marka.Common.Models.Catalog.Attributes;
using Marka.Common.Enums.Catalog.Inventories.Attributes;
using Newtonsoft.Json.Converters;

namespace Marka.Common.Models.Catalog.Inventories
{
    public class InventoryAttribute
    {
        [Key]
        public int Id { get; set; }

        public bool IsRequired { get; set; }
        public bool Disabled { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public AttributeControlType ControlType { get; set; }

        public int DisplayOrder { get; set; }

        public int AttributeId { get; set; }
        public virtual Attribute Attribute { get; set; }

        public int InventoryId { get; set; }

        [JsonIgnore]
        public virtual Inventory Inventory { get; set; }

        public virtual ICollection<InventoryAttributeValue> Values { get; set; }
    }
}
