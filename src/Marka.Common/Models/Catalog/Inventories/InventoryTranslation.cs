﻿using Marka.Common.Models.Languages;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace Marka.Common.Models.Catalog.Inventories
{
    public class InventoryTranslation
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(255, MinimumLength = 1)]
        public string Title { get; set; }
        public string Description { get; set; }

        public int LanguageId { get; set; }

        [JsonIgnore]
        public virtual Language Language { get; set; }

        public int InventoryId { get; set; }

        [JsonIgnore]
        public virtual Inventory Inventory { get; set; }
    }
}