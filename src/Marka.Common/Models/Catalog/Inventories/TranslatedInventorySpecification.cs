﻿using Marka.Common.Models.Catalog.Specifications;

namespace Marka.Common.Models.Catalog.Inventories
{
    public class TranslatedInventorySpecification
    {
        public int Id { get; set; }

        public bool Disabled { get; set; }
        public bool Hidden { get; set; }

        public int DisplayOrder { get; set; }

        public TranslatedSpecification Specification { get; set; }
        public TranslatedSpecificationValue SpecificationValue { get; set; }
    }
}