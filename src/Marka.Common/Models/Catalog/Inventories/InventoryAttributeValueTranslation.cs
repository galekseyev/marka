﻿using Marka.Common.Models.Languages;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace Marka.Common.Models.Catalog.Inventories
{
    public class InventoryAttributeValueTranslation
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(255, MinimumLength = 1)]
        public string Title { get; set; }


        public int LanguageId { get; set; }

        [JsonIgnore]
        public virtual Language Language { get; set; }


        public int InventoryAttributeValueId { get; set; }

        [JsonIgnore]
        public virtual InventoryAttributeValue InventoryAttributeValue { get; set; }
    }
}