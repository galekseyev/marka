﻿namespace Marka.Common.Models.Catalog.Inventories
{
    public class TranslatedInventoryAttributeValue
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public bool Disabled { get; set; }
        public int DisplayOrder { get; set; }

        public InventoryPicture Picture { get; set; }
    }
}