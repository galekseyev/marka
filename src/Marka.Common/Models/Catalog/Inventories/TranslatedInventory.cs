﻿using System.Collections.Generic;

namespace Marka.Common.Models.Catalog.Inventories
{
    public class TranslatedInventory
    {
        public int Id { get; set; }

        public bool Disabled { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }

        public decimal OriginalPrice { get; set; }
        public decimal Price { get; set; }

        public int Quantity { get; set; }

        public ICollection<InventoryPicture> Pictures { get; set; }
        public ICollection<TranslatedInventoryAttribute> Attributes { get; set; }
        public ICollection<TranslatedInventorySpecification> Specifications { get; set; }
    }

    public class TranslatedInventories
    {
        public int Total { get; set; }
        public ICollection<TranslatedInventory> Inventories { get; set; }
    }
}