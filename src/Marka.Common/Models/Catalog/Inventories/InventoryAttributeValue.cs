﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Marka.Common.Models.Catalog.Inventories
{
    public class InventoryAttributeValue
    {
        [Key]
        public int Id { get; set; }

        public bool Disabled { get; set; }
        public int DisplayOrder { get; set; }


        public int? PictureId { get; set; }
        public virtual InventoryPicture Picture { get; set; }

        public int InventoryAttributeId { get; set; }

        [JsonIgnore]
        public virtual InventoryAttribute InventoryAttribute { get; set; }

        public virtual ICollection<InventoryAttributeValueTranslation> Translations { get; set; }


    }
}