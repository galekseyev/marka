﻿using Marka.Common.Models.Catalog.Categories;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Marka.Common.Models.Catalog.Inventories
{
    public class Inventory
    {
        [Key]
        public int Id { get; set; }


        public decimal OriginalPrice { get; set; }
        public decimal Price { get; set; }

        public int Quantity { get; set; }
        public int DisplayOrder { get; set; }

        public bool Disabled { get; set; }
        public bool FeaturedItem { get; set; }

        public int CategoryId { get; set; }
        public virtual Category Category { get; set; }

        public virtual ICollection<InventoryTranslation> Translations { get; set; }
        public virtual ICollection<InventoryPicture> Pictures { get; set; }
        public virtual ICollection<InventoryAttribute> Attributes { get; set; }
        public virtual ICollection<InventorySpecification> Specifications { get; set; }
    }
}