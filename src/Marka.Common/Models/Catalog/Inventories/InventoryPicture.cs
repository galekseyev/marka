﻿using Marka.Common.Models.Shared;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace Marka.Common.Models.Catalog.Inventories
{

    public class InventoryPicture : IPicture
    {
        [Key]
        public int Id { get; set; }

        public string Title { get; set; }
        public string Alt { get; set; }

        [Required]
        public string Thumbnail { get; set; }

        [Required]
        public string Picture { get; set; }

        public int Disabled { get; set; }
        public int DisplayOrder { get; set; }

        public int InventoryId { get; set; }

        [JsonIgnore]
        public virtual Inventory Inventory { get; set; }
    }
}