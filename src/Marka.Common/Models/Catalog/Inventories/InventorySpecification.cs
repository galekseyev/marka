﻿using Marka.Common.Models.Catalog.Specifications;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace Marka.Common.Models.Catalog.Inventories
{
    public class InventorySpecification
    {
        [Key]
        public int Id { get; set; }

        public bool Disabled { get; set; }
        public bool Hidden { get; set; }

        public int DisplayOrder { get; set; }

        public int SpecificationId { get; set; }
        public virtual Specification Specification { get; set; }

        public int SpecificationValueId { get; set; }
        public virtual SpecificationValue SpecificationValue { get; set; }

        public int InventoryId { get; set; }

        [JsonIgnore]
        public virtual Inventory Inventory { get; set; }
    }
}