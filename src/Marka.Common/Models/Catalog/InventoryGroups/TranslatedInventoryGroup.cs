﻿using System.Collections.Generic;

namespace Marka.Common.Models.Catalog.InventoryGroups
{
    public class TranslatedInventoryGroup
    {
        public int Id { get; set; }
        public bool Disabled { get; set; }
        public int DisplayOrder { get; set; }

        public virtual InventoryGroupPicture Picture { get; set; }
        public virtual ICollection<TranslatedInventoryGroupInventory> Inventories { get; set; }
    }


}