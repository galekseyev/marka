﻿using Marka.Common.Models.Catalog.Inventories;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace Marka.Common.Models.Catalog.InventoryGroups
{
    public class InventoryGroupInventory
    {
        [Key]
        public int Id { get; set; }
        public bool Disabled { get; set; }
        public int DisplayOrder { get; set; }

        public int InventoryId { get; set; }
        public virtual Inventory Inventory { get; set; }

        public int InventoryGroupId { get; set; }

        [JsonIgnore]
        public virtual InventoryGroup InventoryGroup { get; set; }

    }
}