﻿using Marka.Common.Models.Catalog.Inventories;

namespace Marka.Common.Models.Catalog.InventoryGroups
{
    public class TranslatedInventoryGroupInventory
    {
        public int Id { get; set; }
        public bool Disabled { get; set; }
        public int DisplayOrder { get; set; }

        public int InventoryId { get; set; }
        public string Title { get; set; }

        public InventoryPicture Picture { get; set; }
    }
}