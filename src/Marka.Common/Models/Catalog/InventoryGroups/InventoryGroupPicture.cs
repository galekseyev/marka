﻿using Marka.Common.Models.Shared;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace Marka.Common.Models.Catalog.InventoryGroups
{
    public class InventoryGroupPicture :IPicture
    {
        [Key]
        public int Id { get; set; }

        public string Title { get; set; }
        public string Alt { get; set; }

        [Required]
        public string Thumbnail { get; set; }

        [Required]
        public string Picture { get; set; }

        [JsonIgnore]
        public virtual InventoryGroup InventoryGroup { get; set; }

    }
}
