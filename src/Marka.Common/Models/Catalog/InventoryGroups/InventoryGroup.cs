﻿using Marka.Common.Models.Catalog.Inventories;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Marka.Common.Models.Catalog.InventoryGroups
{
    public class InventoryGroup
    {
        [Key]
        public int Id { get; set; }
        public bool Disabled { get; set; }
        public int DisplayOrder { get; set; }

        public int? PictureId { get; set; }
        public virtual InventoryGroupPicture Picture { get; set; }
        public virtual ICollection<InventoryGroupInventory> InventoryGroupInventories { get; set; }
    }

    public class FilteredInventoryGroups
    {
        public int Total { get; set; }
        public ICollection<InventoryGroup> InventoryGroups { get; set; }
    }
}