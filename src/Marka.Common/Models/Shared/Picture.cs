﻿using System.ComponentModel.DataAnnotations;

namespace Marka.Common.Models.Shared
{
    public interface IPicture
    {
        string Thumbnail { get; set; }
        string Picture { get; set; }
    }
}