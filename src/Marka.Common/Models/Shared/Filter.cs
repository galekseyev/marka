﻿namespace Marka.Common.Models.Shared
{
    public class Filter
    {
        public int? Take { get; set; }
        public int? Skip { get; set; }
    }
}