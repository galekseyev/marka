﻿using Marka.Common.Models.Countries;
using System.ComponentModel.DataAnnotations;

namespace Marka.Common.Models.Shared
{
    public class Address
    {
        [Required]
        public string Address1 { get; set; }

        public string Address2 { get; set; }
        public string Address3 { get; set; }

        [Required]
        public string City { get; set; }

        public string State { get; set; }

        [Required]
        public string PostalCode { get; set; }

        public int CountryId { get; set; }
        public virtual Country Country { get; set; }
    }
}