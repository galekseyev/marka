﻿using System.ComponentModel.DataAnnotations;

namespace Marka.Common.Models.Languages
{
    public class Language
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string Title { get; set; }

        [Required]
        public string LanguageCode { get; set; }
    }
}