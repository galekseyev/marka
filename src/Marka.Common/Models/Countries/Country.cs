﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Marka.Common.Models.Countries
{
    public class Country
    {
        [Key]
        public int Id { get; set; }

        public string CountryCode { get; set; }
        public string CountryName { get; set; }
        public string NationalIdMask { get; set; }
        public string NationalIdNumber { get; set; }
        public string NationalIdRegEx { get; set; }
        public string PostalCodeRegEx { get; set; }

        public virtual ICollection<CountryState> CountryStates { get; set; }
    }

    public class CountryState
    {
        [Key]
        public int Id { get; set; }

        public string StateCode { get; set; }
        public string StateName { get; set; }

        public virtual Country Country { get; set; }
        public int CountryId { get; set; }
    }
}