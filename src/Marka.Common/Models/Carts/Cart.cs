﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

using Marka.Common.Models.Account.Users;

namespace Marka.Common.Models.Carts
{
    public class Cart
    {
        [Key]
        public string Id { get; set; }

        public DateTime CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }

        #region References

        public virtual ICollection<CartItem> CartItems { get; set; }

        public virtual User User { get; set; }
        public int? UserId { get; set; }

        #endregion
    }
}