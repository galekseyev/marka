﻿using System;
using System.Collections.Generic;

namespace Marka.Common.Models.Carts
{
    public class TranslatedCart
    {
        public string Id { get; set; }

        public DateTime CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }

        public decimal SubTotalPrice { get; set; }
        public decimal ShippingPrice { get; set; }
        public decimal TotalPrice { get; set; }

        public ICollection<TranslatedCartItem> CartItems { get; set; }
    }
}
