﻿using Marka.Common.Models.Catalog.Inventories;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Marka.Common.Models.Carts
{
    public class CartItem
    {
        public CartItem()
        {
            CartItemAttributesValuesXref = new List<CartItemAttributesValuesXref>();
        }

        [Key]
        public int Id { get; set; }
        public int Quantity { get; set; }

        [JsonIgnore]
        public virtual Inventory Inventory { get; set; }
        public int InventoryId { get; set; }

        [JsonIgnore]
        public virtual Cart Cart { get; set; }
        public string CartId { get; set; }

        public virtual ICollection<CartItemAttributesValuesXref> CartItemAttributesValuesXref { get; set; }
    }
}
