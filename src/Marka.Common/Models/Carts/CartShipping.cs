﻿using Marka.Common.Models.Shared;
using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;

namespace Marka.Common.Models.Carts
{
    public class CartShipping : Address
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string Email { get; set; }

        [Required]
        public string FullName { get; set; }

        //[Required]
        public string PhoneNumber { get; set; }

        public DateTime CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }


        [JsonIgnore]
        public virtual Cart Cart { get; set; }
        public string CartId { get; set; }
    }
}