﻿using Marka.Common.Models.Catalog.Inventories;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace Marka.Common.Models.Carts
{
    public class CartItemAttributesValuesXref
    {
        [Key]
        public int Id { get; set; }


        public int InventoryAttributeId { get; set; }
        public virtual InventoryAttribute InventoryAttribute { get; set; }

        public int InventoryAttributeValueId { get; set; }
        public virtual InventoryAttributeValue InventoryAttributeValue { get; set; }


        public int CartItemId { get; set; }

        [JsonIgnore]
        public virtual CartItem CartItem { get; set; }
    }
}