﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Marka.Common.Models.Carts
{
    public class TranslatedCartItemAttributesValuesXref
    {
        public int Id { get; set; }
        public int InventoryAttributeId { get; set; }
        public string InventoryAttributeTitle { get; set; }
        public int InventoryAttributeValueId { get; set; }
        public string InventoryAttributeValueTitle { get; set; }
    }
}