﻿using Marka.Common.Models.Catalog.Inventories;
using System;
using System.Collections.Generic;
using System.Text;

namespace Marka.Common.Models.Carts
{
    public class TranslatedCartItem
    {
        public int Id { get; set; }
        public int Quantity { get; set; }

        //Total allowed inventory quantity
        public int InventoryQuantity { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public decimal OriginalPrice { get; set; }
        public decimal Price { get; set; }

        public InventoryPicture Picture { get; set; }

        public virtual ICollection<TranslatedCartItemAttributesValuesXref> CartItemAttributesValuesXref { get; set; }
    }
}
