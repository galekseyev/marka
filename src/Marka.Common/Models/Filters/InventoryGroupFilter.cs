﻿using Marka.Common.Models.Shared;
using System;
using System.Collections.Generic;
using System.Text;

namespace Marka.Common.Models.Filters
{
    public class InventoryGroupFilter : Filter
    {
        public bool IncludeDisabled { get; set; }
        public bool IncludeEmptyGroups { get; set; }
    }
}