﻿using Marka.Common.Models.Shared;
using System.Collections.Generic;

namespace Marka.Common.Models.Filters
{
    public class InventoryFilter : Filter
    {
        public int? CategoryId { get; set; }

        public string SpecificationValueIds { get; set; }

        public bool IncludeDisabled { get; set; }
        public bool IncludeOutOfStock { get; set; }
    }
}