﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Marka.Common.Models.Shippings
{
    public class ShippingMethod
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string Title { get; set; }

        public string Description { get; set; }

        public bool Disabled { get; set; }
        public int DisplayOrder { get; set; }

        public DateTime CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }

        public virtual ICollection<ShippingRate> ShippingRates { get; set; }
    }
}