﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace Marka.Common.Models.Shippings
{
    public class ShippingRate
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public decimal Threshold { get; set; }

        public decimal? Price { get; set; }

        public int ShippingMethodId { get; set; }

        [JsonIgnore]
        public virtual ShippingMethod ShippingMethod { get; set; }
    }
}