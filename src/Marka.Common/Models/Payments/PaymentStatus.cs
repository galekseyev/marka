﻿using System.ComponentModel.DataAnnotations;

namespace Marka.Common.Models.Payments
{
    public class PaymentStatus
    {
        [Key]
        public int Id { get; set; }
        public string StatusCode { get; set; }
    }
}