﻿using System.ComponentModel.DataAnnotations;

namespace Marka.Common.Models.Payments
{
    public class PaymentType
    {
        [Key]
        public int Id { get; set; }
        public string TypeCode { get; set; }
    }
}