﻿using Marka.Common.Models.Account.Users;
using Marka.Common.Models.Payments;
using Marka.Common.Models.Shipments;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Marka.Common.Models.Orders
{
    public class Order
    {
        [Key]
        public int Id { get; set; }

        public string OrderNumber { get; set; }

        public DateTime CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }

        public decimal SubTotalPrice { get; set; }
        public decimal ShippingPrice { get; set; }
        public decimal TotalPrice { get; set; }

        public string TrackingNumber { get; set; }

        #region References

        public virtual User User { get; set; }
        public int? UserId { get; set; }

        public int OrderStatusId { get; set; }
        public virtual OrderStatus OrderStatus { get; set; }

        public int PaymentTypeId { get; set; }
        public virtual PaymentType PaymentType { get; set; }

        public int PaymentStatusId { get; set; }
        public virtual PaymentStatus PaymentStatus { get; set; }

        public virtual Shipment Shipment { get; set; }

        public virtual ICollection<OrderItem> OrderItems { get; set; }

        #endregion
    }
}