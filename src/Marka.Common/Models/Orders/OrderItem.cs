﻿using Marka.Common.Models.Catalog.Inventories;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Marka.Common.Models.Orders
{
    public class OrderItem
    {
        [Key]
        public int Id { get; set; }

        public decimal Price { get; set; }
        public int Quantity { get; set; }


        public int InventoryId { get; set; }
        public virtual Inventory Inventory { get; set; }

        [JsonIgnore]
        public virtual Order Order { get; set; }
        public int OrderId { get; set; }

        public virtual ICollection<OrderItemAttributesValuesXref> OrderItemAttributesValuesXref { get; set; }
    }

    public class OrderItemAttributesValuesXref
    {
        [Key]
        public int Id { get; set; }

        public int InventoryAttributeId { get; set; }
        public virtual InventoryAttribute InventoryAttribute { get; set; }

        public int InventoryAttributeValueId { get; set; }
        public virtual InventoryAttributeValue InventoryAttributeValue { get; set; }

        public int OrderItemId { get; set; }

        [JsonIgnore]
        public virtual OrderItem OrderItem { get; set; }
    }
}