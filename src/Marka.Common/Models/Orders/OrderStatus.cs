﻿using System.ComponentModel.DataAnnotations;

namespace Marka.Common.Models.Orders
{
    public class OrderStatus
    {
        [Key]
        public int Id { get; set; }
        public string StatusCode { get; set; }
    }
}