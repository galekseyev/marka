﻿using Marka.Common.Models.Catalog.Inventories;
using System;
using System.Collections.Generic;
using System.Text;

namespace Marka.Common.Models.Orders
{
    public class TranslatedOrderItem
    {
        public int Id { get; set; }
        public int Quantity { get; set; }

        public string Title { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }

        public InventoryPicture Picture { get; set; }

        public ICollection<TranslatedOrderItemAttributesValuesXref> OrderItemAttributesValuesXref { get; set; }
    }
}
