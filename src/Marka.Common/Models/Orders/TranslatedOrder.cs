﻿using System;
using System.Collections.Generic;
using Marka.Common.Models.Payments;
using Marka.Common.Models.Shipments;

namespace Marka.Common.Models.Orders
{
    public class TranslatedOrder
    {
        public int Id { get; set; }
        public string OrderNumber { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }

        public PaymentType PaymentType { get; set; }
        public PaymentStatus PaymentStatus { get; set; }

        public OrderStatus OrderStatus { get; set; }

        public decimal SubTotalPrice { get; set; }
        public decimal ShippingPrice { get; set; }
        public decimal TotalPrice { get; set; }

        public Shipment Shipment { get; set; }
        public ICollection<TranslatedOrderItem> OrderItems { get; set; }
    }
}