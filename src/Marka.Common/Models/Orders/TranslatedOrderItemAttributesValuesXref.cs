﻿namespace Marka.Common.Models.Orders
{
    public class TranslatedOrderItemAttributesValuesXref
    {
        public int? Id { get; set; }
        public int InventoryAttributeId { get; set; }
        public string InventoryAttributeTitle { get; set; }
        public int InventoryAttributeValueId { get; set; }
        public string InventoryAttributeValueTitle { get; set; }
    }
}