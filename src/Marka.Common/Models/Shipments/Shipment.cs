﻿using Marka.Common.Models.Orders;
using Marka.Common.Models.Shared;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace Marka.Common.Models.Shipments
{
    public class Shipment : Address
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string Email { get; set; }

        [Required]
        public string FullName { get; set; }

        public string PhoneNumber { get; set; }

        public int OrderId { get; set; }
        public virtual Order Order { get; set; }
    }
}