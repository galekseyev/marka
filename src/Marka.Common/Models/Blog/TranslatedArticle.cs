﻿using System;

namespace Marka.Common.Models.Blog
{
    public class TranslatedArticle
    {
        public int Id { get; set; }

        public string Title { get; set; }
        public string Description { get; set; }


        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }

        public bool Disabled { get; set; }

        public ArticlePicture Picture { get; set; }
    }
}
