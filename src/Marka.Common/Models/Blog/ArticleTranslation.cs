﻿using Marka.Common.Models.Languages;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace Marka.Common.Models.Blog
{
    public class ArticleTranslation
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(255, MinimumLength = 1)]
        public string Title { get; set; }

        public string Description { get; set; }

        public int LanguageId { get; set; }

        [JsonIgnore]
        public virtual Language Language { get; set; }

        public int ArticleId { get; set; }

        [JsonIgnore]
        public virtual Article Article { get; set; }
    }
}