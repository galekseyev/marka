﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Marka.Common.Models.Blog
{
    public class Article
    {
        [Key]
        public int Id { get; set; }

        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }

        public bool Disabled { get; set; }

        public int? PictureId { get; set; }
        public virtual ArticlePicture Picture { get; set; }

        public virtual ICollection<ArticleTranslation> Translations { get; set; }
    }
}