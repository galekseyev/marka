﻿using Marka.Common.Models.Shared;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace Marka.Common.Models.Blog
{
    public class ArticlePicture : IPicture
    {
        [Key]
        public int Id { get; set; }

        public string Title { get; set; }
        public string Alt { get; set; }

        [Required]
        public string Thumbnail { get; set; }

        [Required]
        public string Picture { get; set; }

        [JsonIgnore]
        public virtual Article Article { get; set; }
    }
}