﻿using Marka.BusinessLogic.Interfaces.Countries;
using Marka.Common.Models.Countries;
using Marka.Common.Results;
using Marka.Common.Results.Interfaces;
using Marka.DataLayer.Interfaces.Repositories.Countries;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Marka.BusinessLogic.Services.Countries
{
    public class CountryService : ICountryService
    {
        private readonly IResultFactory _resultFactory;
        private readonly ICountryRepository _countryRepository;

        public CountryService(IResultFactory resultFactory, ICountryRepository countryRepository)
        {
            _resultFactory = resultFactory;
            _countryRepository = countryRepository;
        }

        public async Task<IResult<IEnumerable<Country>>> CreateCountriesAsync(IEnumerable<Country> countries)
        {
            await _countryRepository.AddRangeAsync(countries);

            return _resultFactory.Created(countries);
        }

        public async Task<IResult<IEnumerable<Country>>> GetCountriesAsync()
        {
            var countries = await _countryRepository.GetAllAsync();

            return _resultFactory.Success(countries);
        }
    }
}
