﻿using Marka.BusinessLogic.Interfaces.Orders;
using Marka.Common.Models.Shipments;
using Marka.Common.Results;
using Marka.Common.Results.Interfaces;
using Marka.DataLayer.Interfaces.Repositories.Shipments;
using System.Threading.Tasks;

namespace Marka.BusinessLogic.Services.Orders
{
    public class ShipmentService : IShipmentService
    {
        private readonly IResultFactory _resultFactory;
        private readonly IShipmentRepository _shipmentRepository;

        public ShipmentService(IResultFactory resultFactory, IShipmentRepository shipmentRepository)
        {
            _resultFactory = resultFactory;
            _shipmentRepository = shipmentRepository;
        }

        public async Task<IResult<Shipment>> GetShipmentAsync(int id)
        {
            var shipment = await _shipmentRepository.GetByIdAsync(id);

            if (shipment == null)
                return _resultFactory.NotFound<Shipment>();

            return _resultFactory.Success(shipment);
        }

        public async Task<IResult<Shipment>> CreateShipmentAsync(Shipment shipment)
        {
            await _shipmentRepository.AddAsync(shipment);

            return _resultFactory.Created(shipment);
        }

        public async Task<IResult<Shipment>> UpdateShipmentAsync(Shipment shipment)
        {
            await _shipmentRepository.UpdateAsync(shipment);

            return _resultFactory.Success(shipment);
        }

        public async Task<IResult> DeleteShipmentAsync(int id)
        {
            var shipment = await _shipmentRepository.GetByIdAsync(id);

            if (shipment == null)
                return _resultFactory.NotFound();

            await _shipmentRepository.DeleteAsync(shipment);

            return _resultFactory.Success();
        }
    }
}
