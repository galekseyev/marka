﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Marka.BusinessLogic.Interfaces;
using Marka.Common.Models.Carts;
using Marka.Common.Results;
using Marka.Common.Results.Interfaces;
using Marka.DataLayer.Interfaces.Repositories.Carts;

namespace Marka.BusinessLogic.Services.Carts
{
    public class CartService : ICartService
    {
        private readonly IResultFactory _resultFactory;
        private readonly ICartRepository _cartRepository;

        public CartService(IResultFactory resultFactory, ICartRepository cartRepository)
        {
            _resultFactory = resultFactory;
            _cartRepository = cartRepository;
        }

        public async Task<IResult<Cart>> GetUserCartAsync(int id)
        {
            var cart = await _cartRepository.GetFirstAsync(c => c.UserId == id);

            if (cart != null)
                return _resultFactory.Success(cart);

            return _resultFactory.NotFound<Cart>();
        }

        public async Task<IResult<Cart>> GetCartAsync(string id)
        {
            var cart = await _cartRepository.GetCartAsync(id);

            if (cart != null)
                return _resultFactory.Success(cart);

            return _resultFactory.NotFound<Cart>();
        }

        public async Task<IResult<Cart>> CreateCartAsync(Cart cart)
        {
            cart.CreateDate = DateTime.UtcNow;

            await _cartRepository.AddAsync(cart);

            return _resultFactory.Created(cart);
        }

        public async Task<IResult<Cart>> UpdateCartAsync(Cart cart)
        {
            cart.UpdateDate = DateTime.UtcNow;

            await _cartRepository.UpdateAsync(cart);

            return _resultFactory.Success(cart);
        }

        public async Task<IResult> DeleteCartAsync(string id)
        {
            var cart = await _cartRepository.GetByIdAsync(id);

            if (cart == null)
                return _resultFactory.NotFound();

            await _cartRepository.DeleteAsync(cart);

            return _resultFactory.Success();
        }

        public async Task<IResult<TranslatedCart>> GetTranslatedCartAsync(string id, int? userId, string langCode)
        {
            var cart = await _cartRepository.GetTranslatedCartAsync(id, langCode);

            if (cart == null)
            {
                await CreateCartAsync(new Cart() { Id = id, UserId = userId });

                cart = new TranslatedCart
                {
                    Id = id,
                    CartItems = new List<TranslatedCartItem>()
                };
            }

            cart.ShippingPrice = cart.ShippingPrice;
            cart.SubTotalPrice = cart.CartItems.Sum(c => c.Price * c.Quantity);
            cart.TotalPrice = cart.CartItems.Sum(c => c.Price * c.Quantity) + cart.ShippingPrice;


            return _resultFactory.Success(cart);
        }

        public async Task<IResult<Cart>> AddItemAsync(string id, CartItem item, bool createIfMissing = false)
        {
            var cart = await _cartRepository.GetCartAsync(id);

            if (cart != null)
            {
                cart.CartItems.Add(item);

                return await UpdateCartAsync(cart);
            }
            else
            {
                if (!createIfMissing)
                    return _resultFactory.NotFound<Cart>();

                cart = new Cart { Id = id };
                cart.CartItems.Add(item);

                return await CreateCartAsync(cart);
            }
        }

        public async Task<IResult<Cart>> UpdateItemQuantityAsync(string id, int itemId, int quantity)
        {
            var cart = await _cartRepository.GetCartAsync(id);

            if (cart == null)
                return _resultFactory.NotFound<Cart>();

            var item = cart.CartItems.FirstOrDefault(c => c.Id == itemId);

            if (item == null)
                return _resultFactory.NotFound<Cart>();

            item.Quantity = quantity;

            await _cartRepository.UpdateAsync(cart);

            return _resultFactory.Success(cart);
        }

        public async Task<IResult<Cart>> DeleteItemAsync(string id, int itemId)
        {
            var cart = await _cartRepository.GetCartAsync(id);

            if (cart == null)
                return _resultFactory.NotFound<Cart>();

            var item = cart.CartItems.FirstOrDefault(c => c.Id == itemId);

            if (item == null)
                return _resultFactory.NotFound<Cart>();

            cart.CartItems.Remove(item);

            return await UpdateCartAsync(cart);
        }
    }
}