﻿using Marka.BusinessLogic.Interfaces.Carts;
using Marka.Common.Models.Carts;
using Marka.Common.Results;
using Marka.Common.Results.Interfaces;
using Marka.DataLayer.Interfaces.Repositories.Carts;
using System.Threading.Tasks;

namespace Marka.BusinessLogic.Services.Carts
{
    public class CartShippingService : ICartShippingService
    {
        private readonly IResultFactory _resultFactory;
        private readonly ICartShippingRepository _carShippingRepository;

        public CartShippingService(IResultFactory resultFactory, ICartShippingRepository carShippingRepository)
        {
            _resultFactory = resultFactory;
            _carShippingRepository = carShippingRepository;
        }

        public async Task<IResult<CartShipping>> GetCartShippingAsync(int id)
        {
            var cartShipping = await _carShippingRepository.GetByIdAsync(id);

            if (cartShipping == null)
                return _resultFactory.NotFound<CartShipping>();

            return _resultFactory.Success(cartShipping);
        }

        public async Task<IResult<CartShipping>> CreateCartShippingAsync(CartShipping cartShipping)
        {
            await _carShippingRepository.AddAsync(cartShipping);

            return _resultFactory.Created(cartShipping);
        }

        public async Task<IResult<CartShipping>> UpdateCartShippingAsync(CartShipping cartShipping)
        {
            await _carShippingRepository.UpdateAsync(cartShipping);

            return _resultFactory.Success(cartShipping);
        }

        public async Task<IResult> DeleteCartShippingAsync(int id)
        {
            var cartShipping = await _carShippingRepository.GetByIdAsync(id);

            if (cartShipping == null)
                return _resultFactory.NotFound();

            await _carShippingRepository.DeleteAsync(cartShipping);

            return _resultFactory.Success();
        }
    }
}