﻿using Marka.BusinessLogic.Interfaces.Shippings;
using Marka.Common.Models.Shippings;
using Marka.Common.Results;
using Marka.Common.Results.Interfaces;
using Marka.DataLayer.Interfaces.Repositories.Shippings;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Marka.BusinessLogic.Services.Shippings
{
    public class ShippingMethodService : IShippingMethodService
    {
        private readonly IResultFactory _resultFactory;
        private readonly IShippingMethodRepository _shippingMethodRepository;

        public ShippingMethodService(IResultFactory resultFactory, IShippingMethodRepository shippingMethodRepository)
        {
            _resultFactory = resultFactory;
            _shippingMethodRepository = shippingMethodRepository;
        }

        public async Task<IResult<ShippingMethod>> GetShippingMethodAsync(int id)
        {
            var method = await _shippingMethodRepository.GetByIdAsync(id);

            if (method == null)
                return _resultFactory.NotFound<ShippingMethod>();

            return _resultFactory.Success(method);
        }

        public async Task<IResult<IEnumerable<ShippingMethod>>> GetShippingMethodsAsync()
        {
            var methods = await _shippingMethodRepository.GetAllAsync();

            return _resultFactory.Success(methods);
        }

        public async Task<IResult<ShippingMethod>> CreateShippingMethodAsync(ShippingMethod method)
        {
            await _shippingMethodRepository.AddAsync(method);

            return _resultFactory.Created(method);
        }

        public async Task<IResult<ShippingMethod>> UpdateShippingMethodAsync(ShippingMethod method)
        {
            await _shippingMethodRepository.UpdateAsync(method);

            return _resultFactory.Success(method);
        }

        public async Task<IResult> DeleteShippingMethodAsync(int id)
        {
            var method = await _shippingMethodRepository.GetByIdAsync(id);

            if (method == null)
                return _resultFactory.NotFound();

            await _shippingMethodRepository.DeleteAsync(method);

            return _resultFactory.Success();
        }
    }
}
