﻿using Marka.BusinessLogic.Interfaces.Shippings;
using Marka.Common.Models.Shippings;
using Marka.Common.Results;
using Marka.Common.Results.Interfaces;
using Marka.DataLayer.Interfaces.Repositories.Shippings;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Marka.BusinessLogic.Services.Shippings
{
    public class ShippingRateService : IShippingRateService
    {
        private readonly IResultFactory _resultFactory;
        private readonly IShippingRateRepository _shippingRateRepository;

        public ShippingRateService(IResultFactory resultFactory, IShippingRateRepository shippingRateRepository)
        {
            _resultFactory = resultFactory;
            _shippingRateRepository = shippingRateRepository;
        }

        public async Task<IResult<ShippingRate>> GetShippingRateAsync(int id)
        {
            var method = await _shippingRateRepository.GetByIdAsync(id);

            if (method == null)
                return _resultFactory.NotFound<ShippingRate>();

            return _resultFactory.Success(method);
        }

        public async Task<IResult<IEnumerable<ShippingRate>>> GetShippingRatesAsync(int id)
        {
            var methods = await _shippingRateRepository.GetShippingRatesAsync(id);

            return _resultFactory.Success(methods);
        }

        public async Task<IResult<ShippingRate>> CreateShippingRateAsync(ShippingRate rate)
        {
            await _shippingRateRepository.AddAsync(rate);

            return _resultFactory.Created(rate);
        }

        public async Task<IResult<ShippingRate>> UpdateShippingRateAsync(ShippingRate rate)
        {
            await _shippingRateRepository.UpdateAsync(rate);

            return _resultFactory.Success(rate);
        }

        public async Task<IResult> DeleteShippingRateAsync(int id)
        {
            var rate = await _shippingRateRepository.GetByIdAsync(id);

            if (rate == null)
                return _resultFactory.NotFound();

            await _shippingRateRepository.DeleteAsync(rate);

            return _resultFactory.Success();
        }
    }
}
