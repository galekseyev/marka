﻿using Marka.BusinessLogic.Interfaces.Account;
using Marka.Common.Models.Account.Users;
using Marka.Common.Results;
using Marka.Common.Results.Interfaces;
using Marka.DataLayer.Interfaces.Repositories.Users;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Marka.BusinessLogic.Services.Account
{
    public class UserAddressService : IUserAddressService
    {
        private readonly IResultFactory _resultFactory;
        private readonly IUserAddressRepository _userAddressRepository;

        public UserAddressService(IResultFactory resultFactory, IUserAddressRepository userAddressRepository)
        {
            _resultFactory = resultFactory;
            _userAddressRepository = userAddressRepository;
        }

        public async Task<IResult<UserAddress>> GetUserAddressAsync(int userId)
        {
            var address = await _userAddressRepository.GetUserAddressAsync(userId);

            return _resultFactory.Success(address);
        }

        public async Task<IResult<UserAddress>> CreateUserAddressAsync(UserAddress address)
        {
            await _userAddressRepository.AddAsync(address);

            return _resultFactory.Created(address);
        }

        public async Task<IResult<UserAddress>> UpdateUserAddressAsync(UserAddress address)
        {
            await _userAddressRepository.UpdateAsync(address);

            return _resultFactory.Success(address);
        }

        public async Task<IResult> DeleteUserAddressAsync(int id)
        {
            var address = await _userAddressRepository.GetByIdAsync(id);

            if (address == null)
                return _resultFactory.NotFound();

            await _userAddressRepository.DeleteAsync(address);

            return _resultFactory.Success();
        }
    }
}