﻿using Marka.BusinessLogic.Interfaces.Blog;
using Marka.Common.Models.Blog;
using Marka.Common.Results;
using Marka.Common.Results.Interfaces;
using Marka.DataLayer.Interfaces.Repositories.Blog;
using System.Threading.Tasks;

namespace Marka.BusinessLogic.Services.Blog
{
    public class ArticlePictureService : IArticlePictureService
    {
        private readonly IResultFactory _resultFactory;
        private readonly IArticlePictureRepository _articlePictureRepository;

        public ArticlePictureService(IResultFactory resultFactory, IArticlePictureRepository articlePictureRepository)
        {
            _resultFactory = resultFactory;
            _articlePictureRepository = articlePictureRepository;
        }

        public async Task<IResult<ArticlePicture>> GetArticlePictureAsync(int id)
        {
            var pictures = await _articlePictureRepository.GetArticlePictureAsync(id);

            return _resultFactory.Success(pictures);
        }

        public async Task<IResult<ArticlePicture>> GetPictureAsync(int id)
        {
            var picture = await _articlePictureRepository.GetByIdAsync(id);

            if (picture == null)
                return _resultFactory.NotFound<ArticlePicture>();

            return _resultFactory.Success(picture);
        }

        public async Task<IResult<ArticlePicture>> CreatePictureAsync(ArticlePicture picture)
        {
            await _articlePictureRepository.AddAsync(picture);

            return _resultFactory.Created(picture);
        }

        public async Task<IResult<ArticlePicture>> UpdatePictureAsync(ArticlePicture picture)
        {
            await _articlePictureRepository.UpdateAsync(picture);

            return _resultFactory.Success(picture);
        }

        public async Task<IResult> DeletePictureAsync(int id)
        {
            var picture = await _articlePictureRepository.GetByIdAsync(id);

            if (picture == null)
                return _resultFactory.NotFound();

            await _articlePictureRepository.DeleteAsync(picture);

            return _resultFactory.Success();
        }
    }
}
