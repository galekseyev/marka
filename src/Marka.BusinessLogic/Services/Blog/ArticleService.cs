﻿using Marka.BusinessLogic.Interfaces.Blog;
using Marka.Common.Models.Blog;
using Marka.Common.Results;
using Marka.Common.Results.Interfaces;
using Marka.DataLayer.Interfaces.Repositories.Blog;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Marka.BusinessLogic.Services.Blog
{
    public class ArticleService : IArticleService
    {
        private readonly IResultFactory _resultFactory;
        private readonly IArticleRepository _articleRepository;

        public ArticleService(IResultFactory resultFactory, IArticleRepository articleRepository)
        {
            _resultFactory = resultFactory;

            _articleRepository = articleRepository;
        }

        public async Task<IResult<Article>> GetArticleAsync(int id)
        {
            var article = await _articleRepository.GetArticleAsync(id);

            if (article == null)
                return _resultFactory.NotFound<Article>();

            return _resultFactory.Success(article);
        }

        public async Task<IResult<IEnumerable<Article>>> GetArticlesAsync()
        {
            var articles = await _articleRepository.GetArticlesAsync();

            return _resultFactory.Success(articles);
        }

        public async Task<IResult<Article>> CreateArticleAsync(Article article)
        {
            article.CreateDate = DateTime.UtcNow;

            await _articleRepository.AddAsync(article);

            return _resultFactory.Created(article);
        }

        public async Task<IResult<Article>> UpdateArticleAsync(Article article)
        {
            article.UpdateDate = DateTime.UtcNow;

            await _articleRepository.UpdateAsync(article);

            return _resultFactory.Success(article);
        }

        public async Task<IResult> DeleteArticleAsync(int id)
        {
            var article = await _articleRepository.GetByIdAsync(id);

            if (article == null)
                return _resultFactory.NotFound();

            await _articleRepository.DeleteAsync(article);

            return _resultFactory.Success();
        }

        public async Task<IResult<TranslatedArticle>> GetTranslatedArticleAsync(int id, string langCode)
        {
            var article = await _articleRepository.GetTranslatedArticleAsync(id, langCode);

            if (article == null)
                return _resultFactory.NotFound<TranslatedArticle>();

            return _resultFactory.Success(article);
        }

        public async Task<IResult<IEnumerable<TranslatedArticle>>> GetTranslatedArticlesAsync(string langCode)
        {
            var articles = await _articleRepository.GetTranslatedArticlesAsync(langCode);

            return _resultFactory.Success(articles);
        }
    }
}
