﻿using Marka.BusinessLogic.Interfaces.Catalog.Categories;
using Marka.Common.Models.Catalog.Categories;
using Marka.Common.Results;
using Marka.Common.Results.Interfaces;
using Marka.DataLayer.Interfaces.Repositories.Catalog.Categories;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Marka.BusinessLogic.Services.Catalog.Categories
{
    public class CategoryService : ICategoryService
    {
        private readonly IResultFactory _resultFactory;
        private readonly ICategoryRepository _categoryRepository;

        public CategoryService(IResultFactory resultFactory, ICategoryRepository categoryRepository)
        {
            _resultFactory = resultFactory;

            _categoryRepository = categoryRepository;
        }

        public async Task<IResult<Category>> GetCategoryAsync(int id)
        {
            var category = await _categoryRepository.GetCategoryAsync(id);

            if (category == null)
                return _resultFactory.NotFound<Category>();

            return _resultFactory.Success(category);
        }

        public async Task<IResult<IEnumerable<Category>>> GetCategoriesAsync()
        {
            var categories = await _categoryRepository.GetCategoriesAsync();

            return _resultFactory.Success(categories);
        }

        public async Task<IResult<Category>> CreateCategoryAsync(Category category)
        {
            await _categoryRepository.AddAsync(category);

            return _resultFactory.Created(category);
        }

        public async Task<IResult<Category>> UpdateCategoryAsync(Category category)
        {
            await _categoryRepository.UpdateAsync(category);

            return _resultFactory.Success(category);
        }

        public async Task<IResult> DeleteCategoryAsync(int id)
        {
            var category = await _categoryRepository.GetByIdAsync(id);

            if (category == null)
                return _resultFactory.NotFound();

            await _categoryRepository.DeleteAsync(category);

            return _resultFactory.Success();
        }

        public async Task<IResult<TranslatedCategory>> GetTranslatedCategoryAsync(int id, string langCode)
        {
            var category = await _categoryRepository.GetTranslatedCategoryAsync(id, langCode);

            if (category == null)
                return _resultFactory.NotFound<TranslatedCategory>();

            return _resultFactory.Success(category);
        }

        public async Task<IResult<IEnumerable<TranslatedCategory>>> GetTranslatedCategoriesAsync(string langCode)
        {
            var categories = await _categoryRepository.GetTranslatedCategoriesAsync(langCode);

            return _resultFactory.Success(categories);
        }
    }
}