﻿using Marka.BusinessLogic.Interfaces.Catalog.Categories;
using Marka.Common.Models.Catalog.Categories;
using Marka.Common.Results;
using Marka.Common.Results.Interfaces;
using Marka.DataLayer.Interfaces.Repositories.Catalog.Categories;
using System.Threading.Tasks;

namespace Marka.BusinessLogic.Services.Catalog.Categories
{
    public class CategoryPictureService : ICategoryPictureService
    {
        private readonly IResultFactory _resultFactory;
        private readonly ICategoryPictureRepository _categoryPictureRepository;

        public CategoryPictureService(IResultFactory resultFactory, ICategoryPictureRepository categoryPictureRepository)
        {
            _resultFactory = resultFactory;
            _categoryPictureRepository = categoryPictureRepository;
        }

        public async Task<IResult<CategoryPicture>> GetCategoryPictureAsync(int id)
        {
            var inventoryPictures = await _categoryPictureRepository.GetCategoryPictureAsync(id);

            return _resultFactory.Success(inventoryPictures);
        }

        public async Task<IResult<CategoryPicture>> GetPictureAsync(int id)
        {
            var picture = await _categoryPictureRepository.GetByIdAsync(id);

            if (picture == null)
                return _resultFactory.NotFound<CategoryPicture>();

            return _resultFactory.Success(picture);
        }

        public async Task<IResult<CategoryPicture>> CreatePictureAsync(CategoryPicture picture)
        {
            await _categoryPictureRepository.AddAsync(picture);

            return _resultFactory.Created(picture);
        }

        public async Task<IResult<CategoryPicture>> UpdatePictureAsync(CategoryPicture picture)
        {
            await _categoryPictureRepository.UpdateAsync(picture);

            return _resultFactory.Success(picture);
        }

        public async Task<IResult> DeletePictureAsync(int id)
        {
            var picture = await _categoryPictureRepository.GetByIdAsync(id);

            if (picture == null)
                return _resultFactory.NotFound();

            await _categoryPictureRepository.DeleteAsync(picture);

            return _resultFactory.Success();
        }
    }
}