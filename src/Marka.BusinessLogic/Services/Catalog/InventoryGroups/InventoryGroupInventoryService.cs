﻿using Marka.BusinessLogic.Interfaces.Catalog.InventoryGroups;
using Marka.Common.Models.Catalog.InventoryGroups;
using Marka.Common.Results;
using Marka.Common.Results.Interfaces;
using Marka.DataLayer.Interfaces.Repositories.Catalog.InventoryGroups;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Marka.BusinessLogic.Services.Catalog.InventoryGroups
{
    public class InventoryGroupInventoryService : IInventoryGroupInventoryService
    {
        private readonly IResultFactory _resultFactory;
        private readonly IInventoryGroupInventoryRepository _inventoryGroupInventoryRepository;

        public InventoryGroupInventoryService(IResultFactory resultFactory, IInventoryGroupInventoryRepository inventoryGroupInventoryRepository)
        {
            _resultFactory = resultFactory;
            _inventoryGroupInventoryRepository = inventoryGroupInventoryRepository;
        }


        public async Task<IResult<IEnumerable<InventoryGroupInventory>>> GetInventoryGroupInventoriesAsync(int id)
        {
            var inventoryGroupInventories = await _inventoryGroupInventoryRepository.GetManyAsync(c => c.InventoryId == id);

            return _resultFactory.Success(inventoryGroupInventories);
        }

        public async Task<IResult<InventoryGroupInventory>> GetInventoryGroupInventoryAsync(int id)
        {
            var inventoryGroupInventory = await _inventoryGroupInventoryRepository.GetByIdAsync(id);

            if (inventoryGroupInventory == null)
                return _resultFactory.NotFound<InventoryGroupInventory>();

            return _resultFactory.Success(inventoryGroupInventory);
        }

        public async Task<IResult<InventoryGroupInventory>> CreateInventoryGroupInventoryAsync(InventoryGroupInventory inventoryGroupInventory)
        {
            await _inventoryGroupInventoryRepository.AddAsync(inventoryGroupInventory);

            return _resultFactory.Created(inventoryGroupInventory);
        }

        public async Task<IResult<InventoryGroupInventory>> UpdateInventoryGroupInventoryAsync(InventoryGroupInventory inventoryGroupInventory)
        {
            await _inventoryGroupInventoryRepository.UpdateAsync(inventoryGroupInventory);

            return _resultFactory.Success(inventoryGroupInventory);
        }

        public async Task<IResult> DeleteInventoryGroupInventoryAsync(int id)
        {
            var inventoryGroupInventory = await _inventoryGroupInventoryRepository.GetByIdAsync(id);

            if (inventoryGroupInventory == null)
                return _resultFactory.NotFound();

            await _inventoryGroupInventoryRepository.DeleteAsync(inventoryGroupInventory);

            return _resultFactory.Success();
        }

        public async Task<IResult<IEnumerable<TranslatedInventoryGroupInventory>>> GetTranslatedInventoryGroupInventoriesAsync(int id, string langCode)
        {
            var inventories = await _inventoryGroupInventoryRepository.GetTranslatedInventoryGroupInventoriesAsync(id, langCode);

            return _resultFactory.Success(inventories);
        }
    }
}
