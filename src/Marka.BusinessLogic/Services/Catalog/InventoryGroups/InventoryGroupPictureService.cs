﻿using Marka.BusinessLogic.Interfaces.Catalog.InventoryGroups;
using Marka.Common.Models.Catalog.InventoryGroups;
using Marka.Common.Results;
using Marka.Common.Results.Interfaces;
using Marka.DataLayer.Interfaces.Repositories.Catalog.InventoryGroups;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Marka.BusinessLogic.Services.Catalog.InventoryGroups
{
    public class InventoryGroupPictureService : IInventoryGroupPictureService
    {
        private readonly IResultFactory _resultFactory;
        private readonly IInventoryGroupPictureRepository _inventoryGroupPictureRepository;

        public InventoryGroupPictureService(IResultFactory resultFactory, IInventoryGroupPictureRepository inventoryGroupPictureRepository)
        {
            _resultFactory = resultFactory;
            _inventoryGroupPictureRepository = inventoryGroupPictureRepository;
        }

        public async Task<IResult<InventoryGroupPicture>> GetInventoryGroupPictureAsync(int id)
        {
            var picture = await _inventoryGroupPictureRepository.GetFirstAsync(c => c.InventoryGroup.Id == id);

            if (picture == null)
                return _resultFactory.NotFound<InventoryGroupPicture>();

            return _resultFactory.Success(picture);
        }

        public async Task<IResult<InventoryGroupPicture>> CreateInventoryGroupPictureAsync(InventoryGroupPicture picture)
        {
            await _inventoryGroupPictureRepository.AddAsync(picture);

            return _resultFactory.Created(picture);
        }

        public async Task<IResult<InventoryGroupPicture>> UpdateInventoryGroupPictureAsync(InventoryGroupPicture picture)
        {
            await _inventoryGroupPictureRepository.UpdateAsync(picture);

            return _resultFactory.Success(picture);
        }

        public async Task<IResult> DeleteInventoryGroupPictureAsync(int id)
        {
            var picture = await _inventoryGroupPictureRepository.GetByIdAsync(id);

            if (picture == null)
                return _resultFactory.NotFound();

            await _inventoryGroupPictureRepository.DeleteAsync(picture);

            return _resultFactory.Success();
        }
    }
}
