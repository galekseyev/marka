﻿using Marka.BusinessLogic.Interfaces.Catalog.InventoryGroups;
using Marka.Common.Models.Catalog.InventoryGroups;
using Marka.Common.Models.Filters;
using Marka.Common.Results;
using Marka.Common.Results.Interfaces;
using Marka.DataLayer.Interfaces.Repositories.Catalog.InventoryGroups;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Marka.BusinessLogic.Services.Catalog.InventoryGroups
{
    public class InventoryGroupService : IInventoryGroupService
    {
        private readonly IResultFactory _resultFactory;
        private readonly IInventoryGroupRepository _inventoryGroupRepository;

        public InventoryGroupService(IResultFactory resultFactory, IInventoryGroupRepository inventoryGroupRepository)
        {
            _resultFactory = resultFactory;
            _inventoryGroupRepository = inventoryGroupRepository;
        }

        public async Task<IResult<InventoryGroup>> GetInventoryGroupAsync(int id)
        {
            var inventory = await _inventoryGroupRepository.GetInventoryGroupAsync(id);

            if (inventory == null)
                return _resultFactory.NotFound<InventoryGroup>();

            return _resultFactory.Success(inventory);
        }

        public async Task<IResult<FilteredInventoryGroups>> GetInventoryGroupsAsync(InventoryGroupFilter filter)
        {
            var inventories = await _inventoryGroupRepository.GetInventoryGroupsAsync(filter);

            return _resultFactory.Success(inventories);
        }

        public async Task<IResult<InventoryGroup>> CreateInventoryGroupAsync(InventoryGroup inventoryGroup)
        {
            await _inventoryGroupRepository.AddAsync(inventoryGroup);

            return _resultFactory.Created(inventoryGroup);
        }

        public async Task<IResult<InventoryGroup>> UpdateInventoryGroupAsync(InventoryGroup inventoryGroup)
        {
            await _inventoryGroupRepository.UpdateAsync(inventoryGroup);

            return _resultFactory.Success(inventoryGroup);
        }

        public async Task<IResult> DeleteInventoryGroupAsync(int id)
        {
            var inventoryGroup = await _inventoryGroupRepository.GetByIdAsync(id);

            if (inventoryGroup == null)
                return _resultFactory.NotFound();

            await _inventoryGroupRepository.DeleteAsync(inventoryGroup);

            return _resultFactory.Success();
        }

        public async Task<IResult<TranslatedInventoryGroup>> GetTranslatedInventoryGroup(int id, string langCode)
        {
            var group = await _inventoryGroupRepository.GetTranslatedInventoryGroup(id, langCode);

            if (group == null)
                return _resultFactory.NotFound<TranslatedInventoryGroup>();

            return _resultFactory.Success(group);
        }
    }
}