﻿using Marka.BusinessLogic.Interfaces.Catalog.Inventories;
using Marka.Common.Models.Catalog.Inventories;
using Marka.Common.Results;
using Marka.Common.Results.Interfaces;
using Marka.DataLayer.Interfaces.Repositories.Catalog.Inventories;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Marka.BusinessLogic.Services.Catalog.Inventories
{
    public class InventoryPictureService : IInventoryPictureService
    {
        private readonly IResultFactory _resultFactory;
        private readonly IInventoryPictureRepository _inventoryPictureRepository;

        public InventoryPictureService(IResultFactory resultFactory, IInventoryPictureRepository inventoryPictureRepository)
        {
            _resultFactory = resultFactory;
            _inventoryPictureRepository = inventoryPictureRepository;
        }

        public async Task<IResult<IEnumerable<InventoryPicture>>> GetInventoryPicturesAsync(int id)
        {
            var inventoryPictures = await _inventoryPictureRepository.GetInventoryPicturesAsync(id);

            return _resultFactory.Success(inventoryPictures);
        }

        public async Task<IResult<InventoryPicture>> GetPictureAsync(int id)
        {
            var picture = await _inventoryPictureRepository.GetByIdAsync(id);

            if (picture == null)
                return _resultFactory.NotFound<InventoryPicture>();

            return _resultFactory.Success(picture);
        }

        public async Task<IResult<InventoryPicture>> CreatePictureAsync(InventoryPicture picture)
        {
            await _inventoryPictureRepository.AddAsync(picture);

            return _resultFactory.Created(picture);
        }

        public async Task<IResult<InventoryPicture>> UpdatePictureAsync(InventoryPicture picture)
        {
            await _inventoryPictureRepository.UpdateAsync(picture);

            return _resultFactory.Success(picture);
        }

        public async Task<IResult> DeletePictureAsync(int id)
        {
            var picture = await _inventoryPictureRepository.GetByIdAsync(id);

            if (picture == null)
                return _resultFactory.NotFound();

            await _inventoryPictureRepository.DeleteAsync(picture);

            return _resultFactory.Success();
        }
    }
}
