﻿using Marka.BusinessLogic.Interfaces.Catalog.Inventories;
using Marka.Common.Models.Catalog.Inventories;
using Marka.Common.Results;
using Marka.Common.Results.Interfaces;
using Marka.DataLayer.Interfaces.Repositories.Catalog.Inventories;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Marka.BusinessLogic.Services.Catalog.Inventories
{
    public class InventorySpecificationService : IInventorySpecificationService
    {
        private readonly IResultFactory _resultFactory;
        private readonly IInventorySpecificationRepository _inventorySpecificationRepository;

        public InventorySpecificationService(IResultFactory resultFactory, IInventorySpecificationRepository inventorySpecificationRepository)
        {
            _resultFactory = resultFactory;
            _inventorySpecificationRepository = inventorySpecificationRepository;
        }

        public async Task<IResult<InventorySpecification>> GetInventorySpecificationAsync(int id, int specificationId)
        {
            var picture = await _inventorySpecificationRepository.GetByIdAsync(specificationId);

            if (picture == null)
                return _resultFactory.NotFound<InventorySpecification>();

            return _resultFactory.Success(picture);
        }

        public async Task<IResult<InventorySpecification>> CreateInventorySpecificationAsync(InventorySpecification specification)
        {
            await _inventorySpecificationRepository.AddAsync(specification);

            return _resultFactory.Created(specification);
        }

        public async Task<IResult<InventorySpecification>> UpdateInventorySpecificationAsync(InventorySpecification specification)
        {
            await _inventorySpecificationRepository.UpdateAsync(specification);

            return _resultFactory.Success(specification);
        }

        public async Task<IResult> DeleteInventoryAttributeValueAsync(int id)
        {
            var specification = await _inventorySpecificationRepository.GetByIdAsync(id);

            if (specification == null)
                return _resultFactory.NotFound();

            await _inventorySpecificationRepository.DeleteAsync(specification);

            return _resultFactory.Success();
        }

        public async Task<IResult<IEnumerable<TranslatedInventorySpecification>>> GetTranslatedInventorySpecificationsAsync(int id, string langCode)
        {
            var specifications = await _inventorySpecificationRepository.GetTranslatedInventorySpecificationsAsync(id, langCode);

            return _resultFactory.Success(specifications);
        }
    }
}