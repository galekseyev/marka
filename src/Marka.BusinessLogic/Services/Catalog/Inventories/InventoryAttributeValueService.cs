﻿using Marka.BusinessLogic.Interfaces.Catalog.Inventories;
using Marka.Common.Models.Catalog.Inventories;
using Marka.Common.Results;
using Marka.Common.Results.Interfaces;
using Marka.DataLayer.Interfaces.Repositories.Catalog.Inventories;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Marka.BusinessLogic.Services.Catalog.Inventories
{
    public class InventoryAttributeValueService : IInventoryAttributeValueService
    {
        private readonly IResultFactory _resultFactory;
        private readonly IInventoryAttributeValueRepository _inventoryAttributeValueRepository;

        public InventoryAttributeValueService(IResultFactory resultFactory, IInventoryAttributeValueRepository inventoryAttributeValueRepository)
        {
            _resultFactory = resultFactory;
            _inventoryAttributeValueRepository = inventoryAttributeValueRepository;
        }


        public async Task<IResult<IEnumerable<InventoryAttributeValue>>> GetInventoryAttributeValuesAsync(int id, int attributeId)
        {
            var values = await _inventoryAttributeValueRepository.GetInventoryAttributeValuesAsync(id, attributeId);

            return _resultFactory.Success(values);
        }

        public async Task<IResult<InventoryAttributeValue>> GetInventoryAttributeValueAsync(int attributeValueId)
        {
            var picture = await _inventoryAttributeValueRepository.GetByIdAsync(attributeValueId);

            if (picture == null)
                return _resultFactory.NotFound<InventoryAttributeValue>();

            return _resultFactory.Success(picture);
        }

        public async Task<IResult<InventoryAttributeValue>> CreateInventoryAttributeValueAsync(InventoryAttributeValue value)
        {
            await _inventoryAttributeValueRepository.AddAsync(value);

            return _resultFactory.Created(value);
        }

        public async Task<IResult<InventoryAttributeValue>> UpdateInventoryAttributeValueAsync(InventoryAttributeValue value)
        {
            await _inventoryAttributeValueRepository.UpdateAsync(value);

            return _resultFactory.Success(value);
        }

        public async Task<IResult> DeleteInventoryAttributeValueAsync(int id)
        {
            var picture = await _inventoryAttributeValueRepository.GetByIdAsync(id);

            if (picture == null)
                return _resultFactory.NotFound();

            await _inventoryAttributeValueRepository.DeleteAsync(picture);

            return _resultFactory.Success();
        }

        public async Task<IResult<IEnumerable<TranslatedInventoryAttributeValue>>> GetTranslatedInventoryAttributeValuesAsync(int id, int attributeId, string langCode)
        {
            var attributeValues = await _inventoryAttributeValueRepository.GetTranslatedInventoryAttributeValuesAsync(id, attributeId, langCode);

            return _resultFactory.Success(attributeValues);
        }
    }
}