﻿using Marka.BusinessLogic.Interfaces;
using Marka.BusinessLogic.Interfaces.Catalog.Inventories;
using Marka.Common.Models.Carts;
using Marka.Common.Models.Catalog.Inventories;
using Marka.Common.Models.Filters;
using Marka.Common.Models.Orders;
using Marka.Common.Results;
using Marka.Common.Results.Interfaces;
using Marka.DataLayer.Interfaces.Repositories.Catalog.Inventories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Marka.BusinessLogic.Services.Catalog.Inventories
{
    public class InventoryService : IInventoryService
    {
        private readonly IResultFactory _resultFactory;
        private readonly IInventoryRepository _inventoryRepository;

        public InventoryService(IResultFactory resultFactory, IInventoryRepository inventoryRepository)
        {
            _resultFactory = resultFactory;
            _inventoryRepository = inventoryRepository;

        }

        public async Task<IResult<Inventory>> GetInventoryAsync(int id)
        {
            var inventory = await _inventoryRepository.GetInventoryAsync(id);

            if (inventory == null)
                return _resultFactory.NotFound<Inventory>();

            return _resultFactory.Success(inventory);
        }

        public async Task<IResult<IEnumerable<Inventory>>> GetInventoriesAsync(IEnumerable<int> ids)
        {
            var inventories = await _inventoryRepository.GetManyAsync(c => ids.Contains(c.Id));

            return _resultFactory.Success(inventories);
        }


        public async Task<IResult<IEnumerable<Inventory>>> GetInventoriesAsync()
        {
            var inventories = await _inventoryRepository.GetInventoriesAsync();

            return _resultFactory.Success(inventories);
        }

        public async Task<IResult<Inventory>> CreateInventoryAsync(Inventory inventory)
        {
            inventory.OriginalPrice = inventory.Price;

            await _inventoryRepository.AddAsync(inventory);

            return _resultFactory.Created(inventory);
        }

        public async Task<IResult<Inventory>> UpdateInventoryAsync(Inventory inventory)
        {
            await _inventoryRepository.UpdateAsync(inventory);

            return _resultFactory.Success(inventory);
        }

        public async Task<IResult> DeleteInventoryAsync(int id)
        {
            var inventory = await _inventoryRepository.GetByIdAsync(id);

            if (inventory == null)
                return _resultFactory.NotFound();

            await _inventoryRepository.DeleteAsync(inventory);

            return _resultFactory.Success();
        }

        public async Task<IResult<TranslatedInventory>> GetTranslatedInventoryAsync(int id, string langCode)
        {
            var inventory = await _inventoryRepository.GetTranslatedInventoryAsync(id, langCode);

            if (inventory == null)
                return _resultFactory.NotFound<TranslatedInventory>();

            return _resultFactory.Success(inventory);
        }

        public async Task<IResult<TranslatedInventories>> GetTranslatedInventoriesAsync(InventoryFilter filter, string langCode)
        {
            var inventories = await _inventoryRepository.GetTranslatedInventoriesAsync(filter, langCode);

            return _resultFactory.Success(inventories);
        }

        public async Task<IResult<IEnumerable<Inventory>>> ReduceInventoryQuantities(Cart cart)
        {
            var groupedItems = cart.CartItems.GroupBy(c => c.InventoryId)
                .Select(x => new
                {
                    x.FirstOrDefault().Id,
                    x.FirstOrDefault().CartId,
                    x.FirstOrDefault().InventoryId,
                    Quantity = x.Sum(c => c.Quantity)
                });

            var itemsToUpdate = new List<Inventory>();

            foreach (var cartItem in groupedItems)
            {
                var inventory = await _inventoryRepository.GetByIdAsync(cartItem.InventoryId);

                if (inventory.Quantity < cartItem.Quantity)
                    return _resultFactory.Error<IEnumerable<Inventory>>("Insuficcent inventory item amount");

                inventory.Quantity -= cartItem.Quantity;
                itemsToUpdate.Add(inventory);
            }

            await _inventoryRepository.UpdateManyAsync(itemsToUpdate);

            return _resultFactory.Success(itemsToUpdate);
        }
    }
}