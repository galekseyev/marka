﻿using Marka.BusinessLogic.Interfaces.Catalog.Inventories;
using Marka.Common.Models.Catalog.Inventories;
using Marka.Common.Results;
using Marka.Common.Results.Interfaces;
using Marka.DataLayer.Interfaces.Repositories.Catalog.Inventories;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Marka.BusinessLogic.Services.Catalog.Inventories
{
    public class InventoryAttributeService : IInventoryAttributeService
    {
        private readonly IResultFactory _resultFactory;
        private readonly IInventoryAttributeRepository _inventoryAttributeRepository;

        public InventoryAttributeService(IResultFactory resultFactory, IInventoryAttributeRepository inventoryAttributeRepository)
        {
            _resultFactory = resultFactory;
            _inventoryAttributeRepository = inventoryAttributeRepository;
        }


        public async Task<IResult<IEnumerable<InventoryAttribute>>> GetInventoryAttributesAsync(int id)
        {
            var attributes = await _inventoryAttributeRepository.GetInventoryAttributesAsync(id);

            return _resultFactory.Success(attributes);
        }

        public async Task<IResult<InventoryAttribute>> GetInventoryAttributeAsync(int id)
        {
            var attribute = await _inventoryAttributeRepository.GetInventoryAttributeAsync(id);

            if (attribute == null)
                return _resultFactory.NotFound<InventoryAttribute>();

            return _resultFactory.Success(attribute);
        }

        public async Task<IResult<InventoryAttribute>> CreateInventoryAttributeAsync(InventoryAttribute attribute)
        {
            await _inventoryAttributeRepository.AddAsync(attribute);

            return _resultFactory.Created(attribute);
        }

        public async Task<IResult<InventoryAttribute>> UpdateInventoryAttributeAsync(InventoryAttribute attribute)
        {
            await _inventoryAttributeRepository.UpdateAsync(attribute);

            return _resultFactory.Success(attribute);
        }

        public async Task<IResult> DeleteInventoryAttributeAsync(int id)
        {
            var attribute = await _inventoryAttributeRepository.GetByIdAsync(id);

            if (attribute == null)
                return _resultFactory.NotFound();

            await _inventoryAttributeRepository.DeleteAsync(attribute);

            return _resultFactory.Success();
        }

        public async Task<IResult<IEnumerable<TranslatedInventoryAttribute>>> GetTranslatedInventoryAttributesAsync(int id, string langCode)
        {
            var attributes = await _inventoryAttributeRepository.GetTranslatedInventoryAttributesAsync(id, langCode);

            return _resultFactory.Success(attributes);
        }
    }
}
