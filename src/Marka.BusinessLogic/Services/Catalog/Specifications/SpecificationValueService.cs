﻿using Marka.BusinessLogic.Interfaces.Catalog.Specifications;
using Marka.Common.Models.Catalog.Specifications;
using Marka.Common.Results;
using Marka.Common.Results.Interfaces;
using Marka.DataLayer.Interfaces.Repositories.Catalog.Specifications;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Marka.BusinessLogic.Services.Catalog.Specifications
{
    public class SpecificationValueService : ISpecificationValueService
    {
        private readonly IResultFactory _resultFactory;
        private readonly ISpecificationValueRepository _specificationValueRepository;

        public SpecificationValueService(IResultFactory resultFactory, ISpecificationValueRepository specificationValueRepository)
        {
            _resultFactory = resultFactory;
            _specificationValueRepository = specificationValueRepository;
        }

        public async Task<IResult<IEnumerable<SpecificationValue>>> GetSpecificationValuesAsync(int id)
        {
            var values = await _specificationValueRepository.GetSpecificationValuesAsync(id);

            return _resultFactory.Success(values);
        }

        public async Task<IResult<SpecificationValue>> GetSpecificationValueAsync(int valueId)
        {
            var picture = await _specificationValueRepository.GetByIdAsync(valueId);

            if (picture == null)
                return _resultFactory.NotFound<SpecificationValue>();

            return _resultFactory.Success(picture);
        }

        public async Task<IResult<SpecificationValue>> CreateSpecificationValueAsync(SpecificationValue value)
        {
            await _specificationValueRepository.AddAsync(value);

            return _resultFactory.Created(value);
        }

        public async Task<IResult<SpecificationValue>> UpdateSpecificationValueAsync(SpecificationValue value)
        {
            await _specificationValueRepository.UpdateAsync(value);

            return _resultFactory.Success(value);
        }

        public async Task<IResult> DeleteSpecificationValueAsync(int id)
        {
            var value = await _specificationValueRepository.GetByIdAsync(id);

            if (value == null)
                return _resultFactory.NotFound();

            await _specificationValueRepository.DeleteAsync(value);

            return _resultFactory.Success();
        }

        public async Task<IResult<IEnumerable<TranslatedSpecificationValue>>> GetTranslatedSpecificationValuesAsync(int id, string langCode)
        {
            var values = await _specificationValueRepository.GetTranslatedSpecificationValuesAsync(id, langCode);

            return _resultFactory.Success(values);
        }
    }
}