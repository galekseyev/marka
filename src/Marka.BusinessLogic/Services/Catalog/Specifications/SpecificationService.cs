﻿using Marka.BusinessLogic.Interfaces.Catalog.Specifications;
using Marka.Common.Models.Catalog.Specifications;
using Marka.Common.Results;
using Marka.Common.Results.Interfaces;
using Marka.DataLayer.Interfaces.Repositories.Catalog.Specifications;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Marka.BusinessLogic.Services.Catalog.Specifications
{
    public class SpecificationService : ISpecificationService
    {
        private readonly IResultFactory _resultFactory;
        private readonly ISpecificationRepository _specificationRepository;

        public SpecificationService(IResultFactory resultFactory, ISpecificationRepository specificationRepository)
        {
            _resultFactory = resultFactory;

            _specificationRepository = specificationRepository;
        }

        public async Task<IResult<Specification>> GetSpecificationAsync(int id)
        {
            var specification = await _specificationRepository.GetSpecificationAsync(id);

            if (specification == null)
                return _resultFactory.NotFound<Specification>();

            return _resultFactory.Success(specification);
        }

        public async Task<IResult<IEnumerable<Specification>>> GetSpecificationsAsync()
        {
            var specifications = await _specificationRepository.GetSpecificationsAsync();

            return _resultFactory.Success(specifications);
        }

        public async Task<IResult<Specification>> CreateSpecificationAsync(Specification specification)
        {
            await _specificationRepository.AddAsync(specification);

            return _resultFactory.Created(specification);
        }

        public async Task<IResult<Specification>> UpdateSpecificationAsync(Specification specification)
        {
            await _specificationRepository.UpdateAsync(specification);

            return _resultFactory.Success(specification);
        }

        public async Task<IResult> DeleteSpecificationAsync(int id)
        {
            var specification = await _specificationRepository.GetByIdAsync(id);

            if (specification == null)
                return _resultFactory.NotFound();

            await _specificationRepository.DeleteAsync(specification);

            return _resultFactory.Success();
        }

        public async Task<IResult<TranslatedSpecification>> GetTranslatedSpecificationAsync(int id, string langCode)
        {
            var specification = await _specificationRepository.GetTranslatedSpecificationAsync(id, langCode);

            if (specification == null)
                return _resultFactory.NotFound<TranslatedSpecification>();

            return _resultFactory.Success(specification);
        }

        public async Task<IResult<IEnumerable<TranslatedSpecification>>> GetTranslatedSpecificationsAsync(string langCode)
        {
            var specifications = await _specificationRepository.GetTranslatedSpecificationsAsync(langCode);

            return _resultFactory.Success(specifications);
        }
    }
}
