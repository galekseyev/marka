﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Marka.BusinessLogic.Interfaces.Catalog.Attributes;
using Marka.Common.Models.Catalog.Attributes;
using Marka.Common.Results;
using Marka.Common.Results.Interfaces;
using Marka.DataLayer.Interfaces.Repositories.Catalog.Attributes;

namespace Marka.BusinessLogic.Services.Catalog.Attributes
{
    public class AttributeService : IAttributeService
    {
        private readonly IResultFactory _resultFactory;
        private readonly IAttributeRepository _attributeRepository;

        public AttributeService(IResultFactory resultFactory, IAttributeRepository attributeRepository)
        {
            _resultFactory = resultFactory;

            _attributeRepository = attributeRepository;
        }

        public async Task<IResult<Attribute>> GetAttributeAsync(int id)
        {
            var attribute = await _attributeRepository.GetAttributeAsync(id);

            if (attribute == null)
                return _resultFactory.NotFound<Attribute>();

            return _resultFactory.Success(attribute);
        }

        public async Task<IResult<IEnumerable<Attribute>>> GetAttributesAsync()
        {
            var categories = await _attributeRepository.GetAttributesAsync();

            return _resultFactory.Success(categories);
        }

        public async Task<IResult<Attribute>> CreateAttributeAsync(Attribute attribute)
        {
            await _attributeRepository.AddAsync(attribute);

            return _resultFactory.Created(attribute);
        }

        public async Task<IResult<Attribute>> UpdateAttributeAsync(Attribute attribute)
        {
            await _attributeRepository.UpdateAsync(attribute);

            return _resultFactory.Success(attribute);
        }

        public async Task<IResult> DeleteAttributeAsync(int id)
        {
            var attribute = await _attributeRepository.GetByIdAsync(id);

            if (attribute == null)
                return _resultFactory.NotFound();

            await _attributeRepository.DeleteAsync(attribute);

            return _resultFactory.Success();
        }

        public async Task<IResult<TranslatedAttribute>> GetTranslatedAttributeAsync(int id, string langCode)
        {
            var attribute = await _attributeRepository.GetTranslatedAttributeAsync(id, langCode);

            if (attribute == null)
                return _resultFactory.NotFound<TranslatedAttribute>();

            return _resultFactory.Success(attribute);
        }

        public async Task<IResult<IEnumerable<TranslatedAttribute>>> GetTranslatedAttributesAsync(string langCode)
        {
            var attributes = await _attributeRepository.GetTranslatedAttributesAsync(langCode);

            return _resultFactory.Success(attributes);
        }
    }
}