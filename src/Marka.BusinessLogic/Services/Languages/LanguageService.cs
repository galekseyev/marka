﻿using Marka.BusinessLogic.Interfaces.Languages;
using Marka.Common.Models.Languages;
using Marka.Common.Results;
using Marka.Common.Results.Interfaces;
using Marka.DataLayer.Interfaces.Repositories.Languages;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Marka.BusinessLogic.Services.Languages
{
    public class LanguageService : ILanguageService
    {
        private readonly IResultFactory _resultFactory;
        private readonly ILanguageRepository _languageRepository;

        public LanguageService(IResultFactory resultFactory, ILanguageRepository languageRepository)
        {
            _resultFactory = resultFactory;
            _languageRepository = languageRepository;
        }

        public async Task<IResult<Language>> GetLanguageByCodeAsync(string code)
        {
            var language = await _languageRepository.GetFirstAsync(c => c.LanguageCode == code);

            if (language == null)
                return _resultFactory.NotFound<Language>();

            return _resultFactory.Success(language);
        }

        public async Task<IResult<Language>> GetLanguageAsync(int id)
        {
            var language = await _languageRepository.GetByIdAsync(id);

            if (language == null)
                return _resultFactory.NotFound<Language>();

            return _resultFactory.Success(language);
        }

        public async Task<IResult<IEnumerable<Language>>> GetLanguagesAsync()
        {
            var languages = await _languageRepository.GetAllAsync();

            return _resultFactory.Success(languages);
        }

        public async Task<IResult<Language>> CreateLanguageAsync(Language language)
        {
            await _languageRepository.AddAsync(language);

            return _resultFactory.Created(language);
        }

        public async Task<IResult<Language>> UpdateLanguageAsync(Language language)
        {
            await _languageRepository.UpdateAsync(language);

            return _resultFactory.Success(language);
        }

        public async Task<IResult> DeleteLanguageAsync(int id)
        {
            var language = await _languageRepository.GetByIdAsync(id);

            if (language == null)
                return _resultFactory.NotFound();

            await _languageRepository.DeleteAsync(language);

            return _resultFactory.Success();
        }
    }
}