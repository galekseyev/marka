﻿using Marka.BusinessLogic.Interfaces.Orders;
using Marka.Common.Models.Orders;
using Marka.Common.Results;
using Marka.Common.Results.Interfaces;
using Marka.DataLayer.Interfaces.Repositories.Orders;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Marka.BusinessLogic.Services.Orders
{
    public class OrderStatusService : IOrderStatusService
    {
        private readonly IResultFactory _resultFactory;
        private readonly IOrderStatusRepository _orderStatusRepository;

        public OrderStatusService(IResultFactory resultFactory, IOrderStatusRepository orderStatusRepository)
        {
            _resultFactory = resultFactory;
            _orderStatusRepository = orderStatusRepository;
        }

        public async Task<IResult<ICollection<OrderStatus>>> CreateOrderStatusesAsync(ICollection<OrderStatus> orderStatuses)
        {
            await _orderStatusRepository.AddRangeAsync(orderStatuses);

            return _resultFactory.Created(orderStatuses);
        }

        public async Task<IResult<IEnumerable<OrderStatus>>> GetOrderStatusesAsync()
        {
            var statuses = await _orderStatusRepository.GetAllAsync();

            return _resultFactory.Created(statuses);
        }

        public async Task<IResult<OrderStatus>> GetOrderStatusAsync(int id)
        {
            var status = await _orderStatusRepository.GetFirstAsync(c => c.Id == id);

            if (status == null)
                return _resultFactory.NotFound<OrderStatus>();

            return _resultFactory.Success(status);
        }

        public async Task<IResult<OrderStatus>> GetOrderStatusByCodeAsync(string code)
        {
            var status = await _orderStatusRepository.GetFirstAsync(c => c.StatusCode == code);

            if (status == null)
                return _resultFactory.NotFound<OrderStatus>();

            return _resultFactory.Success(status);
        }
    }
}
