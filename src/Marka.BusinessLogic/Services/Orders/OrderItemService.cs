﻿//using Marka.BusinessLogic.Interfaces.Orders;
//using Marka.Common.Models.Orders;
//using Marka.Common.Results;
//using Marka.Common.Results.Interfaces;
//using Marka.DataLayer.Interfaces.Repositories.Orders;
//using System.Collections.Generic;
//using System.Threading.Tasks;

//namespace Marka.BusinessLogic.Services.Orders
//{
//    public class OrderItemService : IOrderItemService
//    {
//        private readonly IResultFactory _resultFactory;
//        private readonly IOrderItemRepository _orderItemRepository;

//        public OrderItemService(IResultFactory resultFactory, IOrderItemRepository orderItemRepository)
//        {
//            _resultFactory = resultFactory;
//            _orderItemRepository = orderItemRepository;
//        }

//        public async Task<IResult<OrderItem>> GetOrderItemAsync(int id)
//        {
//            var orderItem = await _orderItemRepository.GetByIdAsync(id);

//            if (orderItem == null)
//                return _resultFactory.NotFound<OrderItem>();

//            return _resultFactory.Success(orderItem);
//        }

//        public async Task<IResult<OrderItem>> CreateOrderItemAsync(OrderItem orderItem)
//        {
//            await _orderItemRepository.AddAsync(orderItem);

//            return _resultFactory.Created(orderItem);
//        }

//        public async Task<IResult<ICollection<OrderItem>>> CreateOrderItemsAsync(ICollection<OrderItem> orderItems)
//        {
//            await _orderItemRepository.AddRangeAsync(orderItems);

//            return _resultFactory.Created(orderItems);
//        }

//        public async Task<IResult<OrderItem>> UpdateOrderItemAsync(OrderItem orderItem)
//        {
//            await _orderItemRepository.UpdateAsync(orderItem);

//            return _resultFactory.Success(orderItem);
//        }

//        public async Task<IResult> DeleteOrderItemAsync(int id)
//        {
//            var orderItem = await _orderItemRepository.GetByIdAsync(id);

//            if (orderItem == null)
//                return _resultFactory.NotFound();

//            await _orderItemRepository.DeleteAsync(orderItem);

//            return _resultFactory.Success();
//        }
//    }
//}