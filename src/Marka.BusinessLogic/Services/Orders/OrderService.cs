﻿using Marka.BusinessLogic.Interfaces.Orders;
using Marka.Common.Models.Orders;
using Marka.Common.Results;
using Marka.Common.Results.Interfaces;
using Marka.DataLayer.Interfaces.Repositories.Orders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Marka.BusinessLogic.Services.Orders
{
    public class OrderService : IOrderService
    {
        private readonly IResultFactory _resultFactory;
        private readonly IOrderRepository _orderRepository;

        public OrderService(IResultFactory resultFactory, IOrderRepository orderRepository)
        {
            _resultFactory = resultFactory;
            _orderRepository = orderRepository;
        }



        public async Task<IResult<IEnumerable<Order>>> GetOrdersAsync()
        {
            var orders = await _orderRepository.GetOrdersAsync();

            return _resultFactory.Success(orders);
        }

        public async Task<IResult<Order>> GetOrderAsync(int id)
        {
            var order = await _orderRepository.GetOrderAsync(id);

            if (order == null)
                return _resultFactory.NotFound<Order>();

            return _resultFactory.Success(order);
        }

        public async Task<IResult<IEnumerable<Order>>> GetUserOrdersAsync(int userId)
        {
            var orders = await _orderRepository.GetUserOrdersAsync(userId);

            return _resultFactory.Success(orders);
        }

        public async Task<IResult<Order>> CreateOrderAsync(Order order)
        {
            order.CreateDate = DateTime.UtcNow;

            order.SubTotalPrice = order.OrderItems.Sum(c => c.Price * c.Quantity);
            order.TotalPrice = order.OrderItems.Sum(c => c.Price * c.Quantity) + order.ShippingPrice;

            await _orderRepository.AddAsync(order);

            order.OrderNumber = GenerateOrderNumber(order.Id);

            await _orderRepository.UpdateAsync(order);
            return _resultFactory.Created(order);
        }

        public async Task<IResult<Order>> UpdateOrderAsync(Order order)
        {
            order.UpdateDate = DateTime.UtcNow;

            await _orderRepository.UpdateAsync(order);

            return _resultFactory.Success(order);
        }

        public async Task<IResult> DeleteOrderAsync(int id)
        {
            var order = await _orderRepository.GetByIdAsync(id);

            if (order == null)
                return _resultFactory.NotFound();

            await _orderRepository.DeleteAsync(order);

            return _resultFactory.Success();
        }

        public async Task<IResult<TranslatedOrder>> GetTranslatedOrderAsync(int id, string langCode)
        {
            var order = await _orderRepository.GetTranslatedOrderAsync(id, langCode);

            if (order == null)
                return _resultFactory.NotFound<TranslatedOrder>();

            return _resultFactory.Success(order);
        }

        public async Task<IResult<TranslatedOrder>> GetUserTranslatedOrderAsync(int id, int userId, string langCode)
        {
            var order = await _orderRepository.GetUserTranslatedOrderAsync(id, userId, langCode);

            if (order == null)
                return _resultFactory.NotFound<TranslatedOrder>();

            return _resultFactory.Success(order);
        }

        private string GenerateOrderNumber(int orderId)
        {
            return $"M{DateTime.Now.ToString("ddMMyy")}-{orderId}";
        }
    }
}