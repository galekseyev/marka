﻿using Marka.BusinessLogic.Interfaces.Payments;
using Marka.Common.Models.Payments;
using Marka.Common.Results;
using Marka.Common.Results.Interfaces;
using Marka.DataLayer.Interfaces.Repositories.Payments;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Marka.BusinessLogic.Services.Payments
{
    public class PaymentStatusService : IPaymentStatusService
    {
        private readonly IResultFactory _resultFactory;
        private readonly IPaymentStatusRepository _paymentStatusRepository;

        public PaymentStatusService(IResultFactory resultFactory, IPaymentStatusRepository paymentStatusRepository)
        {
            _resultFactory = resultFactory;
            _paymentStatusRepository = paymentStatusRepository;
        }

        public async Task<IResult<ICollection<PaymentStatus>>> CreatePaymentStatusesAsync(ICollection<PaymentStatus> paymentStatuses)
        {
            await _paymentStatusRepository.AddRangeAsync(paymentStatuses);

            return _resultFactory.Created(paymentStatuses);
        }

        public async Task<IResult<IEnumerable<PaymentStatus>>> GetPaymentStatusesAsync()
        {
            var statuses = await _paymentStatusRepository.GetAllAsync();

            return _resultFactory.Success(statuses);
        }

        public async Task<IResult<PaymentStatus>> GetPaymentStatusByCodeAsync(string code)
        {
            var status = await _paymentStatusRepository.GetFirstAsync(c => c.StatusCode == code);

            if (status == null)
                return _resultFactory.NotFound<PaymentStatus>();

            return _resultFactory.Success(status);
        }
    }
}