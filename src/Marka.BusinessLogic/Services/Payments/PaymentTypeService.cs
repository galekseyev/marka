﻿using Marka.BusinessLogic.Interfaces.Payments;
using Marka.Common.Models.Payments;
using Marka.Common.Results;
using Marka.Common.Results.Interfaces;
using Marka.DataLayer.Interfaces.Repositories.Payments;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Marka.BusinessLogic.Services.Payments
{
    public class PaymentTypeService : IPaymentTypeService
    {
        private readonly IResultFactory _resultFactory;
        private readonly IPaymentTypeRepository _paymentTypeRepository;

        public PaymentTypeService(IResultFactory resultFactory, IPaymentTypeRepository paymentTypeRepository)
        {
            _resultFactory = resultFactory;
            _paymentTypeRepository = paymentTypeRepository;
        }

        public async Task<IResult<ICollection<PaymentType>>> CreatePaymentTypesAsync(ICollection<PaymentType> paymentTypes)
        {
            await _paymentTypeRepository.AddRangeAsync(paymentTypes);

            return _resultFactory.Created(paymentTypes);
        }

        public async Task<IResult<IEnumerable<PaymentType>>> GetPaymentTypesAsync()
        {
            var types = await _paymentTypeRepository.GetAllAsync();

            return _resultFactory.Success(types);
        }

        public async Task<IResult<PaymentType>> GetPaymentTypeByCodeAsync(string code)
        {
            var type = await _paymentTypeRepository.GetFirstAsync(c => c.TypeCode == code);

            if (type == null)
                return _resultFactory.NotFound<PaymentType>();

            return _resultFactory.Success(type);
        }
    }
}
