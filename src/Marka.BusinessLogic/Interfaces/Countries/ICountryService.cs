﻿using Marka.Common.Models.Countries;
using Marka.Common.Results.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Marka.BusinessLogic.Interfaces.Countries
{
    public interface ICountryService
    {
        Task<IResult<IEnumerable<Country>>> CreateCountriesAsync(IEnumerable<Country> countries);
        Task<IResult<IEnumerable<Country>>> GetCountriesAsync();
    }
}