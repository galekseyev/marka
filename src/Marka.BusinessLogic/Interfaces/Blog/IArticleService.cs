﻿using Marka.Common.Models.Blog;
using Marka.Common.Results.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Marka.BusinessLogic.Interfaces.Blog
{
    public interface IArticleService
    {
        Task<IResult<Article>> GetArticleAsync(int id);
        Task<IResult<IEnumerable<Article>>> GetArticlesAsync();
        Task<IResult<Article>> CreateArticleAsync(Article category);
        Task<IResult<Article>> UpdateArticleAsync(Article category);
        Task<IResult> DeleteArticleAsync(int id);

        Task<IResult<IEnumerable<TranslatedArticle>>> GetTranslatedArticlesAsync(string langCode);
        Task<IResult<TranslatedArticle>> GetTranslatedArticleAsync(int id, string langCode);
    }
}
