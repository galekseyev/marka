﻿using Marka.Common.Models.Blog;
using Marka.Common.Results.Interfaces;
using System.Threading.Tasks;

namespace Marka.BusinessLogic.Interfaces.Blog
{
    public interface IArticlePictureService
    {
        Task<IResult<ArticlePicture>> GetArticlePictureAsync(int id);

        Task<IResult<ArticlePicture>> GetPictureAsync(int id);
        Task<IResult<ArticlePicture>> CreatePictureAsync(ArticlePicture picture);
        Task<IResult<ArticlePicture>> UpdatePictureAsync(ArticlePicture picture);
        Task<IResult> DeletePictureAsync(int id);
    }
}