﻿using Marka.Common.Models.Languages;
using Marka.Common.Results.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Marka.BusinessLogic.Interfaces.Languages
{
    public interface ILanguageService
    {
        Task<IResult<Language>> GetLanguageAsync(int id);
        Task<IResult<Language>> GetLanguageByCodeAsync(string code);

        Task<IResult<IEnumerable<Language>>> GetLanguagesAsync();

        Task<IResult<Language>> CreateLanguageAsync(Language language);
        Task<IResult<Language>> UpdateLanguageAsync(Language language);

        Task<IResult> DeleteLanguageAsync(int id);
    }
}