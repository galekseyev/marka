﻿using Marka.Common.Models.Orders;
using Marka.Common.Results.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Marka.BusinessLogic.Interfaces.Orders
{
    public interface IOrderStatusService
    {
        Task<IResult<ICollection<OrderStatus>>> CreateOrderStatusesAsync(ICollection<OrderStatus> orderStatuses);
        Task<IResult<IEnumerable<OrderStatus>>> GetOrderStatusesAsync();
        Task<IResult<OrderStatus>> GetOrderStatusAsync(int id);
        Task<IResult<OrderStatus>> GetOrderStatusByCodeAsync(string code);
    }
}