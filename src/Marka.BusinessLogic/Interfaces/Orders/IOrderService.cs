﻿using Marka.Common.Models.Orders;
using Marka.Common.Results.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Marka.BusinessLogic.Interfaces.Orders
{
    public interface IOrderService
    {
        Task<IResult<Order>> GetOrderAsync(int id);
        Task<IResult<IEnumerable<Order>>> GetOrdersAsync();
        Task<IResult<IEnumerable<Order>>> GetUserOrdersAsync(int userId);

        Task<IResult<Order>> CreateOrderAsync(Order order);
        Task<IResult<Order>> UpdateOrderAsync(Order order);
        Task<IResult> DeleteOrderAsync(int id);

        Task<IResult<TranslatedOrder>> GetTranslatedOrderAsync(int id, string langCode);
        Task<IResult<TranslatedOrder>> GetUserTranslatedOrderAsync(int id, int userId, string langCode);
    }
}