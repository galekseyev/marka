﻿using Marka.Common.Models.Orders;
using Marka.Common.Results.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Marka.BusinessLogic.Interfaces.Orders
{
    public interface IOrderItemService
    {
        Task<IResult<OrderItem>> GetOrderItemAsync(int id);
        Task<IResult<OrderItem>> CreateOrderItemAsync(OrderItem item);
        Task<IResult<ICollection<OrderItem>>> CreateOrderItemsAsync(ICollection<OrderItem> orderItems);
        Task<IResult<OrderItem>> UpdateOrderItemAsync(OrderItem item);
        Task<IResult> DeleteOrderItemAsync(int id);
    }
}
