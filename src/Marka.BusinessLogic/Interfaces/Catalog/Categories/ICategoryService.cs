﻿using Marka.Common.Models.Catalog.Categories;
using Marka.Common.Results.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Marka.BusinessLogic.Interfaces.Catalog.Categories
{
    public interface ICategoryService
    {
        Task<IResult<Category>> GetCategoryAsync(int id);
        Task<IResult<IEnumerable<Category>>> GetCategoriesAsync();
        Task<IResult<Category>> CreateCategoryAsync(Category category);
        Task<IResult<Category>> UpdateCategoryAsync(Category category);
        Task<IResult> DeleteCategoryAsync(int id);

        Task<IResult<IEnumerable<TranslatedCategory>>> GetTranslatedCategoriesAsync(string langCode);
        Task<IResult<TranslatedCategory>> GetTranslatedCategoryAsync(int id, string langCode);
    }
}