﻿using Marka.Common.Models.Catalog.Categories;
using Marka.Common.Results.Interfaces;
using System.Threading.Tasks;

namespace Marka.BusinessLogic.Interfaces.Catalog.Categories
{
    public interface ICategoryPictureService
    {
        Task<IResult<CategoryPicture>> GetCategoryPictureAsync(int id);

        Task<IResult<CategoryPicture>> GetPictureAsync(int id);
        Task<IResult<CategoryPicture>> CreatePictureAsync(CategoryPicture picture);
        Task<IResult<CategoryPicture>> UpdatePictureAsync(CategoryPicture picture);
        Task<IResult> DeletePictureAsync(int id);
    }
}