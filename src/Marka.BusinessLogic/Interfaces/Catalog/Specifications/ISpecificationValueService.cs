﻿using Marka.Common.Models.Catalog.Specifications;
using Marka.Common.Results.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Marka.BusinessLogic.Interfaces.Catalog.Specifications
{
    public interface ISpecificationValueService
    {
        Task<IResult<IEnumerable<SpecificationValue>>> GetSpecificationValuesAsync(int id);
        Task<IResult<SpecificationValue>> GetSpecificationValueAsync(int valueId);
        Task<IResult<SpecificationValue>> CreateSpecificationValueAsync(SpecificationValue specificationValue);
        Task<IResult<SpecificationValue>> UpdateSpecificationValueAsync(SpecificationValue specificationValue);
        Task<IResult> DeleteSpecificationValueAsync(int valueId);

        Task<IResult<IEnumerable<TranslatedSpecificationValue>>> GetTranslatedSpecificationValuesAsync(int id, string langCode);
    }
}