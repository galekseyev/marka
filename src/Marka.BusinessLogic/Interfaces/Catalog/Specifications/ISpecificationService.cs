﻿using Marka.Common.Models.Catalog.Specifications;
using Marka.Common.Results.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Marka.BusinessLogic.Interfaces.Catalog.Specifications
{
    public interface ISpecificationService
    {
        Task<IResult<Specification>> GetSpecificationAsync(int id);
        Task<IResult<IEnumerable<Specification>>> GetSpecificationsAsync();
        Task<IResult<Specification>> CreateSpecificationAsync(Specification specification);
        Task<IResult<Specification>> UpdateSpecificationAsync(Specification specification);
        Task<IResult> DeleteSpecificationAsync(int id);

        Task<IResult<IEnumerable<TranslatedSpecification>>> GetTranslatedSpecificationsAsync(string langCode);
        Task<IResult<TranslatedSpecification>> GetTranslatedSpecificationAsync(int id, string langCode);
    }
}