﻿using Marka.Common.Models.Carts;
using Marka.Common.Models.Catalog.Inventories;
using Marka.Common.Models.Filters;
using Marka.Common.Models.Orders;
using Marka.Common.Results.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Marka.BusinessLogic.Interfaces.Catalog.Inventories
{
    public interface IInventoryService
    {
        Task<IResult<Inventory>> GetInventoryAsync(int id);

        Task<IResult<IEnumerable<Inventory>>> GetInventoriesAsync(IEnumerable<int> ids);
        Task<IResult<IEnumerable<Inventory>>> GetInventoriesAsync();

        Task<IResult<Inventory>> CreateInventoryAsync(Inventory inventory);
        Task<IResult<Inventory>> UpdateInventoryAsync(Inventory inventory);

        Task<IResult> DeleteInventoryAsync(int id);

        Task<IResult<TranslatedInventories>> GetTranslatedInventoriesAsync(InventoryFilter filter, string langCode);
        Task<IResult<TranslatedInventory>> GetTranslatedInventoryAsync(int id, string langCode);
        Task<IResult<IEnumerable<Inventory>>> ReduceInventoryQuantities(Cart cart);
    }
}