﻿using Marka.Common.Models.Catalog.Inventories;
using Marka.Common.Results.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Marka.BusinessLogic.Interfaces.Catalog.Inventories
{
    public interface IInventoryAttributeService
    {
        Task<IResult<InventoryAttribute>> GetInventoryAttributeAsync(int id);
        Task<IResult<IEnumerable<InventoryAttribute>>> GetInventoryAttributesAsync(int id);
        Task<IResult<InventoryAttribute>> CreateInventoryAttributeAsync(InventoryAttribute attribute);
        Task<IResult<InventoryAttribute>> UpdateInventoryAttributeAsync(InventoryAttribute attribute);
        Task<IResult> DeleteInventoryAttributeAsync(int id);

        Task<IResult<IEnumerable<TranslatedInventoryAttribute>>> GetTranslatedInventoryAttributesAsync(int id, string langCode);
    }
}
