﻿using Marka.Common.Models.Catalog.Inventories;
using Marka.Common.Results.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Marka.BusinessLogic.Interfaces.Catalog.Inventories
{
    public interface IInventoryPictureService
    {
        /// <summary>
        /// Get Inventory Pictures By Inventory Id.
        /// </summary>
        /// <param name="id">Inventory Id.</param>
        /// <returns>Collection of Inventory Pictures.</returns>
        Task<IResult<IEnumerable<InventoryPicture>>> GetInventoryPicturesAsync(int id);

        Task<IResult<InventoryPicture>> GetPictureAsync(int id);
        Task<IResult<InventoryPicture>> CreatePictureAsync(InventoryPicture picture);
        Task<IResult<InventoryPicture>> UpdatePictureAsync(InventoryPicture picture);
        Task<IResult> DeletePictureAsync(int id);
    }
}