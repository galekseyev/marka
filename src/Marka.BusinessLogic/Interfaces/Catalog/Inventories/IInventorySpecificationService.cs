﻿using Marka.Common.Models.Catalog.Inventories;
using Marka.Common.Results.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Marka.BusinessLogic.Interfaces.Catalog.Inventories
{
    public interface IInventorySpecificationService
    {
        Task<IResult<InventorySpecification>> GetInventorySpecificationAsync(int id, int specificationId);
        Task<IResult<InventorySpecification>> CreateInventorySpecificationAsync(InventorySpecification specification);
        Task<IResult<InventorySpecification>> UpdateInventorySpecificationAsync(InventorySpecification specification);
        Task<IResult> DeleteInventoryAttributeValueAsync(int id);
        Task<IResult<IEnumerable<TranslatedInventorySpecification>>> GetTranslatedInventorySpecificationsAsync(int id, string langCode);
    }
}