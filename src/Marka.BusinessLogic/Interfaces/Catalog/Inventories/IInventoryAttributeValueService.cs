﻿using Marka.Common.Models.Catalog.Inventories;
using Marka.Common.Results.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Marka.BusinessLogic.Interfaces.Catalog.Inventories
{
    public interface IInventoryAttributeValueService
    {
        Task<IResult<IEnumerable<InventoryAttributeValue>>> GetInventoryAttributeValuesAsync(int id, int attributeId);
        Task<IResult<InventoryAttributeValue>> GetInventoryAttributeValueAsync(int attributeValueId);
        Task<IResult<InventoryAttributeValue>> CreateInventoryAttributeValueAsync(InventoryAttributeValue attribute);
        Task<IResult<InventoryAttributeValue>> UpdateInventoryAttributeValueAsync(InventoryAttributeValue attribute);
        Task<IResult> DeleteInventoryAttributeValueAsync(int id);

        Task<IResult<IEnumerable<TranslatedInventoryAttributeValue>>> GetTranslatedInventoryAttributeValuesAsync(int id, int attributeId, string langCode);
    }
}