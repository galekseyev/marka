﻿using Marka.Common.Models.Catalog.Attributes;
using Marka.Common.Results.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Marka.BusinessLogic.Interfaces.Catalog.Attributes
{
    public interface IAttributeService
    {
        Task<IResult<Attribute>> GetAttributeAsync(int id);
        Task<IResult<IEnumerable<Attribute>>> GetAttributesAsync();
        Task<IResult<Attribute>> CreateAttributeAsync(Attribute attribute);
        Task<IResult<Attribute>> UpdateAttributeAsync(Attribute attribute);
        Task<IResult> DeleteAttributeAsync(int id);

        Task<IResult<IEnumerable<TranslatedAttribute>>> GetTranslatedAttributesAsync(string langCode);
        Task<IResult<TranslatedAttribute>> GetTranslatedAttributeAsync(int id, string langCode);
    }
}