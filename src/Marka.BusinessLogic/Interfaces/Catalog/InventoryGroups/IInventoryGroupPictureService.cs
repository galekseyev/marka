﻿using Marka.Common.Models.Catalog.InventoryGroups;
using Marka.Common.Results.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Marka.BusinessLogic.Interfaces.Catalog.InventoryGroups
{
    public interface IInventoryGroupPictureService
    {
        Task<IResult<InventoryGroupPicture>> GetInventoryGroupPictureAsync(int id);
        Task<IResult<InventoryGroupPicture>> CreateInventoryGroupPictureAsync(InventoryGroupPicture picture);
        Task<IResult<InventoryGroupPicture>> UpdateInventoryGroupPictureAsync(InventoryGroupPicture picture);
        Task<IResult> DeleteInventoryGroupPictureAsync(int id);
    }
}