﻿using Marka.Common.Models.Catalog.InventoryGroups;
using Marka.Common.Models.Filters;
using Marka.Common.Results.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Marka.BusinessLogic.Interfaces.Catalog.InventoryGroups
{
    public interface IInventoryGroupService
    {
        Task<IResult<InventoryGroup>> GetInventoryGroupAsync(int id);
        Task<IResult<FilteredInventoryGroups>> GetInventoryGroupsAsync(InventoryGroupFilter filter);
        Task<IResult<InventoryGroup>> CreateInventoryGroupAsync(InventoryGroup inventory);
        Task<IResult<InventoryGroup>> UpdateInventoryGroupAsync(InventoryGroup inventory);
        Task<IResult> DeleteInventoryGroupAsync(int id);

        Task<IResult<TranslatedInventoryGroup>> GetTranslatedInventoryGroup(int id, string langCode);
    }
}
