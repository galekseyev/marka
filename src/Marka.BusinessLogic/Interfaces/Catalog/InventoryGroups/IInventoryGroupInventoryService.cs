﻿using Marka.Common.Models.Catalog.InventoryGroups;
using Marka.Common.Results.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Marka.BusinessLogic.Interfaces.Catalog.InventoryGroups
{
    public interface IInventoryGroupInventoryService
    {
        Task<IResult<InventoryGroupInventory>> GetInventoryGroupInventoryAsync(int id);
        Task<IResult<IEnumerable<InventoryGroupInventory>>> GetInventoryGroupInventoriesAsync(int id);
        Task<IResult<InventoryGroupInventory>> CreateInventoryGroupInventoryAsync(InventoryGroupInventory inventoryGroupInventory);
        Task<IResult<InventoryGroupInventory>> UpdateInventoryGroupInventoryAsync(InventoryGroupInventory inventoryGroupInventory);
        Task<IResult> DeleteInventoryGroupInventoryAsync(int id);

        Task<IResult<IEnumerable<TranslatedInventoryGroupInventory>>> GetTranslatedInventoryGroupInventoriesAsync(int id, string langCode);
    }
}