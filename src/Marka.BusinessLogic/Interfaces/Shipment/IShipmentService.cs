﻿using System.Threading.Tasks;
using Marka.Common.Models.Shipments;
using Marka.Common.Results.Interfaces;

namespace Marka.BusinessLogic.Interfaces.Orders
{
    public interface IShipmentService
    {
        Task<IResult<Shipment>> GetShipmentAsync(int id);
        Task<IResult<Shipment>> CreateShipmentAsync(Shipment shipping);
        Task<IResult<Shipment>> UpdateShipmentAsync(Shipment shipping);
        Task<IResult> DeleteShipmentAsync(int id);
    }
}