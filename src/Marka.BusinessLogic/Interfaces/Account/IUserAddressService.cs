﻿using Marka.Common.Models.Account.Users;
using Marka.Common.Results.Interfaces;
using System.Threading.Tasks;

namespace Marka.BusinessLogic.Interfaces.Account
{
    public interface IUserAddressService
    {
        Task<IResult<UserAddress>> GetUserAddressAsync(int userId);
        Task<IResult<UserAddress>> CreateUserAddressAsync(UserAddress rate);
        Task<IResult<UserAddress>> UpdateUserAddressAsync(UserAddress rate);
        Task<IResult> DeleteUserAddressAsync(int id);
    }
}