﻿using Marka.Common.Models.Payments;
using Marka.Common.Results.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Marka.BusinessLogic.Interfaces.Payments
{
    public interface IPaymentTypeService
    {
        Task<IResult<ICollection<PaymentType>>> CreatePaymentTypesAsync(ICollection<PaymentType> paymentTypes);
        Task<IResult<IEnumerable<PaymentType>>> GetPaymentTypesAsync();
        Task<IResult<PaymentType>> GetPaymentTypeByCodeAsync(string code);
    }
}