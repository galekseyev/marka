﻿using Marka.Common.Models.Payments;
using Marka.Common.Results.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Marka.BusinessLogic.Interfaces.Payments
{
    public interface IPaymentStatusService
    {
        Task<IResult<ICollection<PaymentStatus>>> CreatePaymentStatusesAsync(ICollection<PaymentStatus> paymentStatuses);
        Task<IResult<IEnumerable<PaymentStatus>>> GetPaymentStatusesAsync();
        Task<IResult<PaymentStatus>> GetPaymentStatusByCodeAsync(string code);
    }
}