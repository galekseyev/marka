﻿using Marka.Common.Models.Shippings;
using Marka.Common.Results.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Marka.BusinessLogic.Interfaces.Shippings
{
    public interface IShippingMethodService
    {
        Task<IResult<ShippingMethod>> GetShippingMethodAsync(int id);
        Task<IResult<IEnumerable<ShippingMethod>>> GetShippingMethodsAsync();
        Task<IResult<ShippingMethod>> CreateShippingMethodAsync(ShippingMethod method);
        Task<IResult<ShippingMethod>> UpdateShippingMethodAsync(ShippingMethod method);
        Task<IResult> DeleteShippingMethodAsync(int id);
    }
}