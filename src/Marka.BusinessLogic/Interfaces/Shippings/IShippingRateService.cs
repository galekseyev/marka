﻿using Marka.Common.Models.Shippings;
using Marka.Common.Results.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Marka.BusinessLogic.Interfaces.Shippings
{
    public interface IShippingRateService
    {
        Task<IResult<ShippingRate>> GetShippingRateAsync(int id);
        Task<IResult<IEnumerable<ShippingRate>>> GetShippingRatesAsync(int id);
        Task<IResult<ShippingRate>> CreateShippingRateAsync(ShippingRate rate);
        Task<IResult<ShippingRate>> UpdateShippingRateAsync(ShippingRate rate);
        Task<IResult> DeleteShippingRateAsync(int id);
    }
}