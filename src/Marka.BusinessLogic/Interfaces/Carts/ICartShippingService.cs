﻿using Marka.Common.Models.Carts;
using Marka.Common.Results.Interfaces;
using System.Threading.Tasks;

namespace Marka.BusinessLogic.Interfaces.Carts
{
    public interface ICartShippingService
    {
        Task<IResult<CartShipping>> GetCartShippingAsync(int id);
        Task<IResult<CartShipping>> CreateCartShippingAsync(CartShipping cartShipping);
        Task<IResult<CartShipping>> UpdateCartShippingAsync(CartShipping cartShipping);
        Task<IResult> DeleteCartShippingAsync(int id);
    }
}
