﻿using Marka.Common.Models.Carts;
using Marka.Common.Results.Interfaces;
using System.Threading.Tasks;

namespace Marka.BusinessLogic.Interfaces
{
    public interface ICartService
    {
        Task<IResult<Cart>> GetCartAsync(string id);
        Task<IResult<Cart>> GetUserCartAsync(int id);

        /// <summary>
        /// Create Cart
        /// </summary>
        /// <param name="cart">Cart.</param>
        /// <returns>Result of cart.</returns>
        Task<IResult<Cart>> CreateCartAsync(Cart cart);

        /// <summary>
        /// Update Cart
        /// </summary>
        /// <param name="cart">Cart.</param>
        /// <returns>Result of cart.</returns>
        Task<IResult<Cart>> UpdateCartAsync(Cart cart);

        /// <summary>
        /// Delete Cart
        /// </summary>
        /// <param name="id">Cart Id.</param>
        /// <returns>Result.</returns>
        Task<IResult> DeleteCartAsync(string id);

        Task<IResult<TranslatedCart>> GetTranslatedCartAsync(string id, int? userId, string langCode);

        /// <summary>
        /// Add Item To Cart
        /// </summary>
        /// <param name="id">Cart Id.</param>
        /// <param name="item">Cart Item.</param>
        /// <param name="createIfMissing">Create Cart If Missing.</param>
        /// <returns>Result of cart.</returns>
        Task<IResult<Cart>> AddItemAsync(string id, CartItem item, bool createIfMissing = false);

        /// <summary>
        /// Update Item Quantity
        /// </summary>
        /// <param name="id"></param>
        /// <param name="itemId"></param>
        /// <param name="quantity"></param>
        /// <returns></returns>
        Task<IResult<Cart>> UpdateItemQuantityAsync(string id, int itemId, int quantity);

        /// <summary>
        /// Delete Item From Cart
        /// </summary>
        /// <param name="id">Cart Id.</param>
        /// <param name="itemId">Cart Item Id.</param>
        /// <returns>Result.</returns>
        Task<IResult<Cart>> DeleteItemAsync(string id, int itemId);
    }
}