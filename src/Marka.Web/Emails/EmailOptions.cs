﻿namespace Marka.Web.Emails
{
    public class EmailOptions
    {
        public string Domain { get; set; }
        public string ApiKey { get; set; }
        public string ApiBaseUri { get; set; }
        public string RequestUri { get; set; }

        public string FromEmail { get; set; }
        public string FromDisplay { get; set; }

        public string RecipientEmail { get; set; }
    }
}