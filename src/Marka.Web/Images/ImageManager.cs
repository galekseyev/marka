﻿using Amazon.S3;
using Amazon.S3.Model;
using Marka.Web.Options;
using Microsoft.Extensions.Options;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Marka.Web.Images
{
    public interface IImageManager
    {
        Task<string> UploadToS3Async(string path, string title, string image, bool isThumbnail = false);
    }
    public class ImageManager : IImageManager
    {
        private const string Base64StringCode = "data:image/";

        private readonly ImageOptions _options;
        private readonly AwsOptions _awsOptions;

        public ImageManager(IOptions<ImageOptions> options, IOptions<AwsOptions> awsOptions)
        {
            _options = options.Value;
            _awsOptions = awsOptions.Value;
        }

        public async Task<string> UploadToS3Async(string path, string title, string image, bool isThumbnail = false)
        {
            if (string.IsNullOrEmpty(path))
                throw new ArgumentNullException(nameof(path));

            if (string.IsNullOrEmpty(title))
                throw new ArgumentNullException(nameof(title));

            if (string.IsNullOrEmpty(image))
                throw new ArgumentNullException(nameof(image));

            if (!image.StartsWith(Base64StringCode))
                return image;

            path = $"{_options.ImageRootPath}/{path}";

            if (isThumbnail)
                path = $"{path}/thumb";

            path = $"{path}/{title.Replace(" ", string.Empty)}.jpeg";

            var imageString = image.Split(',').Last();
            var imageBytes = Convert.FromBase64String(imageString);

            var ClientConfig = new AmazonS3Config()
            {
                ServiceURL = _awsOptions.ServiceURL,
            };

            var s3Client = new AmazonS3Client(_awsOptions.AccessKey, _awsOptions.SecretKey, ClientConfig);

            var stream = new MemoryStream(imageBytes);

            var request = new PutObjectRequest
            {
                BucketName = _awsOptions.BucketName,
                Key = path,
                InputStream = stream,
                CannedACL = S3CannedACL.PublicRead
            };

            var response = await s3Client.PutObjectAsync(request);

            return $"{_awsOptions.ServiceAcessUrl}/{path}";
        }
    }
}