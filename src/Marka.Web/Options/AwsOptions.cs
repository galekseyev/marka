﻿namespace Marka.Web.Options
{
    public class AwsOptions
    {
        public string Region { get; set; }
        public string ServiceURL { get; set; }
        public string ServiceAcessUrl { get; set; }
        public string BucketName { get; set; }
        public string AccessKey { get; set; }
        public string SecretKey { get; set; }
    }
}