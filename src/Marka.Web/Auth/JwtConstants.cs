﻿namespace Marka.Web.Auth
{
    public static class JwtConstants
    {
        public static class Strings
        {
            public static class JwtClaimIdentifiers
            {
                public const string Email = "email";
                public const string Role = "rol";
            }
        }
    }
}