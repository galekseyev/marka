﻿namespace Marka.Web.Auth
{
    public class JwtKeysOptions
    {
        public string SigningKey { get; set; }
    }
}