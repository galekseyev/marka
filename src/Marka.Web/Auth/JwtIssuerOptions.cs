﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Threading.Tasks;

namespace Marka.Web.Auth
{
    public class JwtIssuerOptions
    {
        public string Issuer { get; set; }

        public string Subject { get; set; }

        public string Audience { get; set; }

        public DateTime Expiration => IssuedAt.Add(ValidFor);

        public DateTime NotBefore => UtcNow();

        public DateTime IssuedAt => UtcNow();

        public TimeSpan ValidFor { get; set; } = TimeSpan.FromMinutes(60);

        public Func<Task<string>> JtiGenerator => () => Task.FromResult(Guid.NewGuid().ToString());

        public Func<DateTime> UtcNow => () => DateTime.UtcNow;

        public SigningCredentials SigningCredentials { get; set; }


        /// <returns>Date converted to seconds since Unix epoch (Jan 1, 1970, midnight UTC).</returns>
        public static long ToUnixEpochDate(DateTime date)
        {
            var offset = new DateTimeOffset(1970, 1, 1, 0, 0, 0, TimeSpan.Zero);

            return (long)Math.Round((date.ToUniversalTime() - offset).TotalSeconds);
        }
    }
}