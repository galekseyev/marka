﻿using FluentEmail.Core;
using Marka.Web.Emails;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Marka.Web.Communication
{
    public class EmailSender : IEmailSender
    {
        private readonly EmailOptions _emailOptions;

        public EmailSender(IOptions<EmailOptions> emailOptions)
        {
            _emailOptions = emailOptions.Value;
        }

        public async Task SendEmailAsync(string email, string subject, string htmlMessage)
        {
            if (string.IsNullOrEmpty(email))
                throw new ArgumentNullException(nameof(email));

            if (string.IsNullOrEmpty(subject))
                throw new ArgumentNullException(nameof(subject));

            if (string.IsNullOrEmpty(htmlMessage))
                throw new ArgumentNullException(nameof(htmlMessage));

            var message = new MailMessage()
            {
                From = new MailAddress(_emailOptions.FromEmail, _emailOptions.FromDisplay),
            };

            var sender = Email.From(_emailOptions.FromEmail, _emailOptions.FromDisplay)
                .To(email)
                .Subject(subject)
                .Body(htmlMessage, true);

            var response = await sender.SendAsync();
        }
    }
}