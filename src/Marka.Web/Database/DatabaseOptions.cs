﻿namespace Marka.Web.Database
{
    public class DatabaseOptions
    {
        public string ConnectionString { get; set; }

        public string DefaultAdminEmail { get; set; }
        public string DefaultAdminPassword { get; set; }
    }
}