﻿using Marka.BusinessLogic.Interfaces.Countries;
using Marka.BusinessLogic.Interfaces.Languages;
using Marka.BusinessLogic.Interfaces.Orders;
using Marka.BusinessLogic.Interfaces.Payments;
using Marka.Common.Constants;
using Marka.Common.Models.Account.Roles;
using Marka.Common.Models.Account.Users;
using Marka.Common.Models.Countries;
using Marka.Common.Models.Languages;
using Marka.Common.Models.Orders;
using Marka.Common.Models.Payments;
using Marka.Common.Results;
using Marka.DataLayer.Interfaces.Domain;
using Marka.Web.Constants;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace Marka.Web.Database
{
    public interface IDatabaseInitialization
    {
        Task Initialize();
    }

    public class DatabaseInitialization : IDatabaseInitialization
    {
        private readonly DatabaseOptions _databaseOptions;

        private readonly IDataContext _dataContext;

        private readonly RoleManager<Role> _roleManager;
        private readonly UserManager<User> _userManager;

        private readonly ILanguageService _languageService;
        private readonly ICountryService _countryService;

        private readonly IOrderStatusService _orderStatusService;

        private readonly IPaymentTypeService _paymentTypeService;
        private readonly IPaymentStatusService _paymentStatusServie;

        public DatabaseInitialization(
            IOptions<DatabaseOptions> databaseOptions,
            IDataContext dataContext,
            RoleManager<Role> roleManager,
            UserManager<User> userManager,
            ILanguageService languageService,
            ICountryService countryService,
            IOrderStatusService orderStatusService,
            IPaymentStatusService paymentStatusService,
            IPaymentTypeService paymentTypeService)
        {
            _databaseOptions = databaseOptions.Value;

            _dataContext = dataContext;

            _roleManager = roleManager;
            _userManager = userManager;

            _languageService = languageService;
            _countryService = countryService;

            _orderStatusService = orderStatusService;

            _paymentStatusServie = paymentStatusService;
            _paymentTypeService = paymentTypeService;
        }

        public async Task Initialize()
        {
            if (!_dataContext.Database.EnsureCreated())
                return;

            await InitRoles();
            await InitAdministrator();

            await InitLanguages();
            await InitCountries();

            await InitOrderStatuses();

            await InitPaymentStatuses();
            await InitPaymentTypes();
        }

        private async Task InitOrderStatuses()
        {
            var statuses = new List<OrderStatus>
            {
                new OrderStatus { StatusCode = OrderStatusCode.Pending },
                new OrderStatus { StatusCode = OrderStatusCode.Shipped },
                new OrderStatus { StatusCode = OrderStatusCode.Delivered },
                new OrderStatus { StatusCode = OrderStatusCode.Canceled },
                new OrderStatus { StatusCode = OrderStatusCode.Refunded },
                new OrderStatus { StatusCode = OrderStatusCode.PartiallyRefunded }
            };

            var result = await _orderStatusService.CreateOrderStatusesAsync(statuses);

            if (!result.IsSuccessful())
                throw new Exception($"An error occurred creating order statuses.", result.Exception);
        }

        private async Task InitPaymentTypes()
        {
            var types = new List<PaymentType>
            {
                new PaymentType { TypeCode = PaymentTypeCode.Cash},
                new PaymentType { TypeCode = PaymentTypeCode.BankTransfer},
                new PaymentType { TypeCode = PaymentTypeCode.CreditDebitCard},
                new PaymentType { TypeCode = PaymentTypeCode.Paypal},
            };

            var result = await _paymentTypeService.CreatePaymentTypesAsync(types);

            if (!result.IsSuccessful())
                throw new Exception($"An error occurred creating payment types.", result.Exception);
        }

        private async Task InitPaymentStatuses()
        {
            var statuses = new List<PaymentStatus>
            {
                new PaymentStatus { StatusCode = PaymentStatusCode.Pending },
                new PaymentStatus { StatusCode = PaymentStatusCode.Paid },
                new PaymentStatus { StatusCode = PaymentStatusCode.Canceled },
                new PaymentStatus { StatusCode = PaymentStatusCode.Refunded },
                new PaymentStatus { StatusCode = PaymentStatusCode.PartiallyRefunded }
            };

            var result = await _paymentStatusServie.CreatePaymentStatusesAsync(statuses);

            if (!result.IsSuccessful())
                throw new Exception($"An error occurred creating payment statuses.", result.Exception);
        }

        private async Task InitCountries()
        {
            var countries = new List<Country>
            {
                new Country {
                    CountryCode = "AT",
                    CountryName="Austria"
                },
                new Country {
                    CountryCode = "BE",
                    CountryName="Belgium"
                },
                new Country {
                    CountryCode = "BG",
                    CountryName="Bulgaria"
                },
                new Country {
                    CountryCode = "CZ",
                    CountryName="Czech Republic",
                },
                new Country {
                    CountryCode = "DK",
                    CountryName="Denmark"
                },
                new Country {
                    CountryCode = "FR",
                    CountryName="France"
                },
                new Country {
                    CountryCode = "GR",
                    CountryName="Greece",
                },
                new Country {
                    CountryCode = "HR",
                    CountryName="Croatia"
                },
                new Country {
                    CountryCode = "EE",
                    CountryName="Estonia",
                },
                new Country {
                    CountryCode = "IT",
                    CountryName="Italy"
                },
                new Country {
                    CountryCode = "IE",
                    CountryName="Ireland"
                },
                new Country {
                    CountryCode = "GB",
                    CountryName="Great Britain"
                },
                new Country {
                    CountryCode = "LT",
                    CountryName="Lithuania"
                },
                new Country {
                    CountryCode = "LU",
                    CountryName="Luxembourg"
                },
                new Country {
                    CountryCode = "NL",
                    CountryName="Netherlands",
                },
                new Country {
                    CountryCode = "PL",
                    CountryName="Poland",
                },
                new Country {
                    CountryCode = "PT",
                    CountryName="Portugal"
                },
                new Country {
                    CountryCode = "SK",
                    CountryName="Slovakia",
                },
                new Country {
                    CountryCode = "SI",
                    CountryName="Slovenia",
                },
                new Country {
                    CountryCode = "FI",
                    CountryName="Finland",
                },
                new Country {
                    CountryCode = "ES",
                    CountryName="Spain"
                },
                new Country {
                    CountryCode = "HU",
                    CountryName="Hungary",
                },
                new Country {
                    CountryCode = "DE",
                    CountryName="Germany",
                },
                new Country {
                    CountryCode = "SE",
                    CountryName="Sweden"
                },
                new Country {
                    CountryCode = "GB-ENG",
                    CountryName="England",
                },
                new Country {
                    CountryCode = "US",
                    CountryName="United States",
                    CountryStates= new List<CountryState>
                    {
                        new CountryState
                        {
                          StateCode= "ID",
                          StateName= "Idaho"
                        },
                        new CountryState
                        {
                          StateCode= "PA",
                          StateName= "Pennsylvania"
                        },
                        new CountryState
                        {
                          StateCode= "FM",
                          StateName= "Federated States of Micronesia"
                        },
                        new CountryState
                        {
                          StateCode= "AA",
                          StateName= "Armed Forces the Americas"
                        },
                        new CountryState
                        {
                          StateCode= "NC",
                          StateName= "North Carolina"
                        },
                        new CountryState
                        {
                          StateCode= "FL",
                          StateName= "Florida"
                        },
                        new CountryState
                        {
                          StateCode= "ME",
                          StateName= "Maine"
                        },
                        new CountryState
                        {
                          StateCode= "NE",
                          StateName= "Nebraska"
                        },
                        new CountryState
                        {
                          StateCode= "IA",
                          StateName= "Iowa"
                        },
                        new CountryState
                        {
                          StateCode= "MD",
                          StateName= "Maryland"
                        },
                        new CountryState
                        {
                          StateCode= "PW",
                          StateName= "Palau"
                        },
                        new CountryState
                        {
                          StateCode= "AE",
                          StateName= "Armed Forces Europe"
                        },
                        new CountryState
                        {
                          StateCode= "TN",
                          StateName= "Tennessee"
                        },
                        new CountryState
                        {
                          StateCode= "MN",
                          StateName= "Minnesota"
                        },
                        new CountryState
                        {
                          StateCode= "CA",
                          StateName= "California"
                        },
                        new CountryState
                        {
                          StateCode= "IN",
                          StateName= "Indiana"
                        },
                        new CountryState
                        {
                          StateCode= "MA",
                          StateName= "Massachusetts"
                        },
                        new CountryState
                        {
                          StateCode= "VI",
                          StateName= "Virgin Islands"
                        },
                        new CountryState
                        {
                          StateCode= "GU",
                          StateName= "Guam"
                        },
                        new CountryState
                        {
                          StateCode= "WI",
                          StateName= "Wisconsin"
                        },
                        new CountryState
                        {
                          StateCode= "AL",
                          StateName= "Alabama"
                        },
                        new CountryState
                        {
                          StateCode= "AR",
                          StateName= "Arkansas"
                        },
                        new CountryState
                        {
                          StateCode= "MH",
                          StateName= "Marshall Islands"
                        },
                        new CountryState
                        {
                          StateCode= "WA",
                          StateName= "Washington"
                        },
                        new CountryState
                        {
                          StateCode= "CO",
                          StateName= "Colorado"
                        },
                        new CountryState
                        {
                          StateCode= "MP",
                          StateName= "Northern Mariana Islands"
                        },
                        new CountryState
                        {
                          StateCode= "AP",
                          StateName= "Armed Forces Pacific"
                        },
                        new CountryState
                        {
                          StateCode= "ND",
                          StateName= "North Dakota"
                        },
                        new CountryState
                        {
                          StateCode= "MI",
                          StateName= "Michigan"
                        },
                        new CountryState
                        {
                          StateCode= "SC",
                          StateName= "South Carolina"
                        },
                        new CountryState
                        {
                          StateCode= "NH",
                          StateName= "New Hampshire"
                        },
                        new CountryState
                        {
                          StateCode= "OK",
                          StateName= "Oklahoma"
                        },
                        new CountryState
                        {
                          StateCode= "MO",
                          StateName= "Missouri"
                        },
                        new CountryState
                        {
                          StateCode= "NM",
                          StateName= "New Mexico"
                        },
                        new CountryState
                        {
                          StateCode= "NV",
                          StateName= "Nevada"
                        },
                        new CountryState
                        {
                          StateCode= "KY",
                          StateName= "Kentucky"
                        },
                        new CountryState
                        {
                          StateCode= "AS",
                          StateName= "American Samoa"
                        },
                        new CountryState
                        {
                          StateCode= "SD",
                          StateName= "South Dakota"
                        },
                        new CountryState
                        {
                          StateCode= "UT",
                          StateName= "Utah"
                        },
                        new CountryState
                        {
                          StateCode= "RI",
                          StateName= "Rhode Island"
                        },
                        new CountryState
                        {
                          StateCode= "OR",
                          StateName= "Oregon"
                        },
                        new CountryState
                        {
                          StateCode= "GA",
                          StateName= "Georgia"
                        },
                        new CountryState
                        {
                          StateCode= "NY",
                          StateName= "New York"
                        },
                        new CountryState
                        {
                          StateCode= "PR",
                          StateName= "Puerto Rico"
                        },
                        new CountryState
                        {
                          StateCode= "CT",
                          StateName= "Connecticutt"
                        },
                        new CountryState
                        {
                          StateCode= "IL",
                          StateName= "Illinois"
                        },
                        new CountryState
                        {
                          StateCode= "KS",
                          StateName= "Kansas"
                        },
                        new CountryState
                        {
                          StateCode= "DC",
                          StateName= "District of Columbia"
                        },
                        new CountryState
                        {
                          StateCode= "MT",
                          StateName= "Montana"
                        },
                        new CountryState
                        {
                          StateCode= "OH",
                          StateName= "Ohio"
                        },
                        new CountryState
                        {
                          StateCode= "DE",
                          StateName= "Deleware"
                        },
                        new CountryState
                        {
                          StateCode= "WV",
                          StateName= "West Virginia"
                        },
                        new CountryState
                        {
                          StateCode= "MS",
                          StateName= "Mississippi"
                        },
                        new CountryState
                        {
                          StateCode= "TX",
                          StateName= "Texas"
                        },
                        new CountryState
                        {
                          StateCode= "HI",
                          StateName= "Hawaii"
                        },
                        new CountryState
                        {
                          StateCode= "VT",
                          StateName= "Vermont"
                        },
                        new CountryState
                        {
                          StateCode= "LA",
                          StateName= "Louisiana"
                        },
                        new CountryState
                        {
                          StateCode= "VA",
                          StateName= "Virginia"
                        },
                        new CountryState
                        {
                          StateCode= "AK",
                          StateName= "Alaska"
                        },
                        new CountryState
                        {
                          StateCode= "WY",
                          StateName= "Wyoming"
                        },
                        new CountryState
                        {
                          StateCode= "NJ",
                          StateName= "New Jersey"
                        },
                        new CountryState
                        {
                          StateCode= "AZ",
                          StateName= "Arizona"
                        }
                    },
                    NationalIdMask= "999-99-9999",
                    NationalIdNumber= "SSN",
                    NationalIdRegEx= "^\\d{3}-\\d{2}-\\d{4}$",
                    PostalCodeRegEx= " ^(\\d{5}(-\\d{4})?)$"
                },
                new Country
                {
                    CountryCode = "BY",
                    CountryName = "Belarus",
                },
                new Country
                {
                    CountryCode = "HK",
                    CountryName = "Hong Kong",
                },
                new Country
                {
                    CountryCode = "IS",
                    CountryName = "Iceland",
                },
                new Country
                {
                    CountryCode = "JP",
                    CountryName = "Japan"
                },
                new Country
                {
                    CountryCode = "CA",
                    CountryName = "Canada"
                },
                new Country
                {
                    CountryCode = "KR",
                    CountryName = "South Korea",
                },
                new Country
                {
                    CountryCode = "RU",
                    CountryName = "Russia",
                },
                new Country
                {
                    CountryCode = "NO",
                    CountryName = "Norway"
                },
                new Country
                {
                    CountryCode = "UA",
                    CountryName = "Ukraine"
                },
                new Country
                {
                    CountryCode = "LV",
                    CountryName = "Latvia",
                    NationalIdMask = "999999-99999",
                    NationalIdNumber = "Personal Code",
                    NationalIdRegEx = "[0-9]{2}[0,1][0-9][0-9]-[0-9]{5}",
                    PostalCodeRegEx = "^(LV-)[0-9]{4}$",
                },
            };

            var result = await _countryService.CreateCountriesAsync(countries);

            if (!result.IsSuccessful())
                throw new Exception($"An error occurred creating countries.", result.Exception);
        }

        private async Task InitLanguages()
        {
            var englishResult = await _languageService.GetLanguageByCodeAsync(LanguageConstants.LanguageCodes.English);

            if (englishResult.Code == HttpStatusCode.NotFound)
            {
                var result = await _languageService.CreateLanguageAsync(new Language
                {
                    Title = LanguageConstants.LanguageTitles.English,
                    LanguageCode = LanguageConstants.LanguageCodes.English
                });

                if (!result.IsSuccessful())
                    throw new Exception($"An error occurred creating English Language.", result.Exception);
            }

            var russianResult = await _languageService.GetLanguageByCodeAsync(LanguageConstants.LanguageCodes.Russian);

            if (russianResult.Code == HttpStatusCode.NotFound)
            {
                var result = await _languageService.CreateLanguageAsync(new Language
                {
                    Title = LanguageConstants.LanguageTitles.Russian,
                    LanguageCode = LanguageConstants.LanguageCodes.Russian
                });

                if (!result.IsSuccessful())
                    throw new Exception($"An error occurred creating Russian Language.", result.Exception);
            }

            var latvianResult = await _languageService.GetLanguageByCodeAsync(LanguageConstants.LanguageCodes.Latvian);

            if (latvianResult.Code == HttpStatusCode.NotFound)
            {
                var result = await _languageService.CreateLanguageAsync(new Language
                {
                    Title = LanguageConstants.LanguageTitles.Latvian,
                    LanguageCode = LanguageConstants.LanguageCodes.Latvian
                });

                if (!result.IsSuccessful())
                    throw new Exception($"An error occurred creating Latvian Language.", result.Exception);
            }
        }

        private async Task InitRoles()
        {
            if (!await _roleManager.RoleExistsAsync(RoleName.User))
            {
                var result = await _roleManager.CreateAsync(new Role(RoleName.User));

                if (!result.Succeeded)
                    throw new Exception($"An error occurred creating User role. {string.Join('.', result.Errors)}");
            }

            if (!await _roleManager.RoleExistsAsync(RoleName.Admin))
            {
                var result = await _roleManager.CreateAsync(new Role(RoleName.Admin));

                if (!result.Succeeded)
                    throw new Exception($"An error occurred creating Admin role. {string.Join('.', result.Errors)}");
            }
        }

        private async Task InitAdministrator()
        {
            var user = await _userManager.FindByEmailAsync(_databaseOptions.DefaultAdminEmail);

            if (user != null)
                return;

            user = new User
            {
                UserName = _databaseOptions.DefaultAdminEmail,
                Email = _databaseOptions.DefaultAdminEmail,

                FullName = "Administrator",

                CreateDate = DateTime.UtcNow,
                UpdateDate = DateTime.UtcNow,
            };

            var result = await _userManager.CreateAsync(user, _databaseOptions.DefaultAdminPassword);

            if (!result.Succeeded)
                throw new Exception($"An error occurred creating Admin record. {string.Join('.', result.Errors)}");

            result = await _userManager.AddToRolesAsync(user, new[] { RoleName.User, RoleName.Admin });

            if (!result.Succeeded)
                throw new Exception($"An error occurred adding Admin record to the roles. {string.Join('.', result.Errors)}");
        }
    }
}