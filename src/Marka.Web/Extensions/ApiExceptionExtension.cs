﻿using Marka.Common.Exceptions;
using Marka.Common.Responses;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Globalization;

namespace Marka.Web.Exceptions
{
    public static class ApiExceptionExtension
    {
        public static ApiResponse ToApiResponse(this ApiException exception)
        {
            return new ApiResponse(exception.Message, exception.Code);
        }

        public static IActionResult ToActionResult(this ApiException exception)
        {
            return new ObjectResult(exception.ToApiResponse())
            {
                StatusCode = (int)exception.Code
            };
        }
    }
}