﻿using Microsoft.Extensions.DependencyInjection;
using Marka.Common.Results;
using Marka.Common.Results.Interfaces;
using Marka.BusinessLogic.Interfaces.Catalog.Attributes;
using Marka.BusinessLogic.Interfaces.Catalog.Categories;
using Marka.BusinessLogic.Interfaces.Catalog.Inventories;
using Marka.BusinessLogic.Services.Catalog.Attributes;
using Marka.BusinessLogic.Services.Catalog.Categories;
using Marka.BusinessLogic.Services.Catalog.Inventories;
using Marka.DataLayer.Domain;
using Marka.DataLayer.Interfaces.Domain;
using Marka.DataLayer.Interfaces.Repositories.Catalog.Attributes;
using Marka.DataLayer.Interfaces.Repositories.Catalog.Categories;
using Marka.DataLayer.Interfaces.Repositories.Catalog.Inventories;
using Marka.DataLayer.Repositories.Catalog.Attributes;
using Marka.DataLayer.Repositories.Catalog.Categories;
using Marka.DataLayer.Repositories.Catalog.Inventories;
using Marka.DataLayer.Interfaces.Repositories.Carts;
using Marka.DataLayer.Repositories.Carts;
using Marka.BusinessLogic.Interfaces;
using Marka.BusinessLogic.Services.Carts;
using Marka.DataLayer.Interfaces.Repositories.Shippings;
using Marka.DataLayer.Repositories.Shippings;
using Marka.BusinessLogic.Interfaces.Shippings;
using Marka.BusinessLogic.Services.Shippings;
using Marka.Web.Database;
using Marka.Web.Images;
using Marka.DataLayer.Interfaces.Repositories.Orders;
using Marka.DataLayer.Repositories.Orders;
using Marka.BusinessLogic.Interfaces.Orders;
using Marka.BusinessLogic.Services.Orders;
using Marka.BusinessLogic.Interfaces.Carts;
using Marka.Web.Communication;
using Microsoft.AspNetCore.Identity.UI.Services;
using Marka.BusinessLogic.Interfaces.Languages;
using Marka.BusinessLogic.Services.Languages;
using Marka.DataLayer.Interfaces.Repositories.Languages;
using Marka.DataLayer.Repositories.Languages;
using Marka.BusinessLogic.Interfaces.Catalog.Specifications;
using Marka.BusinessLogic.Services.Catalog.Specifications;
using Marka.DataLayer.Repositories.Catalog.Specifications;
using Marka.DataLayer.Interfaces.Repositories.Catalog.Specifications;
using Marka.DataLayer.Interfaces.Repositories.Catalog.InventoryGroups;
using Marka.DataLayer.Repositories.Catalog.InventoryGroups;
using Marka.BusinessLogic.Interfaces.Catalog.InventoryGroups;
using Marka.BusinessLogic.Services.Catalog.InventoryGroups;
using Marka.DataLayer.Interfaces.Repositories.Blog;
using Marka.BusinessLogic.Interfaces.Blog;
using Marka.DataLayer.Repositories.Blog;
using Marka.BusinessLogic.Services.Blog;
using Marka.DataLayer.Repositories.Shipments;
using Marka.DataLayer.Interfaces.Repositories.Shipments;
using Marka.DataLayer.Interfaces.Repositories.Payments;
using Marka.DataLayer.Repositories.Payments;
using Marka.BusinessLogic.Interfaces.Payments;
using Marka.BusinessLogic.Services.Payments;
using Marka.Web.Core.Managers;
using Marka.BusinessLogic.Services.Countries;
using Marka.BusinessLogic.Interfaces.Countries;
using Marka.DataLayer.Interfaces.Repositories.Countries;
using Marka.DataLayer.Repositories.Countries;
using Marka.BusinessLogic.Interfaces.Account;
using Marka.BusinessLogic.Services.Account;
using Marka.DataLayer.Interfaces.Repositories.Users;
using Marka.DataLayer.Repositories.Users;

namespace Marka.Web.Extensions.ServiceCollectionExtensions
{
    public static class ServiceCollectionExtension
    {
        public static void AddApplicationServices(this IServiceCollection services)
        {
            services.AddScoped<IResultFactory, ResultFactory>();

            services.AddScoped<IJwtFactory, JwtFactory>();

            services.AddScoped<IDataContext, DataContext>();
            services.AddScoped<IDatabaseInitialization, DatabaseInitialization>();

            services.AddScoped<IEmailSender, EmailSender>();

            //User
            services.AddScoped<IUserAddressService, UserAddressService>();
            services.AddScoped<IUserAddressRepository, UserAddressRepository>();

            //Languages
            services.AddScoped<ILanguageService, LanguageService>();
            services.AddScoped<ILanguageRepository, LanguageRepository>();

            //Countries
            services.AddScoped<ICountryService, CountryService>();
            services.AddScoped<ICountryRepository, CountryRepository>();


            //Categories
            services.AddScoped<ICategoryRepository, CategoryRepository>();
            services.AddScoped<ICategoryService, CategoryService>();

            services.AddScoped<ICategoryPictureRepository, CategoryPictureRepository>();
            services.AddScoped<ICategoryPictureService, CategoryPictureService>();

            //Attributes
            services.AddScoped<IAttributeService, AttributeService>();
            services.AddScoped<IAttributeRepository, AttributeRepository>();

            //Specifications
            services.AddScoped<ISpecificationService, SpecificationService>();
            services.AddScoped<ISpecificationRepository, SpecificationRepository>();

            services.AddScoped<ISpecificationValueService, SpecificationValueService>();
            services.AddScoped<ISpecificationValueRepository, SpecificationValueRepository>();

            //Inventories
            services.AddScoped<IInventoryService, InventoryService>();
            services.AddScoped<IInventoryRepository, InventoryRepository>();

            services.AddScoped<IImageManager, ImageManager>();

            services.AddScoped<IInventoryPictureService, InventoryPictureService>();
            services.AddScoped<IInventoryPictureRepository, InventoryPictureRepository>();

            services.AddScoped<IInventoryAttributeService, InventoryAttributeService>();
            services.AddScoped<IInventoryAttributeRepository, InventoryAttributeRepository>();

            services.AddScoped<IInventoryAttributeValueService, InventoryAttributeValueService>();
            services.AddScoped<IInventoryAttributeValueRepository, InventoryAttributeValueRepository>();

            services.AddScoped<IInventorySpecificationService, InventorySpecificationService>();
            services.AddScoped<IInventorySpecificationRepository, InventorySpecificationRepository>();

            //Inventory Groups
            services.AddScoped<IInventoryGroupRepository, InventoryGroupRepository>();
            services.AddScoped<IInventoryGroupService, InventoryGroupService>();

            services.AddScoped<IInventoryGroupPictureRepository, InventoryGroupPictureRepository>();
            services.AddScoped<IInventoryGroupPictureService, InventoryGroupPictureService>();

            services.AddScoped<IInventoryGroupInventoryRepository, InventoryGroupInventoryRepository>();
            services.AddScoped<IInventoryGroupInventoryService, InventoryGroupInventoryService>();

            //Shipping
            services.AddScoped<IShippingMethodRepository, ShippingMethodRepository>();
            services.AddScoped<IShippingRateService, ShippingRateService>();

            services.AddScoped<IShippingRateRepository, ShippingRateRepository>();
            services.AddScoped<IShippingRateService, ShippingRateService>();

            //Shopping Cart
            services.AddScoped<ICartRepository, CartRepository>();
            services.AddScoped<ICartService, CartService>();
            services.AddScoped<ICartManager, CartManager>();

            services.AddScoped<ICartShippingRepository, CartShippingRepository>();
            services.AddScoped<ICartShippingService, CartShippingService>();

            //Order
            services.AddScoped<IOrderRepository, OrderRepository>();
            services.AddScoped<IOrderService, OrderService>();
            services.AddScoped<IOrderStatusRepository, OrderStatusRepository>();
            services.AddScoped<IOrderStatusService, OrderStatusService>();

            //Payment
            services.AddScoped<IPaymentTypeRepository, PaymentTypeRepository>();
            services.AddScoped<IPaymentTypeService, PaymentTypeService>();
            services.AddScoped<IPaymentStatusRepository, PaymentStatusRepository>();
            services.AddScoped<IPaymentStatusService, PaymentStatusService>();


            //services.AddScoped<IOrderItemRepository, OrderItemRepository>();
            //services.AddScoped<IOrderItemService, OrderItemService>();

            services.AddScoped<IShipmentRepository, ShipmentRepository>();
            services.AddScoped<IShipmentService, ShipmentService>();

            //Blog
            services.AddScoped<IArticleRepository, ArticleRepository>();
            services.AddScoped<IArticleService, ArticleService>();

            services.AddScoped<IArticlePictureRepository, ArticlePictureRepository>();
            services.AddScoped<IArticlePictureService, ArticlePictureService>();
        }
    }
}