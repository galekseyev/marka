﻿using AutoMapper;
using Marka.Common.Responses.Base;
using Microsoft.AspNetCore.Mvc;
using System;

namespace Marka.Web.Extensions
{
    public static class MemberConfigurationExpressionExtension
    {
        public static void MapHalLinks<TSource, TDest>(
            this IMemberConfigurationExpression<TSource, TDest, HalLinks> configurationExpression,
            Func<IUrlHelper, TSource, HalLinks> linksGenerator)
        {
            if (configurationExpression == null)
            {
                throw new ArgumentNullException(nameof(configurationExpression));
            }
            if (linksGenerator == null)
            {
                throw new ArgumentNullException(nameof(linksGenerator));
            }

            configurationExpression.ResolveUsing((source, dest, original, context) =>
            {
                var urlHelper = context.Options.GetUrlHelper();

                return linksGenerator(urlHelper, source);
            });
        }

        public static void MapHalLinks<TSource, TDest>(
           this IMemberConfigurationExpression<TSource, TDest, HalLinks> configurationExpression,
           Func<IUrlHelper, TSource, TDest, HalLinks> linksGenerator)
        {
            if (configurationExpression == null)
            {
                throw new ArgumentNullException(nameof(configurationExpression));
            }
            if (linksGenerator == null)
            {
                throw new ArgumentNullException(nameof(linksGenerator));
            }

            configurationExpression.ResolveUsing((source, dest, original, context) =>
            {
                var urlHelper = context.Options.GetUrlHelper();

                return linksGenerator(urlHelper, source, dest);
            });
        }
    }
}
