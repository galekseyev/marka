﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;

namespace Marka.Web.Extensions
{
    public static class AutoMapperExtension
    {
        private const string UrlHelperKey = "Marka.Web.Extensions.IUrlHelper";

        public static IMappingOperationOptions AddUrlHelper(this IMappingOperationOptions options, IUrlHelper urlHelper)
        {
            options.Items[UrlHelperKey] = urlHelper;

            return options;
        }

        public static IUrlHelper GetUrlHelper(this IMappingOperationOptions options)
        {
            object urlHelper;

            if (options.Items.TryGetValue(UrlHelperKey, out urlHelper))
                return urlHelper as IUrlHelper;
            else
                return null;
        }
    }
}
