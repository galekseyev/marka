﻿using Marka.Common.Responses;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Globalization;

namespace Marka.Web.Extensions
{
    public static class ApiResponseExtension
    {
        public static IActionResult ToActionResult(this ApiResponse response)
        {
            return new ObjectResult(response)
            {
                StatusCode = (int)response.Code
            };
        }
    }
}