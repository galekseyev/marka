using System;
using System.Text;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SpaServices.Webpack;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using AutoMapper;
using Marka.Common.Models.Account.Roles;
using Marka.Common.Models.Account.Users;
using Marka.DataLayer.Domain;
using Marka.Web.Auth;
using Marka.Web.Extensions.ServiceCollectionExtensions;
using Marka.Web.Database;
using Marka.Web.Images;
using Marka.Web.Emails;
using Marka.Web.Options;
using System.Globalization;
using Microsoft.AspNetCore.Localization;
using Marka.Web.Constants;
using Marka.Common.Constants;
using FluentEmail.Mailgun;
using FluentEmail.Core;
using Microsoft.AspNetCore.HttpOverrides;

namespace Marka.Web
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var databaseOptions = Configuration.GetSection(nameof(DatabaseOptions));

            // Add Entity Framework services to the services container.
            services.AddDbContext<DataContext>(options => options.UseMySql(databaseOptions[nameof(DatabaseOptions.ConnectionString)]));

            // Add Identity services with custom UserStore and RoleStore with EF6 support to the services container.
            //services.AddIdentity<User, Role>(o =>
            //{
            //    // configure identity options
            //    o.Password.RequireDigit = false;
            //    o.Password.RequireLowercase = false;
            //    o.Password.RequireUppercase = false;
            //    o.Password.RequireNonAlphanumeric = false;
            //    o.Password.RequireDigit = false;
            //    o.Password.RequiredLength = 8;
            //})
            //.AddEntityFrameworkStores<DataContext>()
            //.AddDefaultTokenProviders();

            var builder = services.AddIdentityCore<User>(o =>
            {
                // configure identity options
                o.Password.RequireDigit = false;
                o.Password.RequireLowercase = false;
                o.Password.RequireUppercase = false;
                o.Password.RequireNonAlphanumeric = false;
                o.Password.RequireDigit = false;
                o.Password.RequiredLength = 8;
            });

            builder = new IdentityBuilder(builder.UserType, typeof(Role), builder.Services);
            builder.AddEntityFrameworkStores<DataContext>();

            builder.AddRoleValidator<RoleValidator<Role>>();
            builder.AddRoleManager<RoleManager<Role>>();
            builder.AddSignInManager<SignInManager<User>>();
            builder.AddDefaultTokenProviders();

            var jwtSigningKey = Configuration["JwtKeysOptions:SigningKey"];
            var jwtSecurityKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(jwtSigningKey));

            var jwtAppSettingOptions = Configuration.GetSection(nameof(JwtIssuerOptions));

            services.Configure<DatabaseOptions>(o =>
            {
                o.DefaultAdminEmail = databaseOptions[nameof(DatabaseOptions.DefaultAdminEmail)];
                o.DefaultAdminPassword = databaseOptions[nameof(DatabaseOptions.DefaultAdminPassword)];
            });

            services.Configure<JwtIssuerOptions>(options =>
            {
                options.Issuer = jwtAppSettingOptions[nameof(JwtIssuerOptions.Issuer)];
                options.Audience = jwtAppSettingOptions[nameof(JwtIssuerOptions.Audience)];
                options.SigningCredentials = new SigningCredentials(jwtSecurityKey, SecurityAlgorithms.HmacSha256);
            });

            var imageAppSettingsOptions = Configuration.GetSection(nameof(ImageOptions));

            services.Configure<ImageOptions>(o =>
            {
                o.ImageRootPath = imageAppSettingsOptions[nameof(ImageOptions.ImageRootPath)];
            });

            var awsAppSettingsOptions = Configuration.GetSection(nameof(AwsOptions));

            services.Configure<AwsOptions>(o =>
            {
                o.Region = awsAppSettingsOptions[nameof(AwsOptions.Region)];
                o.ServiceURL = awsAppSettingsOptions[nameof(AwsOptions.ServiceURL)];
                o.ServiceAcessUrl = awsAppSettingsOptions[nameof(AwsOptions.ServiceAcessUrl)];
                o.BucketName = awsAppSettingsOptions[nameof(AwsOptions.BucketName)];
                o.AccessKey = awsAppSettingsOptions[nameof(AwsOptions.AccessKey)];
                o.SecretKey = awsAppSettingsOptions[nameof(AwsOptions.SecretKey)];
            });

            var emailOptions = Configuration.GetSection(nameof(EmailOptions));

            services.Configure<EmailOptions>(emailOptions);

            var emailSender = new MailgunSender(
                emailOptions[nameof(EmailOptions.Domain)],
                emailOptions[nameof(EmailOptions.ApiKey)]);

            Email.DefaultSender = emailSender;

            var cartOptions = Configuration.GetSection(nameof(CartOptions));

            services.Configure<CartOptions>(o =>
            {
                o.CartEncryptionPurpose = cartOptions[nameof(CartOptions.CartEncryptionPurpose)];
            });

            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidateIssuer = true,
                ValidIssuer = jwtAppSettingOptions[nameof(JwtIssuerOptions.Issuer)],

                ValidateAudience = true,
                ValidAudience = jwtAppSettingOptions[nameof(JwtIssuerOptions.Audience)],

                ValidateIssuerSigningKey = true,
                IssuerSigningKey = jwtSecurityKey,

                RequireExpirationTime = true,
                ValidateLifetime = true,

                ClockSkew = TimeSpan.Zero
            };

            services.AddAuthentication(options =>
                    {
                        options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                        options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;

                    }).AddJwtBearer(configureOptions =>
                    {
                        configureOptions.ClaimsIssuer = jwtAppSettingOptions[nameof(JwtIssuerOptions.Issuer)];
                        configureOptions.TokenValidationParameters = tokenValidationParameters;
                        //configureOptions.SaveToken = true;
                    });

            services.AddAuthorization(options =>
            {
                options.AddPolicy("User", policy => policy.RequireClaim(JwtConstants.Strings.JwtClaimIdentifiers.Role, RoleName.User));
                options.AddPolicy("Admin", policy => policy.RequireClaim(JwtConstants.Strings.JwtClaimIdentifiers.Role, RoleName.Admin));
            });

            //services.AddCors(options =>
            //{
            //    options.AddPolicy("EnableCors", builder =>
            //    {
            //        builder.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod().AllowCredentials().Build();
            //    });
            //});

            services.AddLocalization(options => options.ResourcesPath = "Resources");

            services.AddMvc()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services.AddApplicationServices();
            services.AddAutoMapper();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IDatabaseInitialization databaseInitalization)
        {
            //Initialize database if not exists.
            databaseInitalization.Initialize().Wait();

            if (env.IsDevelopment())
            {
                app.UseExceptionHandler("/error");

                app.UseWebpackDevMiddleware(new WebpackDevMiddlewareOptions
                {
                    HotModuleReplacement = true
                });
            }
            else
            {
                app.UseExceptionHandler("/error");
                app.UseHsts();
            }

            //Research how to extract this so user will be able to create languages in Administration module.
            var supportedCultures = new[]
            {
                new CultureInfo(LanguageConstants.LanguageCodes.English),
                new CultureInfo(LanguageConstants.LanguageCodes.Latvian),
                new CultureInfo(LanguageConstants.LanguageCodes.Russian)
            };

            var requestLocalizationOptions = new RequestLocalizationOptions
            {
                DefaultRequestCulture = new RequestCulture(LanguageConstants.LanguageCodes.English),
                SupportedCultures = supportedCultures,
                SupportedUICultures = supportedCultures
            };

            app.UseRequestLocalization(requestLocalizationOptions);

            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
            });

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseAuthentication();

            //app.UseCors("EnableCORS");

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");

                routes.MapSpaFallbackRoute(
                    name: "spa-fallback",
                    defaults: new { controller = "Home", action = "Index" });
            });
        }
    }
}