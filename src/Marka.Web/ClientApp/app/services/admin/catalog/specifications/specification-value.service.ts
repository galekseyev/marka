﻿import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { SpecificationValue } from "../../../../models/catalog/specifications/specification-value";
import { EmbeddedApiResponse } from "../../../../models/response/response";
import { ListResponse } from "../../../../models/response/list.response";


@Injectable()
export class AdminSpecificationValueService {

    constructor(private http: HttpClient) { }

    list(id: number) {
        return this.http.get<ListResponse<EmbeddedApiResponse<SpecificationValue>>>('api/admin/specification/' + id + '/value/');
    }

    get(id: number, valueId: number) {
        return this.http.get<EmbeddedApiResponse<SpecificationValue>>('api/admin/specification/' + id + '/value/' + valueId);
    }

    save(id: number, value: SpecificationValue) {
        if (value.id) {
            return this.http.put<EmbeddedApiResponse<SpecificationValue>>('api/admin/specification/' + id + '/value/' + value.id, value);
        }
        else {
            return this.http.post<EmbeddedApiResponse<SpecificationValue>>('api/admin/specification/' + id + '/value/', value);
        }
    }

    delete(id: number, valueId: number) {
        return this.http.delete('api/admin/specification/' + id + '/value/' + valueId);
    }
}