﻿import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { BaseService } from "../../../base.service";
import { Specification } from "../../../../models/catalog/specifications/specification";


@Injectable()
export class AdminSpecificationService extends BaseService<Specification> {
    constructor(
        http: HttpClient) {
        super('api/admin/specification', http);
    }
}