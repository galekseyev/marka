﻿import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { EmbeddedApiResponse } from "../../../../models/response/response";
import { ListResponse } from "../../../../models/response/list.response";
import { CategoryPicture } from "../../../../models/catalog/categories/category-picture";
import { InventoryGroupPicture } from "../../../../models/catalog/inventory-groups/inventory-group-picture";


@Injectable()
export class AdminInventoryGroupPictureService {

    constructor(private http: HttpClient) { }

    get(id: number) {
        return this.http.get<EmbeddedApiResponse<InventoryGroupPicture>>('api/admin/inventory-group/' + id + '/picture');
    }

    save(id: number, picture: CategoryPicture) {
        if (picture.id) {
            return this.http.put<EmbeddedApiResponse<InventoryGroupPicture>>('api/admin/inventory-group/' + id + '/picture/' + picture.id, picture);
        }
        else {
            return this.http.post<EmbeddedApiResponse<InventoryGroupPicture>>('api/admin/inventory-group/' + id + '/picture', picture);
        }
    }

    delete(id: number, pictureId: number) {
        return this.http.delete('api/admin/inventory-group/' + id + '/picture/' + pictureId);
    }
}
