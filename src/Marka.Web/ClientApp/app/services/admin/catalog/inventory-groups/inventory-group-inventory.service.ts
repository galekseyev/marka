﻿import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { EmbeddedApiResponse } from "../../../../models/response/response";
import { ListResponse } from "../../../../models/response/list.response";
import { InventoryGroupInventory } from "../../../../models/catalog/inventory-groups/inventory-group-inventory";

@Injectable()
export class AdminInventoryGroupInventoryService {

    constructor(private http: HttpClient) { }

    list(id: number) {
        return this.http.get<ListResponse<EmbeddedApiResponse<InventoryGroupInventory>>>('api/admin/inventory-group/' + id + '/inventory/');
    }

    get(id: number, inventoryGroupInventoryId: number) {
        return this.http.get<EmbeddedApiResponse<InventoryGroupInventory>>('api/admin/inventory-group/' + id + '/inventory/' + inventoryGroupInventoryId);
    }

    save(id: number, inventoryGroupInventory: InventoryGroupInventory) {
        if (inventoryGroupInventory.id) {
            return this.http.put<EmbeddedApiResponse<InventoryGroupInventory>>('api/admin/inventory-group/' + id + '/inventory/' + inventoryGroupInventory.id, inventoryGroupInventory);
        }
        else {
            return this.http.post<EmbeddedApiResponse<InventoryGroupInventory>>('api/admin/inventory-group/' + id + '/inventory', inventoryGroupInventory);
        }
    }

    delete(id: number, inventoryGroupInventoryId: number) {
        return this.http.delete('api/admin/inventory-group/' + id + '/inventory/' + inventoryGroupInventoryId);
    }
}