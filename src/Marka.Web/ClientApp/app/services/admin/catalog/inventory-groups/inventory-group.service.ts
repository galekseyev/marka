﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseService } from '../../../base.service';
import { InventoryGroup } from '../../../../models/catalog/inventory-groups/inventory-group';

@Injectable()
export class AdminInventoryGroupService extends BaseService<InventoryGroup> {
    constructor(
        http: HttpClient) {
        super('api/admin/inventory-group', http);
    }
}