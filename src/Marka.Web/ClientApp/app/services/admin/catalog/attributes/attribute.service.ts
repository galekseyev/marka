﻿import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { BaseService } from "../../../base.service";
import { Attribute } from "../../../../models/catalog/attributes/attribute";


@Injectable()
export class AdminAttributeService extends BaseService<Attribute> {
    constructor(
        http: HttpClient) {
        super('api/admin/attribute', http);
    }
}
