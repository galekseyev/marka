﻿import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { BaseService } from "../../../base.service";
import { Inventory, TranslatedInventory } from "../../../../models/catalog/inventories/inventory";
import { ListResponse } from "../../../../models/response/list.response";
import { EmbeddedApiResponse } from "../../../../models/response/response";

@Injectable()
export class AdminInventoryService extends BaseService<Inventory> {

    constructor(
        http: HttpClient) {
        super('api/admin/inventory', http);
    }

    public getTranslatedInventories(filter?: any) {
        let url = this.baseUrl + this.queryString(filter);

        return this.http.get<ListResponse<EmbeddedApiResponse<TranslatedInventory>>>(url);
    }
}