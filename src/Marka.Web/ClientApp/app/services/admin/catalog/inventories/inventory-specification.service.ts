﻿import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { EmbeddedApiResponse } from "../../../../models/response/response";
import { InventorySpecification } from "../../../../models/catalog/inventories/inventory-specification";

@Injectable()
export class AdminInventorySpecificationService {

    constructor(private http: HttpClient) { }

    get(id: number, specificationId: number) {
        return this.http.get<EmbeddedApiResponse<InventorySpecification>>('api/admin/inventory/' + id + '/specification/' + specificationId);
    }

    save(id: number, specification: InventorySpecification) {
        if (specification.id) {
            return this.http.put<EmbeddedApiResponse<InventorySpecification>>('api/admin/inventory/' + id + '/specification/' + specification.id, specification);
        }
        else {
            return this.http.post<EmbeddedApiResponse<InventorySpecification>>('api/admin/inventory/' + id + '/specification/', specification);
        }
    }

    delete(id: number, specificationId: number) {
        return this.http.delete('api/admin/inventory/' + id + '/specification/' + specificationId);
    }
}