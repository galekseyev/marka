﻿import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { EmbeddedApiResponse } from "../../../../models/response/response";
import { InventoryAttributeValue } from "../../../../models/catalog/inventories/inventory.attribute.value";
import { ListResponse } from "../../../../models/response/list.response";

@Injectable()
export class AdminInventoryAttributeValueService {

    constructor(private http: HttpClient) { }

    list(id: number, attributeId: number) {
        return this.http.get<ListResponse<EmbeddedApiResponse<InventoryAttributeValue>>>('api/admin/inventory/' + id + '/attribute/' + attributeId + '/value/' );
    }

    get(id: number, attributeId: number, valueId: number) {
        return this.http.get<EmbeddedApiResponse<InventoryAttributeValue>>('api/admin/inventory/' + id + '/attribute/' + attributeId + '/value/' + valueId);
    }

    save(id: number, attributeId: number, value: InventoryAttributeValue) {
        if (value.id) {
            return this.http.put<EmbeddedApiResponse<InventoryAttributeValue>>('api/admin/inventory/' + id + '/attribute/' + attributeId + '/value/' + value.id, value);
        }
        else {
            return this.http.post<EmbeddedApiResponse<InventoryAttributeValue>>('api/admin/inventory/' + id + '/attribute/' + attributeId + '/value/', value);
        }
    }

    delete(id: number, attributeId: number, valueId: number) {
        return this.http.delete('api/admin/inventory/' + id + '/attribute/' + attributeId + '/value/' + valueId);
    }
}