﻿import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { InventoryAttribute } from "../../../../models/catalog/inventories/inventory.attribute";
import { EmbeddedApiResponse } from "../../../../models/response/response";
import { ListResponse } from "../../../../models/response/list.response";

@Injectable()
export class AdminInventoryAttributeService {

    constructor(private http: HttpClient) { }

    list(id: number) {
        return this.http.get<ListResponse<EmbeddedApiResponse<InventoryAttribute>>>('api/admin/inventory/' + id + '/attribute/');
 
    }

    get(id: number, attributeId: number) {
        return this.http.get<EmbeddedApiResponse<InventoryAttribute>>('api/admin/inventory/' + id + '/attribute/' + attributeId);
    }

    save(id: number, attribute: InventoryAttribute) {
        if (attribute.id) {
            return this.http.put<EmbeddedApiResponse<InventoryAttribute>>('api/admin/inventory/' + id + '/attribute/' + attribute.id, attribute);
        }
        else {
            return this.http.post<EmbeddedApiResponse<InventoryAttribute>>('api/admin/inventory/' + id + '/attribute', attribute);
        }
    }

    delete(id: number, attributeId: number) {
        return this.http.delete('api/admin/inventory/' + id + '/attribute/' + attributeId);
    }
}