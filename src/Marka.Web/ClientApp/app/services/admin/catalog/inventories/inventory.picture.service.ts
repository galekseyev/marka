﻿import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { EmbeddedApiResponse } from "../../../../models/response/response";
import { InventoryPicture } from "../../../../models/catalog/inventories/inventory.picture";
import { ListResponse } from "../../../../models/response/list.response";


@Injectable()
export class AdminInventoryPictureService {

    constructor(private http: HttpClient) { }

    list(id: number) {
        return this.http.get<ListResponse<EmbeddedApiResponse<InventoryPicture>>>('api/admin/inventory/' + id + '/picture');
    }

    save(id: number, picture: InventoryPicture) {
        if (picture.id) {
            return this.http.put<EmbeddedApiResponse<InventoryPicture>>('api/admin/inventory/' + id + '/picture/' + picture.id, picture);
        }
        else {
            return this.http.post<EmbeddedApiResponse<InventoryPicture>>('api/admin/inventory/' + id + '/picture', picture);
        }
    }

    delete(id: number, pictureId: number) {
        return this.http.delete('api/admin/inventory/' + id + '/picture/' + pictureId);
    }
}