﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseService } from '../../../base.service';
import { Category } from '../../../../models/catalog/categories/category';

@Injectable()
export class AdminCategoryService extends BaseService<Category> {
    constructor(
        http: HttpClient) {
        super('api/admin/category', http);
    }
}