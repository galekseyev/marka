﻿import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { EmbeddedApiResponse } from "../../../../models/response/response";
import { ListResponse } from "../../../../models/response/list.response";
import { CategoryPicture } from "../../../../models/catalog/categories/category-picture";


@Injectable()
export class AdminCategoryPictureService {

    constructor(private http: HttpClient) { }

    get(id: number) {
        return this.http.get<EmbeddedApiResponse<CategoryPicture>>('api/admin/category/' + id + '/picture');
    }

    save(id: number, picture: CategoryPicture) {

        console.log(picture);
        if (picture.id) {
            return this.http.put<EmbeddedApiResponse<CategoryPicture>>('api/admin/category/' + id + '/picture/' + picture.id, picture);
        }
        else {
            return this.http.post<EmbeddedApiResponse<CategoryPicture>>('api/admin/category/' + id + '/picture', picture);
        }
    }

    delete(id: number, pictureId: number) {
        return this.http.delete('api/admin/category/' + id + '/picture/' + pictureId);
    }
}
