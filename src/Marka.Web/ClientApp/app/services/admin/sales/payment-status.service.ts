﻿import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { BaseService } from "../../base.service";
import { PaymentStatus } from "../../../models/orders/order";

@Injectable()
export class AdminPaymentStatusService extends BaseService<PaymentStatus> {

    constructor(
        http: HttpClient) {
        super('api/admin/payment-status', http);
    }
}