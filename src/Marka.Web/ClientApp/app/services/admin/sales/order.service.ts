﻿import { Injectable } from "@angular/core";
import { BaseService } from "../../base.service";
import { Order } from "../../../models/orders/order";
import { HttpClient } from "@angular/common/http";
import { EmbeddedApiResponse } from "../../../models/response/response";

@Injectable()
export class AdminOrderService extends BaseService<Order> {

    constructor(
        http: HttpClient) {
        super('api/admin/order', http);
    }

    processOrderStatus(id: number, statusId: number) {
        let url = 'api/admin/order/' + id + '/status';
        return this.http.post<EmbeddedApiResponse<Order>>(url, statusId);
    }

    processPaymentStatus(id: number, statusId: number) {
        let url = 'api/admin/order/' + id + '/status';
        return this.http.put<EmbeddedApiResponse<Order>>(url, statusId);
    }
}