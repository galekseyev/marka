﻿import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { BaseService } from "../../base.service";
import { OrderStatus } from "../../../models/orders/order";

@Injectable()
export class AdminOrderStatusService extends BaseService<OrderStatus> {

    constructor(
        http: HttpClient) {
        super('api/admin/order-status', http);
    }
}