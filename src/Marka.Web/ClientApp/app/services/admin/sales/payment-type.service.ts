﻿import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { BaseService } from "../../base.service";
import { PaymentType } from "../../../models/orders/order";

@Injectable()
export class AdminPaymentTypeService extends BaseService<PaymentType> {

    constructor(
        http: HttpClient) {
        super('api/admin/payment-type', http);
    }
}