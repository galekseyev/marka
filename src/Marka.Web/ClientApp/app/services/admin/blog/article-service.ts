﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Article } from '../../../models/blog/article';
import { BaseService } from '../../base.service';


@Injectable()
export class AdminArticleService extends BaseService<Article> {
    constructor(
        http: HttpClient) {
        super('api/admin/article', http);
    }
}