﻿import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { ArticlePicture } from "../../../models/blog/article-picture";
import { EmbeddedApiResponse } from "../../../models/response/response";


@Injectable()
export class AdminArticlePictureService {

    constructor(private http: HttpClient) { }

    get(id: number) {
        return this.http.get<EmbeddedApiResponse<ArticlePicture>>('api/admin/article/' + id + '/picture');
    }

    save(id: number, picture: ArticlePicture) {

        if (picture.id) {
            return this.http.put<EmbeddedApiResponse<ArticlePicture>>('api/admin/article/' + id + '/picture/' + picture.id, picture);
        }
        else {
            return this.http.post<EmbeddedApiResponse<ArticlePicture>>('api/admin/article/' + id + '/picture', picture);
        }
    }

    delete(id: number, pictureId: number) {
        return this.http.delete('api/admin/article/' + id + '/picture/' + pictureId);
    }
}