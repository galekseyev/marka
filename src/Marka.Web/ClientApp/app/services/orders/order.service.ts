﻿import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { BaseService } from "../base.service";
import { Order } from "../../models/orders/order";



@Injectable()
export class OrderService extends BaseService<Order> {

    constructor(
        http: HttpClient) {
        super('api/order', http);
    }
}

@Injectable()
export class AccountOrderService extends BaseService<Order>{
    constructor(
        http: HttpClient) {
        super('api/account/order', http);
    }
}