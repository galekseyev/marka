﻿import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class LoaderService {

    private loaderSubject = new Subject<LoaderState>();

    loaderState = this.loaderSubject.asObservable();

    constructor() { }

    show() {
        this.loaderSubject.next(<LoaderState>{ isLoading: true });
    }

    hide() {
        this.loaderSubject.next(<LoaderState>{ isLoading: false });
    }
}

export interface LoaderState {
    isLoading: boolean;
}