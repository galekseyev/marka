﻿import { Injectable } from "@angular/core";
import { BaseService } from "../../base.service";
import { TranslatedArticle } from "../../../models/blog/article";
import { HttpClient } from "@angular/common/http";


@Injectable()
export class ArticleService extends BaseService<TranslatedArticle> {
    constructor(
        http: HttpClient) {
        super('api/article', http);
    }
}