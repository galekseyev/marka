﻿import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';

import { Token } from '../models/auth/token.interface';
import { AuthService } from './auth.service';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { CartService } from './carts/cart.service';

@Injectable()
export class AccountService {
    constructor(
        private httpClient: HttpClient,
        private authService: AuthService,
        private cartService: CartService) { }

    authenticate(model: any) {

        return this.httpClient.post<Token>("/api/account/login", model)
            .map(token => {

                if (token && token.token) {
                    this.authService.setToken(token);
                    this.cartService.initCartState();
                }
            })
    }

    register(model: any) {
        return this.httpClient.post<Token>("/api/account/register", model)
            .map(token => {

                if (token && token.token) {
                    this.authService.setToken(token);
                    this.cartService.initCartState();
                }
            });
    }

    logout() {
        this.authService.removeToken();
        this.cartService.initCartState();
    }

    recover(email: string) {
        return this.httpClient.post("/api/account/recover", email);
    }

    reset(model: any) {
        return this.httpClient.post("/api/account/reset", model);
    }

    validateUniqueEmail(email: string, userId?: number) {
        return this.httpClient.post("/api/account/validate-email", {email, userId})
    }
}