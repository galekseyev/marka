﻿import { Inject } from "@angular/core";
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Token } from "../models/auth/token.interface";
import { JwtHelperService } from "@auth0/angular-jwt"
import { Router } from "@angular/router";


export class AuthService {

    private userRoles = new BehaviorSubject<string[]>([]);

    public userRolesState = this.userRoles.asObservable();

    public helper = new JwtHelperService();

    constructor(
        @Inject('LOCAL_STORAGE')
        private localStorage: any,
        private router: Router) {
    }

    getToken() {
        if (this.localStorage == null)
            return null;

        return this.localStorage.getItem("_token");
    }

    setToken(token: Token) {
        if (this.localStorage == null)
            return null;

        localStorage.setItem('_token', token.token);
        this.userRoles.next(this.helper.decodeToken(token.token).rol);
    }

    removeToken() {
        if (this.localStorage == null)
            return null;

        localStorage.removeItem("_token");

        this.userRoles.next([]);
    }

    public initUserRoles() {

        if (this.isAuthenticated()) {
            let token = this.getToken();

            if (token) {
                this.userRoles.next(this.helper.decodeToken(token).rol);
            }
        } else {
            this.userRoles.next([]);
        }
    }

    public isAuthenticated() {

        let token = this.getToken();
        let helper = new JwtHelperService();

        if (token && !helper.isTokenExpired(token))
            return true;

        return false;
    }
}