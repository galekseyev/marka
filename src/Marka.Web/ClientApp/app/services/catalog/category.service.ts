﻿import { Injectable } from '@angular/core';
import { BaseService } from '../base.service';
import { HttpClient } from '@angular/common/http';
import { TranslatedCategory } from '../../models/catalog/categories/category';


@Injectable()
export class CategoryService extends BaseService<TranslatedCategory> {
    constructor(
        http: HttpClient) {
        super('api/category', http);
    }
}