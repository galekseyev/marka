﻿import { Injectable } from "@angular/core";
import { InventoryGroup, TranslatedInventoryGroup } from "../../../models/catalog/inventory-groups/inventory-group";
import { ListResponse } from "../../../models/response/list.response";
import { HttpClient } from "@angular/common/http";
import { EmbeddedApiResponse } from "../../../models/response/response";
import { BaseService } from "../../base.service";
import { TranslatedInventory } from "../../../models/catalog/inventories/inventory";


@Injectable()
export class InventoryGroupService extends BaseService<TranslatedInventoryGroup>{

    constructor(http: HttpClient) {
        super('api/inventory-group', http);
    }

    listGroups(filter: any) {
        let url = 'api/inventory-group' + this.queryString(filter);
        return this.http.get<ListResponse<EmbeddedApiResponse<InventoryGroup>>>(url);
    }
}