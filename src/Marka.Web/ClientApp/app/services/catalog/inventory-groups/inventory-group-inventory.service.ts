﻿import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { ListResponse } from "../../../models/response/list.response";
import { EmbeddedApiResponse } from "../../../models/response/response";
import { TranslatedInventoryGroupInventory } from "../../../models/catalog/inventory-groups/inventory-group-inventory";


@Injectable()
export class InventoryGroupInventoryService {

    constructor(private http: HttpClient) { }

    list(id: number) {
        return this.http.get<ListResponse<EmbeddedApiResponse<TranslatedInventoryGroupInventory>>>('api/inventory-group/' + id + '/inventory/');
    }
}