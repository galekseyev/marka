﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseService } from '../../base.service';
import { TranslatedAttribute } from '../../../models/catalog/attributes/attribute';

@Injectable()
export class AttributeService extends BaseService<TranslatedAttribute> {
    constructor(
        http: HttpClient) {
        super('api/attribute', http);
    }
}