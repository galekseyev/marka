﻿import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { BaseService } from "../../base.service";
import { TranslatedInventory } from "../../../models/catalog/inventories/inventory";


@Injectable()
export class InventoryService extends BaseService<TranslatedInventory> {

    constructor(
        http: HttpClient) {
        super('api/inventory', http);
    }
}