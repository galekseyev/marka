﻿import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { EmbeddedApiResponse } from "../../../models/response/response";
import { ListResponse } from "../../../models/response/list.response";
import { TranslatedInventoryAttribute } from "../../../models/catalog/inventories/inventory.attribute";

@Injectable()
export class InventoryAttributeService {

    constructor(private http: HttpClient) { }

    list(id: number) {
        return this.http.get<ListResponse<EmbeddedApiResponse<TranslatedInventoryAttribute>>>('api/inventory/' + id + '/attribute/');
    }
}