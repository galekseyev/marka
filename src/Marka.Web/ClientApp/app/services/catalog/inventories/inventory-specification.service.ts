﻿import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { ListResponse } from "../../../models/response/list.response";
import { EmbeddedApiResponse } from "../../../models/response/response";
import { TranslatedInventorySpecification } from "../../../models/catalog/inventories/inventory-specification";

@Injectable()
export class InventorySpecificationService {

    constructor(private http: HttpClient) { }

    list(id: number) {
        return this.http.get<ListResponse<EmbeddedApiResponse<TranslatedInventorySpecification>>>('api/inventory/' + id + '/specification/');
    }
}