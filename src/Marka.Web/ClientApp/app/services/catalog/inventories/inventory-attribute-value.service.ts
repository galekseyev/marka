﻿import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { ListResponse } from "../../../models/response/list.response";
import { EmbeddedApiResponse } from "../../../models/response/response";
import { TranslatedInventoryAttributeValue } from "../../../models/catalog/inventories/inventory.attribute.value";

@Injectable()
export class InventoryAttributeValueService {

    constructor(private http: HttpClient) { }

    list(id: number, attributeId: number) {
        return this.http.get<ListResponse<EmbeddedApiResponse<TranslatedInventoryAttributeValue>>>('api/inventory/' + id + '/attribute/' + attributeId + '/value');
    }
}