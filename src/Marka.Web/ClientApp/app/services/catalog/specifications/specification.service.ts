﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseService } from '../../base.service';
import { TranslatedSpecification } from '../../../models/catalog/specifications/specification';

@Injectable()
export class SpecificationService extends BaseService<TranslatedSpecification> {
    constructor(
        http: HttpClient) {
        super('api/specification', http);
    }
}