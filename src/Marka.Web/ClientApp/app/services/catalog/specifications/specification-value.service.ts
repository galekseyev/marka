﻿import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { TranslatedSpecificationValue } from "../../../models/catalog/specifications/specification-value";
import { EmbeddedApiResponse } from "../../../models/response/response";
import { ListResponse } from "../../../models/response/list.response";

@Injectable()
export class SpecificationValueService {

    constructor(private http: HttpClient) { }

    list(id: number) {
        return this.http.get<ListResponse<EmbeddedApiResponse<TranslatedSpecificationValue>>>('api/specification/' + id + '/value');
    }
}