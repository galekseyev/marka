﻿import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Cart, CartItem } from "../../models/carts/cart";
import { EmbeddedApiResponse } from "../../models/response/response";
import { BehaviorSubject } from "rxjs";
import { ErrorHandler } from "../../common/error.handler";
import { AuthService } from "../auth.service";
import { CartTokenService } from "./cart-token.service";

@Injectable()
export class CartService {

    private cartSubject = new BehaviorSubject<Cart>(new Cart());

    public cartState = this.cartSubject.asObservable();

    constructor(
        protected http: HttpClient,
        protected authService: AuthService,
        protected cartTokenService: CartTokenService) { }

    public initCartState() {
        this.http.get<EmbeddedApiResponse<Cart>>('api/cart/')
            .subscribe(result => {
                console.log(result.embedded);

                this.cartTokenService.setCartToken(result.embedded.key);
                this.cartSubject.next(result.embedded);
            }, error => {
                ErrorHandler.handleError(error);
            });
    }

    public create() {
        let cart = new Cart();

        this.http.post<EmbeddedApiResponse<Cart>>('api/cart/', cart)
            .subscribe(result => {
                this.cartTokenService.setCartToken(result.embedded.key);
                this.cartSubject.next(result.embedded);
            }, error => {
                ErrorHandler.handleError(error);
            });
    }

    public addItem(item: CartItem) {
        this.http.post<EmbeddedApiResponse<Cart>>('api/cart/item/', item)
            .subscribe(result => {
                this.cartSubject.next(result.embedded);
            }, error => {
                ErrorHandler.handleError(error);
            });
    }

    public updateItemQuantity(id: number, quantity: number) {
        this.http.put<EmbeddedApiResponse<Cart>>('api/cart/item/' + id, quantity)
            .subscribe(result => {
                this.cartSubject.next(result.embedded);
            }, error => {
                ErrorHandler.handleError(error);
            });
    }

    public deleteItem(cartItemId: number) {
        this.http.delete<EmbeddedApiResponse<Cart>>('api/cart/item/' + cartItemId)
            .subscribe(result => {
                this.cartSubject.next(result.embedded);
            }, error => {
                ErrorHandler.handleError(error);
            });
    }
}