﻿import { Injectable, Inject } from "@angular/core";

@Injectable()
export class CartTokenService {
    constructor(
        @Inject('LOCAL_STORAGE')
        private localStorage: any) {
    }

    public getCartToken() {
        return localStorage.getItem('_cartToken');
    }

    public setCartToken(token: string) {
        if (token)  
            localStorage.setItem('_cartToken', token);
    }
}