﻿import { Injectable } from '@angular/core';
import { BaseService } from '../base.service';
import { HttpClient } from '@angular/common/http';
import { Language } from '../../models/languages/language';


@Injectable()
export class LanguageService extends BaseService<Language> {
    constructor(
        http: HttpClient) {
        super('api/language', http);
    }
}
