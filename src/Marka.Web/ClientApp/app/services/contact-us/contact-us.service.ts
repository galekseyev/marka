﻿import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { ContactUs } from "../../models/contact/contact-us";



@Injectable()
export class ContactUsService {
    constructor(private http: HttpClient) { }

    public SendMessage(model: ContactUs) {
        return this.http.post('api/contact', model);
    }
}