﻿import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { EmbeddedApiResponse } from "../../models/response/response";
import { UserProfile } from "../../models/account/user-profile";



@Injectable()
export class AccountProfileService {

    url = "api/account/profile";

    constructor(
        private http: HttpClient) {
    }

    public get() {
        return this.http.get<EmbeddedApiResponse<UserProfile>>(this.url);
    }

    public save(model:any) {
        return this.http.post<EmbeddedApiResponse<UserProfile>>(this.url, model);
    }
}