﻿import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { BaseService } from "../base.service";
import { Country } from "../../models/countries/country";

@Injectable()
export class CountryService extends BaseService<Country> {
    constructor(
        http: HttpClient) {
        super('api/country', http);
    }
}