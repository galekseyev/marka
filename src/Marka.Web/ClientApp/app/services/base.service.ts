﻿import { HttpClient, HttpParams } from "@angular/common/http";
import { ListResponse } from "../models/response/list.response";
import { EmbeddedApiResponse } from "../models/response/response";
import { IModel } from "../interfaces/model";
import { Injectable } from "@angular/core";

@Injectable()
export class BaseService<T extends IModel>{

    constructor(
        protected baseUrl: string,
        protected http: HttpClient) { }

    public list(filter?: any) {
        let url = this.baseUrl + this.queryString(filter);

        return this.http.get<ListResponse<EmbeddedApiResponse<T>>>(url);
    }

    public get(id: number) {
        return this.http.get<EmbeddedApiResponse<T>>(this.baseUrl + '/' + id);
    }

    public save(model: T) {

        if (model.id) {
            return this.http.put<EmbeddedApiResponse<T>>(this.baseUrl + '/' + model.id, model);
        }
        else {
            return this.http.post<EmbeddedApiResponse<T>>(this.baseUrl, model);
        }
    }

    public delete(id: number) {
        return this.http.delete(this.baseUrl + '/' + id);
    }

    protected queryString(obj: any) {
        if (obj) {
            var parts = [];

            for (var property in obj) {
                var value = obj[property];
                if (value != null && value != undefined) {

                    parts.push(encodeURIComponent(property) + '=' + encodeURIComponent(value));
                }
            }

            return '?' + parts.join('&');
        }

        return '';
    }
}