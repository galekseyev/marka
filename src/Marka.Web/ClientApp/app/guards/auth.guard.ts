﻿import { Injectable } from "@angular/core";
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from "@angular/router";
import { JwtHelperService } from "@auth0/angular-jwt"
import { AuthService } from "../services/auth.service";


@Injectable()
export class AuthGuard implements CanActivate {

    constructor(
        private router: Router,
        private authService: AuthService) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

        if (this.authService.isAuthenticated()) {

            let allowRoles = route.data["roles"];

            if (allowRoles && allowRoles.length) {
                let helper = new JwtHelperService();

                let token = helper.decodeToken(this.authService.getToken());
                for (let role of token.rol) {
                    if (allowRoles.find(c => c === role.toLowerCase()))
                        return true;
                }
            }
            else {
                return true;
            }
        }

        this.router.navigate(["/login"], { queryParams: { returnUrl: state.url } });
        return false;

    }
}