﻿import { AccountService } from "../services/account.service";
import { AbstractControl, ValidationErrors } from "@angular/forms";
import { of, timer } from 'rxjs';
import { switchMap } from 'rxjs/operators';

export class UniqueEmailValidator {
    static createValidator(accountService: AccountService, userId?: number) {
        return (control: AbstractControl) => {
            return timer(500).pipe(
                switchMap(() => {
                    if (!control.value)
                        return of(null);

                    return accountService.validateUniqueEmail(control.value, userId)
                        .map(result => {
                            console.log(result);
                            //unique is a validator active state 
                            return { unique: !result };
                        })
                })
            )
        };
    }
}