﻿import { IModel } from "../../interfaces/model";

export class Language implements IModel {
    id?: number;
    title: string = '';
    code: string = '';
}