﻿export class ApiResponse {
    code: number = 200;
    message: string = 'Ok';
    errors: string[] = [];
}

export class EmbeddedApiResponse<T> extends ApiResponse{
    links: any = {};
    embedded: T = {} as T;
}