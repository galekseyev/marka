﻿export class ListResponse<T> {
    code: number = 200;
    message: string = 'Ok';
    errors: string[] = [];

    count: number = 0;
    total: number = 0;

    embedded: T[] = [];
    links: any = {}
}