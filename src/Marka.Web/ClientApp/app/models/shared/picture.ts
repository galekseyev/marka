﻿
export class Picture {

    constructor() {
        this.thumbnail = 'http://via.placeholder.com/500x750';
        this.picture = '';
    }

    thumbnail: string;
    picture: string;
}