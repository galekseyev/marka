﻿import { IModel } from "../../../interfaces/model";

export class Attribute implements IModel {
    id?: number;
    translations: AttributeTranslation[] = []
}

export class AttributeTranslation implements IModel {
    id?: number;
    languageId: number;
    title: string = '';
    description: string = '';
}

export class TranslatedAttribute implements IModel {
    id?: number;
    title: string = '';
    description: string = '';
    picture: string = '';
}