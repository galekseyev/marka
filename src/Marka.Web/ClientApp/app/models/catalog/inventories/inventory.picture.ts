﻿import { IModel } from "../../../interfaces/model";

export class InventoryPicture implements IModel {
    id?: number;
    title: string = '';
    alt: string = '';
    thumbnail: string = '';
    picture: string = '';
}