﻿import { IModel } from "../../../interfaces/model";
import { Specification, TranslatedSpecification } from "../specifications/specification";
import { SpecificationValue, TranslatedSpecificationValue } from "../specifications/specification-value";


export class InventorySpecification implements IModel {

    /** Inventory Attribute Value Id */
    id?: number;

    disabled: boolean = false;
    hidden: boolean = false;

    displayOrder: number = 0;

    specificationId: number;
    specificationValueId: number;
}

export class TranslatedInventorySpecification implements IModel {
    /** Inventory Attribute Value Id */
    id?: number;

    disabled: boolean = false;
    hidden: boolean = false;

    displayOrder: number = 0;

    specification: TranslatedSpecification;
    specificationValue: TranslatedSpecificationValue;
}