﻿import { IModel } from "../../../interfaces/model";
import { TranslatedInventoryAttributeValue } from "./inventory.attribute.value";
import { TranslatedAttribute } from "../attributes/attribute";

export class InventoryAttribute implements IModel {

    /** Inventory Attribute Id */
    id?: number;

    attributeId?: number;
    controlType?: number;

    isRequired: boolean = false;

    displayOrder: number = 0;
}


export class TranslatedInventoryAttribute implements IModel {
    /** Inventory Attribute Id */
    id?: number;

    attributeId?: number;
    attribute: TranslatedAttribute;

    controlType?: number;

    isRequired: boolean = false;

    displayOrder: number = 0;

    values: TranslatedInventoryAttributeValue[] = [];
}