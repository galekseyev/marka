﻿import { IModel } from "../../../interfaces/model";
import { InventoryPicture } from "./inventory.picture";
import { TranslatedInventoryAttribute } from "./inventory.attribute";
import { TranslatedInventorySpecification } from "./inventory-specification";

export class Inventory implements IModel {
    id?: number;

    quantity: number = 0;

    originalPrice: number = 0;
    price: number = 0;

    categoryId?: number;

    translations: InventoryTranslation[] = [];
}

export class InventoryTranslation implements IModel {
    id?: number;
    languageId: number;
    title: string = '';
    description: string = '';
}

export class TranslatedInventory implements IModel {
    id?: number;

    title: string = '';
    description: string = '';

    originalPrice: number = 0;
    price: number = 0;

    quantity: number = 0;

    pictures: InventoryPicture[] = [];
    attributes: TranslatedInventoryAttribute[] = [];
    specifications: TranslatedInventorySpecification[] = [];
}