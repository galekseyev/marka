﻿import { IModel } from "../../../interfaces/model";
import { InventoryPicture } from "./inventory.picture";

export class InventoryAttributeValue implements IModel {

    /** Inventory Attribute Value Id */
    id?: number;

    displayOrder: number = 0;

    pictureId?: number;

    translations: InventoryAttributeValueTranslation[] = [];
}

export class InventoryAttributeValueTranslation implements IModel {
    id?: number;
    languageId: number;
    title: string = '';
}

export class TranslatedInventoryAttributeValue implements IModel {

    /** Inventory Attribute Value Id */
    id?: number;

    title: string = '';
    displayOrder: number = 0;

    picture = new InventoryPicture();
}