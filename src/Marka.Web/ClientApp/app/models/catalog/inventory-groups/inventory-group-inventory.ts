﻿import { IModel } from "../../../interfaces/model";
import { InventoryPicture } from "../inventories/inventory.picture";

export class InventoryGroupInventory implements IModel {
    id?: number;
    displayOrder: number = 0;
    disabled: boolean = false;;

    inventoryId: number;
}

export class TranslatedInventoryGroupInventory implements IModel {
    id?: number;
    displayOrder: number = 0;
    disabled: boolean = false;;

    inventoryId: number;
    title: string;
    picture = new InventoryPicture();
}