﻿import { IModel } from "../../../interfaces/model";
import { InventoryGroupPicture } from "./inventory-group-picture";
import { TranslatedInventoryGroupInventory } from "./inventory-group-inventory";

export class InventoryGroup implements IModel {
    id?: number;
    displayOrder = 0;
    disabled = false;

    picture = new InventoryGroupPicture();
}

export class TranslatedInventoryGroup implements IModel {
    id?: number;
    displayOrder = 0;
    disabled = false;

    picture: InventoryGroupPicture;
    inventories: TranslatedInventoryGroupInventory[];
}