﻿import { IModel } from "../../../interfaces/model";
import { Picture } from "../../shared/picture";

export class CategoryPicture extends Picture implements IModel {
    id?: number;
    title: string = '';
    alt: string = '';
}