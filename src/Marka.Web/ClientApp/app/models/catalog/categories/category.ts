﻿import { IModel } from "../../../interfaces/model";
import { Picture } from "../../shared/picture";

export class Category implements IModel {
    id?: number;
    picture: Picture = new Picture();
    translations: CategoryTranslation[] = []
}

export class CategoryTranslation implements IModel {
    id?: number;
    languageId: number;
    title: string = '';
    description: string = '';
}

export class TranslatedCategory implements IModel{
    id?: number;
    title: string = '';
    description: string = '';
    picture: string = '';
}