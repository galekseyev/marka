﻿import { IModel } from "../../../interfaces/model";
import { TranslatedSpecificationValue } from "./specification-value";

export class Specification implements IModel {
    id?: number;
    translations: SpecificationTranslation[] = []
}

export class SpecificationTranslation implements IModel {
    id?: number;
    languageId: number;
    title: string = '';
    description: string = '';
}

export class TranslatedSpecification implements IModel {
    id?: number;
    title: string = '';
    description: string = '';
    picture: string = '';

    values: TranslatedSpecificationValue[] = [];
}