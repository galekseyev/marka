﻿import { IModel } from "../../../interfaces/model";

export class SpecificationValue implements IModel {

    /** Specification Value Id */
    id?: number;

    displayOrder: number = 0;

    translations: SpecificationValueTranslation[] = [];
}

export class SpecificationValueTranslation implements IModel {
    id?: number;
    languageId: number;
    title: string = '';
}

export class TranslatedSpecificationValue implements IModel {

    /** Specification Value Id */
    id?: number;

    title: string = '';
    displayOrder: number = 0;
}