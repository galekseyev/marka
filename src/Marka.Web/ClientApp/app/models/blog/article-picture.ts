﻿import { IModel } from "../../interfaces/model";
import { Picture } from "../shared/picture";

export class ArticlePicture extends Picture implements IModel {
    constructor() {
        super();
        this.thumbnail = 'http://via.placeholder.com/700x393';
    }

    id?: number;
    title: string = '';
    alt: string = '';
}