﻿import { Picture } from "../shared/picture";
import { IModel } from "../../interfaces/model";
import { ArticlePicture } from "./article-picture";

export class Article implements IModel {
    id?: number;
    createDate: Date;
    disabled: boolean = false;
    translations: ArticleTranslation[] = []
}

export class ArticleTranslation implements IModel {
    id?: number;
    languageId: number;
    title: string = '';
    description: string = '';
}

export class TranslatedArticle implements IModel {
    id?: number;
    title: string = '';
    description: string = '';
    disabled: boolean;
    createDate: string;
    picture: ArticlePicture;
}