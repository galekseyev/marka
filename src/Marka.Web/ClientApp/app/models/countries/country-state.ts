﻿
export class CountryState {
    id?: number;

    stateCode: string;
    stateName: string;
}