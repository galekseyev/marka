﻿import { CountryState } from "./country-state";
import { IModel } from "../../interfaces/model";

export class Country implements IModel {
    id?: number;

    countryCode: string;
    countryName: string;
    nationalIdMask: string;
    nationalIdNumber: string;
    nationalIdRegEx: string;
    postalCodeRegEx: string;

    states = new Array<CountryState>();
}