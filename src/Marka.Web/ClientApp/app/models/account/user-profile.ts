﻿import { Country } from "../countries/country";


export class UserProfile  {
    fullName: string;

    address = new UserAddress();

}

export class UserAddress {
    id?: number;
    address1: string;
    city: string;
    country = new Country();
    postalCode: string;
}
