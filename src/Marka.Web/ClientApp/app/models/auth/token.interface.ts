﻿export interface Token {
    id: string;
    token: string;
    expiration: number;
}