﻿import { IModel } from "../../interfaces/model";
import { Country } from "../countries/country";

export class Shipment implements IModel {
    /** Shipment Id */
    id?: number;
    email: string;
    fullName: string;

    address1: string;
    city: string;
    country = new Country();
    postalCode: string;
}