﻿import { IModel } from "../../interfaces/model";
import { InventoryPicture } from "../catalog/inventories/inventory.picture";
import { CatalogAttribute } from "../carts/cart";
import { Shipment } from "../shipment/shipment";


export class Order implements IModel {
    id?: number;

    orderNumber: string;
    orderStatus = new OrderStatus();

    paymentType = new PaymentType();
    paymentStatus = new PaymentStatus();

    orderItems = new Array<OrderItem>();

    subTotalPrice: number = 0;
    shippingPrice: number = 0;
    totalPrice: number = 0;

    shipment = new Shipment();

    createDate: string;
}

export class OrderItem {
    id?: number;
    inventoryId: number;
    orderItemAttributesValuesXref = new Array<CatalogAttribute>();
    quantity: number = 1;
    price: number;
    picture: InventoryPicture;
}

export class OrderStatus {
    id: number;
    statusCode: string;
}

export class PaymentType {
    id: number;
    typeCode: string;
}

export class PaymentStatus {
    id: number;
    statusCode: string;
}