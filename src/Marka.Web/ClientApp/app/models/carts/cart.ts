﻿import { IModel } from "../../interfaces/model";
import { InventoryPicture } from "../catalog/inventories/inventory.picture";

export class Cart {
    key: string;

    cartItems = new Array<CartItem>();

    subTotalPrice: number = 0;
    shippingPrice: number = 0;
    totalPrice: number = 0;
}

export class CartItem {
    id?: number;
    inventoryId: number;
    cartItemAttributesValuesXref: CatalogAttribute[] = [];
    quantity: number = 1;
    inventoryQuantity: number;
    price: number;
    picture = new InventoryPicture();

}

export class CatalogAttribute {

    constructor(attrId: number) {
        this.inventoryAttributeId = attrId;
    }

    id?: number;

    inventoryAttributeId: number;
    inventoryAttributeValueId: string;
}