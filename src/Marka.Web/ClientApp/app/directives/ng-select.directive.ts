﻿import { Directive, ElementRef, HostListener } from "@angular/core";
import { Subscription } from "rxjs";
import { FormControlName } from "@angular/forms";

@Directive({
    selector: '[ngSelectDirective]'
})
export class NgSelectDirective {

    subscription: Subscription;

    focus = false;

    constructor(
        private el: ElementRef,
        private formControlName: FormControlName
    ) { }

    ngDoCheck() {
        if (!this.focus)
            this.setActive(this.formControlName.value);
        console.log('doCheck')
    }

    @HostListener('focus') onFocus() {
        this.focus = true;
        this.setActive(true);
        console.log('focus')
    }

    @HostListener('blur') onBlur() {
        this.focus = false;
        this.setActive(this.formControlName.value);
    }

    private setActive(value) {
        const inputId = this.el.nativeElement.getAttribute('id'),
            label = document.querySelector(`label[for="${inputId}"]`);

        if (label) {
            if (value)
                label.classList.add('active');
            else
                label.classList.remove('active')
        }
    }
}
