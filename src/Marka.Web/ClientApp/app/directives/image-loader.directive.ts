﻿import { Directive, ElementRef, Input, Renderer2, HostListener } from '@angular/core';

@Directive({
    selector: '[imageLoader]'
})
export class ImageLoaderDirective {
    @Input() imageLoader;

    imgLoadingElement: any;
    imgLoaderElement:any;

    constructor(private el: ElementRef, private renderer: Renderer2) {

        this.imgLoadingElement = renderer.createElement('div');
        renderer.addClass(this.imgLoadingElement, 'image-loading');

        //this.imgLoaderElement = renderer.createElement('div');
        //renderer.addClass(this.imgLoaderElement, 'loader');

        //renderer.appendChild(this.imgLoadingElement, this.imgLoaderElement);
        renderer.appendChild(el.nativeElement, this.imgLoadingElement);
    }

    ngOnInit() {
        let image = new Image();
        image.addEventListener('load', () => {
            this.renderer.removeChild(this.el.nativeElement, this.imgLoadingElement);
            this.renderer.setStyle(this.el.nativeElement,
                'background-image',
                `url(${this.imageLoader})`);
        });
      
        image.src = this.imageLoader;
    }
}