﻿import { Directive, ElementRef, ViewContainerRef, TemplateRef, Input, OnInit } from "@angular/core";
import { Subscription } from "rxjs";
import { AuthService } from "../services/auth.service";
import { DomAdapter } from "@angular/platform-browser/src/dom/dom_adapter";


@Directive({
    selector: '[authGuard]',

})
export class AuthGuardDirective implements OnInit {

    private userRolesSubscription: Subscription;

    private roles: string[] = [];
    private userRoles;

    private isHidden = true;

    constructor(
        private element: ElementRef,
        private templateRef: TemplateRef<any>,
        private viewContainer: ViewContainerRef,
        private authService: AuthService
    ) {

    }

    @Input()
    set authGuard(val) {
        this.roles = val;
        this.updateView();
    }

    ngOnInit() {
        this.userRolesSubscription = this.authService.userRolesState.subscribe(result => {
            this.userRoles = result;
            this.updateView();
        });
    }

    ngOnDestroy() {
        this.userRolesSubscription.unsubscribe();
    }

    private hasAccess() {
        if (this.roles && this.roles.length) {

            const isAuthenticated = this.authService.isAuthenticated();
            if (this.roles.find(c => c == 'guest'))
                return !isAuthenticated;
            
            if (isAuthenticated) {
                if (Array.isArray(this.userRoles)) {
                    for (let role of this.userRoles) {
                        if (this.roles.find(c => c == role.toLowerCase()))
                            return true;
                    }
                } else {
                    if (this.userRoles)
                        return this.roles.find(c => c == this.userRoles.toLowerCase())
                }
            }
            else {
                return false;
            }
        }
        else {
            throw new DOMException('authGuard can not be used without roles provider')
        }
    }

    private updateView() {
        if (this.hasAccess()) {
            if (this.isHidden) {
                this.viewContainer.createEmbeddedView(this.templateRef);
                this.isHidden = false;
            }

        } else {
            this.isHidden = true;
            this.viewContainer.clear();
        }
    }
}