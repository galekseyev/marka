﻿import { Component, OnInit } from '@angular/core';
import { TranslatedCategory } from '../../../../models/catalog/categories/category';
import { ErrorHandler } from '../../../../common/error.handler';
import { AdminInventoryGroupService } from '../../../../services/admin/catalog/inventory-groups/inventory-group.service';
import { InventoryGroup } from '../../../../models/catalog/inventory-groups/inventory-group';


@Component({
    templateUrl: './inventory-groups.component.html',
    styleUrls: ['./inventory-groups.component.css'],
    providers: [AdminInventoryGroupService]
})

export class AdminInventoryGroupsComponent implements OnInit {
    groups: InventoryGroup[] = [];

    constructor(
        private adminInventoryGroupService: AdminInventoryGroupService) { }

    ngOnInit() {
        this.initGroups();
    }

    private initGroups() {
        this.adminInventoryGroupService.list()
            .subscribe(result => {
                this.groups = result.embedded.map(s => s.embedded);
                console.log(this.groups)
            }, error => {
                ErrorHandler.handleError(error);
            });
    }

    delete(id: number) {
        this.adminInventoryGroupService.delete(id)
            .subscribe(result => {
                let index = this.groups.findIndex(c => c.id === id);
                this.groups.splice(index, 1);
            }, error => {
                ErrorHandler.handleError(error);
            });
    }
}