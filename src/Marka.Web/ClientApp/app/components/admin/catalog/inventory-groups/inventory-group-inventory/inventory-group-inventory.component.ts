﻿import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { InventoryGroupInventory, TranslatedInventoryGroupInventory } from '../../../../../models/catalog/inventory-groups/inventory-group-inventory';
import { TranslatedInventory } from '../../../../../models/catalog/inventories/inventory';
import { ActivatedRoute, Router } from '@angular/router';
import { InventoryService } from '../../../../../services/catalog/inventories/inventory.service';
import { AdminInventoryGroupInventoryService } from '../../../../../services/admin/catalog/inventory-groups/inventory-group-inventory.service';
import { ErrorHandler } from '../../../../../common/error.handler';
import { InventoryGroupInventoryService } from '../../../../../services/catalog/inventory-groups/inventory-group-inventory.service';

@Component({
    selector: 'admin-inventory-group-inventory',
    templateUrl: './inventory-group-inventory.component.html',
    providers: [InventoryService, InventoryGroupInventoryService, AdminInventoryGroupInventoryService]
})
export class AdminInventoryGroupInventoriesComponent implements OnInit {

    //Inventory Group Id
    id: number;

    form: FormGroup;

    model = new InventoryGroupInventory();

    inventories: TranslatedInventory[] = [];
    inventoryGroupInventories: TranslatedInventoryGroupInventory[] = [];

    errors: string[] = [];

    get inventory() { return this.form.get('inventoryId'); }
    get displayOrder() { return this.form.get('displayOrder'); }

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private formBuilder: FormBuilder,
        private inventoryService: InventoryService,
        private inventoryGroupInventoryService: InventoryGroupInventoryService,
        private adminInventoryGroupInventoryService: AdminInventoryGroupInventoryService) { }

    ngOnInit() {

        this.initForm();

        this.route.parent.params.subscribe(params => {
            if (params['id']) {
                this.id = params['id'];
                this.initInventories();
                this.load();
                
            }
        })
    }

    public load() {
        this.inventoryGroupInventoryService.list(this.id)
            .subscribe(result => {
                this.inventoryGroupInventories = result.embedded.map(c => c.embedded);
                console.log(this.inventoryGroupInventories)
            }, error => ErrorHandler.handleError(error));
    }

    public save() {
        this.adminInventoryGroupInventoryService.save(this.id, this.form.value)
            .subscribe(result => {
                this.load();
                this.model = new InventoryGroupInventory();

                this.initForm();
            }, error => {
                this.errors = error.error.errors;
                ErrorHandler.handleError(error);
            });
    }

    public delete(inventoryGroupInventoryId: number) {
        this.adminInventoryGroupInventoryService.delete(this.id, inventoryGroupInventoryId)
            .subscribe(result => {
                let index = this.inventoryGroupInventories.findIndex(c => c.id === inventoryGroupInventoryId);
                this.inventoryGroupInventories.splice(index, 1);
            })
    }

    private initForm() {
        this.form = this.formBuilder.group({
            id: this.model.id,
            displayOrder: [this.model.displayOrder, [Validators.required]],
            disabled: this.model.disabled,
            inventoryId: [this.model.inventoryId, [Validators.required]],
        });
    }

    private initInventories() {
        this.inventoryService.list()
            .subscribe(result => {
                this.inventories = result.embedded.map(c => c.embedded);
            }, error => ErrorHandler.handleError(error));
    }
}