﻿import { Component } from '@angular/core';

@Component({
    template: '<router-outlet></router-outlet>'
})
export class AdminInventoryGroupComponent {
    constructor() { }

    ngOnInit() {

    }
}