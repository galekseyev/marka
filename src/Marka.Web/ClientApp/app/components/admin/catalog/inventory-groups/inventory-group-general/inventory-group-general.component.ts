﻿import { Component } from '@angular/core';
import { InventoryGroup } from '../../../../../models/catalog/inventory-groups/inventory-group';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AdminInventoryGroupService } from '../../../../../services/admin/catalog/inventory-groups/inventory-group.service';
import { ErrorHandler } from '../../../../../common/error.handler';

@Component({
    selector: 'admin-inventory-group-general',
    templateUrl: './inventory-group-general.component.html',
    providers: [AdminInventoryGroupService]
})
export class AdminInventoryGroupGeneralComponent {

    form: FormGroup;

    model = new InventoryGroup();

    errors: string[] = [];

    get displayOrder() { return this.form.get('displayOrder'); }

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private formBuilder: FormBuilder,
        private inventoryGroupService: AdminInventoryGroupService) { }

    ngOnInit() {
        this.initForm();

        this.route.parent.params.subscribe(params => {
            if (params['id']) {
                this.model.id = params['id'];
                this.load(params['id']);
            }
            else {
                this.model = new InventoryGroup();
            }
        })
    }

    public load(id: number) {
        this.inventoryGroupService.get(id)
            .subscribe(result => {
                this.model = result.embedded;
                this.initForm();
            }, error => ErrorHandler.handleError(error));
    }

    public save() {
        console.log(this.form.value)
        this.inventoryGroupService.save(this.form.value)
            .subscribe(result => {
                this.router.navigate(['/admin/inventory-group/' + result.embedded.id + '/picture']);
            }, error => {
                this.errors = error.error.errors;
                ErrorHandler.handleError(error);
            });
    }

    private initForm() {
        this.form = this.formBuilder.group({
            id: this.model.id,
            displayOrder: [this.model.displayOrder, [Validators.required]],
            disabled: this.model.disabled
        });
    }
}