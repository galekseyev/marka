﻿import { Component, Input } from '@angular/core';

@Component({
    selector: 'admin-inventory-group-menu',
    templateUrl: './inventory-group-menu.component.html',
})
/** inventory-group-menu component*/
export class AdminInventoryGroupMenuComponent {

    //Inventory Group Id
    @Input()
    id?: number;

    /** inventory-group-menu ctor */
    constructor() {

    }
}