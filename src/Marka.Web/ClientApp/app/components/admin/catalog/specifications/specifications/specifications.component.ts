﻿import { Component, OnInit } from '@angular/core';
import { AdminSpecificationService } from '../../../../../services/admin/catalog/specifications/specification.service';
import { SpecificationService } from '../../../../../services/catalog/specifications/specification.service';
import { TranslatedSpecification } from '../../../../../models/catalog/specifications/specification';
import { ErrorHandler } from '../../../../../common/error.handler';

@Component({
    templateUrl: './specifications.component.html',
    providers: [SpecificationService, AdminSpecificationService]
})

export class AdminSpecificationsComponent implements OnInit {

    specifications: TranslatedSpecification[] = [];

    constructor(
        private specificationService: SpecificationService,
        private adminSpecificationService: AdminSpecificationService) { }

    ngOnInit(): void {
        this.initSpecifications();
    }

    initSpecifications() {
        this.specificationService.list()
            .subscribe(result => {
                this.specifications = result.embedded.map(s => s.embedded);
            }, error => {
                ErrorHandler.handleError(error);
            });
    }

    delete(id: number) {
        this.adminSpecificationService.delete(id)
            .subscribe(result => {
                let index = this.specifications.findIndex(c => c.id === id);
                this.specifications.splice(index, 1);
            }, error => {
                ErrorHandler.handleError(error);
            });
    }
}