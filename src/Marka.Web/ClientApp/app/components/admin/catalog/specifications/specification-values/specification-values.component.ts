﻿import { Component } from '@angular/core';
import { FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { LanguageService } from '../../../../../services/languages/language.service';
import { Language } from '../../../../../models/languages/language';
import { SpecificationValueService } from '../../../../../services/catalog/specifications/specification-value.service';
import { AdminSpecificationValueService } from '../../../../../services/admin/catalog/specifications/specification-value.service';
import { SpecificationValue, TranslatedSpecificationValue } from '../../../../../models/catalog/specifications/specification-value';
import { ErrorHandler } from '../../../../../common/error.handler';


@Component({
    templateUrl: './specification-values.component.html',
    providers: [LanguageService, SpecificationValueService, AdminSpecificationValueService]

})
export class AdminSpecificationValuesComponent {

    //Specification Id
    id: number;

    form: FormGroup;

    model = new SpecificationValue();

    languages: Language[] = [];
    values: TranslatedSpecificationValue[] = [];
    errors: string[] = [];

    get translations() { return (this.form.controls.translations as FormArray).controls; }

    get displayOrder() { return this.form.get('displayOrder'); }

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private formBuilder: FormBuilder,
        private languageService: LanguageService,
        private specificationValueService: SpecificationValueService,
        private adminSpecificationValueService: AdminSpecificationValueService) { }

    ngOnInit() {

        this.initForm();

        this.languageService.list().subscribe(
            result => {
                this.languages = result.embedded.map(s => s.embedded);
                this.initTranslationFormGroup();
            },
            error => ErrorHandler.handleError(error));

        this.route.parent.params.subscribe(params => {
            if (params['id']) {
                this.id = params['id'];
                console.log(this.id)
                this.load();

            }
        })
    }

    public load() {
        this.specificationValueService.list(this.id)
            .subscribe(result => {
                console.log(result)
                this.values = result.embedded.map(c => c.embedded);
            }, error => ErrorHandler.handleError(error));
    }

    public save() {
        this.adminSpecificationValueService.save(this.id, this.form.value)
            .subscribe(result => {
                this.load();

                this.model = new SpecificationValue();

                this.initForm();
                this.initTranslationFormGroup();
            }, error => {
                this.errors = error.error.errors;
                ErrorHandler.handleError(error);
            });
    }

    public delete(valueId: number) {
        this.adminSpecificationValueService.delete(this.id, valueId)
            .subscribe(result => {
                let index = this.values.findIndex(c => c.id === valueId);
                this.values.splice(index, 1);
            })
    }

    private initForm() {
        this.form = this.formBuilder.group({
            id: this.model.id,
            displayOrder: [this.model.displayOrder, [Validators.required]],
            translations: this.formBuilder.array([])
        });
    }

    private initTranslationFormGroup() {
        for (let language of this.languages) {

            let translation = this.model.translations.find(c => c.languageId == language.id);

            let translationId: any;
            let title = '';

            if (translation) {
                translationId = translation.id;
                title = translation.title;
            }

            let group = this.formBuilder.group({
                id: translationId,
                languageId: language.id,
                languageTitle: language.title,
                title: [title, [Validators.required, Validators.minLength(1), Validators.maxLength(255)]]
            })

            let translations = this.form.get('translations') as FormArray;
            translations.push(group);
        }
    }
}