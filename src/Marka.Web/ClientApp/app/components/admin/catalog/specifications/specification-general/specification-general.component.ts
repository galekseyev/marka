﻿import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AdminSpecificationService } from '../../../../../services/admin/catalog/specifications/specification.service';
import { LanguageService } from '../../../../../services/languages/language.service';
import { FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms';
import { Specification } from '../../../../../models/catalog/specifications/specification';
import { Language } from '../../../../../models/languages/language';
import { ErrorHandler } from "../../../../../common/error.handler";

@Component({
    templateUrl: './specification-general.component.html',
    providers: [LanguageService, AdminSpecificationService]
})
export class AdminSpecificationGeneralComponent implements OnInit {

    form: FormGroup;

    model = new Specification();

    languages: Language[] = [];
    errors: string[] = [];

    get translations() { return (this.form.controls.translations as FormArray).controls; }

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private formBuilder: FormBuilder,
        private languageService: LanguageService,
        private attributeService: AdminSpecificationService) { }

    ngOnInit() {
        this.initForm();

        this.languageService.list().subscribe(
            result => {
                this.languages = result.embedded.map(s => s.embedded);
                this.initTranslationFormGroup();
            },
            error => ErrorHandler.handleError(error));

        this.route.parent.params.subscribe(params => {
            if (params['id']) {
                this.model.id = params['id'];
                this.load(params['id']);
            }
            else {
                this.model = new Specification();
            }
        })
    }

    public load(id: number) {
        this.attributeService.get(id)
            .subscribe(result => {
                this.model = result.embedded;

                this.initForm();
                this.initTranslationFormGroup();
            }, error => ErrorHandler.handleError(error));
    }

    public save() {
        this.attributeService.save(this.form.value)
            .subscribe(result => {
                this.router.navigate(['/admin/specifications']);
            }, error => {
                this.errors = error.error.errors;
                ErrorHandler.handleError(error);
            });
    }

    private initForm() {
        this.form = this.formBuilder.group({
            id: this.model.id,
            translations: this.formBuilder.array([])
        });
    }

    private initTranslationFormGroup() {
        for (let language of this.languages) {

            let translation = this.model.translations.find(c => c.languageId == language.id);

            let translationId: any;
            let title = '';
            let description = '';

            if (translation) {
                translationId = translation.id;
                title = translation.title;
                description = translation.description;
            }

            let group = this.formBuilder.group({
                id: translationId,
                languageId: language.id,
                languageTitle: language.title,
                title: [title, [Validators.required, Validators.minLength(1), Validators.maxLength(255)]],
                description: description
            })

            let translations = this.form.get('translations') as FormArray;
            translations.push(group);
        }
    }
}