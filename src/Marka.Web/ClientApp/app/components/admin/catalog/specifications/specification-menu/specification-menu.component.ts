﻿import { Component, Input } from '@angular/core';

@Component({
    selector: 'admin-specification-menu',
    templateUrl: './specification-menu.component.html',
})
export class AdminSpecificationMenuComponent {

    //Specification Id
    @Input()
    id?: number;

    constructor() { }
}