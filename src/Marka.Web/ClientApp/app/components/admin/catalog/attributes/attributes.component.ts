﻿import { Component, OnInit } from '@angular/core';
import { AdminAttributeService } from '../../../../services/admin/catalog/attributes/attribute.service';
import { ErrorHandler } from '../../../../common/error.handler';
import { TranslatedAttribute } from "../../../../models/catalog/attributes/attribute";
import { AttributeService } from '../../../../services/catalog/attributes/attribute.service';

@Component({
    templateUrl: './attributes.component.html',
    providers: [AttributeService, AdminAttributeService]
})

export class AdminAttributesComponent implements OnInit {

    attributes: TranslatedAttribute[] = [];

    constructor(
        private attributeService : AttributeService,
        private adminAttributeService: AdminAttributeService) { }

    ngOnInit(): void {
        this.initAttributes();
    }

    initAttributes() {
        this.attributeService.list()
            .subscribe(result => {
                this.attributes = result.embedded.map(s => s.embedded);
            },
            error => {
                ErrorHandler.handleError(error);
            });
    }

    delete(id: number) {
        this.adminAttributeService.delete(id)
            .subscribe(result => {
                let index = this.attributes.findIndex(c => c.id === id);
                this.attributes.splice(index, 1);
            }, error => {
                ErrorHandler.handleError(error);
            });
    }
}