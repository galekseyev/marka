﻿import { Component } from "@angular/core";
import { FormGroup, Validators, FormControl, FormArray, FormBuilder } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { Attribute } from "../../../../models/catalog/attributes/attribute";
import { AdminAttributeService } from "../../../../services/admin/catalog/attributes/attribute.service";
import { ErrorHandler } from "../../../../common/error.handler";
import { LanguageService } from "../../../../services/languages/language.service";
import { Language } from '../../../../models/languages/language';

@Component({
    templateUrl: './attribute.component.html',
    providers: [LanguageService, AdminAttributeService]
})

export class AdminAttributeComponent {
    form: FormGroup;

    model = new Attribute();

    languages: Language[] = [];
    errors: string[] = [];

    get translations() { return (this.form.controls.translations as FormArray).controls; }
    //get title() { return this.form.get('title'); }
    //get description() { return this.form.get('description') }

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private formBuilder: FormBuilder,
        private languageService: LanguageService,
        private attributeService: AdminAttributeService) { }

    ngOnInit() {
        this.initForm();

        this.languageService.list().subscribe(
            result => {
                this.languages = result.embedded.map(s => s.embedded);
                this.initTranslationFormGroup();
            },
            error => ErrorHandler.handleError(error));

        this.route.params.subscribe(params => {
            if (params['id']) {
                this.load(params['id']);
            }
            else {
                this.model = new Attribute();
            }
        })
    }

    public load(id: number) {
        this.attributeService.get(id)
            .subscribe(result => {
                this.model = result.embedded;

                this.initForm();
                this.initTranslationFormGroup();
            }, error => ErrorHandler.handleError(error));
    }

    public save() {
        this.attributeService.save(this.form.value)
            .subscribe(result => {
                this.router.navigate(['/admin/attributes']);
            }, error => {
                this.errors = error.error.errors;
                ErrorHandler.handleError(error);
            });
    }

    private initForm() {
        this.form = this.formBuilder.group({
            id: this.model.id,
            translations: this.formBuilder.array([])
        });
    }

    private initTranslationFormGroup() {
        for (let language of this.languages) {

            let translation = this.model.translations.find(c => c.languageId == language.id);

            let translationId: any;
            let title = '';
            let description = '';

            if (translation) {
                translationId = translation.id;
                title = translation.title;
                description = translation.description;
            }

            let group = this.formBuilder.group({
                id: translationId,
                languageId: language.id,
                languageTitle: language.title,
                title: [title, [Validators.required, Validators.minLength(1), Validators.maxLength(255)]],
                description: description
            })

            let translations = this.form.get('translations') as FormArray;
            translations.push(group);
        }
    }
}