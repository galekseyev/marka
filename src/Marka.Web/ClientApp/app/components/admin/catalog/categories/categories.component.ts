﻿import { Component, OnInit } from '@angular/core';
import { TranslatedCategory } from '../../../../models/catalog/categories/category';
import { ErrorHandler } from '../../../../common/error.handler';
import { CategoryService } from '../../../../services/catalog/category.service';
import { AdminCategoryService } from '../../../../services/admin/catalog/categories/category.service';


@Component({
    templateUrl: './categories.component.html',
    providers: [CategoryService, AdminCategoryService]
})

export class AdminCategoriesComponent implements OnInit {
    categories: TranslatedCategory[] = [];

    constructor(
        private categoryService: CategoryService,
        private adminCategoryService: AdminCategoryService) { }

    ngOnInit() {
        this.initCategories();
    }

    initCategories() {
        this.categoryService.list()
            .subscribe(result => {
                this.categories = result.embedded.map(s => s.embedded);
            }, error => {
                ErrorHandler.handleError(error);
            });
    }

    delete(id: number) {
        this.adminCategoryService.delete(id)
            .subscribe(result => {
                let index = this.categories.findIndex(c => c.id === id);
                this.categories.splice(index, 1);
            }, error => {
                ErrorHandler.handleError(error);
            });
    }
}