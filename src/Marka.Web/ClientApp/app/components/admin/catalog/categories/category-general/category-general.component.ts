﻿import { Component } from '@angular/core';
import { FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ErrorHandler } from '../../../../../common/error.handler';
import { LanguageService } from '../../../../../services/languages/language.service';
import { AdminCategoryService } from '../../../../../services/admin/catalog/categories/category.service';
import { Category } from '../../../../../models/catalog/categories/category';
import { Language } from '../../../../../models/languages/language';

@Component({
    templateUrl: './category-general.component.html',
    providers: [LanguageService, AdminCategoryService]
})
export class AdminCategoryGeneralComponent {

    form: FormGroup;

    model = new Category();

    languages: Language[] = [];
    errors: string[] = [];

    get translations() { return (this.form.controls.translations as FormArray).controls; }

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private formBuilder: FormBuilder,
        private languageService: LanguageService,
        private categoryService: AdminCategoryService) { }

    ngOnInit() {
        this.initForm();

        this.languageService.list().subscribe(
            result => {
                this.languages = result.embedded.map(s => s.embedded);
                this.initTranslationFormGroup();
            },
            error => ErrorHandler.handleError(error));

        this.route.parent.params.subscribe(params => {
            if (params['id']) {
                this.model.id = params['id'];
                this.load(params['id']);
            }
            else {
                this.model = new Category();
            }
        })
    }

    public load(id: number) {
        this.categoryService.get(id)
            .subscribe(result => {
                this.model = result.embedded;

                this.initForm();
                this.initTranslationFormGroup();
            }, error => ErrorHandler.handleError(error));
    }

    public save() {
        this.categoryService.save(this.form.value)
            .subscribe(result => {
                this.router.navigate(['/admin/category/' + result.embedded.id + '/picture']);
            }, error => {
                this.errors = error.error.errors;
                ErrorHandler.handleError(error);
            });
    }

    private initForm() {
        this.form = this.formBuilder.group({
            id: this.model.id,
            translations: this.formBuilder.array([])
        });
    }

    private initTranslationFormGroup() {
        for (let language of this.languages) {

            let translation = this.model.translations.find(c => c.languageId == language.id);

            let translationId: any;
            let title = '';
            let description = '';

            if (translation) {
                translationId = translation.id;
                title = translation.title;
                description = translation.description;
            }

            let group = this.formBuilder.group({
                id: translationId,
                languageId: language.id,
                languageTitle: language.title,
                title: [title, [Validators.required, Validators.minLength(1), Validators.maxLength(255)]],
                description: description
            })

            let translations = this.form.get('translations') as FormArray;
            translations.push(group);
        }
    }
}