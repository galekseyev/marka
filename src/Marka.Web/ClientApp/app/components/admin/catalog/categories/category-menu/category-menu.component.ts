﻿import { Component, Input } from '@angular/core';

@Component({
    selector: 'admin-category-menu',
    templateUrl: './category-menu.component.html'
})
export class AdminCategoryMenuComponent {

    //Category Id
    @Input()
    id?: number;

    constructor() {

    }
}