﻿import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ErrorHandler } from '../../../../../common/error.handler';
import { CategoryPicture } from '../../../../../models/catalog/categories/category-picture';
import { AdminCategoryPictureService } from '../../../../../services/admin/catalog/categories/category-picture.service';

@Component({
    selector: 'admin-category-picture',
    templateUrl: './category-picture.component.html',
    providers: [AdminCategoryPictureService]
})
export class AdminCategoryPictureComponent implements OnInit {

    //Category Id
    id: number = 0;

    form: FormGroup;

    model = new CategoryPicture();

    errors: string[] = [];

    get title() { return this.form.get('title'); }
    get alt() { return this.form.get('alt'); }

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private formBuilder: FormBuilder,
        private categoryPictureService: AdminCategoryPictureService) {
    }

    ngOnInit() {
        this.initForm();

        this.route.parent.params.subscribe(params => {
            if (params['id']) {
                this.id = params['id'];
            }
        });

        this.load();
    }

    public load() {
        this.categoryPictureService.get(this.id)
            .subscribe(result => {
                if (result) {
                    this.model = result.embedded;
                    this.initForm();
                }
            }, error => {
                ErrorHandler.handleError(error);
            });
    }

    public save() {
        let request: any = {};

        request.id = this.form.value.id;
        request.title = this.form.value.title;
        request.alt = this.form.value.alt;
        request.picture = this.form.value.picture.picture;
        request.thumbnail = this.form.value.picture.thumbnail;

        this.categoryPictureService.save(this.id, request)
            .subscribe(result => {
                this.router.navigate(['/admin/categories']);
            }, error => {
                this.errors = error.error.errors;
                ErrorHandler.handleError(error);
            });
    }

    private initForm() {
        let picture: any = {};

        picture.thumbnail = this.model.thumbnail;
        picture.picture = this.model.picture;

        this.form = this.formBuilder.group({
            id: this.model.id,
            title: [this.model.title, [Validators.required, Validators.minLength(1), Validators.maxLength(255)]],
            alt: [this.model.alt, [Validators.required, Validators.minLength(1), Validators.maxLength(255)]],
            picture: picture
        });
    }
}