﻿import { Component, OnInit } from '@angular/core';
import { LanguageService } from '../../../../../services/languages/language.service';
import { AdminInventoryAttributeValueService } from '../../../../../services/admin/catalog/inventories/inventory.attribute.value.service';
import { InventorySpecificationService } from '../../../../../services/catalog/inventories/inventory-specification.service';
import { AdminInventorySpecificationService } from '../../../../../services/admin/catalog/inventories/inventory-specification.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { InventorySpecification, TranslatedInventorySpecification } from '../../../../../models/catalog/inventories/inventory-specification';
import { Language } from '../../../../../models/languages/language';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslatedSpecification, Specification } from '../../../../../models/catalog/specifications/specification';
import { TranslatedSpecificationValue } from '../../../../../models/catalog/specifications/specification-value';
import { SpecificationValueService } from '../../../../../services/catalog/specifications/specification-value.service';
import { SpecificationService } from '../../../../../services/catalog/specifications/specification.service';
import { ErrorHandler } from '../../../../../common/error.handler';

@Component({
    templateUrl: './inventory-specifications.component.html',
    providers: [SpecificationService, SpecificationValueService, InventorySpecificationService, AdminInventorySpecificationService]
})

export class AdminInventorySpecificationsComponent implements OnInit {

    //Inventory Id
    id: number;

    form: FormGroup;

    model = new InventorySpecification();

    specifications: TranslatedSpecification[] = [];
    specificationValues: TranslatedSpecificationValue[] = [];

    inventorySpecifications: TranslatedInventorySpecification[] = [];

    errors: string[] = [];

    get specification() { return this.form.get('specificationId'); }
    get specificationValue() { return this.form.get('specificationValueId'); }
    get hidden() { return this.form.get('hidden'); }
    get displayOrder() { return this.form.get('displayOrder'); }

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private formBuilder: FormBuilder,
        private specificationService: SpecificationService,
        private specificationValueService: SpecificationValueService,
        private inventorySpecificationService: InventorySpecificationService,
        private adminInventorySpecificationService: AdminInventorySpecificationService) { }

    ngOnInit() {

        this.initForm();

        this.route.parent.params.subscribe(params => {
            if (params['id']) {
                this.id = params['id'];
                this.load();
                this.initSpecifications();
            }
        })
    }

    public load() {
        this.inventorySpecificationService.list(this.id)
            .subscribe(result => {
                this.inventorySpecifications = result.embedded.map(c => c.embedded);
                console.log(this.inventorySpecifications);
            }, error => ErrorHandler.handleError(error));
    }

    public save() {
        this.adminInventorySpecificationService.save(this.id, this.form.value)
            .subscribe(result => {
                this.load();
                this.model = new InventorySpecification();

                this.initForm();
            }, error => {
                this.errors = error.error.errors;
                ErrorHandler.handleError(error);
            });
    }

    public delete(specId: number) {
        this.adminInventorySpecificationService.delete(this.id, specId)
            .subscribe(result => {
                let index = this.inventorySpecifications.findIndex(c => c.id === specId);
                this.inventorySpecifications.splice(index, 1);
            })
    }

    private initForm() {
        this.form = this.formBuilder.group({
            id: this.model.id,
            displayOrder: [this.model.displayOrder, [Validators.required]],
            hidden: [this.model.hidden, [Validators.required]],
            specificationId: [this.model.specificationId, [Validators.required]],
            specificationValueId: [this.model.specificationValueId, [Validators.required]]
        });
    }

    private initSpecifications() {
        this.specificationService.list()
            .subscribe(result => {
                this.specifications = result.embedded.map(c => c.embedded);
            }, error => ErrorHandler.handleError(error));
    }

    public onSpecificationChange(specification: Specification) {
        if (specification) {
            this.specificationValueService.list(specification.id)
                .subscribe(result => {
                    this.specificationValues = result.embedded.map(c => c.embedded);
                }, error => ErrorHandler.handleError(error));
        }
        else {
            this.specificationValue.reset();
            this.specificationValues = [];
        }
    }
}