﻿import { Component, OnInit, Input } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { AdminInventoryAttributeService } from "../../../../../../services/admin/catalog/inventories/inventory.attribute.service";
import { AdminAttributeService } from "../../../../../../services/admin/catalog/attributes/attribute.service";


@Component({
    templateUrl: './inventory-attribute.component.html',
    providers: [AdminAttributeService, AdminInventoryAttributeService]
})

export class AdminInventoryAttributeComponent {
    //Inventory Id
    id?: number;

    //Attribute Id
    attributeId?: number;
    /** admin-inventory-attribute-menu ctor */
    constructor(private route: ActivatedRoute) {

    }

    ngOnInit() {
        this.route.parent.params.subscribe(params => {
            if (params['id']) {
                this.id = params['id'];
            }
        })

        this.route.params.subscribe(params => {

            if (params['attributeId']) {
                this.attributeId = params['attributeId']
            }
        })
    }
  
}