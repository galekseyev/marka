﻿import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormArray, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { InventoryAttributeValue, TranslatedInventoryAttributeValue } from '../../../../../../../models/catalog/inventories/inventory.attribute.value';
import { InventoryPicture } from '../../../../../../../models/catalog/inventories/inventory.picture';
import { AdminInventoryPictureService } from '../../../../../../../services/admin/catalog/inventories/inventory.picture.service';
import { AdminInventoryAttributeValueService } from '../../../../../../../services/admin/catalog/inventories/inventory.attribute.value.service';
import { Language } from '../../../../../../../models/languages/language';
import { LanguageService } from '../../../../../../../services/languages/language.service';
import { ErrorHandler } from '../../../../../../../common/error.handler';
import { InventoryAttributeService } from '../../../../../../../services/catalog/inventories/inventory-attribute.service';
import { InventoryAttributeValueService } from '../../../../../../../services/catalog/inventories/inventory-attribute-value.service';

@Component({
    templateUrl: './inventory-attribute-values.component.html',
    providers: [LanguageService, InventoryAttributeValueService, AdminInventoryPictureService, AdminInventoryAttributeValueService]
})

export class AdminInventoryAttributeValuesComponent implements OnInit {

    //Inventory Id
    id: number;

    //Inventory Attribute Id
    attributeId: number;

    form: FormGroup;

    model = new InventoryAttributeValue();

    languages: Language[] = [];
    pictures: InventoryPicture[] = [];
    values: TranslatedInventoryAttributeValue[] = [];
    errors: string[] = [];

    get translations() { return (this.form.controls.translations as FormArray).controls; }

    get displayOrder() { return this.form.get('displayOrder'); }

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private formBuilder: FormBuilder,
        private languageService: LanguageService,
        private inventoryAttributeValueService: InventoryAttributeValueService,
        private adminInventoryPictureService: AdminInventoryPictureService,
        private adminInventoryAttributeValueService: AdminInventoryAttributeValueService) { }

    ngOnInit() {

        this.initForm();

        this.languageService.list().subscribe(
            result => {
                this.languages = result.embedded.map(s => s.embedded);
                this.initTranslationFormGroup();
            },
            error => ErrorHandler.handleError(error));

        this.route.parent.parent.params.subscribe(params => {
            if (params['id']) {
                this.id = params['id'];
                this.initPictures(this.id);
            }
        })

        this.route.parent.params.subscribe(params => {
            if (params['attributeId']) {
                this.attributeId = params['attributeId'];
                this.load();
            }
        })
    }

    public load() {
        this.inventoryAttributeValueService.list(this.id, this.attributeId)
            .subscribe(result => {
                this.values = result.embedded.map(c => c.embedded);

            }, error => ErrorHandler.handleError(error));
    }

    public save() {
        this.adminInventoryAttributeValueService.save(this.id, this.attributeId, this.form.value)
            .subscribe(result => {
                this.load();
                this.model = new InventoryAttributeValue();

                this.initForm();
                this.initTranslationFormGroup();
            }, error => {
                this.errors = error.error.errors;
                ErrorHandler.handleError(error);
            });
    }

    public delete(valueId: number) {
        this.adminInventoryAttributeValueService.delete(this.id, this.attributeId, valueId)
            .subscribe(result => {
                let index = this.values.findIndex(c => c.id === valueId);
                this.values.splice(index, 1);
            })
    }

    private initForm() {
        this.form = this.formBuilder.group({
            id: this.model.id,
            displayOrder: [this.model.displayOrder, [Validators.required]],
            pictureId: this.model.pictureId,
            translations: this.formBuilder.array([])
        });
    }

    private initTranslationFormGroup() {
        for (let language of this.languages) {

            let translation = this.model.translations.find(c => c.languageId == language.id);

            let translationId: any;
            let title = '';

            if (translation) {
                translationId = translation.id;
                title = translation.title;
            }

            let group = this.formBuilder.group({
                id: translationId,
                languageId: language.id,
                languageTitle: language.title,
                title: [title, [Validators.required, Validators.minLength(1), Validators.maxLength(255)]]
            })

            let translations = this.form.get('translations') as FormArray;
            translations.push(group);
        }
    }

    private initPictures(id: number) {
        this.adminInventoryPictureService.list(id)
            .subscribe(result => {
                this.pictures = result.embedded.map(c => c.embedded);
            }, error => ErrorHandler.handleError(error));
    }

    
}