﻿import { Component, OnInit } from '@angular/core';
import { TranslatedInventory } from '../../../../models/catalog/inventories/inventory';
import { ErrorHandler } from '../../../../common/error.handler';
import { AdminInventoryService } from '../../../../services/admin/catalog/inventories/inventory.service';

@Component({
    templateUrl: './inventories.component.html',
    providers: [AdminInventoryService]
})

export class AdminInventoriesComponent implements OnInit {
    inventories: TranslatedInventory[] = [];

    constructor(private inventoryService: AdminInventoryService) { }

    ngOnInit() {
        this.initInventories();
    }

    initInventories() {
        this.inventoryService.getTranslatedInventories()
            .subscribe(result => {
                this.inventories = result.embedded.map(c => c.embedded);
            }, error => {
                ErrorHandler.handleError(error);
            });
    }

    delete(id: number) {
        this.inventoryService.delete(id)
            .subscribe(result => {
                let index = this.inventories.findIndex(c => c.id === id);
                this.inventories.splice(index, 1);
            }, error => {
                ErrorHandler.handleError(error);
            });
    }
}