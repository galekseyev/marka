﻿import { Component, OnInit, Input } from "@angular/core";
import { AdminInventoryPictureService } from "../../../../../services/admin/catalog/inventories/inventory.picture.service";
import { Picture } from "../../../../../models/shared/picture";
import { InventoryPicture } from "../../../../../models/catalog/inventories/inventory.picture";
import { ActivatedRoute, Router } from "@angular/router";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { ErrorHandler } from '../../../../../common/error.handler';

@Component({
    selector: 'inventory-picture',
    templateUrl: './inventory-picture.component.html',
    styleUrls: ['./inventory-picture.component.css'],
    providers: [AdminInventoryPictureService]
})

export class AdminInventoryPictureComponent implements OnInit {

    //Inventory Id
    id: number = 0;

    form: FormGroup;

    pictures: InventoryPicture[] = [];

    errors: string[] = [];

    get title() { return this.form.get('title'); }
    get alt() { return this.form.get('alt'); }

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private formBuilder: FormBuilder,
        private inventoryPictureService: AdminInventoryPictureService) { }

    ngOnInit() {
        this.initForm()

        this.route.parent.params.subscribe(params => {
            if (params['id']) {
                this.id = params['id'];
            }
        })

        this.load();
    }

    public load() {
        this.inventoryPictureService.list(this.id)
            .subscribe(result => {
                this.pictures = result.embedded.map(c => c.embedded);
            }, error => {
                ErrorHandler.handleError(error);
            });
    }

    public save() {
        let request: any = {};

        request.id = this.form.value.id;
        request.title = this.form.value.title;
        request.alt = this.form.value.alt;
        request.picture = this.form.value.picture.picture;
        request.thumbnail = this.form.value.picture.thumbnail;

        this.inventoryPictureService.save(this.id, request)
            .subscribe(result => {
                this.pictures.push(result.embedded);
                this.initForm();
            }, error => {
                this.errors = error.error.errors;
                ErrorHandler.handleError(error);
            });
    }

    public delete(pictureId: number) {

        this.inventoryPictureService.delete(this.id, pictureId)
            .subscribe(result => {
                let index = this.pictures.findIndex(c => c.id === pictureId);
                this.pictures.splice(index, 1);
            }, error => {
                console.log(error);
            })
    }

    private initForm() {
        this.form = this.formBuilder.group({
            id: '',
            title: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(255)]],
            alt: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(255)]],
            picture: new Picture()
        });
    }
}