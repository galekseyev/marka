﻿import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'admin-inventory-attribute-menu',
    templateUrl: './inventory-attribute-menu.component.html'
})
export class AdminInventoryAttributeMenuComponent {

    //Inventory Id
    @Input()
    id: number;

    //Attribute Id
    @Input()
    attributeId?: number;

    constructor(private route: ActivatedRoute) { }
}