﻿import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from '@angular/router';
import { TranslatedInventoryAttribute } from "../../../../../models/catalog/inventories/inventory.attribute";
import { AdminInventoryAttributeService } from "../../../../../services/admin/catalog/inventories/inventory.attribute.service";
import { InventoryAttributeService } from "../../../../../services/catalog/inventories/inventory-attribute.service";
import { ErrorHandler } from '../../../../../common/error.handler';

@Component({
    templateUrl: './inventory-attributes.component.html',
    providers: [InventoryAttributeService, AdminInventoryAttributeService]
})

export class AdminInventoryAttributesComponent implements OnInit {

    id: number;

    attributes: TranslatedInventoryAttribute[] = [];

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private inventoryAttributeService: InventoryAttributeService,
        private adminInventoryAttributeService: AdminInventoryAttributeService) { }

    ngOnInit() {
        this.route.parent.params.subscribe(params => {
            if (params['id']) {
                this.id = params['id'];
                this.initAttributes(this.id);
            }
        })
    }

    initAttributes(id: number) {
        this.inventoryAttributeService.list(id)
            .subscribe(result => {
                this.attributes = result.embedded.map(c => c.embedded);
            }, error => {
                ErrorHandler.handleError(error);
            });
    }

    delete(id: number, attributeId: number) {

        this.adminInventoryAttributeService.delete(id, attributeId)
            .subscribe(result => {
                let index = this.attributes.findIndex(c => c.id === attributeId);
                this.attributes.splice(index, 1);
            }, error => {
                console.log(error);
            })
    }
}