﻿import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, Validators, FormArray, FormBuilder } from '@angular/forms';
import { Inventory } from '../../../../../models/catalog/inventories/inventory';
import { CategoryService } from '../../../../../services/catalog/category.service';
import { TranslatedCategory } from '../../../../../models/catalog/categories/category';
import { ErrorHandler } from '../../../../../common/error.handler';
import { AdminInventoryService } from '../../../../../services/admin/catalog/inventories/inventory.service';
import { LanguageService } from '../../../../../services/languages/language.service';
import { Language } from '../../../../../models/languages/language';

@Component({
    templateUrl: './inventory-general.component.html',
    providers: [LanguageService, CategoryService, AdminInventoryService]
})

export class AdminInventoryGeneralComponent implements OnInit {
    form: FormGroup;

    model = new Inventory();

    languages: Language[] = [];
    categories: TranslatedCategory[] = [];
    errors: string[] = [];

    get translations() { return (this.form.controls.translations as FormArray).controls; }

    get category() { return this.form.get('categoryId'); }
    get quantity() { return this.form.get('quantity'); }
    get price() { return this.form.get('price'); }

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private formBuilder: FormBuilder,
        private languageService: LanguageService,
        private categoryService: CategoryService,
        private inventoryService: AdminInventoryService) { }

    ngOnInit() {
        this.initForm();

        this.languageService.list().subscribe(
            result => {
                this.languages = result.embedded.map(s => s.embedded);
                this.initTranslationFormGroup();
            },
            error => ErrorHandler.handleError(error));

        this.initCategories();
        this.initData();
    }

    public load(id: number) {
        this.inventoryService.get(id)
            .subscribe(
                result => {
                    this.model = result.embedded;

                    this.initForm();
                    this.initTranslationFormGroup();
                }, error => ErrorHandler.handleError(error));
    }

    public save() {
        this.inventoryService.save(this.form.value)
            .subscribe(result => {
                this.router.navigate(['/admin/inventory/' + result.embedded.id + '/pictures']);
            }, error => {
                this.errors = error.error.errors;
                ErrorHandler.handleError(error);
            });
    }

    private initForm() {
        this.form = this.formBuilder.group({
            id: this.model.id,
            categoryId: [this.model.categoryId, [Validators.required]],
            quantity: [this.model.quantity, [Validators.required]],
            price: [this.model.price, [Validators.required]],
            translations: this.formBuilder.array([])
        });
    }

    private initTranslationFormGroup() {
        for (let language of this.languages) {

            let translation = this.model.translations.find(c => c.languageId == language.id);

            let translationId: any;
            let title = '';
            let description = '';

            if (translation) {
                translationId = translation.id;
                title = translation.title;
                description = translation.description;
            }

            let group = this.formBuilder.group({
                id: translationId,
                languageId: language.id,
                languageTitle: language.title,
                title: [title, [Validators.required, Validators.minLength(1), Validators.maxLength(255)]],
                description: description
            })

            let translations = this.form.get('translations') as FormArray;
            translations.push(group);
        }
    }

    private initCategories() {
        this.categoryService.list()
            .subscribe(
                result => this.categories = result.embedded.map(s => s.embedded),
                er => ErrorHandler.handleError(er));
    }

    private initData() {
        this.route.parent.params.subscribe(params => {
            if (params['id']) {
                this.model.id = params['id'];
                this.load(params['id']);
            }
            else {
                this.model = new Inventory();
            }
        })
    }
}