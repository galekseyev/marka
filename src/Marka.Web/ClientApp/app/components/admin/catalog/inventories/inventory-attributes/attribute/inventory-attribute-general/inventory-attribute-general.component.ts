﻿import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AdminInventoryAttributeService } from '../../../../../../../services/admin/catalog/inventories/inventory.attribute.service';
import { InventoryAttribute } from '../../../../../../../models/catalog/inventories/inventory.attribute';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { TranslatedAttribute } from '../../../../../../../models/catalog/attributes/attribute';
import { AttributeService } from '../../../../../../../services/catalog/attributes/attribute.service';
import { ErrorHandler } from '../../../../../../../common/error.handler';

@Component({
    selector: 'admin-inventory-attribute-general',
    templateUrl: './inventory-attribute-general.component.html',
    providers: [AttributeService, AdminInventoryAttributeService]
})
/** inventory-attribute-general component*/
export class AdminInventoryAttributeGeneralComponent {

    //Inventory Id
    id: number;

    //Inventory Attribute Id
    attributeId?: number;


    form: FormGroup;

    model = new InventoryAttribute();

    attributes: TranslatedAttribute[] = [];

    controlTypes: any[] = [];
    errors: string[] = [];

    get attribute() { return this.form.get('attributeId'); }
    get controlType() { return this.form.get('controlType'); }
    get displayOrder() { return this.form.get('displayOrder'); }

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private formBuilder: FormBuilder,
        private attributeService: AttributeService,
        private inventoryAttributeService: AdminInventoryAttributeService) { }

    ngOnInit(): void {
        this.initForm();

        this.initControlTypes();
        this.initAttributes();

        this.route.parent.parent.params.subscribe(params => {
            if (params['id']) {
                this.id = params['id'];
            }
        })

        this.route.parent.params.subscribe(params => {
            if (params['attributeId']) {
                this.attributeId = params['attributeId'];
                this.load(this.id, this.attributeId);
            }
            else {
                this.model = new InventoryAttribute();
            }
        })
    }

    public load(id: number, attributeId: number) {
        this.inventoryAttributeService.get(id, attributeId)
            .subscribe(result => {
                this.model = result.embedded;
                this.initForm();
            }, error => ErrorHandler.handleError(error));
    }

    public save() {
        this.inventoryAttributeService.save(this.id, this.form.value)
            .subscribe(result => {
                this.router.navigate(['/admin/inventory/' + this.id + '/attribute/' + result.embedded.id + '/values']);
            }, error => {
                this.errors = error.error.errors;
                ErrorHandler.handleError(error);
            });
    }

    private initForm() {
        this.form = this.formBuilder.group({
            id: this.model.id,
            attributeId: [this.model.attributeId, [Validators.required]],
            controlType: [this.model.controlType, [Validators.required]],
            displayOrder: [this.model.displayOrder, [Validators.required]],
            isRequired: this.model.isRequired
        });
    }

    private initControlTypes() {
        this.controlTypes = [
            { "title": "Links", "id": "Links" }
        ]
    }

    private initAttributes() {
        this.attributeService.list()
            .subscribe(result => {
                this.attributes = result.embedded.map(s => s.embedded);
            }, error => {
                ErrorHandler.handleError(error);
            });
    }
}