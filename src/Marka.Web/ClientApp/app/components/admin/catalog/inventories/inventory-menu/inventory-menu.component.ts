﻿import { Component, Input } from '@angular/core';

@Component({
    selector: 'admin-inventory-menu',
    templateUrl: './inventory-menu.component.html'
})
export class AdminInventoryMenuComponent {

    //Inventory Id
    @Input()
    id?: number;

    constructor() {

    }
}