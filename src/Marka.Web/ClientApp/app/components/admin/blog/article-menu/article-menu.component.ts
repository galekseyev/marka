﻿import { Component, Input } from '@angular/core';

@Component({
    selector: 'admin-article-menu',
    templateUrl: './article-menu.component.html',
})
export class AdminArticleMenuComponent {
    //Article Id
    @Input()
    id?: number;

    constructor() {

    }
}