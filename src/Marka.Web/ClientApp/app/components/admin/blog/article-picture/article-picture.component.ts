﻿import { Component, OnInit } from '@angular/core';
import { ErrorHandler } from '../../../../common/error.handler';
import { AdminArticlePictureService } from '../../../../services/admin/blog/article-picture.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ArticlePicture } from '../../../../models/blog/article-picture';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
    templateUrl: './article-picture.component.html',
    providers: [AdminArticlePictureService]
})
export class AdminArticlePictureComponent implements OnInit {

    //Article Id
    id: number = 0;

    form: FormGroup;

    model = new ArticlePicture();

    errors: string[] = [];

    get title() { return this.form.get('title'); }
    get alt() { return this.form.get('alt'); }

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private formBuilder: FormBuilder,
        private articlePictureService: AdminArticlePictureService) {
    }

    ngOnInit() {
        this.initForm();

        this.route.parent.params.subscribe(params => {
            if (params['id']) {
                this.id = params['id'];
            }
        });

        this.load();
    }

    public load() {
        this.articlePictureService.get(this.id)
            .subscribe(result => {
                if (result) {
                    this.model = result.embedded;
                    this.initForm();
                }
            }, error => {
                ErrorHandler.handleError(error);
            });
    }

    public save() {
        let request: any = {};

        request.id = this.form.value.id;
        request.title = this.form.value.title;
        request.alt = this.form.value.alt;
        request.picture = this.form.value.picture.picture;
        request.thumbnail = this.form.value.picture.thumbnail;

        this.articlePictureService.save(this.id, request)
            .subscribe(result => {
                this.router.navigate(['/admin/articles']);
            }, error => {
                this.errors = error.error.errors;
                ErrorHandler.handleError(error);
            });
    }

    private initForm() {
        let picture: any = {};

        picture.thumbnail = this.model.thumbnail;
        picture.picture = this.model.picture;

        this.form = this.formBuilder.group({
            id: this.model.id,
            title: [this.model.title, [Validators.required, Validators.minLength(1), Validators.maxLength(255)]],
            alt: [this.model.alt, [Validators.required, Validators.minLength(1), Validators.maxLength(255)]],
            picture: picture
        });
    }
}