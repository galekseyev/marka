﻿import { Component } from '@angular/core';
import { LanguageService } from '../../../../services/languages/language.service';
import { AdminArticleService } from '../../../../services/admin/blog/article-service';
import { FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms';
import { Language } from '../../../../models/languages/language';
import { Article } from '../../../../models/blog/article';
import { ActivatedRoute, Router } from '@angular/router';
import { ErrorHandler } from '../../../../common/error.handler';

@Component({
    templateUrl: './article-general.component.html',
    providers: [LanguageService, AdminArticleService]
})
export class AdminArticleGeneralComponent {

    form: FormGroup

    model = new Article();

    languages: Language[] = [];
    errors: string[] = [];

    loading = false;

    get translations() { return (this.form.controls.translations as FormArray).controls; }

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private formBuilder: FormBuilder,
        private languageService: LanguageService,
        private articleService: AdminArticleService) { }

    ngOnInit() {
        this.initForm();

        this.languageService.list().subscribe(
            result => {
                this.languages = result.embedded.map(s => s.embedded);
                this.initTranslationFormGroup();
            },
            error => ErrorHandler.handleError(error));

        this.route.parent.params.subscribe(params => {
            if (params['id']) {
                this.model.id = params['id'];
                this.loading = true;
                this.load(params['id']);
            }
            else {
                this.model = new Article();
            }
        })
    }

    public load(id: number) {
        this.articleService.get(id)
            .subscribe(result => {
                this.model = result.embedded;

                this.initForm();
                this.initTranslationFormGroup();
                this.loading = false;
            }, error => ErrorHandler.handleError(error));
    }

    public save() {
        console.log(this.form.value);
        this.articleService.save(this.form.value)
            .subscribe(result => {
                this.router.navigate(['/admin/article/' + result.embedded.id + '/picture']);
            }, error => {
                this.errors = error.error.errors;
                ErrorHandler.handleError(error);
            });
    }

    private initForm() {
        this.form = this.formBuilder.group({
            id: this.model.id,
            createDate: this.model.createDate,
            disabled: this.model.disabled,
            translations: this.formBuilder.array([])
        });
    }

    private initTranslationFormGroup() {
        for (let language of this.languages) {

            let translation = this.model.translations.find(c => c.languageId == language.id);

            let translationId: any;
            let title = '';
            let description = '';

            if (translation) {
                translationId = translation.id;
                title = translation.title;
                description = translation.description;
            }

            let group = this.formBuilder.group({
                id: translationId,
                languageId: language.id,
                languageTitle: language.title,
                title: [title, [Validators.required, Validators.minLength(1), Validators.maxLength(255)]],
                description: [description, [Validators.required]]
            })

            let translations = this.form.get('translations') as FormArray;
            translations.push(group);
        }
    }
}