﻿import { Component, OnInit } from '@angular/core';
import { AdminArticleService } from '../../../../services/admin/blog/article-service';
import { ArticleService } from '../../../../services/blog/articles/article.service';
import { TranslatedArticle } from '../../../../models/blog/article';
import { ErrorHandler } from '../../../../common/error.handler';

@Component({
    templateUrl: './articles.component.html',
    providers: [ArticleService, AdminArticleService]
})

export class AdminArticlesComponent implements OnInit {

    articles: TranslatedArticle[] = [];

    constructor(
        private articleService: ArticleService,
        private adminArticleService: AdminArticleService) { }

    ngOnInit() {
        this.initCategories();
    }

    initCategories() {
        this.articleService.list()
            .subscribe(result => {
                this.articles = result.embedded.map(s => s.embedded);
            }, error => {
                ErrorHandler.handleError(error);
            });
    }

    delete(id: number) {
        this.adminArticleService.delete(id)
            .subscribe(result => {
                let index = this.articles.findIndex(c => c.id === id);
                this.articles.splice(index, 1);
            }, error => {
                ErrorHandler.handleError(error);
            });
    }
}