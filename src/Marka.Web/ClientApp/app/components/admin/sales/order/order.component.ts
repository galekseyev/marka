﻿import { Component, OnInit, ViewChild } from '@angular/core';
import { AdminOrderService } from '../../../../services/admin/sales/order.service';
import { Order, OrderStatus, PaymentStatus, PaymentType } from '../../../../models/orders/order';
import { ActivatedRoute } from '@angular/router';
import { ErrorHandler } from '../../../../common/error.handler';
import { ModalDirective } from 'angular-bootstrap-md';
import { AdminOrderStatusService } from '../../../../services/admin/sales/order-status.service';
import { AdminPaymentTypeService } from '../../../../services/admin/sales/payment-type.service';
import { AdminPaymentStatusService } from '../../../../services/admin/sales/payment-status.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
    selector: 'admin-order',
    templateUrl: './order.component.html',
    styleUrls: ['./order.component.css'],
    providers: [AdminOrderService, AdminOrderStatusService, AdminPaymentTypeService, AdminPaymentStatusService]
})
export class AdminOrderComponent implements OnInit {

    @ViewChild('processOrderModal') processOrderModal: ModalDirective;
    @ViewChild('processPaymentModal') processPaymentModal: ModalDirective;

    processOrderForm: FormGroup;
    processPaymentForm: FormGroup;

    orderStatuses: OrderStatus[] = [];
    paymentStatuses: PaymentStatus[] = [];

    order = new Order();

    orderErrors: string[] = [];
    paymentErrors: string[] = [];

    get orderStatus() { return this.processOrderForm.get('orderStatus'); }
    get paymentStatus() { return this.processPaymentForm.get('paymentStatus'); }

    constructor(
        private route: ActivatedRoute,
        private builder: FormBuilder,
        private orderService: AdminOrderService,
        private orderStatusService: AdminOrderStatusService,
        private paymentStatusService: AdminPaymentStatusService) { }

    ngOnInit() {
        this.route.params.subscribe(params => {
            if (params['id']) {
                this.load(params['id']);
            }
        })

        this.initOrderStatusForm();
        this.initPaymentStatusForm();

        this.initOrderStatuses();
        this.initPaymentStatuses();
    }

    public load(id: number) {

        this.orderService.get(id)
            .subscribe(result => {
                this.order = result.embedded;
                this.initOrderStatusForm();
                this.initPaymentStatusForm();
            }, error => ErrorHandler.handleError(error));
    }

    public processOrder() {

        this.orderService.processOrderStatus(this.order.id, this.processOrderForm.value.orderStatus)
            .subscribe(result => {
                this.order = result.embedded;
                this.processOrderModal.hide();
            }, error => {
                this.orderErrors = ['An error occured processing order.'];
                ErrorHandler.handleError(error);
            });

    }

    public processPayment() {
        console.log(this.processPaymentForm.value)
        this.orderService.processPaymentStatus(this.order.id, this.processPaymentForm.value.paymentStatus)
            .subscribe(result => {
                this.order = result.embedded;
                this.processPaymentModal.hide();
            }, error => {
                this.orderErrors = ['An error occured processing order.'];
                ErrorHandler.handleError(error);
            });
    }

    private initOrderStatusForm() {
        this.processOrderForm = this.builder.group({
            orderStatus: [this.order.orderStatus.id, Validators.required],
        });
    }

    private initPaymentStatusForm() {
        this.processPaymentForm = this.builder.group({
            paymentStatus: [this.order.paymentStatus.id, Validators.required],
        });
    }

    private initOrderStatuses() {
        this.orderStatusService.list()
            .subscribe(result => {
                this.orderStatuses = result.embedded.map(c => c.embedded);
            }, error => {
                ErrorHandler.handleError(error);
            });
    }

    private initPaymentStatuses() {
        this.paymentStatusService.list()
            .subscribe(result => {
                this.paymentStatuses = result.embedded.map(c => c.embedded);
            }, error => {
                ErrorHandler.handleError(error);
            });
    }
}