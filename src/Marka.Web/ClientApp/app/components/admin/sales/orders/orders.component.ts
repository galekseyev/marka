﻿import { Component, OnInit } from '@angular/core';
import { Order } from '../../../../models/orders/order';
import { AdminOrderService } from '../../../../services/admin/sales/order.service';
import { ErrorHandler } from '../../../../common/error.handler';

@Component({
    selector: 'admin-orders',
    templateUrl: './orders.component.html',
    providers: [AdminOrderService]
})
/** orders component*/
export class AdminOrdersComponent implements OnInit {

    orders: Order[] = [];


    constructor(private orderService: AdminOrderService) {

    }

    ngOnInit() {
        this.initOrders();
    }

    initOrders() {
        this.orderService.list()
            .subscribe(result => {
                this.orders = result.embedded.map(s => s.embedded);
            }, error => {
                ErrorHandler.handleError(error);
            });
    }
}