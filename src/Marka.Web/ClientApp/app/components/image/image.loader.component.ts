﻿import { Component, Input, forwardRef } from "@angular/core";
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from "@angular/forms";
import { ImageCropperModule } from 'ngx-image-cropper';
import { Picture } from "../../models/shared/picture";


@Component({
    selector: 'image-loader',
    templateUrl: './image.loader.component.html',
    styleUrls: ['./image.loader.component.css'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => ImageLoaderComponent),
            multi: true
        }
    ]
})

export class ImageLoaderComponent implements ControlValueAccessor {

    @Input()
    aspectRatio: string;

    @Input()
    resizeToWidth: string;

    @Input()
    picture = new Picture();

    imageChangedEvent: any = '';

    propagateChange = (_: any) => { };

    writeValue(value: any) {
        if (value) {
            this.picture = value;
        }
    }

    registerOnChange(fn: any) {
        this.propagateChange = fn;
    }

    registerOnTouched() { }

    fileChangeEvent(event: any): void {
        if (event.target.files.length) {
            this.setOriginalPicture(event.target.files[0]);
            this.imageChangedEvent = event;
        }
    }

    private setOriginalPicture(file: File) {
        let reader = new FileReader();

        reader.onloadend = (e) => {
            this.picture.picture = reader.result.toString();
        }

        reader.readAsDataURL(file);
    }

    imageCropped(image: string) {
        this.picture.thumbnail = image;
        this.propagateChange(this.picture);
    }

    imageLoaded() { }

    loadImageFailed() { }
}