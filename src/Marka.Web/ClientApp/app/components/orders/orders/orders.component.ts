﻿import { Component, OnInit } from '@angular/core';
import { Order } from '../../../models/orders/order';
import { AccountOrderService } from '../../../services/orders/order.service';
import { ErrorHandler } from '../../../common/error.handler';

@Component({
    selector: 'app-orders',
    templateUrl: './orders.component.html',
    styleUrls: ['./orders.component.css'],
    providers: [AccountOrderService]

})
export class OrdersComponent implements OnInit {

    orders: Order[] = [];
    loading: boolean = false;

    constructor(private orderService: AccountOrderService) {

    }

    ngOnInit() {
        this.initOrders();
    }

    initOrders() {
        this.loading = true;

        this.orderService.list()
            .subscribe(result => {
                this.orders = result.embedded.map(s => s.embedded);
                this.loading = false;
            }, error => {
                ErrorHandler.handleError(error);
            });
    }
}