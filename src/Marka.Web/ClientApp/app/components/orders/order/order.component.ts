﻿import { Component, OnInit } from '@angular/core';
import { Order } from '../../../models/orders/order';
import { OrderService, AccountOrderService } from '../../../services/orders/order.service';
import { ActivatedRoute } from '@angular/router';
import { ErrorHandler } from '../../../common/error.handler';

@Component({
    selector: 'app-order',
    templateUrl: './order.component.html',
    styleUrls: ['./order.component.css'],
    providers: [AccountOrderService]
})
export class OrderComponent implements OnInit {

    order = new Order();
    loading: boolean = false;

    constructor(
        private route: ActivatedRoute,
        private orderService: AccountOrderService) { }

    ngOnInit() {
        this.route.params.subscribe(params => {
            if (params['id']) {
                this.load(params['id']);
            }
        })
    }

    public load(id: number) {
        this.loading = true;

        this.orderService.get(id)
            .subscribe(result => {
                this.order = result.embedded;
                this.loading = false;
            }, error => ErrorHandler.handleError(error));
    }
}