﻿import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { LoaderService, LoaderState } from '../../services/loader/loader.service';

@Component({
    selector: 'app-loader',
    templateUrl: './loader.component.html',
    styleUrls: ['./loader.component.css']
})
/** loader component*/
export class LoaderComponent implements OnInit, OnDestroy {

    isLoading = false;

    private subscription: Subscription;

    constructor(private loaderService: LoaderService) {

    }

    ngOnInit() {
        this.subscription = this.loaderService.loaderState
            .subscribe((state: LoaderState) => {
                this.isLoading = state.isLoading;
            });
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }
}