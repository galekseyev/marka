import { Component, Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
    selector: 'fetchdata',
    templateUrl: './fetchdata.component.html',
})

 @Injectable()
export class FetchDataComponent {
    public forecasts: WeatherForecast[] = [];

    constructor(
        private http: HttpClient) {
    }

    ngOnInit() {
        this.getForecasts();
    }

    getForecasts() {
        this.http.get('api/SampleData/WeatherForecastsAdmin')
            .subscribe(result => {
                this.forecasts = result as WeatherForecast[];
            })
    }
}

interface WeatherForecast {
    dateFormatted: string;
    temperatureC: number;
    temperatureF: number;
    summary: string;
}
