﻿import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CountryService } from '../../../services/countries/country.service';
import { Country } from '../../../models/countries/country';
import { ErrorHandler } from '../../../common/error.handler';
import { AccountProfileService } from '../../../services/account/account-profile.service';
import { UserProfile, UserAddress } from '../../../models/account/user-profile';

@Component({
    selector: 'app-profile',
    templateUrl: './profile.component.html',
    providers: [CountryService, AccountProfileService]
})
export class ProfileComponent {

    profileForm: FormGroup;
    changePasswordForm: FormGroup;

    profileSuccess = false;

    profileErrors: string[] = [];
    changePasswordErrors: string[] = [];

    model: UserProfile = new UserProfile();
    countries: Country[] = [];



    get fullName() { return this.profileForm.get('fullName'); }

    get address1() { return this.profileForm.get('address1'); }
    get city() { return this.profileForm.get('city'); }
    get countryId() { return this.profileForm.get('countryId'); }
    get postalCode() { return this.profileForm.get('postalCode'); }


    get password() { return this.changePasswordForm.get('password'); }
    get newPassword() { return this.changePasswordForm.get('newPassword'); }
    get confirmPassword() { return this.changePasswordForm.get('confirmPassword'); }

    constructor(
        private builder: FormBuilder,
        private countryService: CountryService,
        private accountProfileService: AccountProfileService) {

    }

    ngOnInit() {
        this.initCountries();
        this.initProfileForm();
        this.initChangePasswordForm();

        this.initProfile();
    }

    public saveProfile() {
        this.accountProfileService.save(this.profileForm.value)
            .subscribe(result => {
                this.model = result.embedded;

                this.initProfileForm();
                this.profileSuccess = true;
            }, error => {
                ErrorHandler.handleError(error);
                this.profileErrors = error.error.errors;
            })
    }

    public changePassword() {
        return false;
    }

    private initProfileForm() {

        this.profileForm = this.builder.group({
            fullName: [this.model.fullName, Validators.required],
            addressId: [this.model.address.id],
            address1: [this.model.address.address1, Validators.required],
            city: [this.model.address.city, Validators.required],
            countryId: [this.model.address.country.id, Validators.required],
            postalCode: [this.model.address.postalCode],
        });
    }

    private initChangePasswordForm() {
        this.changePasswordForm = this.builder.group({
            password: [null, [Validators.required, Validators.minLength(8)]],
            newPassword: [null, [Validators.required, Validators.minLength(8)]],
            confirmPassword: [null, [Validators.required, Validators.minLength(8)]]
        }, { validator: this.passwordConfirmation })
    }

    private initProfile() {
        this.accountProfileService.get()
            .subscribe(result => {
                this.model = result.embedded;
                if (!this.model.address)
                    this.model.address = new UserAddress();

                this.initProfileForm();
            }, error => {
                ErrorHandler.handleError(error);
            })
    }

    private initCountries() {

        this.countryService.list()
            .subscribe(result => {
                this.countries = result.embedded.map(c => c.embedded);
            }, er => {
                ErrorHandler.handleError(er);
            });
    }

    public onCountryChanged(country) {
        if (country && country.postalCodeRegEx)
            this.postalCode.setValidators([Validators.required, Validators.pattern(country.postalCodeRegEx)]);
        else
            this.postalCode.setValidators(Validators.required);

        this.postalCode.updateValueAndValidity();
    }

    passwordConfirmation(group: FormGroup) {
        let pass = group.controls.newPassword.value;
        let confirmPassword = group.controls.confirmPassword.value;

        return pass === confirmPassword
            ? group.controls.confirmPassword.setErrors(null)
            : group.controls.confirmPassword.setErrors({ equalValidator: true });
    }
}