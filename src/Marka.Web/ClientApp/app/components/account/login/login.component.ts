﻿import { Component } from '@angular/core';
import { FormsModule, FormGroup, FormBuilder, FormControl, Validators, AbstractControl, ValidatorFn } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { AccountService } from '../../../services/account.service';
import { CartService } from '../../../services/carts/cart.service';
import { UniqueEmailValidator } from '../../../validators/UniqueEmailValidator';

@Component({
    selector: 'login',
    templateUrl: './login.component.html',
    providers: [AccountService]
})

export class LoginComponent {

    authForm: any = {};

    loginErrors: string[] = [];
    registerErrors: string[] = [];

    registerForm: FormGroup;

    get fullName() { return this.registerForm.get('fullName'); }
    get email() { return this.registerForm.get('email'); }
    get password() { return this.registerForm.get('password'); }
    get confirmPassword() { return this.registerForm.get('confirmPassword'); }


    get loginEmail() { return this.authForm.get('email'); }
    get loginPassword() { return this.authForm.get('password'); }

    returnUrl = '/';

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private builder: FormBuilder,
        private accountService: AccountService) { }

    ngOnInit() {
        // reset login status
        this.accountService.logout();

        this.initForms();

        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    }

    private initForms() {

        this.authForm = new FormGroup({
            'email': new FormControl(null, [
                Validators.required,
                Validators.email
            ]),
            'password': new FormControl(null, [
                Validators.required,
            ])
        });

        this.registerForm = this.builder.group({
                'fullName': [null, Validators.required],
                'email': [null, [Validators.required, Validators.email]],
                'password': [null, [Validators.required, Validators.minLength(8)]],
                'confirmPassword': [null, [Validators.required, Validators.minLength(8)]]
            },
            { validator: this.passwordConfirmation });
    }

    public authenticate() {
        this.accountService.authenticate(this.authForm.value)
            .subscribe(
                result => {
                    this.router.navigate([this.returnUrl]);
                },
                error => {
                    this.loginErrors = error.error.errors;
                });
    }

    public register() {

        this.accountService.register(this.registerForm.value)
            .subscribe(
                result => {
                    this.router.navigate(['/']);
                },
                error => {
                    this.registerErrors = error.error.errors;
                });
    }

    passwordConfirmation(group: FormGroup) {
        let pass = group.controls.password.value;
        let confirmPassword = group.controls.confirmPassword.value;

        return pass === confirmPassword
            ? group.controls.confirmPassword.setErrors(null)
            : group.controls.confirmPassword.setErrors({ equalValidator: true });
    }
}
