﻿import { Component } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { AccountService } from '../../../services/account.service';
import { ErrorHandler } from '../../../common/error.handler';

@Component({
    selector: 'forgot-password',
    templateUrl: './forgot-password.component.html',
    providers: [AccountService]
})

export class ForgotPasswordComponent {

    form: FormGroup;

    success = false;
    errors: string[] = [];

    get email() { return this.form.get('email'); }

    constructor(
        private accountService: AccountService) { }

    ngOnInit() {
        this.form = new FormGroup({
            'email': new FormControl('', [
                Validators.required,
                Validators.email
            ])
        });
    }

    public forgotPassword() {
        this.accountService.recover(this.form.value)
            .subscribe(
                result => {
                    this.success = true;
                },
                error => {
                    this.errors = error.error.errors;
                    ErrorHandler.handleError(error);
                });
    }
}