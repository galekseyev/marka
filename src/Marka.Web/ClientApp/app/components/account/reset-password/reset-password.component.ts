﻿import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { AccountService } from '../../../services/account.service';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'reset-password',
    templateUrl: './reset-password.component.html',
    providers: [AccountService]
})
export class ResetPasswordComponent {

    form: FormGroup;

    token: string;
    errors: string[] = [];

    success = false;

    get email() { return this.form.get('email'); }
    get password() { return this.form.get('password'); }
    get confirmPassword() { return this.form.get('confirmPassword'); }

    constructor(
        private route: ActivatedRoute,
        private formBuilder: FormBuilder,
        private accountService: AccountService) { }

    ngOnInit() {
        this.form = this.formBuilder.group({
            email: ['', [Validators.required]],
            password: ['', [Validators.required, Validators.minLength(8)]],
            confirmPassword: ['', [Validators.required, Validators.minLength(8)]],
        }, { validator: this.passwordConfirmation })

        this.route.params.subscribe(params => {
            if (params['token']) {
                this.token = params['token'];
            }
        })
    }

    passwordConfirmation(group: FormGroup) {
        let pass = group.controls.password.value;
        let confirmPassword = group.controls.confirmPassword.value;

        return pass === confirmPassword
            ? group.controls.confirmPassword.setErrors(null)
            : group.controls.confirmPassword.setErrors({ equalValidator: true });
    }

    resetPassword() {
        let model: any = {};

        model.token = this.token;
        model.email = this.form.value.email;
        model.password = this.form.value.password;
        model.confirmPassword = this.form.value.confirmPassword;

        this.accountService.reset(model)
            .subscribe(
                result => {
                    this.success = true;
                },
            error => {
                console.log(error.error.errors)
                    this.errors = error.error.errors;
                });
    }

}