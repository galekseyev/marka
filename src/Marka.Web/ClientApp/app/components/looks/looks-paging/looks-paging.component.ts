﻿import { Component, Input, EventEmitter, Output } from '@angular/core';

@Component({
    selector: 'looks-paging',
    templateUrl: './looks-paging.component.html',
})
/** looks-paging component*/
export class LooksPagingComponent {
    @Input() loading: boolean;

    @Input() pageSize: number;
    @Input() total: number;

    @Output() pageChangedEvent = new EventEmitter<any>();

    currentPage: number = 1;

    get totalPages() { return Math.ceil(this.total / this.pageSize); }

    constructor() {

    }

    public setPage(page: number) {

        // ensure current page isn't out of range
        if (page <= 1) {
            page = 1;
        } else if (page > this.totalPages) {
            page = this.totalPages;
        }

        //this.pager = this.getPager(page);
        this.currentPage = page;

        let skip = (this.currentPage - 1) * this.pageSize;

        this.pageChangedEvent.emit(skip);
    }

    private getPages() {

        let startPage: number, endPage: number;

        if (this.totalPages <= 10) {
            // less than 10 total pages so show all
            startPage = 1;
            endPage = this.totalPages;
        } else {
            // more than 10 total pages so calculate start and end pages
            if (this.currentPage <= 6) {
                startPage = 1;
                endPage = 10;
            } else if (this.currentPage + 4 >= this.totalPages) {
                startPage = this.totalPages - 9;
                endPage = this.totalPages;
            } else {
                startPage = this.currentPage - 5;
                endPage = this.currentPage + 4;
            }
        }

        // create an array of pages to ng-repeat in the pager control
        return Array.from(Array((endPage + 1) - startPage).keys()).map(i => startPage + i);
    }
}