﻿import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { InventoryGroupService } from '../../../services/catalog/inventory-groups/inventory-group.service';
import { TranslatedInventoryGroup } from '../../../models/catalog/inventory-groups/inventory-group';

@Component({
    selector: 'look',
    templateUrl: './look.component.html',
    styleUrls: ['./look.component.css'],
    providers: [InventoryGroupService]
})
export class LookComponent implements OnInit {

    group = new TranslatedInventoryGroup();

    constructor(
        private route: ActivatedRoute,
        private inventoryGroupService: InventoryGroupService) {
    }

    ngOnInit(): void {

        this.route.params.subscribe(params => {
            if (params['id']) {
                this.load(params['id']);
            }
        })
    }

    private load(id: number) {
        this.inventoryGroupService.get(id)
            .subscribe(result => {
                console.log(result.embedded);
                this.group = result.embedded;

            })
    }
}