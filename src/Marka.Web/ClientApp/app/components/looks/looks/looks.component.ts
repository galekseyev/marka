﻿import { Component, OnInit } from '@angular/core';
import { InventoryGroup } from '../../../models/catalog/inventory-groups/inventory-group';
import { ActivatedRoute } from '@angular/router';
import { InventoryGroupService } from '../../../services/catalog/inventory-groups/inventory-group.service';
import { ErrorHandler } from '../../../common/error.handler';


@Component({
    selector: 'looks',
    templateUrl: './looks.component.html',
    styleUrls: ['./looks.component.css'],
    providers: [InventoryGroupService]
})
/** looks component*/
export class LooksComponent implements OnInit {

    groups: InventoryGroup[] = [];

    filter: any = {};

    page: number = 1;

    pageSize: number = 6;
    total: number = 0;

    skip: number = 0;

    loading: boolean = false;

    constructor(
        private route: ActivatedRoute,
        private inventoryGroupService: InventoryGroupService) {
    }

    ngOnInit() {
        this.load();
    }

    public onPageChanged(skip) {
        this.skip = skip;
        this.load();
    }

    private load() {
        this.loading = true;

        this.filter.skip = this.skip;
        this.filter.take = this.pageSize;

        this.inventoryGroupService.listGroups(this.filter)
            .subscribe(result => {
                this.groups = result.embedded.map(s => s.embedded);
                this.total = result.total;

                this.loading = false;
            }, error => {
                ErrorHandler.handleError(error);
            });
    }

}