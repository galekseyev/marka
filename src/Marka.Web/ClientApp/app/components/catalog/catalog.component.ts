import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { InventoryService } from '../../services/catalog/inventories/inventory.service';
import { TranslatedInventory } from '../../models/catalog/inventories/inventory';
import { ActivatedRoute } from '@angular/router';
import { ErrorHandler } from '../../common/error.handler';
import { CatalogPagingComponent } from './catalog-paging/catalog-paging.component';

@Component({
    selector: 'catalog',
    templateUrl: './catalog.component.html',
    styleUrls: ['./catalog.component.css'],
    providers: [InventoryService]
})
export class CatalogComponent implements OnInit {

    @ViewChild(CatalogPagingComponent) catalogPaging: CatalogPagingComponent;

    categoryId?: number;
    specificationValueIds?: string;

    inventories: TranslatedInventory[] = [];

    filter: any = {};

    page: number = 1;

    pageSize: number = 4;
    total: number = 0;

    skip: number = 0;

    loading: boolean = false;

    constructor(
        private route: ActivatedRoute,
        private inventoryService: InventoryService) { }

    ngOnInit() {
        this.route.params.subscribe(params => {
            if (params['categoryId']) {
                this.categoryId = params['categoryId'];
            }

            this.load();
        })
    }

    public load() {
        this.loading = true;

        this.filter.skip = this.skip;
        this.filter.take = this.pageSize;

        if (this.categoryId)
            this.filter.categoryId = this.categoryId;

        this.filter.specificationValueIds = this.specificationValueIds

        this.inventoryService.list(this.filter)
            .subscribe(result => {
                this.inventories = result.embedded.map(c => c.embedded);
                this.total = result.total;
                this.loading = false;
            }, error => {
                ErrorHandler.handleError(error);
            });
    }

    public onCatalogFilter(specIds) {
        if (specIds && specIds.length > 0) {
            this.specificationValueIds = specIds;
        }
        else {
            this.specificationValueIds = null;
        }

        this.catalogPaging.setPage(1);
    }

    public onPageChanged(skip) {
        this.skip = skip;
        this.load();
    }
}