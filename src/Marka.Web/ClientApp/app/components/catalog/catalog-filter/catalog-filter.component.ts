﻿import { Component, Injectable, OnInit, Output, EventEmitter } from '@angular/core';
import { TranslatedInventorySpecification } from '../../../models/catalog/inventories/inventory-specification';
import { SpecificationService } from '../../../services/catalog/specifications/specification.service';
import { TranslatedSpecification } from '../../../models/catalog/specifications/specification';
import { ActivatedRoute } from '@angular/router';
import { ErrorHandler } from '../../../common/error.handler';
import { FormGroup, FormBuilder, FormArray, FormControl } from '@angular/forms';

@Component({
    selector: 'catalog-filter',
    templateUrl: './catalog-filter.component.html',
    providers: [SpecificationService]
})

@Injectable()
export class CatalogFilterComponent implements OnInit {

    form: FormGroup;


    @Output() filterEvent = new EventEmitter<any>();


    //Inventory Id
    id: number;

    //Category Id
    categoryId: number;

    get specifications() { return (this.form.controls.specifications as FormArray).controls; }

    constructor(
        private route: ActivatedRoute,
        private formBuilder: FormBuilder,
        private specificationService: SpecificationService) {

    }

    ngOnInit() {
        this.initForm();
        this.initSpecifications();
    }

    private initForm() {
        this.form = this.formBuilder.group({
            specifications: this.formBuilder.array([])
        });


    }

    private initSpecifications() {
        this.specificationService.list()
            .subscribe(result => {

                for (let spec of result.embedded.map(c => c.embedded)) {

                    let group = this.formBuilder.group({
                        id: spec.id,
                        title: spec.title,
                        specValues: this.formBuilder.array([])
                    })

                    let values = group.get('specValues') as FormArray;

                    for (let value of spec.values) {

                        let valueGroup = this.formBuilder.group({
                            valueId: value.id,
                            valueTitle: value.title,
                            valueChecked: false
                        });

                        values.push(valueGroup);
                    }

                    let specifications = this.form.get('specifications') as FormArray;
                    specifications.push(group);
                }
            }, error => ErrorHandler.handleError(error));
    }

    private onCheckboxChange() {

        let checkedValues: number[] = [];

        for (let specification of this.form.value.specifications) {

            let checked = specification.specValues.filter(c => c.valueChecked).map(c => c.valueId);
            if (checked && checked.length > 0) {
                checkedValues.push(checked);
            }
        }

        this.filterEvent.emit(checkedValues);
    }
}