﻿import { Component, OnInit } from '@angular/core';
import { InventoryService } from '../../../services/catalog/inventories/inventory.service';
import { TranslatedInventory } from '../../../models/catalog/inventories/inventory';
import { ActivatedRoute } from '@angular/router';
import { CatalogAttribute, CartItem } from '../../../models/carts/cart';
import { CartService } from '../../../services/carts/cart.service';

@Component({
    selector: 'catalog-item',
    templateUrl: './catalog-item.component.html',
    styleUrls: ['./catalog-item.component.css'],
    providers: [InventoryService]
})
export class CatalogItemComponent implements OnInit {

    inventory = new TranslatedInventory();

    model = new CartItem();

    loading: boolean = false;

    constructor(
        private route: ActivatedRoute,
        private inventoryService: InventoryService,
        private cartService: CartService
    ) { }

    ngOnInit(): void {

        this.route.params.subscribe(params => {
            if (params['id']) {
                this.load(params['id']);
            }
        })
    }

    private load(id: number) {

        this.loading = true;

        this.inventoryService.get(id)
            .subscribe(result => {

                this.inventory = result.embedded;
                this.model.id = result.embedded.id;
                for (let attribute of result.embedded.attributes) {
                    var attributeModel = new CatalogAttribute(attribute.id);

                    attributeModel.inventoryAttributeValueId = attribute.values[0].id.toString();

                    this.model.cartItemAttributesValuesXref.push(attributeModel);
                }

                this.loading = false;
            })
    }

    addItem() {
        this.model.inventoryId = this.inventory.id;

        if (this.model.quantity < 1)
            this.model.quantity = 1;

        this.cartService.addItem(this.model);
    }
}
