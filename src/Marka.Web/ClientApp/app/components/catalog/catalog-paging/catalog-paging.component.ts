﻿import { Component, Input, EventEmitter, Output, OnInit } from '@angular/core';

@Component({
    selector: 'catalog-paging',
    templateUrl: './catalog-paging.component.html'
})
export class CatalogPagingComponent {
    @Input() loading: boolean;

    @Input() pageSize: number;
    @Input() total: number;



    @Output() pageChangedEvent = new EventEmitter<any>();

    //// pager object
    //pager: any = {

    //};

    currentPage: number = 1;

    get totalPages() { return Math.ceil(this.total / this.pageSize); }






    constructor() {

    }

    //public init(total: number, pageSize: number) {
    //    console.log('pager init');
    //    this.total = total;
    //    this.pageSize = pageSize;

    //    this.pager = this.getPager(1);

    //    console.log(this.pager)
    //}

    public setPage(page: number) {

        // ensure current page isn't out of range
        if (page <= 1) {
            page = 1;
        } else if (page > this.totalPages) {
            page = this.totalPages;
        }

        //this.pager = this.getPager(page);
        this.currentPage = page;

        let skip = (this.currentPage - 1) * this.pageSize;

        this.pageChangedEvent.emit(skip);
    }

    private getPages() {
        // calculate total pages
        //let totalPages = Math.ceil(this.total / this.pageSize);



        let startPage: number, endPage: number;

        if (this.totalPages <= 10) {
            // less than 10 total pages so show all
            startPage = 1;
            endPage = this.totalPages;
        } else {
            // more than 10 total pages so calculate start and end pages
            if (this.currentPage <= 6) {
                startPage = 1;
                endPage = 10;
            } else if (this.currentPage + 4 >= this.totalPages) {
                startPage = this.totalPages - 9;
                endPage = this.totalPages;
            } else {
                startPage = this.currentPage - 5;
                endPage = this.currentPage + 4;
            }
        }

        //// calculate start and end item indexes
        //let startIndex =
        //    let endIndex =


        // create an array of pages to ng-repeat in the pager control
        return Array.from(Array((endPage + 1) - startPage).keys()).map(i => startPage + i);

        //console.log(pages);
        // return object with all pager properties required by the view
        //let pager = {
        //    totalItems: this.total,
        //    currentPage: this.currentPage,
        //    pageSize: this.pageSize,
        //    totalPages: this.totalPages,
        //    startPage: startPage,
        //    endPage: endPage,
        //    startIndex: startIndex,
        //    endIndex: endIndex,
        //    pages: pages
        //};

        //console.log(pager);
        //return pager;
    }
}