﻿import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { CategoryService } from '../../../services/catalog/category.service';
import { TranslatedCategory } from '../../../models/catalog/categories/category';

@Component({
    selector: 'catalog-top-menu',
    templateUrl: './catalog.top.menu.component.html',
    styleUrls: ['./catalog.top.menu.component.css'],
    providers: [CategoryService]
})
export class CatalogTopMenuComponent implements OnInit {

    @Output() categoryEvent = new EventEmitter<number>();

    categories: TranslatedCategory[] = [];

    constructor(
        private categoryService: CategoryService) { }

    ngOnInit() {
        this.initCategories();
    }

    initCategories() {
        this.categoryService.list()
            .subscribe(result => {
                this.categories = result.embedded.map(s => s.embedded);
            },
                error => {
                    console.log(error);

                });
    }

    selectCategory(id: number) {
        this.categoryEvent.emit(id);
    }
}
