﻿import { Component } from '@angular/core';

@Component({
    selector: 'checkout-complete',
    templateUrl: './checkout-complete.component.html',
    styleUrls: ['./checkout-complete.component.css']
})
/** checkout-complete component*/
export class CheckoutCompleteComponent {
    /** checkout-complete ctor */
    constructor() {

    }
}