﻿import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ErrorHandler } from '../../../common/error.handler';
import { OrderService } from '../../../services/orders/order.service';
import { CartService } from '../../../services/carts/cart.service';
import { Cart } from '../../../models/carts/cart';
import { Subscription } from 'rxjs';
import { CountryService } from '../../../services/countries/country.service';

@Component({
    selector: 'checkout',
    templateUrl: './checkout.component.html',
    styleUrls: ['./checkout.component.css'],
    providers: [CountryService, OrderService]
})

export class CheckoutComponent implements OnInit {

    private cartSubscription: Subscription;

    public form: FormGroup;

    public cart: Cart;
    public countries: any[] = [];

    public errors: string[] = [];

    get email() { return this.form.get('email'); }
    get fullName() { return this.form.get('fullName'); }

    get address1() { return this.form.get('address1'); }
    get city() { return this.form.get('city'); }
    get countryId() { return this.form.get('countryId'); }
    get postalCode() { return this.form.get('postalCode'); }

    get paymentTypeCode() { return this.form.get('paymentTypeCode'); }

    constructor(
        private router: Router,
        private builder: FormBuilder,
        private countryService: CountryService,
        private cartService: CartService,
        private orderService: OrderService) {
    }

    ngOnInit() {
        this.subscribeToCart();
        this.initCountries();
        this.initForm();
    }

    ngOnDestroy() {
        this.unsubscribeFromCart();
    }

    public checkout() {
        this.orderService.save(this.form.value)
            .subscribe(result => {
                this.cartService.create();
                this.router.navigate(['/thankyou'])
            }, er => {
                ErrorHandler.handleError(er);
                this.errors = er.error.errors
            })
    }

    private subscribeToCart() {
        this.cartSubscription = this.cartService.cartState.subscribe(
            result => {
                this.cart = result;
            });
    }

    private unsubscribeFromCart() {
        this.cartSubscription.unsubscribe();
    }

    private initForm() {
        this.form = this.builder.group({
            email: ['', [Validators.required, Validators.email]],
            fullName: ['', Validators.required],
            address1: ['', Validators.required],
            city: ['', Validators.required],
            countryId: [null, Validators.required],
            postalCode: ['',],
            paymentTypeCode: ['BNTNF', Validators.required]
        });
    }

    private initCountries() {

        this.countryService.list()
            .subscribe(result => {
                console.log(result);
                this.countries = result.embedded.map(c => c.embedded);
            }, er => {
                ErrorHandler.handleError(er);
                this.errors = er.error.errors
            })
    }

    public onCountryChanged(country) {
        if (country && country.postalCodeRegEx)
            this.postalCode.setValidators([Validators.required, Validators.pattern(country.postalCodeRegEx)]);
        else
            this.postalCode.setValidators(Validators.required);

        this.postalCode.updateValueAndValidity();
    }
}