import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { Language, LocaleService, TranslationService } from 'angular-l10n';
import { CartService } from '../../services/carts/cart.service';
import { AuthService } from '../../services/auth.service';
import { ModalDirective } from 'angular-bootstrap-md';

@Component({
    selector: 'app',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

    @Language() lang = '';

    @ViewChild('frame') frame: ModalDirective;

    constructor(
        @Inject('LOCAL_STORAGE')
        private localStorage: any,
        private authSerive: AuthService,
        private cartService: CartService,
        public locale: LocaleService,
        public translation: TranslationService) {

        this.locale.defaultLocaleChanged.subscribe((item: string) => {
            this.onLanguageCodeChangedDataRecieved(item);
        });
    }

    ngOnInit() {
        this.authSerive.initUserRoles();
        this.cartService.initCartState();
    }

    ngAfterViewInit() {
        let cookie = this.localStorage.getItem('_euCookie');

        if (!cookie)
            this.frame.show();
    }

    public acceptCookies() {
        this.localStorage.setItem('_euCookie', true);
        this.frame.hide();
    }

    public ChangeCulture(language: string, country: string) {
        this.locale.setDefaultLocale(language, country);
    }

    private onLanguageCodeChangedDataRecieved(item: string) {
        console.log('onLanguageCodeChangedDataRecieved App');
        console.log(item);
    }
}