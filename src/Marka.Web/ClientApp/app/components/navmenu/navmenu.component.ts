import { Component, OnInit, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import { AccountService } from '../../services/account.service';
import { AuthService } from '../../services/auth.service';
import { CategoryService } from '../../services/catalog/category.service';
import { TranslatedCategory } from '../../models/catalog/categories/category';
import { CartService } from '../../services/carts/cart.service';
import { ErrorHandler } from '../../common/error.handler';
import { Subscription } from 'rxjs';


@Component({
    selector: 'nav-menu',
    templateUrl: './navmenu.component.html',
    styleUrls: ['./navmenu.component.css'],
    providers: [
        AccountService,
        CategoryService
    ]
})
export class NavMenuComponent implements OnInit, OnDestroy {

    private cartSubscription: Subscription;

    itemCount: number = 0;

    categories: TranslatedCategory[] = [];

    constructor(
        private router: Router,
     
        private accountService: AccountService,
        private categoryService: CategoryService,
        private cartService: CartService) { }

    ngOnInit() {
        this.initCategories();

        this.subscribeToCart();
    }

    ngOnDestroy() {
        this.unsubscribeFromCart();
    }

    initCategories() {
        this.categoryService.list()
            .subscribe(result => {
                this.categories = result.embedded.map(s => s.embedded);
            }, error => {
                ErrorHandler.handleError(error);
            });
    }

    logout() {
        this.accountService.logout();
    }

    getNavigationStyles() {
        if (this.router.url == "/" || this.router.url == "/home")
            return "scrolling-navbar navbar-transparent";
        else
            return "navbar-default";
    }

    private subscribeToCart() {
        this.cartSubscription = this.cartService.cartState
            .subscribe(result => {
                this.itemCount = result.cartItems.length;
            })
    }

    private unsubscribeFromCart() {
        this.cartSubscription.unsubscribe();
    }
}