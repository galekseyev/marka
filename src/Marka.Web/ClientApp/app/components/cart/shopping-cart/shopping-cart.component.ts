﻿import { Component, OnInit, OnDestroy } from '@angular/core';
import { CartService } from '../../../services/carts/cart.service';
import { Cart, CartItem } from '../../../models/carts/cart';
import { Subscription } from 'rxjs';

@Component({
    selector: 'shopping-cart',
    templateUrl: './shopping-cart.component.html',
    styleUrls: ['./shopping-cart.component.css'],
})

export class ShoppingCartComponent implements OnInit, OnDestroy {

    private cartSubscription: Subscription;

    public cart: Cart;

    constructor(
        private cartService: CartService) { }

    ngOnInit() {
        this.subscribeToCart();
    }

    ngOnDestroy() {
        this.unsubscribeFromCart();
    }

    private subscribeToCart() {
        this.cartSubscription = this.cartService.cartState.subscribe(result => this.cart = result);
    }

    private unsubscribeFromCart() {
        this.cartSubscription.unsubscribe();
    }

    public delete(id: number) {
        this.cartService.deleteItem(id);
    }

    public updateQuantity(item: CartItem) {
        if (item.quantity < 1)
            item.quantity = 1;

        if (item.quantity > item.inventoryQuantity)
            item.quantity = item.inventoryQuantity;

        this.cartService.updateItemQuantity(item.id, item.quantity);
    }
}