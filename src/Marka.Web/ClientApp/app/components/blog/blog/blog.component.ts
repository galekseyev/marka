﻿import { Component, OnInit } from '@angular/core';
import { TranslatedArticle } from '../../../models/blog/article';
import { ActivatedRoute } from '@angular/router';
import { ArticleService } from '../../../services/blog/articles/article.service';
import { ErrorHandler } from '../../../common/error.handler';

@Component({
    selector: 'blog',
    templateUrl: './blog.component.html',
    styleUrls: ['./blog.component.css'],
    providers: [ArticleService]
})
export class BlogComponent implements OnInit {

    articles: TranslatedArticle[] = [];

    filter: any = {};

    page: number = 1;

    pageSize: number = 4;
    total: number = 0;

    skip: number = 0;

    loading: boolean = false;

    constructor(
        private route: ActivatedRoute,
        private articleService: ArticleService) {
    }

    ngOnInit() {
        this.load();
    }

    public onPageChanged(skip) {
        this.skip = skip;
        this.load();
    }

    private load() {
        this.loading = true;

        this.filter.skip = this.skip;
        this.filter.take = this.pageSize;

        this.articleService.list(this.filter)
            .subscribe(result => {
                this.articles = result.embedded.map(s => s.embedded);
                this.total = result.total;

                this.loading = false;
            }, error => {
                ErrorHandler.handleError(error);
            });
    }
}