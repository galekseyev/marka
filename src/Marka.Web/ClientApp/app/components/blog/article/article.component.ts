﻿import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ArticleService } from '../../../services/blog/articles/article.service';
import { TranslatedArticle } from '../../../models/blog/article';
import { ActivatedRoute } from '@angular/router';
import { ErrorHandler } from '../../../common/error.handler';

@Component({
    selector: 'article',
    templateUrl: './article.component.html',
    styleUrls: ['./article.component.css'],
    providers: [ArticleService],
    encapsulation: ViewEncapsulation.None,
})
export class ArticleComponent implements OnInit {

    article = new TranslatedArticle();
    loading: boolean = false;

    constructor(
        private route: ActivatedRoute,
        private articleService: ArticleService) { }

    ngOnInit() {
        this.route.params.subscribe(params => {
            if (params['id']) {
                this.load(params['id']);
            }
        })
    }

    private load(id: number) {
        this.loading = true;

        this.articleService.get(id)
            .subscribe(result => {
                this.article = result.embedded;
                this.loading = false;
            }, error => {
                ErrorHandler.handleError(error);
            });
    }
}