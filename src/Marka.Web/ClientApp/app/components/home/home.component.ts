import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ContactUsService } from '../../services/contact-us/contact-us.service';
import { ErrorHandler } from '../../common/error.handler';

@Component({
    selector: 'home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.css'],
    providers: [ContactUsService]
})
export class HomeComponent implements OnInit {

    public contactUsForm: FormGroup;

    public success = false;
    public errors: string[] = [];

    get email() { return this.contactUsForm.get('email'); }
    get name() { return this.contactUsForm.get('name'); }
    get message() { return this.contactUsForm.get('message'); }

    constructor(
        private builder: FormBuilder,
        private contactService: ContactUsService) {

    }

    ngOnInit() {
        this.initContactForm();
    }

    public sendMessage() {
        this.contactService.SendMessage(this.contactUsForm.value)   
            .subscribe(result => {
                this.initContactForm();
                this.success = true;
            }, er => {
                ErrorHandler.handleError(er);
                this.errors = er.error.errors;
            });
    }

    private initContactForm() {
        this.contactUsForm = this.builder.group({
            email: ['', [Validators.required, Validators.email]],
            name: ['', Validators.required],
            message: ['', Validators.required]
        });
    }
}
