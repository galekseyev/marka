﻿import { Component } from '@angular/core';

@Component({
    selector: 'default-layout',
    templateUrl: './default.layout.component.html',
    styleUrls: ['./default.layout.component.css']
})
/** default.layout component*/
export class DefaultLayoutComponent {
    /** default.layout ctor */
    constructor() {

    }
}