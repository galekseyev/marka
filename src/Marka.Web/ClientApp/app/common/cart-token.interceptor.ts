﻿import { Injectable } from "@angular/core";
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from "@angular/common/http";
import { Router } from "@angular/router";
import { Observable } from "rxjs";
import { CartTokenService } from "../services/carts/cart-token.service";


@Injectable()
export class CartTokenInterceptor implements HttpInterceptor {

    constructor(
        private router: Router,
        private cartTokenService: CartTokenService) { }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        let token = this.cartTokenService.getCartToken();

        if (!token)
            return next.handle(req);

        const clonedRequest = req.clone({ headers: req.headers.set('CartToken', token) });

        return next.handle(clonedRequest);
    }
}