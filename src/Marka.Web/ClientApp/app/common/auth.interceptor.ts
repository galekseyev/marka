﻿import { Injectable } from "@angular/core";
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from "@angular/common/http";
import { Router } from "@angular/router";
import { AuthService } from "../services/auth.service";
import { Observable } from "rxjs/Observable";
import 'rxjs/add/observable/throw'
import 'rxjs/add/operator/catch';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

    constructor(
        private router: Router,
        private authService: AuthService) { }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        let token = this.authService.getToken();

        if (!token)
            return next.handle(req);

        const clonedRequest = req.clone({ headers: req.headers.set('Authorization', 'Bearer ' + token) });

        return next.handle(clonedRequest)
            .catch((error, caught) => {

                if (error.status === 401 || error.status === 403) {

                    this.authService.removeToken
                    this.router.navigate(['/login']);
                }

                return Observable.throw(error);

            }) as any;
    }
}