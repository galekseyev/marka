﻿import { Injectable } from "@angular/core";
import { HttpInterceptor, HttpHandler, HttpEvent, HttpRequest, HttpResponse } from "@angular/common/http";
import { LoaderService } from "../services/loader/loader.service";
import { Observable } from "rxjs";
import { catchError, finalize, map } from 'rxjs/operators';


@Injectable({
    providedIn: 'root'
})

export class LoaderInterceptor implements HttpInterceptor {
    constructor(private loaderService: LoaderService) { }
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        this.showLoader();

        return next.handle(req).pipe(
            map(event => {
                return event;
            }),
            catchError(error => {
                return Observable.throw(error);
            }),
            finalize(() => {
                this.hideLoader();
            })
        )
    }

    private showLoader(): void {
        console.log("showLOader")
        this.loaderService.show();
    }

    private hideLoader(): void {
        console.log("hideLoader")
        this.loaderService.hide();
    }
}