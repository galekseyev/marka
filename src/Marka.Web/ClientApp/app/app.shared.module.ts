import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { MDBBootstrapModule, WavesModule } from 'angular-bootstrap-md';
import { NgSelectModule } from '@ng-select/ng-select'
import { EditorModule } from '@tinymce/tinymce-angular';

//App
import { AppComponent } from './components/app/app.component';

import { DefaultLayoutComponent } from './layouts/default-layout/default.layout.component';

import { ImageLoaderComponent } from './components/image/image.loader.component';
import { NavMenuComponent } from './components/navmenu/navmenu.component';
import { HomeComponent } from './components/home/home.component';
import { FetchDataComponent } from './components/fetchdata/fetchdata.component';

import { LoginComponent } from './components/account/login/login.component';
import { ForgotPasswordComponent } from './components/account/forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './components/account/reset-password/reset-password.component';

//Blog
import { BlogComponent } from './components/blog/blog/blog.component';
import { BlogPagingComponent } from './components/blog/blog-paging/blog-paging.component';
import { ArticleComponent } from './components/blog/article/article.component';

//Catalog
import { CatalogComponent } from './components/catalog/catalog.component';
import { CatalogTopMenuComponent } from './components/catalog/catalog-top-menu/catalog.top.menu.component';
import { CatalogFilterComponent } from './components/catalog/catalog-filter/catalog-filter.component';
import { CatalogItemComponent } from './components/catalog/catalog-item/catalog-item.component';
import { CatalogPagingComponent } from './components/catalog/catalog-paging/catalog-paging.component';


//Looks
import { LooksComponent } from './components/looks/looks/looks.component';
import { LookComponent } from './components/looks/look/look.component';
import { LooksPagingComponent } from './components/looks/looks-paging/looks-paging.component';

//Administration Module
import { AdminMenuComponent } from './components/admin/navmenu/navmenu.component';
import { AdminDashboardComponent } from './components/admin/dashboard/dashboard.component';
import { AdminDashboardStatsComponent } from './components/admin/dashboard/dashboard-stats/dashboard-stats.component';


//Administration Module - Orders
import { AdminOrdersComponent } from './components/admin/sales/orders/orders.component';
import { AdminOrderComponent } from './components/admin/sales/order/order.component';


//Administration Module - Category
import { AdminCategoriesComponent } from './components/admin/catalog/categories/categories.component';
import { AdminCategoryMenuComponent } from './components/admin/catalog/categories/category-menu/category-menu.component';
import { AdminCategoryComponent } from './components/admin/catalog/categories/category.component';
import { AdminCategoryGeneralComponent } from './components/admin/catalog/categories/category-general/category-general.component';
import { AdminCategoryPictureComponent } from './components/admin/catalog/categories/category-picture/category-picture.component';


//Administration Module - Attribute
import { AdminAttributesComponent } from './components/admin/catalog/attributes/attributes.component';
import { AdminAttributeComponent } from './components/admin/catalog/attributes/attribute.component';


//Administration Module - Specification
import { AdminSpecificationsComponent } from './components/admin/catalog/specifications/specifications/specifications.component';
import { AdminSpecificationMenuComponent } from './components/admin/catalog/specifications/specification-menu/specification-menu.component';
import { AdminSpecificationComponent } from './components/admin/catalog/specifications/specification/specification.component';
import { AdminSpecificationGeneralComponent } from './components/admin/catalog/specifications/specification-general/specification-general.component';
import { AdminSpecificationValuesComponent } from './components/admin/catalog/specifications/specification-values/specification-values.component';


//Administration Module - Inventory
import { AdminInventoriesComponent } from './components/admin/catalog/inventories/inventories.component';
import { AdminInventoryComponent } from './components/admin/catalog/inventories/inventory.component';
import { AdminInventoryMenuComponent } from './components/admin/catalog/inventories/inventory-menu/inventory-menu.component';
import { AdminInventoryGeneralComponent } from './components/admin/catalog/inventories/inventory-general/inventory-general.component';
import { AdminInventoryPictureComponent } from './components/admin/catalog/inventories/inventory-picture/inventory-picture.component';
import { AdminInventoryAttributesComponent } from './components/admin/catalog/inventories/inventory-attributes/inventory-attributes.component';
import { AdminInventoryAttributeComponent } from './components/admin/catalog/inventories/inventory-attributes/attribute/inventory-attribute.component';
import { AdminInventoryAttributeMenuComponent } from './components/admin/catalog/inventories/inventory-attributes/attribute/inventory-attribute-menu/inventory-attribute-menu.component';
import { AdminInventoryAttributeGeneralComponent } from './components/admin/catalog/inventories/inventory-attributes/attribute/inventory-attribute-general/inventory-attribute-general.component';
import { AdminInventoryAttributeValuesComponent } from './components/admin/catalog/inventories/inventory-attributes/attribute/inventory-attribute-values/inventory-attribute-values.component';
import { AdminInventorySpecificationsComponent } from './components/admin/catalog/inventories/inventory-specifications/inventory-specifications.component';


//Administration Module - Inventory Groups
import { AdminInventoryGroupsComponent } from './components/admin/catalog/inventory-groups/inventory-groups.component';
import { AdminInventoryGroupComponent } from './components/admin/catalog/inventory-groups/inventory-group.component';
import { AdminInventoryGroupGeneralComponent } from './components/admin/catalog/inventory-groups/inventory-group-general/inventory-group-general.component';
import { AdminInventoryGroupPictureComponent } from './components/admin/catalog/inventory-groups/inventory-group-picture/inventory-group-picture.component';
import { AdminInventoryGroupInventoriesComponent } from './components/admin/catalog/inventory-groups/inventory-group-inventory/inventory-group-inventory.component';
import { AdminInventoryGroupMenuComponent } from './components/admin/catalog/inventory-groups/inventory-group-menu/inventory-group-menu.component';


//Administration Module - Articles
import { AdminArticlesComponent } from './components/admin/blog/articles/articles.component';
import { AdminArticleComponent } from './components/admin/blog/article.component';
import { AdminArticleGeneralComponent } from './components/admin/blog/article-general/article-general.component';
import { AdminArticlePictureComponent } from './components/admin/blog/article-picture/article-picture.component';
import { AdminArticleMenuComponent } from './components/admin/blog/article-menu/article-menu.component';



import { AuthGuard } from './guards/auth.guard';
import { AuthService } from './services/auth.service';
import { AuthInterceptor } from './common/auth.interceptor';

import { ImageCropperModule } from 'ngx-image-cropper';


import { ShoppingCartComponent } from './components/cart/shopping-cart/shopping-cart.component';
import { CheckoutComponent } from './components/checkout/checkout/checkout.component';
import { CheckoutCompleteComponent } from './components/checkout/checkout-complete/checkout-complete.component';
import { CartService } from './services/carts/cart.service';
import { CartTokenService } from './services/carts/cart-token.service';
import { FooterComponent } from './components/footer/footer.component';
import { ProfileComponent } from './components/account/profile/profile.component';
import { OrderComponent } from './components/orders/order/order.component';
import { OrdersComponent } from './components/orders/orders/orders.component';
import { CustomerServiceComponent } from './components/customer-service/customer-service.component';
import { AboutComponent } from './components/about/about.component';
import { LoaderInterceptor } from './common/loader.interceptor';
import { LoaderComponent } from './components/loader/loader.component';

import { L10nConfig, L10nLoader, TranslationModule, StorageStrategy, ProviderType } from 'angular-l10n';
import { Sanitizer } from './pipes/sanitizer';
import { AuthGuardDirective } from './directives/auth-guard.directive';
import { CartTokenInterceptor } from './common/cart-token.interceptor';
import { NgSelectDirective } from './directives/ng-select.directive';
import { ImageLoaderDirective } from './directives/image-loader.directive';


const l10nConfig: L10nConfig = {
    locale: {
        languages: [
            { code: 'en', dir: 'ltr' },
            { code: 'lv', dir: 'ltr' },
            { code: 'ru', dir: 'ltr' }
        ],
        language: 'en',
        storage: StorageStrategy.Cookie
    },
    translation: {
        providers: [
            { type: ProviderType.Static, prefix: './i18n/locale-' }
        ],
        caching: true,
        missingValue: 'Missing Translation Key'
    }
};

@NgModule({
    declarations: [
        AuthGuardDirective,
        NgSelectDirective,
        ImageLoaderDirective,
        Sanitizer,
        AppComponent,
        DefaultLayoutComponent,
        FooterComponent,
        LoaderComponent,
        ImageLoaderComponent,
        NavMenuComponent,

        //Blog
        BlogComponent,
        BlogPagingComponent,
        ArticleComponent,

        //Catalog
        CatalogComponent,
        CatalogTopMenuComponent,
        CatalogFilterComponent,
        CatalogItemComponent,
        CatalogPagingComponent,

        //Looks
        LooksComponent,
        LooksPagingComponent,
        LookComponent,

        //Cart
        ShoppingCartComponent,
        CheckoutComponent,
        CheckoutCompleteComponent,

        //Orders
        OrderComponent,
        OrdersComponent,

        FetchDataComponent,
        HomeComponent,
        AboutComponent,
        CustomerServiceComponent,
        LoginComponent,
        ForgotPasswordComponent,
        ResetPasswordComponent,
        ProfileComponent,


        AdminMenuComponent,

        //Admin Modue - Dashboard
        AdminDashboardComponent,
        AdminDashboardStatsComponent,

        //Admin - Module - Orders
        AdminOrdersComponent,
        AdminOrderComponent,

        //Admin Module - Catalog - Category
        AdminCategoriesComponent,
        AdminCategoryMenuComponent,
        AdminCategoryComponent,
        AdminCategoryGeneralComponent,
        AdminCategoryPictureComponent,

        //Admin Module - Catalog - Attribute
        AdminAttributesComponent,
        AdminAttributeComponent,

        //Admin Module - Catalog - Specification
        AdminSpecificationsComponent,
        AdminSpecificationMenuComponent,
        AdminSpecificationComponent,
        AdminSpecificationGeneralComponent,
        AdminSpecificationValuesComponent,

        //Admin Module - Catalog - Inventory
        AdminInventoriesComponent,
        AdminInventoryComponent,
        AdminInventoryMenuComponent,
        AdminInventoryGeneralComponent,
        AdminInventoryPictureComponent,
        AdminInventoryAttributesComponent,
        AdminInventoryAttributeComponent,
        AdminInventoryAttributeMenuComponent,
        AdminInventoryAttributeGeneralComponent,
        AdminInventoryAttributeValuesComponent,
        AdminInventorySpecificationsComponent,

        //Admin Module - Catalog - Inventory Groups
        AdminInventoryGroupsComponent,
        AdminInventoryGroupComponent,
        AdminInventoryGroupMenuComponent,
        AdminInventoryGroupGeneralComponent,
        AdminInventoryGroupPictureComponent,
        AdminInventoryGroupInventoriesComponent,

        //Admin Module - Blog
        AdminArticlesComponent,
        AdminArticleComponent,
        AdminArticleMenuComponent,
        AdminArticleGeneralComponent,
        AdminArticlePictureComponent
    ],
    providers: [
        AuthService,
        AuthGuard,
        {
            provide: 'LOCAL_STORAGE',
            useFactory: getLocalStorage
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: AuthInterceptor,
            multi: true,
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: CartTokenInterceptor,
            multi: true,
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: LoaderInterceptor,
            multi: true,
        },
        CartTokenService,
        CartService
    ],
    imports: [
        BrowserModule,
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        HttpClientModule,
        MDBBootstrapModule.forRoot(),
        TranslationModule.forRoot(l10nConfig),
        WavesModule,
        NgSelectModule,
        ImageCropperModule,
        EditorModule,
        RouterModule.forRoot([
            {
                path: '', component: DefaultLayoutComponent, children: [
                    { path: '', component: HomeComponent },
                    { path: 'about', component: AboutComponent },
                    { path: 'customer-service', component: CustomerServiceComponent },
                    { path: 'blog', component: BlogComponent },
                    { path: 'article/:id', component: ArticleComponent },
                    { path: 'catalog', component: CatalogComponent },
                    { path: 'catalog/:categoryId', component: CatalogComponent },
                    { path: 'catalog-item/:id', component: CatalogItemComponent },
                    { path: 'looks', component: LooksComponent },
                    { path: 'look/:id', component: LookComponent },
                    { path: 'cart', component: ShoppingCartComponent },
                    { path: 'checkout', component: CheckoutComponent },
                    { path: 'thankyou', component: CheckoutCompleteComponent },
                    { path: 'login', component: LoginComponent },
                    { path: 'recover', component: ForgotPasswordComponent },
                    { path: 'reset-password/:token', component: ResetPasswordComponent},
                    { path: 'orders', component: OrdersComponent, canActivate: [AuthGuard] },
                    { path: 'order/:id', component: OrderComponent, canActivate: [AuthGuard] },
                    { path: 'profile', component: ProfileComponent, canActivate: [AuthGuard] }
                ]
            },
            {
                path: 'admin',
                component: AdminMenuComponent,
                children: [
                    { path: '', redirectTo: "dashboard", pathMatch: 'full' },
                    { path: 'dashboard', component: AdminDashboardComponent, canActivate: [AuthGuard], data: { roles: ['admin'] } },

                    //Orders
                    { path: 'orders', component: AdminOrdersComponent, canActivate: [AuthGuard], data: { roles: ['admin'] } },
                    { path: 'order/:id', component: AdminOrderComponent, canActivate: [AuthGuard], data: { roles: ['admin'] } },
                    //Category
                    { path: 'categories', component: AdminCategoriesComponent, canActivate: [AuthGuard], data: { roles: ['admin'] } },
                    {
                        path: 'category', component: AdminCategoryComponent, children: [
                            { path: '', redirectTo: "general", pathMatch: 'full' },
                            { path: 'general', component: AdminCategoryGeneralComponent, canActivate: [AuthGuard], data: { roles: ['admin'] } }
                        ]
                    },
                    {
                        path: 'category/:id', component: AdminCategoryComponent, children: [
                            { path: '', redirectTo: "general", pathMatch: 'full' },
                            { path: 'general', component: AdminCategoryGeneralComponent, canActivate: [AuthGuard], data: { roles: ['admin'] } },
                            { path: 'picture', component: AdminCategoryPictureComponent, canActivate: [AuthGuard], data: { roles: ['admin'] } }
                        ]
                    },
                    //Attribute
                    { path: 'attributes', component: AdminAttributesComponent, canActivate: [AuthGuard], data: { roles: ['admin'] } },
                    { path: 'attribute', component: AdminAttributeComponent, canActivate: [AuthGuard], data: { roles: ['admin'] } },
                    { path: 'attribute/:id', component: AdminAttributeComponent, canActivate: [AuthGuard], data: { roles: ['admin'] } },
                    //Specification
                    { path: 'specifications', component: AdminSpecificationsComponent, canActivate: [AuthGuard], data: { roles: ['admin'] } },
                    {
                        path: 'specification', component: AdminSpecificationComponent, children: [
                            { path: '', redirectTo: "general", pathMatch: 'full' },
                            { path: 'general', component: AdminSpecificationGeneralComponent, canActivate: [AuthGuard], data: { roles: ['admin'] } }
                        ]
                    },
                    {
                        path: 'specification/:id', component: AdminSpecificationComponent, children: [
                            { path: '', redirectTo: "general", pathMatch: 'full' },
                            { path: 'general', component: AdminSpecificationGeneralComponent, canActivate: [AuthGuard], data: { roles: ['admin'] } },
                            { path: 'values', component: AdminSpecificationValuesComponent, canActivate: [AuthGuard], data: { roles: ['admin'] } }
                        ]
                    },
                    //Inventory
                    { path: 'inventories', component: AdminInventoriesComponent, canActivate: [AuthGuard], data: { roles: ['admin'] } },
                    {
                        path: 'inventory', component: AdminInventoryComponent, children: [
                            { path: '', redirectTo: "general", pathMatch: 'full' },
                            { path: 'general', component: AdminInventoryGeneralComponent, canActivate: [AuthGuard], data: { roles: ['admin'] } },
                        ]
                    },
                    {
                        path: 'inventory/:id', component: AdminInventoryComponent, children: [
                            { path: '', redirectTo: "general", pathMatch: 'full' },
                            { path: 'general', component: AdminInventoryGeneralComponent, canActivate: [AuthGuard], data: { roles: ['admin'] } },
                            { path: 'pictures', component: AdminInventoryPictureComponent, canActivate: [AuthGuard], data: { roles: ['admin'] } },
                            { path: 'attributes', component: AdminInventoryAttributesComponent, canActivate: [AuthGuard], data: { roles: ['admin'] } },
                            {
                                path: 'attribute', component: AdminInventoryAttributeComponent, children: [
                                    { path: '', redirectTo: "general", pathMatch: 'full' },
                                    { path: 'general', component: AdminInventoryAttributeGeneralComponent, canActivate: [AuthGuard], data: { roles: ['admin'] } },
                                ]
                            },
                            {
                                path: 'attribute/:attributeId', component: AdminInventoryAttributeComponent, children: [
                                    { path: '', redirectTo: "general", pathMatch: 'full' },
                                    { path: 'general', component: AdminInventoryAttributeGeneralComponent, canActivate: [AuthGuard], data: { roles: ['admin'] } },
                                    { path: 'values', component: AdminInventoryAttributeValuesComponent, canActivate: [AuthGuard], data: { roles: ['admin'] } }
                                ]
                            },
                            { path: 'specifications', component: AdminInventorySpecificationsComponent, canActivate: [AuthGuard], data: { roles: ['admin'] } }
                        ]
                    },
                    //Inventory Groups
                    { path: 'inventory-groups', component: AdminInventoryGroupsComponent, canActivate: [AuthGuard], data: { roles: ['admin'] } },
                    {
                        path: 'inventory-group', component: AdminInventoryGroupComponent, children: [
                            { path: '', redirectTo: "general", pathMatch: 'full' },
                            { path: 'general', component: AdminInventoryGroupGeneralComponent, canActivate: [AuthGuard], data: { roles: ['admin'] } },
                        ]
                    },
                    {
                        path: 'inventory-group/:id', component: AdminInventoryComponent, children: [
                            { path: '', redirectTo: "general", pathMatch: 'full' },
                            { path: 'general', component: AdminInventoryGroupGeneralComponent, canActivate: [AuthGuard], data: { roles: ['admin'] } },
                            { path: 'picture', component: AdminInventoryGroupPictureComponent, canActivate: [AuthGuard], data: { roles: ['admin'] } },
                            { path: 'inventory', component: AdminInventoryGroupInventoriesComponent, canActivate: [AuthGuard], data: { roles: ['admin'] } }
                        ]
                    },
                    //Blog
                    { path: 'articles', component: AdminArticlesComponent, canActivate: [AuthGuard], data: { roles: ['admin'] } },
                    {
                        path: 'article', component: AdminArticleComponent, children: [
                            { path: '', redirectTo: "general", pathMatch: 'full' },
                            { path: 'general', component: AdminArticleGeneralComponent, canActivate: [AuthGuard], data: { roles: ['admin'] } },
                        ]
                    },
                    {
                        path: 'article/:id', component: AdminArticleComponent, children: [
                            { path: '', redirectTo: "general", pathMatch: 'full' },
                            { path: 'general', component: AdminArticleGeneralComponent, canActivate: [AuthGuard], data: { roles: ['admin'] } },
                            { path: 'picture', component: AdminArticlePictureComponent, canActivate: [AuthGuard], data: { roles: ['admin'] } },
                        ]
                    },
                ]
            },
            { path: '**', redirectTo: '' }
        ])
    ],
    schemas: [NO_ERRORS_SCHEMA]
})

export class AppModuleShared {
    constructor(public l10nLoader: L10nLoader) {
        this.l10nLoader.load();
    }
}

export function getLocalStorage() {
    return (typeof window !== "undefined") ? window.localStorage : null;
}