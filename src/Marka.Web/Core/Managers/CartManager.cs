﻿using Marka.BusinessLogic.Interfaces;
using Marka.Common.Models.Account.Users;
using Marka.Common.Results;
using Marka.Web.Options;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Marka.Web.Core.Managers
{
    public interface ICartManager
    {
        //Task<string> GetUserCartId();
        Task<int?> GetUserIdAsync();
        Task<string> GetCartIdAsync();
    }

    public class CartManager : ICartManager
    {
        private const string CartTokenHeader = "CartToken";

        private readonly IHttpContextAccessor _context;
        private readonly IDataProtector _protector;

        private readonly UserManager<User> _userManager;
        private readonly ICartService _cartService;

        public CartManager(
            IOptions<CartOptions> options,
            IHttpContextAccessor context,
            IDataProtectionProvider provider,
            UserManager<User> userManager,
            ICartService cartService)
        {
            _context = context;
            _protector = provider.CreateProtector(options.Value.CartEncryptionPurpose);

            _userManager = userManager;
            _cartService = cartService;
        }

        //Extract to context helper
        public async Task<int?> GetUserIdAsync()
        {
            var userPrincipal = _context.HttpContext.User;

            if (userPrincipal.Identity.IsAuthenticated)
            {
                var email = userPrincipal.FindFirst(ClaimTypes.NameIdentifier)?.Value;
                var user = await _userManager.FindByEmailAsync(email);

                return user?.Id;
            }

            return null;
        }

        public async Task<string> GetUserCartId()
        {
            var email = _context.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier)?.Value;

            if (email != null)
            {
                var user = await _userManager.FindByEmailAsync(email);
                if (user != null)
                {
                    var result = await _cartService.GetUserCartAsync(user.Id);

                    if (result.IsSuccessful())
                        return result.Value.Id;
                }
            }

            return null;
        }

        public async Task<string> GetCartIdAsync()
        {
            var userPrincipal = _context.HttpContext.User;

            var id = Guid.NewGuid();

            if (userPrincipal.Identity.IsAuthenticated)
            {
                var email = userPrincipal.FindFirst(ClaimTypes.NameIdentifier)?.Value;

                if (email != null)
                {
                    var user = await _userManager.FindByEmailAsync(email);
                    if (user != null)
                    {
                        var result = await _cartService.GetUserCartAsync(user.Id);

                        if (result.IsSuccessful())
                            return result.Value.Id;
                    }
                }
            }
            else
            {
                var token = GetCartToken();

                if (!string.IsNullOrEmpty(token))
                    Guid.TryParse(_protector.Unprotect(token), out id);
            }

            return id.ToString("N");
        }

        //Extract to context helper
        private string GetCartToken()
        {
            return _context.HttpContext.Request.Headers[CartTokenHeader].FirstOrDefault();
        }
    }
}
