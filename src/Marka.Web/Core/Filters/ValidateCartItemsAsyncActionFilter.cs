﻿using Marka.BusinessLogic.Interfaces;
using Marka.BusinessLogic.Interfaces.Catalog.Inventories;
using Marka.Common.Responses;
using Marka.Common.Results;
using Marka.Web.Constants;
using Marka.Web.Core.Managers;
using Marka.Web.Extensions;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Marka.Web.Core.Filters
{
    public class ValidateCartItemsAsyncActionFilter : ActionFilterAttribute
    {
        public override async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            var response = new ApiResponse(HttpStatusCode.BadRequest);

            var cartManager = context.HttpContext.RequestServices.GetService<ICartManager>();

            var cartService = context.HttpContext.RequestServices.GetService<ICartService>();
            var inventoryService = context.HttpContext.RequestServices.GetService<IInventoryService>();

            var cartId = await cartManager.GetCartIdAsync();

            var cartResult = await cartService.GetCartAsync(cartId);

            if (cartResult.IsSuccessful())
            {
                if(cartResult.Value.CartItems.Any())
                {
                    foreach (var cartItem in cartResult.Value.CartItems)
                    {
                        var langCode = LanguageConstants.LanguageCodes.English;
                        var inventoryResult = await inventoryService.GetTranslatedInventoryAsync(cartItem.InventoryId, langCode);

                        if (inventoryResult.IsSuccessful())
                        {
                            var inventory = inventoryResult.Value;

                            if (inventory.Disabled || inventory.Quantity < 1)
                                response.AddError($"{inventory.Title} is not available anymore.");
                            else if (cartItem.Quantity > inventory.Quantity)
                                response.AddError($"{inventory.Title} is not available in quantity of {cartItem.Quantity} anymore. Available quantity: {inventory.Quantity}.");
                        }
                    }
                }
                else
                    response.AddError($"Shopping Cart has no items.");


                if (response.Errors != null && response.Errors.Any())
                    context.Result = response.ToActionResult();

                await base.OnActionExecutionAsync(context, next);
            }

            context.Result = response.ToActionResult();
        }
    }
}