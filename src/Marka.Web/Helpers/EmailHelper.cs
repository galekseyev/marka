﻿using Marka.Common.Constants;
using Marka.Common.Models.Orders;
using Marka.Common.Responses.Contact;
using System.IO;
using System.Threading.Tasks;

namespace Marka.Web.Helpers
{
    public static class EmailHelper
    {
        public static async Task<string> GetEmailBodyTemplate(string rootPath, string templateType, string templateName)
        {
            var body = string.Empty;

            var path = Path.Combine(rootPath, "HtmlTemplates", "EmailTemplates", templateType, $"{templateName}.html");

            using (StreamReader reader = new StreamReader(path))
            {
                body = await reader.ReadToEndAsync();
            }

            return body;
        }

        public static string GetPendingEmailTemplateName(string paymentTypeCode)
        {
            if (paymentTypeCode == PaymentTypeCode.BankTransfer)
                return $"{PaymentTypeCode.BankTransfer}-{OrderStatusCode.Pending}";

            return OrderStatusCode.Pending;
        }

        public static string ReplaceOrderEmailPlaceholders(string body, TranslatedOrder order)
        {
            var itemsHtml = string.Empty;

            foreach (var item in order.OrderItems)
            {
                var picture = item.Picture;
                var pictureUrl = picture != null ? picture.Thumbnail : string.Empty;

                itemsHtml += $@"<tr><td><img width='150' height='auto' src='{pictureUrl}'></td><td>{item.Title}</td><td>{item.Quantity}</td><td>€{item.Price.ToString("F")}</td>";
            }

            body = body.Replace("{{order_number}}", order.OrderNumber);
            body = body.Replace("{{inventory_items}}", itemsHtml);

            body = body.Replace("{{order_total}}", order.TotalPrice.ToString("F"));

            body = body.Replace("{{full_name}}", order.Shipment.FullName);
            body = body.Replace("{{address}}", order.Shipment.Address1);
            body = body.Replace("{{country}}", order.Shipment.Country.CountryName);
            body = body.Replace("{{city}}", order.Shipment.City);
            body = body.Replace("{{postal_code}}", order.Shipment.PostalCode);

            return body;
        }

        public static string ReplaceContactUsEmailPlaceholders(string body, SendMessageRequest sendMessageRequest)
        {
            return body.Replace("{{name}}", sendMessageRequest.Name)
                .Replace("{{email}}", sendMessageRequest.Email)
                .Replace("{{message}}", sendMessageRequest.Message);
        }
    }
}