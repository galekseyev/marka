﻿namespace Marka.Web.Constants
{
    public static class LanguageConstants
    {
        public static class LanguageTitles
        {
            public const string English = "English";
            public const string Russian = "Russian";
            public const string Latvian = "Latvian";
        }

        public static class LanguageCodes
        {
            public const string English = "en-US";
            public const string Russian = "ru-RU";
            public const string Latvian = "lv-LV";
        }
    }
}