﻿using AutoMapper;
using Marka.Common.Models.Carts;
using Marka.Common.Models.Orders;
using Marka.Common.ResponseModels.Carts;
using Marka.Common.Responses;
using Marka.Common.Responses.Base;
using Marka.Common.Responses.Carts;
using Marka.Web.Extensions;
using Microsoft.AspNetCore.Mvc;

namespace Marka.Web.Mappings.Carts
{
    public class CartMappingProfile : Profile
    {
        public CartMappingProfile()
        {
            #region Cart

            CreateMap<CartRequest, Cart>()
                .ForMember(c => c.Id, c => c.Ignore());

            CreateMap<Cart, CartModel>()
                .ForMember(m => m.Key, m => m.Ignore());

            CreateMap<Cart, CartResponse>()
               .ForMember(m => m.Embedded, m => m.MapFrom(p => p))
               .ForMember(m => m.Links, m => m.MapHalLinks((url, source) => new HalLinks
               {
                   Self = new HalLink(url.Action("GetCart", "Cart"))
               }));

            #endregion

            #region CartItems

            CreateMap<CartItemRequest, CartItem>()
                .ForMember(c => c.Id, c => c.Ignore())
                .ForMember(c => c.CartItemAttributesValuesXref, c => c.MapFrom(x => x.CartItemAttributesValuesXref));

            CreateMap<CartItem, CartItemModel>();

            CreateMap<CartItemAttributesValuesXref, CartItemAttributesValuesXrefModel>();
            CreateMap<CartItemAttributesValuesXrefModel, CartItemAttributesValuesXref>();

            #endregion

            CreateMap<CartShipping, CartShippingModel>();

            #region TranslatedCart

            CreateMap<TranslatedCart, TranslatedCartModel>()
                .ForMember(m => m.Key, m => m.Ignore());

            CreateMap<TranslatedCart, TranslatedCartResponse>()
                .ForMember(m => m.Embedded, m => m.MapFrom(p => p))
                .ForMember(m => m.Links, m => m.MapHalLinks((url, source) => new HalLinks
                {
                    Self = new HalLink(url.Action("GetCart", "Cart"))
                }));

            CreateMap<TranslatedCartItem, TranslatedCartItemModel>();

            CreateMap<TranslatedCartItemAttributesValuesXref, TranslatedCartItemAttributesValuesXrefModel>();

            #endregion

            #region Cart -> Order

            CreateMap<CartItem, OrderItem>()
                .ForMember(c => c.Id, c => c.Ignore())
                .ForMember(c => c.OrderId, c => c.Ignore())
                .ForMember(c => c.Price, c => c.MapFrom(x => x.Inventory.Price))
                .ForMember(c => c.OrderItemAttributesValuesXref, c => c.MapFrom(x => x.CartItemAttributesValuesXref));

            CreateMap<CartItemAttributesValuesXref, OrderItemAttributesValuesXref>()
               .ForMember(c => c.Id, c => c.Ignore())
               .ForMember(c => c.OrderItemId, c => c.Ignore());

            #endregion
        }
    }
}
