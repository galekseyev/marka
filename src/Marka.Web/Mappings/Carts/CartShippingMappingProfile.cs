﻿using AutoMapper;
using Marka.Common.Models.Carts;
using Marka.Common.ResponseModels.Carts;
using Marka.Common.Responses;
using Marka.Common.Responses.Base;
using Marka.Common.Responses.Carts;
using Marka.Web.Extensions;
using Microsoft.AspNetCore.Mvc;

namespace Marka.Web.Mappings.Carts
{
    public class CartShippingMappingProfile : Profile
    {
        public CartShippingMappingProfile()
        {
            CreateMap<CartShippingRequest, CartShipping>()
                .ForMember(m => m.Id, m => m.Ignore());

            CreateMap<CartShipping, CartShippingModel>();

            CreateMap<CartShipping, CartShippingResponse>()
                .ForMember(m => m.Embedded, m => m.MapFrom(p => p))

                .ForMember(m => m.Links, m => m.MapHalLinks((url, source) => new HalLinks
                {
                    Self = new HalLink(url.Action("GetCartShipping", "CartShipping", new
                    {
                        cartShippingId = source.Id
                    }))
                }));
        }
    }
}