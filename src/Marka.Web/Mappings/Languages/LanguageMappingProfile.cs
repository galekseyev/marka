﻿using AutoMapper;
using Marka.Common.Models.Languages;
using Marka.Common.ResponseModels.Languages;
using Marka.Common.Responses;
using Marka.Common.Responses.Base;
using Marka.Common.Responses.Languages;
using Marka.Web.Extensions;
using Microsoft.AspNetCore.Mvc;

namespace Marka.Web.Mappings.Languages
{
    public class LanguageMappingProfile : Profile
    {
        public LanguageMappingProfile()
        {
            CreateMap<Language, LanguageModel>();

            CreateMap<Language, LanguageResponse>()
                .ForMember(m => m.Embedded, m => m.MapFrom(p => p))
                .ForMember(m => m.Links, m => m.MapHalLinks((url, source) => new HalLinks
                {
                    Self = new HalLink(url.Action("GetLanguage", "Language", new
                    {
                        id = source.Id
                    }))
                }));
        }
    }
}
