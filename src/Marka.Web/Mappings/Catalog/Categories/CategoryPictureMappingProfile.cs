﻿using AutoMapper;
using Marka.Common.Models.Catalog.Categories;
using Marka.Common.ResponseModels.Catalog.Categories;
using Marka.Common.Responses;
using Marka.Common.Responses.Base;
using Marka.Common.Responses.Catalog.Categories.Pictures;
using Marka.Web.Extensions;
using Microsoft.AspNetCore.Mvc;

namespace Marka.Web.Mappings.Catalog.Categories
{
    public class CategoryPictureMappingProfile : Profile
    {
        public CategoryPictureMappingProfile()
        {
            CreateMap<CategoryPictureRequest, CategoryPicture>()
                .ForMember(c => c.Id, m => m.Ignore());

            CreateMap<CategoryPicture, CategoryPictureModel>();

            CreateMap<CategoryPicture, CategoryPictureResponse>()
                .ForMember(m => m.Embedded, m => m.MapFrom(p => p))
                .ForMember(m => m.Links, m => m.MapHalLinks((url, source) => new HalLinks
                {
                    Self = new HalLink(url.Action("GetCategoryPicture", "CategoryPicture", new
                    {
                        id = source.Id
                    }))
                }));

        }
    }
}
