﻿using AutoMapper;
using Marka.Common.Models.Catalog.Categories;
using Marka.Common.ResponseModels.Catalog.Categories;
using Marka.Common.Responses;
using Marka.Common.Responses.Base;
using Marka.Common.Responses.Catalog.Categories;
using Marka.Web.Extensions;
using Microsoft.AspNetCore.Mvc;

namespace Marka.Web.Mappings.Catalog.Categories
{
    public class CategoryMappingProfile : Profile
    {
        public CategoryMappingProfile()
        {
            CreateMap<CategoryTranslationModel, CategoryTranslation>();
            CreateMap<CategoryTranslation, CategoryTranslationModel>();

            CreateMap<CategoryRequest, Category>()
                .ForMember(m => m.Id, m => m.Ignore());

            CreateMap<Category, CategoryModel>();

            CreateMap<Category, CategoryResponse>()
                .ForMember(m => m.Embedded, m => m.MapFrom(p => p))
                .ForMember(m => m.Links, m => m.MapHalLinks((url, source) => new HalLinks
                {
                    Self = new HalLink(url.Action("GetCategory", "Category", new
                    {
                        id = source.Id
                    }))
                }));

            //Translated
            CreateMap<TranslatedCategory, TranslatedCategoryModel>();

            CreateMap<TranslatedCategory, TranslatedCategoryResponse>()
                .ForMember(m => m.Embedded, m => m.MapFrom(p => p))
                .ForMember(m => m.Links, m => m.MapHalLinks((url, source) => new HalLinks
                {
                    Self = new HalLink(url.Action("GetTranslatedCategory", "Category", new
                    {
                        id = source.Id
                    }))
                }));
        }
    }
}
