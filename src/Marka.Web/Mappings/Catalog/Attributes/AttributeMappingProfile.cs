﻿using AutoMapper;
using Marka.Common.Models.Catalog.Attributes;
using Marka.Common.ResponseModels.Catalog.Attributes;
using Marka.Common.Responses;
using Marka.Common.Responses.Base;
using Marka.Common.Responses.Catalog.Attributes;
using Marka.Web.Extensions;
using Microsoft.AspNetCore.Mvc;

namespace Marka.Web.Mappings.Catalog.Attributes
{
    public class AttributeMappingProfile : Profile
    {
        public AttributeMappingProfile()
        {
            CreateMap<AttributeTranslationModel, AttributeTranslation>();
            CreateMap<AttributeTranslation, AttributeTranslationModel>();

            CreateMap<AttributeRequest, Attribute>()
                .ForMember(m => m.Id, m => m.Ignore());

            CreateMap<Attribute, AttributeModel>();

            CreateMap<Attribute, AttributeResponse>()
                .ForMember(m => m.Embedded, m => m.MapFrom(p => p))
                .ForMember(m => m.Links, m => m.MapHalLinks((url, source) => new HalLinks
                {
                    Self = new HalLink(url.Action("GetAttribute", "Attribute", new
                    {
                        id = source.Id
                    }))
                }));

            //Translated
            CreateMap<TranslatedAttribute, TranslatedAttributeModel>();

            CreateMap<TranslatedAttribute, TranslatedAttributeResponse>()
                .ForMember(m => m.Embedded, m => m.MapFrom(p => p))
                .ForMember(m => m.Links, m => m.MapHalLinks((url, source) => new HalLinks
                {
                    Self = new HalLink(url.Action("GetTranslatedAttribute", "Attribute", new
                    {
                        id = source.Id
                    }))
                }));
        }
    }
}