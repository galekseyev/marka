﻿using AutoMapper;
using Marka.Common.Models.Catalog.Inventories;
using Marka.Common.ResponseModels.Catalog.Inventories;
using Marka.Common.Responses;
using Marka.Common.Responses.Base;
using Marka.Common.Responses.Catalog.Inventories.Pictures;
using Marka.Web.Extensions;
using Microsoft.AspNetCore.Mvc;

namespace Marka.Web.Mappings.Catalog.Inventories
{
    public class InventoryPictureMappingProfile : Profile
    {
        public InventoryPictureMappingProfile()
        {
            CreateMap<InventoryPictureRequest, InventoryPicture>()
                .ForMember(m => m.Id, m => m.Ignore())
                .ForMember(m => m.InventoryId, m => m.Ignore())
                .ForMember(m => m.Picture, m => m.Ignore())
                .ForMember(m => m.Thumbnail, m => m.Ignore());

            CreateMap<InventoryPicture, InventoryPictureModel>();

            CreateMap<InventoryPicture, InventoryPictureResponse>()
                .ForMember(m => m.Embedded, m => m.MapFrom(p => p))
                .ForMember(m => m.Links, m => m.MapHalLinks((url, source) => new HalLinks
                {
                    Self = new HalLink(url.Action("GetInventoryPicture", "InventoryPicture", new
                    {
                        id = source.InventoryId,
                        pictureId = source.Id
                    }))
                }));
        }
    }
}