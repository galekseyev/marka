﻿using AutoMapper;
using Marka.Common.Models.Catalog.Inventories;
using Marka.Common.ResponseModels.Catalog.Inventories;
using Marka.Common.Responses;
using Marka.Common.Responses.Base;
using Marka.Common.Responses.Catalog.Inventories.Attributes;
using Marka.Web.Extensions;
using Microsoft.AspNetCore.Mvc;

namespace Marka.Web.Mappings.Catalog.Inventories
{
    public class InventoryAttributeMappingProfile : Profile
    {
        public InventoryAttributeMappingProfile()
        {
            CreateMap<InventoryAttributeRequest, InventoryAttribute>()
                .ForMember(m => m.Id, m => m.Ignore())
                .ForMember(m => m.InventoryId, m => m.Ignore());

            CreateMap<InventoryAttribute, InventoryAttributeModel>();

            CreateMap<InventoryAttribute, InventoryAttributeResponse>()
                .ForMember(m => m.Embedded, m => m.MapFrom(p => p))

                .ForMember(m => m.Links, m => m.MapHalLinks((url, source) => new HalLinks
                {
                    Self = new HalLink(url.Action("GetInventoryAttribute", "InventoryAttribute", new
                    {
                        id = source.InventoryId,
                        pictureId = source.Id
                    }))
                }));


            //Translated
            CreateMap<TranslatedInventoryAttribute, TranslatedInventoryAttributeModel>();

            CreateMap<TranslatedInventoryAttribute, TranslatedInventoryAttributeResponse>()
                .ForMember(m => m.Embedded, m => m.MapFrom(p => p))
                .ForMember(m => m.Links, m => m.MapHalLinks((url, source) => new HalLinks
                {
                    Self = new HalLink(url.Action("GetTranslatedInventoryAttributes", "InventoryAttribute", new
                    {
                        id = source.Id
                    }))
                }));
        }
    }
}
