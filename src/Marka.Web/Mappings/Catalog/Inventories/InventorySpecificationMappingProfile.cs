﻿using AutoMapper;
using Marka.Common.Models.Catalog.Inventories;
using Marka.Common.ResponseModels.Catalog.Inventories;
using Marka.Common.Responses;
using Marka.Common.Responses.Base;
using Marka.Common.Responses.Catalog.Inventories.Specifications;
using Marka.Web.Extensions;
using Microsoft.AspNetCore.Mvc;

namespace Marka.Web.Mappings.Catalog.Inventories
{
    public class InventorySpecificationMappingProfile : Profile
    {
        public InventorySpecificationMappingProfile()
        {
            CreateMap<InventorySpecificationRequest, InventorySpecification>()
                .ForMember(m => m.Id, m => m.Ignore());

            CreateMap<InventorySpecification, InventorySpecificationModel>();

            CreateMap<InventorySpecification, InventorySpecificationResponse>()
                .ForMember(m => m.Embedded, m => m.MapFrom(p => p))
                .ForMember(m => m.Links, m => m.MapHalLinks((url, source) => new HalLinks
                {
                    Self = new HalLink(url.Action("GetInventorySpecification", "InventorySpecification", new
                    {
                        id = source.Id
                    }))
                }));

            //Translated
            CreateMap<TranslatedInventorySpecification, TranslatedInventorySpecificationModel>();

            CreateMap<TranslatedInventorySpecification, TranslatedInventorySpecificationResponse>()
                .ForMember(m => m.Embedded, m => m.MapFrom(p => p))
                .ForMember(m => m.Links, m => m.MapHalLinks((url, source) => new HalLinks
                {
                    Self = new HalLink(url.Action("GetTranslatedInventorySpecifications", "InventorySpecification", new
                    {
                        id = source.Id
                    }))
                }));
        }
    }
}