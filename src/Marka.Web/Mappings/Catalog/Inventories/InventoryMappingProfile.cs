﻿using AutoMapper;
using Marka.Common.Models.Catalog.Inventories;
using Marka.Common.ResponseModels.Catalog.Inventories;
using Marka.Common.Responses;
using Marka.Common.Responses.Base;
using Marka.Common.Responses.Catalog.Inventories;
using Marka.Web.Extensions;
using Microsoft.AspNetCore.Mvc;

namespace Marka.Web.Mappings.Catalog.Inventories
{
    public class InventoryMappingProfile : Profile
    {
        public InventoryMappingProfile()
        {
            CreateMap<InventoryTranslationModel, InventoryTranslation>();
            CreateMap<InventoryTranslation, InventoryTranslationModel>();

            CreateMap<InventoryRequest, Inventory>()
                .ForMember(m => m.Id, m => m.Ignore());

            CreateMap<Inventory, InventoryModel>();

            CreateMap<Inventory, InventoryResponse>()
                .ForMember(m => m.Embedded, m => m.MapFrom(p => p))
                .ForMember(m => m.Links, m => m.MapHalLinks((url, source) => new HalLinks
                {
                    Self = new HalLink(url.Action("GetInventory", "Inventory", new
                    {
                        id = source.Id
                    }))
                }));

            //Translated
            CreateMap<TranslatedInventory, TranslatedInventoryModel>();

            CreateMap<TranslatedInventory, TranslatedInventoryResponse>()
                .ForMember(m => m.Embedded, m => m.MapFrom(p => p))
                .ForMember(m => m.Links, m => m.MapHalLinks((url, source) => new HalLinks
                {
                    Self = new HalLink(url.Action("GetTranslatedCategory", "Category", new
                    {
                        id = source.Id
                    }))
                }));
        }
    }
}