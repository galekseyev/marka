﻿using AutoMapper;
using Marka.Common.Models.Catalog.Inventories;
using Marka.Common.ResponseModels.Catalog.Inventories;
using Marka.Common.Responses;
using Marka.Common.Responses.Base;
using Marka.Common.Responses.Catalog.Inventories.Attributes;
using Marka.Web.Extensions;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Marka.Web.Mappings.Catalog.Inventories
{
    public class InventoryAttributeValueMappingProfile : Profile
    {
        public InventoryAttributeValueMappingProfile()
        {
            CreateMap<InventoryAttributeValueTranslationModel, InventoryAttributeValueTranslation>();
            CreateMap<InventoryAttributeValueTranslation, InventoryAttributeValueTranslationModel>();

            CreateMap<InventoryAttributeValueRequest, InventoryAttributeValue>()
                .ForMember(m => m.Id, m => m.Ignore());

            CreateMap<InventoryAttributeValue, InventoryAttributeValueModel>();

            CreateMap<InventoryAttributeValue, InventoryAttributeValueResponse>()
                .ForMember(m => m.Embedded, m => m.MapFrom(p => p))
                .ForMember(m => m.Links, m => m.MapHalLinks((url, source) => new HalLinks
                {
                    Self = new HalLink(url.Action("GetInventoryAttributeValue", "InventoryAttributeValue", new
                    {
                        id = source.Id
                    }))
                }));

            //Translated
            CreateMap<TranslatedInventoryAttributeValue, TranslatedInventoryAttributeValueModel>();

            CreateMap<TranslatedInventoryAttributeValue, TranslatedInventoryAttributeValueResponse>()
                .ForMember(m => m.Embedded, m => m.MapFrom(p => p))
                .ForMember(m => m.Links, m => m.MapHalLinks((url, source) => new HalLinks
                {
                    Self = new HalLink(url.Action("GetTranslatedInventoryAttributeValues", "InventoryAttributeValue", new
                    {
                        id = source.Id
                    }))
                }));
        }
    }
}