﻿using AutoMapper;
using Marka.Common.Models.Catalog.Specifications;
using Marka.Common.ResponseModels.Catalog.Specifications;
using Marka.Common.Responses;
using Marka.Common.Responses.Base;
using Marka.Common.Responses.Catalog.Specifications;
using Marka.Web.Extensions;
using Microsoft.AspNetCore.Mvc;

namespace Marka.Web.Mappings.Catalog.Specificaitons
{
    public class SpecificationMappingProfile : Profile
    {
        public SpecificationMappingProfile()
        {
            CreateMap<SpecificationTranslationModel, SpecificationTranslation>();
            CreateMap<SpecificationTranslation, SpecificationTranslationModel>();

            CreateMap<SpecificationRequest, Specification>()
                .ForMember(m => m.Id, m => m.Ignore());

            CreateMap<Specification, SpecificationModel>();

            CreateMap<Specification, SpecificationResponse>()
                .ForMember(m => m.Embedded, m => m.MapFrom(p => p))
                .ForMember(m => m.Links, m => m.MapHalLinks((url, source) => new HalLinks
                {
                    Self = new HalLink(url.Action("GetSpecification", "Specification", new
                    {
                        id = source.Id
                    }))
                }));

            //Translated
            CreateMap<TranslatedSpecification, TranslatedSpecificationModel>();

            CreateMap<TranslatedSpecification, TranslatedSpecificationResponse>()
                .ForMember(m => m.Embedded, m => m.MapFrom(p => p))
                .ForMember(m => m.Links, m => m.MapHalLinks((url, source) => new HalLinks
                {
                    Self = new HalLink(url.Action("GetTranslatedSpecification", "Specification", new
                    {
                        id = source.Id
                    }))
                }));
        }
    }
}