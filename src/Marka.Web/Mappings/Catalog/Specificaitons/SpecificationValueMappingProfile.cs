﻿using AutoMapper;
using Marka.Common.Models.Catalog.Inventories;
using Marka.Common.Models.Catalog.Specifications;
using Marka.Common.ResponseModels.Catalog.Inventories;
using Marka.Common.ResponseModels.Catalog.Specifications;
using Marka.Common.Responses;
using Marka.Common.Responses.Base;
using Marka.Common.Responses.Catalog.Specifications;
using Marka.Web.Extensions;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Marka.Web.Mappings.Catalog.Specificaitons
{
    public class SpecificationValueMappingProfile : Profile
    {
        public SpecificationValueMappingProfile()
        {
            CreateMap<SpecificationValueTranslationModel, SpecificationValueTranslation>();
            CreateMap<SpecificationValueTranslation, SpecificationValueTranslationModel>();

            CreateMap<SpecificationValueRequest, SpecificationValue>()
                .ForMember(m => m.Id, m => m.Ignore());

            CreateMap<SpecificationValue, SpecificationValueModel>();

            CreateMap<SpecificationValue, SpecificationValueResponse>()
                .ForMember(m => m.Embedded, m => m.MapFrom(p => p))
                .ForMember(m => m.Links, m => m.MapHalLinks((url, source) => new HalLinks
                {
                    Self = new HalLink(url.Action("GetSpecificationValue", "SpecificationValue", new
                    {
                        id = source.Id
                    }))
                }));

            //Translated
            CreateMap<TranslatedSpecificationValue, TranslatedSpecificationValueModel>();

            CreateMap<TranslatedSpecificationValue, TranslatedSpecificationValueResponse>()
                .ForMember(m => m.Embedded, m => m.MapFrom(p => p))
                .ForMember(m => m.Links, m => m.MapHalLinks((url, source) => new HalLinks
                {
                    Self = new HalLink(url.Action("GetSpecificationValues", "SpecificationValue", new
                    {
                        id = source.Id
                    }))
                }));
        }
    }
}
