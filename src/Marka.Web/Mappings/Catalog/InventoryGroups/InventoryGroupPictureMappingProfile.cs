﻿using AutoMapper;
using Marka.Common.Models.Catalog.InventoryGroups;
using Marka.Common.ResponseModels.Catalog.InventoryGroups;
using Marka.Common.Responses;
using Marka.Common.Responses.Base;
using Marka.Common.Responses.Catalog.InventoryGroups.Pictures;
using Marka.Web.Extensions;
using Microsoft.AspNetCore.Mvc;

namespace Marka.Web.Mappings.Catalog.InventoryGroups
{
    public class InventoryGroupPictureMappingProfile : Profile
    {
        public InventoryGroupPictureMappingProfile()
        {
            CreateMap<InventoryGroupPictureRequest, InventoryGroupPicture>()
                .ForMember(m => m.Id, m => m.Ignore())
                .ForMember(m => m.Picture, m => m.Ignore())
                .ForMember(m => m.Thumbnail, m => m.Ignore());

            CreateMap<InventoryGroupPicture, InventoryGroupPictureModel>();

            CreateMap<InventoryGroupPicture, InventoryGroupPictureResponse>()
                .ForMember(m => m.Embedded, m => m.MapFrom(p => p))
                .ForMember(m => m.Links, m => m.MapHalLinks((url, source) => new HalLinks
                {
                    Self = new HalLink(url.Action("GetInventoryGroupPicture", "InventoryGroupPicture", new
                    {
                        id = source.InventoryGroup,
                        pictureId = source.Id
                    }))
                }));
        }
    }
}
