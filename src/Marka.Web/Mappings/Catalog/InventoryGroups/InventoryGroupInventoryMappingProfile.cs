﻿using AutoMapper;
using Marka.Common.Models.Catalog.InventoryGroups;
using Marka.Common.ResponseModels.Catalog.InventoryGroups;
using Marka.Common.Responses;
using Marka.Common.Responses.Base;
using Marka.Common.Responses.Catalog.InventoryGroups.Inventories;
using Marka.Web.Extensions;
using Microsoft.AspNetCore.Mvc;

namespace Marka.Web.Mappings.Catalog.InventoryGroups
{
    public class InventoryGroupInventoryMappingProfile : Profile
    {
        public InventoryGroupInventoryMappingProfile()
        {
            CreateMap<InventoryGroupInventoryRequest, InventoryGroupInventory>()
               .ForMember(m => m.Id, m => m.Ignore());

            CreateMap<InventoryGroupInventory, InventoryGroupInventoryModel>();

            CreateMap<InventoryGroupInventory, InventoryGroupInventoryResponse>()
                .ForMember(m => m.Embedded, m => m.MapFrom(p => p))
                .ForMember(m => m.Links, m => m.MapHalLinks((url, source) => new HalLinks
                {
                    Self = new HalLink(url.Action("GetInventoryGroupInventory", "InventoryGroupInventory", new
                    {
                        id = source.Id
                    }))
                }));

            //Translated
            CreateMap<TranslatedInventoryGroupInventory, TranslatedInventoryGroupInventoryModel>();

            CreateMap<TranslatedInventoryGroupInventory, TranslatedInventoryGroupInventoryResponse>()
                .ForMember(m => m.Embedded, m => m.MapFrom(p => p))
                .ForMember(m => m.Links, m => m.MapHalLinks((url, source) => new HalLinks
                {
                    Self = new HalLink(url.Action("GetTranslatedInventoryGroupInventories", "InventoryGroupInventory", new
                    {
                        id = source.Id
                    }))
                }));
        }
    }
}
