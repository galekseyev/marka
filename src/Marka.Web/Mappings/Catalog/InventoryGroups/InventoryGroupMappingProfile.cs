﻿using AutoMapper;
using Marka.Common.Models.Catalog.Inventories;
using Marka.Common.Models.Catalog.InventoryGroups;
using Marka.Common.ResponseModels.Catalog.Inventories;
using Marka.Common.ResponseModels.Catalog.InventoryGroups;
using Marka.Common.Responses;
using Marka.Common.Responses.Base;
using Marka.Common.Responses.Catalog.InventoryGroups;
using Marka.Web.Extensions;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Marka.Web.Mappings.Catalog.InventoryGroups
{
    public class InventoryGroupMappingProfile : Profile
    {
        public InventoryGroupMappingProfile()
        {
            //CreateMap<InventoryGroupTranslationModel, InventoryTranslation>();
            //CreateMap<InventoryTranslation, InventoryTranslationModel>();

            CreateMap<InventoryGroupRequest, InventoryGroup>()
                .ForMember(m => m.Id, m => m.Ignore());

            CreateMap<InventoryGroup, InventoryGroupModel>();

            CreateMap<InventoryGroup, InventoryGroupResponse>()
                .ForMember(m => m.Embedded, m => m.MapFrom(p => p))
                .ForMember(m => m.Links, m => m.MapHalLinks((url, source) => new HalLinks
                {
                    Self = new HalLink(url.Action("GetInventoryGroup", "InventoryGroup", new
                    {
                        id = source.Id
                    }))
                }));

            //Translated
            CreateMap<TranslatedInventoryGroup, TranslatedInventoryGroupModel>();

            CreateMap<TranslatedInventoryGroup, TranslatedInventoryGroupResponse>()
                .ForMember(m => m.Embedded, m => m.MapFrom(p => p))
                .ForMember(m => m.Links, m => m.MapHalLinks((url, source) => new HalLinks
                {
                    Self = new HalLink(url.Action("GetTranslatedInventoryGroup", "InventoryGroup", new
                    {
                        id = source.Id
                    }))
                }));
        }
    }
}
