﻿using AutoMapper;
using Marka.Common.Models.Orders;
using Marka.Common.ResponseModels.Orders;
using Marka.Common.Responses;
using Marka.Common.Responses.Base;
using Marka.Common.Responses.Orders;
using Marka.Web.Extensions;
using Microsoft.AspNetCore.Mvc;

namespace Marka.Web.Mappings.Orders
{
    public class OrderStatusMappingProfile : Profile
    {
        public OrderStatusMappingProfile()
        {
            CreateMap<OrderStatus, OrderStatusModel>();

            CreateMap<OrderStatus, OrderStatusResponse>()
                .ForMember(m => m.Embedded, m => m.MapFrom(p => p))
                .ForMember(m => m.Links, m => m.MapHalLinks((url, source) => new HalLinks
                {
                    Self = new HalLink(url.Action("GetOrderStatus", "OrderStatus", new
                    {
                        id = source.Id
                    }))
                }));
        }
    }
}