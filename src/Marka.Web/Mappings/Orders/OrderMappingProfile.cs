﻿using AutoMapper;
using Marka.Common.Models.Orders;
using Marka.Common.ResponseModels.Orders;
using Marka.Common.Responses;
using Marka.Common.Responses.Base;
using Marka.Common.Responses.Orders;
using Marka.Web.Extensions;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace Marka.Web.Mappings.Orders
{
    public class OrderMappingProfile : Profile
    {
        public OrderMappingProfile()
        {
            CreateMap<OrderRequest, Order>()
                .ForMember(c => c.Id, c => c.Ignore())
                .ForMember(c => c.Shipment, c => c.MapFrom(x => x));

            CreateMap<Order, OrderModel>();

            CreateMap<Order, OrderResponse>()
                .ForMember(m => m.Embedded, m => m.MapFrom(p => p))
                .ForMember(m => m.Links, m => m.MapHalLinks((url, source) => new HalLinks
                {
                    Self = new HalLink(url.Action("GetOrder", "Order", new
                    {
                        id = source.Id
                    }))
                }));



            CreateMap<OrderItem, OrderItemModel>();

            CreateMap<OrderItemAttributesValuesXref, OrderItemAttributesValuesXrefModel>();



            CreateMap<TranslatedOrder, TranslatedOrderModel>();

            CreateMap<TranslatedOrder, TranslatedOrderResponse>()
                .ForMember(m => m.Embedded, m => m.MapFrom(p => p))
                .ForMember(m => m.Links, m => m.MapHalLinks((url, source) => new HalLinks
                {
                    Self = new HalLink(url.Action("GetOrder", "Order"))
                }));

            CreateMap<TranslatedOrderItem, TranslatedOrderItemModel>();

            CreateMap<TranslatedOrderItemAttributesValuesXref, TranslatedOrderItemAttributesValuesXrefModel>();
        }
    }
}