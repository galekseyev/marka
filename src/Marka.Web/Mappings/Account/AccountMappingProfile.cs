﻿using AutoMapper;
using Marka.Common.Models.Account.Users;
using Marka.Common.ResponseModels.Account;
using Marka.Common.Responses;
using Marka.Common.Responses.Account;
using Marka.Common.Responses.Base;
using Marka.Web.Extensions;
using Microsoft.AspNetCore.Mvc;

namespace Marka.Web.Mappings.Account
{
    public class AccountMappingProfile : Profile
    {
        public AccountMappingProfile()
        {
            CreateMap<User, UserModel>();

            CreateMap<User, UserResponse>()
                .ForMember(m => m.Embedded, m => m.MapFrom(p => p))
                .ForMember(m => m.Links, m => m.MapHalLinks((url, source) => new HalLinks
                {
                    Self = new HalLink(url.Action("GetUser", "Account", new
                    {
                        id = source.Id
                    }))
                }));

            CreateMap<User, UserProfileModel>();
            CreateMap<User, UserProfileResponse>()
                .ForMember(m => m.Embedded, m => m.MapFrom(p => p))
                .ForMember(m => m.Links, m => m.MapHalLinks((url, source) => new HalLinks
                {
                    Self = new HalLink(url.Action("GetUserProfile", "Account"))
                }));

            CreateMap<UserAddress, UserAddressModel>();
            CreateMap<UserProfileRequest, UserAddress>()
                .ForMember(m => m.Id, m => m.MapFrom(c => c.AddressId));
        }
    }
}