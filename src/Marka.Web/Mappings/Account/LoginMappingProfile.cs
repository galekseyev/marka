﻿using AutoMapper;
using Marka.Common.Responses.Account;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Marka.Web.Mappings.Account
{
    public class LoginMappingProfile : Profile
    {
        public LoginMappingProfile()
        {
            CreateMap<SignInResult, LoginResponse>()
                .ForMember(m => m.Token, m => m.Ignore());
        }
    }
}