﻿using AutoMapper;
using Marka.Common.Models.Shipments;
using Marka.Common.ResponseModels.Shipment;
using Marka.Common.Responses.Orders;

namespace Marka.Web.Mappings.Shipments
{
    public class ShipmentMappingProfile : Profile
    {
        public ShipmentMappingProfile()
        {
            CreateMap<OrderRequest, Shipment>()
                .ForMember(c => c.Id, c => c.Ignore());

            CreateMap<Shipment, ShipmentModel>();
        }
    }
}