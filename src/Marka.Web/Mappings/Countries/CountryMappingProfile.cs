﻿using AutoMapper;
using Marka.Common.Models.Countries;
using Marka.Common.ResponseModels.Countries;
using Marka.Common.Responses;
using Marka.Common.Responses.Base;
using Marka.Common.Responses.Countries;
using Marka.Web.Extensions;
using Microsoft.AspNetCore.Mvc;

namespace Marka.Web.Mappings.Countries
{
    public class CountryMappingProfile : Profile
    {
        public CountryMappingProfile()
        {
            CreateMap<Country, CountryModel>();
            CreateMap<CountryState, CountryStateModel>();

            CreateMap<Country, CountryResponse>()
                .ForMember(m => m.Embedded, m => m.MapFrom(p => p))
                .ForMember(m => m.Links, m => m.MapHalLinks((url, source) => new HalLinks
                {
                    Self = new HalLink(url.Action("GetCountriesAsync", "Country", new
                    {
                        id = source.Id
                    }))
                }));
        }
    }
}