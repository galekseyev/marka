﻿using AutoMapper;
using Marka.Common.Models.Blog;
using Marka.Common.ResponseModels.Blog;
using Marka.Common.Responses;
using Marka.Common.Responses.Base;
using Marka.Common.Responses.Blog;
using Marka.Web.Extensions;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Marka.Web.Mappings.Blog
{
    public class ArticleMappingProfile : Profile
    {
        public ArticleMappingProfile()
        {
            CreateMap<ArticleTranslationModel, ArticleTranslation>();
            CreateMap<ArticleTranslation, ArticleTranslationModel>();

            CreateMap<ArticleRequest, Article>()
                .ForMember(m => m.Id, m => m.Ignore());

            CreateMap<Article, ArticleModel>();

            CreateMap<Article, ArticleResponse>()
                .ForMember(m => m.Embedded, m => m.MapFrom(p => p))
                .ForMember(m => m.Links, m => m.MapHalLinks((url, source) => new HalLinks
                {
                    Self = new HalLink(url.Action("GetArticle", "Article", new
                    {
                        id = source.Id
                    }))
                }));

            //Translated
            CreateMap<TranslatedArticle, TranslatedArticleModel>();

            CreateMap<TranslatedArticle, TranslatedArticleResponse>()
                .ForMember(m => m.Embedded, m => m.MapFrom(p => p))
                .ForMember(m => m.Links, m => m.MapHalLinks((url, source) => new HalLinks
                {
                    Self = new HalLink(url.Action("GetTranslatedArticles", "Article", new
                    {
                        id = source.Id
                    }))
                }));
        }
    }
}
