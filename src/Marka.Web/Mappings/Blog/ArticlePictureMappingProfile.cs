﻿using AutoMapper;
using Marka.Common.Models.Blog;
using Marka.Common.ResponseModels.Blog;
using Marka.Common.Responses;
using Marka.Common.Responses.Base;
using Marka.Common.Responses.Blog.Pictures;
using Marka.Web.Extensions;
using Microsoft.AspNetCore.Mvc;

namespace Marka.Web.Mappings.Blog
{
    public class ArticlePictureMappingProfile : Profile
    {
        public ArticlePictureMappingProfile()
        {
            CreateMap<ArticlePictureRequest, ArticlePicture>()
                .ForMember(c => c.Id, m => m.Ignore());

            CreateMap<ArticlePicture, ArticlePictureModel>();

            CreateMap<ArticlePicture, ArticlePictureResponse>()
                .ForMember(m => m.Embedded, m => m.MapFrom(p => p))
                .ForMember(m => m.Links, m => m.MapHalLinks((url, source) => new HalLinks
                {
                    Self = new HalLink(url.Action("GetArticlePicture", "ArticlePicture", new
                    {
                        id = source.Id
                    }))
                }));

        }
    }
}
