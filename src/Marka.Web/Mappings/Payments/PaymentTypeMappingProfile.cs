﻿using AutoMapper;
using Marka.Common.Models.Payments;
using Marka.Common.ResponseModels.Payments;
using Marka.Common.Responses;
using Marka.Common.Responses.Base;
using Marka.Common.Responses.Payments;
using Marka.Web.Extensions;
using Microsoft.AspNetCore.Mvc;

namespace Marka.Web.Mappings.Payments
{
    public class PaymentTypeMappingProfile : Profile
    {
        public PaymentTypeMappingProfile()
        {
            CreateMap<PaymentType, PaymentTypeModel>();

            CreateMap<PaymentType, PaymentTypeResponse>()
                .ForMember(m => m.Embedded, m => m.MapFrom(p => p))
                .ForMember(m => m.Links, m => m.MapHalLinks((url, source) => new HalLinks
                {
                    Self = new HalLink(url.Action("GetPaymentType", "PaymentType", new
                    {
                        id = source.Id
                    }))
                }));
        }
    }
}