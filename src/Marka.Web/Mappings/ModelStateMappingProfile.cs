﻿using AutoMapper;
using Marka.Common.Responses;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System.Linq;
using System.Net;

namespace Marka.Web.Mappings
{
    public class ModelStateMappingProfile : Profile
    {
        public ModelStateMappingProfile()
        {
            CreateMap<ModelStateDictionary, ApiResponse>()
              .ConstructUsing(c =>
              {
                  return new ApiResponse(HttpStatusCode.BadRequest);
              })
              .ForMember(m => m.Errors, mapping => mapping.ResolveUsing(source =>
              {
                  return
                      source.Where(p => p.Value.ValidationState == ModelValidationState.Invalid)
                          .SelectMany(p => p.Value.Errors)
                          .Select(p => p.ErrorMessage)
                          .ToList();
              }));
        }
    }
}