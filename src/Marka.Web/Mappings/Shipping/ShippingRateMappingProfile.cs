﻿using AutoMapper;
using Marka.Common.Models.Shippings;
using Marka.Common.Responses;
using Marka.Common.Responses.Base;
using Marka.Common.Responses.Shippings;
using Marka.Web.Extensions;
using Microsoft.AspNetCore.Mvc;

namespace Marka.Web.Mappings.Shipping
{
    public class ShippingRateMappingProfile : Profile
    {
        public ShippingRateMappingProfile()
        {
            CreateMap<ShippingRateRequest, ShippingRate>()
                .ForMember(m => m.Id, m => m.Ignore());

            CreateMap<ShippingRate, ShippingRateResponse>()
                .ForMember(m => m.Embedded, m => m.MapFrom(p => p))
                .ForMember(m => m.Links, m => m.MapHalLinks((url, source) => new HalLinks
                {
                    Self = new HalLink(url.Action("GetShippingRate", "ShippingRate", new
                    {
                        id = source.Id
                    }))
                }));
        }
    }
}