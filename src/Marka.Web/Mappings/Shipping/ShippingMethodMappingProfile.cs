﻿using AutoMapper;
using Marka.Common.Models.Shippings;
using Marka.Common.Responses;
using Marka.Common.Responses.Base;
using Marka.Common.Responses.Shippings;
using Marka.Web.Extensions;
using Microsoft.AspNetCore.Mvc;

namespace Marka.Web.Mappings.Shipping
{
    public class ShippingMethodMappingProfile : Profile
    {
        public ShippingMethodMappingProfile()
        {
            CreateMap<ShippingMethodRequest, ShippingMethod>()
                .ForMember(m => m.Id, m => m.Ignore());

            CreateMap<ShippingMethod, ShippingMethodResponse>()
                .ForMember(m => m.Embedded, m => m.MapFrom(p => p))
                .ForMember(m => m.Links, m => m.MapHalLinks((url, source) => new HalLinks
                {
                    Self = new HalLink(url.Action("GetShippingMethod", "ShippingMethod", new
                    {
                        id = source.Id
                    }))
                }));
        }
    }
}