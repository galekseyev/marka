﻿using AutoMapper;
using Marka.Common.Models.Filters;
using Marka.Common.Models.Shared;
using Marka.Common.Responses;
using Marka.Common.Responses.Filters;

namespace Marka.Web.Mappings.Shared
{
    public class FilterMappingProfile : Profile
    {
        public FilterMappingProfile()
        {
            CreateMap<FilterRequest, Filter>();
            CreateMap<InventoryFilterRequest, InventoryFilter>();
        }
    }
}