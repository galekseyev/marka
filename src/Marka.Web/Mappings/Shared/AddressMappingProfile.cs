﻿using AutoMapper;
using Marka.Common.Models.Shared;
using Marka.Common.ResponseModels.Shared;

namespace Marka.Web.Mappings.Shared
{
    public class AddressMappingProfile : Profile
    {
        public AddressMappingProfile()
        {
            CreateMap<Address, AddressModel>();
        }
    }
}