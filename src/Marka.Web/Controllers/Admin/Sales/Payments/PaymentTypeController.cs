﻿using AutoMapper;
using Marka.BusinessLogic.Interfaces.Payments;
using Marka.Common.Responses;
using Marka.Common.Responses.Base;
using Marka.Common.Responses.Payments;
using Marka.Common.Results;
using Marka.Web.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Marka.Web.Controllers.Admin.Sales.Payments
{
    [Authorize(Policy = "Admin")]
    [Route("api/admin/payment-type")]
    public class PaymentTypeController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IPaymentTypeService _paymentTypeService;

        public PaymentTypeController(
            IMapper mapper,
            IPaymentTypeService paymentTypeService)
        {
            _mapper = mapper;
            _paymentTypeService = paymentTypeService;
        }

        [HttpGet]
        public async Task<IActionResult> GetPaymentTypes()
        {
            var result = await _paymentTypeService.GetPaymentTypesAsync();

            if (result.IsSuccessful())
            {
                var response = new PaymentTypesResponse
                {
                    Embedded = _mapper.Map<IEnumerable<PaymentTypeResponse>>(result.Value, opt => opt.AddUrlHelper(Url)),
                    Count = result.Value.Count(),
                    Total = result.Value.Count(),
                    Links = new HalLinks
                    {
                        Self = new HalLink(Url.Action("GetPaymentTypes"))
                    }
                };

                return Ok(response);
            }

            return new ApiResponse(HttpStatusCode.BadRequest).ToActionResult();
        }
    }
}
