﻿using AutoMapper;
using Marka.BusinessLogic.Interfaces.Payments;
using Marka.Common.Responses;
using Marka.Common.Responses.Base;
using Marka.Common.Responses.Payments;
using Marka.Common.Results;
using Marka.Web.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Marka.Web.Controllers.Admin.Sales.Payments
{
    [Authorize(Policy = "Admin")]
    [Route("api/admin/payment-status")]
    public class PaymentStatusController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IPaymentStatusService _paymentStatusService;

        public PaymentStatusController(
            IMapper mapper,
            IPaymentStatusService paymentStatusService)
        {
            _mapper = mapper;
            _paymentStatusService = paymentStatusService;
        }

        [HttpGet]
        public async Task<IActionResult> GetPaymentStatuses()
        {
            var result = await _paymentStatusService.GetPaymentStatusesAsync();

            if (result.IsSuccessful())
            {
                var response = new PaymentStatusesResponse
                {
                    Embedded = _mapper.Map<IEnumerable<PaymentStatusResponse>>(result.Value, opt => opt.AddUrlHelper(Url)),
                    Count = result.Value.Count(),
                    Total = result.Value.Count(),
                    Links = new HalLinks
                    {
                        Self = new HalLink(Url.Action("GetPaymentStatuses"))
                    }
                };

                return Ok(response);
            }

            return new ApiResponse(HttpStatusCode.BadRequest).ToActionResult();
        }
    }
}