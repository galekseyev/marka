﻿using AutoMapper;
using Marka.BusinessLogic.Interfaces.Orders;
using Marka.Common.Responses;
using Marka.Common.Responses.Base;
using Marka.Common.Responses.Orders;
using Marka.Common.Results;
using Marka.Web.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Marka.Web.Controllers.Admin.Sales.Orders
{
    [Authorize(Policy = "Admin")]
    [Route("api/admin/order-status")]
    public class OrderStatusesController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IOrderStatusService _orderStatusService;

        public OrderStatusesController(
            IMapper mapper,
            IOrderStatusService orderStatusService)
        {
            _mapper = mapper;
            _orderStatusService = orderStatusService;
        }

        [HttpGet]
        public async Task<IActionResult> GetOrderStatuses()
        {
            var result = await _orderStatusService.GetOrderStatusesAsync();

            if (result.IsSuccessful())
            {
                var response = new OrderStatusesResponse
                {
                    Embedded = _mapper.Map<IEnumerable<OrderStatusResponse>>(result.Value, opt => opt.AddUrlHelper(Url)),
                    Count = result.Value.Count(),
                    Total = result.Value.Count(),
                    Links = new HalLinks
                    {
                        Self = new HalLink(Url.Action("GetOrderStatuses"))
                    }
                };

                return Ok(response);
            }

            return new ApiResponse(HttpStatusCode.BadRequest).ToActionResult();
        }
    }
}