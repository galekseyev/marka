﻿using AutoMapper;
using Marka.BusinessLogic.Interfaces.Orders;
using Marka.Common.Responses;
using Marka.Common.Responses.Base;
using Marka.Common.Responses.Orders;
using Marka.Common.Results;
using Marka.Web.Constants;
using Marka.Web.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Marka.Web.Controllers.Admin.Sales.Orders
{
    [Authorize(Policy = "Admin")]
    [Route("api/admin/order")]
    public class OrderController: Controller
    {
        private readonly IMapper _mapper;
        private readonly IOrderService _orderService;

        public OrderController(
            IHostingEnvironment env,
            IMapper mapper,
            IEmailSender emailSender,
            IOrderService orderService)
        {
            _mapper = mapper;
            _orderService = orderService;
        }


        [HttpGet("{id}")]
        public async Task<IActionResult> GetOrder(int id)
        {
            var result = await _orderService.GetTranslatedOrderAsync(id, LanguageConstants.LanguageCodes.English);

            if (result.IsSuccessful())
            {
                var response = _mapper.Map<TranslatedOrderResponse>(result.Value, opt => opt.AddUrlHelper(Url));

                return Ok(response);
            }

            return new ApiResponse(result.Code).ToActionResult();
        }

        [HttpGet]
        public async Task<IActionResult> GetOrders()
        {
            var result = await _orderService.GetOrdersAsync();

            if (result.IsSuccessful())
            {
                var response = new OrdersResponse
                {
                    Embedded = _mapper.Map<IEnumerable<OrderResponse>>(result.Value, opt => opt.AddUrlHelper(Url)),
                    Count = result.Value.Count(),
                    Total = result.Value.Count(),
                    Links = new HalLinks
                    {
                        Self = new HalLink(Url.Action("GetOrders"))
                    }
                };

                return Ok(response);
            }

            return new ApiResponse(HttpStatusCode.BadRequest).ToActionResult();
        }
    }
}
