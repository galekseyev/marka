﻿using AutoMapper;
using Marka.BusinessLogic.Interfaces.Orders;
using Marka.Common.Constants;
using Marka.Common.Responses;
using Marka.Common.Responses.Orders;
using Marka.Common.Results;
using Marka.Web.Constants;
using Marka.Web.Extensions;
using Marka.Web.Helpers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Marka.Web.Controllers.Admin.Sales.Orders
{
    [Authorize(Policy = "Admin")]
    [Route("api/admin/order/{id}/status")]
    public class OrderStatusController : Controller
    {
        private readonly IHostingEnvironment _env;
        private readonly IMapper _mapper;
        private readonly IEmailSender _emailSender;
        private readonly IOrderService _orderService;
        private readonly IOrderStatusService _orderStatusService;

        public OrderStatusController(
            IHostingEnvironment env,
            IMapper mapper,
            IEmailSender emailSender,
            IOrderService orderService,
            IOrderStatusService orderStatusService)
        {
            _env = env;
            _mapper = mapper;
            _emailSender = emailSender;
            _orderService = orderService;
            _orderStatusService = orderStatusService;
        }

        [HttpPost]
        public async Task<IActionResult> UpdateOrderStatus(int id, [FromBody] int statusId)
        {
            var orderTask = _orderService.GetOrderAsync(id);
            var orderStatusTask = _orderStatusService.GetOrderStatusAsync(statusId);

            await Task.WhenAll(orderTask, orderStatusTask);

            if (orderTask.Result.IsSuccessful() &&
                orderStatusTask.Result.IsSuccessful())
            {
                var order = orderTask.Result.Value;
                var orderStatus = orderStatusTask.Result.Value;

                order.OrderStatus = orderStatus;

                var result = await _orderService.UpdateOrderAsync(order);

                if (result.IsSuccessful())
                {
                    var translatedResult = await _orderService.GetTranslatedOrderAsync(id, LanguageConstants.LanguageCodes.English);

                    if (translatedResult.IsSuccessful())
                    {
                        var emailTemplate = await EmailHelper.GetEmailBodyTemplate(_env.WebRootPath, EmailTemplateType.Order, orderStatus.StatusCode);
                        var body = EmailHelper.ReplaceOrderEmailPlaceholders(emailTemplate, translatedResult.Value);

                        //Translate order status code instead
                        var title = $"[M] - Your order status changed.";

                        await _emailSender.SendEmailAsync(translatedResult.Value.Shipment.Email, title, body);

                        var response = _mapper.Map<TranslatedOrderResponse>(translatedResult.Value, opt => opt.AddUrlHelper(Url));

                        return Ok(response);
                    }
                }
            }

            return new ApiResponse(HttpStatusCode.BadRequest).ToActionResult();
        }

        [HttpPut]
        public async Task<IActionResult> UpdatePaymentStatus(int id, [FromBody] int statusId)
        {
            var orderResult = await _orderService.GetOrderAsync(id);

            if (orderResult.IsSuccessful())
            {
                orderResult.Value.PaymentStatusId = statusId;

                var result = await _orderService.UpdateOrderAsync(orderResult.Value);

                if (result.IsSuccessful())
                {
                    var translatedResult = await _orderService.GetTranslatedOrderAsync(id, LanguageConstants.LanguageCodes.English);

                    if (translatedResult.IsSuccessful())
                    {
                        var response = _mapper.Map<TranslatedOrderResponse>(translatedResult.Value, opt => opt.AddUrlHelper(Url));

                        return Ok(response);
                    }
                }
            }

            return new ApiResponse(HttpStatusCode.BadRequest).ToActionResult();
        }
    }
}
