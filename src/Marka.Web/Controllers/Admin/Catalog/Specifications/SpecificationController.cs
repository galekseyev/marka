﻿using AutoMapper;
using Marka.BusinessLogic.Interfaces.Catalog.Specifications;
using Marka.Common.Exceptions;
using Marka.Common.Models.Catalog.Specifications;
using Marka.Common.Responses;
using Marka.Common.Responses.Base;
using Marka.Common.Responses.Catalog.Specifications;
using Marka.Common.Results;
using Marka.Web.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Marka.Web.Controllers.Admin.Catalog.Specifications
{
    [Authorize(Policy = "Admin")]
    [Route("api/admin/specification")]
    public class SpecificationController : Controller
    {
        private readonly IMapper _mapper;
        private readonly ISpecificationService _specificationService;

        public SpecificationController(IMapper mapper, ISpecificationService specificationService)
        {
            _mapper = mapper;
            _specificationService = specificationService;
        }

        [HttpGet]
        public async Task<IActionResult> GetSpecifications()
        {
            var result = await _specificationService.GetSpecificationsAsync();

            if (result.IsSuccessful())
            {
                var response = new SpecificationsResponse
                {
                    Embedded = _mapper.Map<IEnumerable<SpecificationResponse>>(result.Value, opt => opt.AddUrlHelper(Url)),
                    Count = result.Value.Count(),
                    Total = result.Value.Count(),
                    Links = new HalLinks
                    {
                        Self = new HalLink(Url.Action("GetSpecifications"))
                    }
                };

                //if (skip > 0)
                //{
                //    response.Links.Prev = new HalLink(Url.Action("GetPassTypesList", new { skip = Math.Max(0, skip - take), take }));
                //}

                //if (response.Count >= take)
                //{
                //    response.Links.Next =
                //        new HalLink(Url.Action("GetPassTypesList", new { skip = Math.Max(0, skip + passTypes.Count), take }));
                //}

                return Ok(response);
            }

            return new ApiResponse(HttpStatusCode.BadRequest).ToActionResult();
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetSpecification(int id)
        {
            var result = await _specificationService.GetSpecificationAsync(id);

            if (result.IsSuccessful())
            {
                var response = _mapper.Map<SpecificationResponse>(result.Value, opt => opt.AddUrlHelper(Url));

                return Ok(response);
            }

            return new ApiResponse(HttpStatusCode.BadRequest).ToActionResult();
        }

        [HttpPost]
        public async Task<IActionResult> CreateSpecification([FromBody] SpecificationRequest request)
        {
            if (request == null)
                throw new ApiException(HttpStatusCode.BadRequest.ToString(), HttpStatusCode.BadRequest);

            if (!ModelState.IsValid)
                return BadRequest(_mapper.Map<ApiResponse>(ModelState));

            var specification = _mapper.Map<Specification>(request);

            var result = await _specificationService.CreateSpecificationAsync(specification);

            if (result.IsSuccessful())
            {
                var response = _mapper.Map<SpecificationResponse>(result.Value, opt => opt.AddUrlHelper(Url));

                response.Code = HttpStatusCode.Created;
                response.Message = HttpStatusCode.Created.ToString();

                return Created(Url.Action(), response);
            }

            return new ApiResponse(HttpStatusCode.BadRequest).ToActionResult();
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateSpecification(int id, [FromBody] SpecificationRequest request)
        {
            if (request == null)
                throw new ApiException(HttpStatusCode.BadRequest.ToString(), HttpStatusCode.BadRequest);

            if (!ModelState.IsValid)
                return BadRequest(_mapper.Map<ApiResponse>(ModelState));

            var specification = _mapper.Map<Specification>(request);

            specification.Id = id;

            var result = await _specificationService.UpdateSpecificationAsync(specification);

            if (result.IsSuccessful())
            {
                var response = _mapper.Map<SpecificationResponse>(result.Value, opt => opt.AddUrlHelper(Url));

                return Ok(response);
            }

            return new ApiResponse(HttpStatusCode.BadRequest).ToActionResult();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteSpecification(int id)
        {
            var result = await _specificationService.DeleteSpecificationAsync(id);

            if (result.IsSuccessful())
                return new ApiResponse().ToActionResult();

            return new ApiResponse(HttpStatusCode.BadRequest).ToActionResult();
        }
    }
}