﻿using AutoMapper;
using Marka.BusinessLogic.Interfaces.Catalog.Specifications;
using Marka.Common.Exceptions;
using Marka.Common.Models.Catalog.Specifications;
using Marka.Common.Responses;
using Marka.Common.Responses.Base;
using Marka.Common.Responses.Catalog.Inventories.Attributes;
using Marka.Common.Responses.Catalog.Specifications;
using Marka.Common.Results;
using Marka.Web.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Marka.Web.Controllers.Admin.Catalog.Specifications
{
    [Authorize(Policy = "Admin")]
    [Route("api/admin/specification/{id}/value")]
    public class SpecificationValueController : Controller
    {
        private readonly IMapper _mapper;
        private readonly ISpecificationValueService _specificationValueService;

        public SpecificationValueController(IMapper mapper, ISpecificationValueService specificationValueService)
        {
            _mapper = mapper;
            _specificationValueService = specificationValueService;
        }

        [HttpGet]
        public async Task<IActionResult> GetSpecificationValues(int id, int attributeId)
        {
            var result = await _specificationValueService.GetSpecificationValuesAsync(id);

            if (result.IsSuccessful())
            {
                var response = new SpecificationValuesResponse
                {
                    Embedded = _mapper.Map<IEnumerable<SpecificationValueResponse>>(result.Value, opt => opt.AddUrlHelper(Url)),
                    Count = result.Value.Count(),
                    Total = result.Value.Count(),
                    Links = new HalLinks
                    {
                        Self = new HalLink(Url.Action("GetSpecificationValues"))
                    }
                };

                //if (skip > 0)
                //{
                //    response.Links.Prev = new HalLink(Url.Action("GetPassTypesList", new { skip = Math.Max(0, skip - take), take }));
                //}

                //if (response.Count >= take)
                //{
                //    response.Links.Next =
                //        new HalLink(Url.Action("GetPassTypesList", new { skip = Math.Max(0, skip + passTypes.Count), take }));
                //}

                return Ok(response);
            }

            return new ApiResponse(result.Code).ToActionResult();
        }

        [HttpGet("{valueId}")]
        public async Task<IActionResult> GetSpecificationValue(int id, int valueId)
        {
            var result = await _specificationValueService.GetSpecificationValueAsync(valueId);

            if (result.IsSuccessful())
            {
                var response = _mapper.Map<SpecificationValueResponse>(result.Value, opt => opt.AddUrlHelper(Url));

                return Ok(response);
            }

            return new ApiResponse(result.Code).ToActionResult();
        }

        [HttpPost]
        public async Task<IActionResult> CreateSpecificationValue(int id, [FromBody]SpecificationValueRequest request)
        {
            if (request == null)
                throw new ApiException(HttpStatusCode.BadRequest.ToString(), HttpStatusCode.BadRequest);

            if (!ModelState.IsValid)
                return BadRequest(_mapper.Map<ApiResponse>(ModelState));

            var value = _mapper.Map<SpecificationValue>(request);

            value.SpecificationId = id;

            var result = await _specificationValueService.CreateSpecificationValueAsync(value);

            if (result.IsSuccessful())
            {
                var response = _mapper.Map<SpecificationValueResponse>(result.Value, opt => opt.AddUrlHelper(Url));

                response.Code = HttpStatusCode.Created;
                response.Message = HttpStatusCode.Created.ToString();

                return Created(Url.Action(), response);
            }

            return new ApiResponse(HttpStatusCode.BadRequest).ToActionResult();
        }

        [HttpPut("{valueId}")]
        public async Task<IActionResult> UpdateSpecificationValue(int id, int valueId, [FromBody]InventoryAttributeValueRequest request)
        {
            if (request == null)
                throw new ApiException(HttpStatusCode.BadRequest.ToString(), HttpStatusCode.BadRequest);

            if (!ModelState.IsValid)
                return BadRequest(_mapper.Map<ApiResponse>(ModelState));

            var value = _mapper.Map<SpecificationValue>(request);

            value.Id = valueId;
            value.SpecificationId = id;

            var result = await _specificationValueService.UpdateSpecificationValueAsync(value);

            if (result.IsSuccessful())
            {
                var response = _mapper.Map<SpecificationValueResponse>(result.Value, opt => opt.AddUrlHelper(Url));

                return Ok(response);
            }

            return new ApiResponse(result.Code).ToActionResult();
        }

        [HttpDelete("{valueId}")]
        public async Task<IActionResult> DeleteSpecificationValue(int id, int valueId)
        {
            var result = await _specificationValueService.DeleteSpecificationValueAsync(valueId);

            if (result.IsSuccessful())
                return new ApiResponse().ToActionResult();

            return new ApiResponse(result.Code).ToActionResult();
        }
    }
}