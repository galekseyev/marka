﻿using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Marka.BusinessLogic.Interfaces.Catalog.InventoryGroups;
using Marka.Common.Exceptions;
using Marka.Common.Models.Catalog.InventoryGroups;
using Marka.Common.Responses;
using Marka.Common.Responses.Catalog.InventoryGroups.Pictures;
using Marka.Common.Results;
using Marka.Web.Extensions;
using Marka.Web.Images;
using Microsoft.AspNetCore.Mvc;

namespace Marka.Web.Controllers.Admin.Catalog.InventoryGroups
{
    [Route("api/admin/inventory-group/{id}/picture")]
    public class InventoryGroupPictureController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IImageManager _imageManager;
        private readonly IInventoryGroupService _inventoryGroupService;
        private readonly IInventoryGroupPictureService _inventoryGroupPictureService;

        public InventoryGroupPictureController(IMapper mapper, IImageManager imageManager, IInventoryGroupService inventoryGroupService, IInventoryGroupPictureService inventoryGroupPictureService)
        {
            _mapper = mapper;

            _imageManager = imageManager;

            _inventoryGroupService = inventoryGroupService;
            _inventoryGroupPictureService = inventoryGroupPictureService;
        }

        public async Task<IActionResult> GetInventoryGroupPicture(int id)
        {
            var result = await _inventoryGroupPictureService.GetInventoryGroupPictureAsync(id);

            if (result.IsSuccessful())
            {
                var response = _mapper.Map<InventoryGroupPictureResponse>(result.Value, opt => opt.AddUrlHelper(Url));

                return Ok(response);
            }

            return new ApiResponse(result.Code).ToActionResult();
        }

        [HttpPost]
        public async Task<IActionResult> CreateInventoryGroupPicture(int id, [FromBody]InventoryGroupPictureRequest request)
        {
            if (request == null)
                throw new ApiException(HttpStatusCode.BadRequest.ToString(), HttpStatusCode.BadRequest);

            if (!ModelState.IsValid)
                return BadRequest(_mapper.Map<ApiResponse>(ModelState));

            var groupResult = await _inventoryGroupService.GetInventoryGroupAsync(id);

            if (groupResult.IsSuccessful())
            {
                var group = groupResult.Value;
                var picture = _mapper.Map<InventoryGroupPicture>(request);

                var path = $"catalog/inventory-groups/{id}";

                picture.Thumbnail = await _imageManager.UploadToS3Async(path, request.Title, request.Thumbnail, true);
                picture.Picture = await _imageManager.UploadToS3Async(path, request.Title, request.Picture);

                group.Picture = picture;

                var result = await _inventoryGroupService.UpdateInventoryGroupAsync(group);

                if (result.IsSuccessful())
                {
                    var response = _mapper.Map<InventoryGroupPictureResponse>(result.Value.Picture, opt => opt.AddUrlHelper(Url));

                    response.Code = HttpStatusCode.Created;
                    response.Message = HttpStatusCode.Created.ToString();

                    return Created(Url.Action(), response);
                }
            }

            return new ApiResponse(HttpStatusCode.BadRequest).ToActionResult();
        }

        [HttpPut("{pictureId}")]
        public async Task<IActionResult> UpdateInventoryGroupPicture(int id, int pictureId, [FromBody]InventoryGroupPictureRequest request)
        {
            if (request == null)
                throw new ApiException(HttpStatusCode.BadRequest.ToString(), HttpStatusCode.BadRequest);

            if (!ModelState.IsValid)
                return BadRequest(_mapper.Map<ApiResponse>(ModelState));

            var groupResult = await _inventoryGroupService.GetInventoryGroupAsync(id);

            if (groupResult.IsSuccessful())
            {
                var group = groupResult.Value;

                var path = $"catalog/inventory-groups/{id}";

                var picture = _mapper.Map<InventoryGroupPicture>(request);

                picture.Id = pictureId;

                picture.Thumbnail = await _imageManager.UploadToS3Async(path, request.Title, request.Thumbnail, true);
                picture.Picture = await _imageManager.UploadToS3Async(path, request.Title, request.Picture);

                group.Picture = picture;

                var result = await _inventoryGroupService.UpdateInventoryGroupAsync(group);

                if (result.IsSuccessful())
                {
                    var response = _mapper.Map<InventoryGroupPictureResponse>(result.Value.Picture, opt => opt.AddUrlHelper(Url));

                    return Ok(response);
                }
            }

            return new ApiResponse(HttpStatusCode.BadRequest).ToActionResult();
        }

        [HttpDelete("{pictureId}")]
        public async Task<IActionResult> DeleteInventoryGroupPicture(int id, int pictureId)
        {
            var result = await _inventoryGroupPictureService.DeleteInventoryGroupPictureAsync(pictureId);

            if (result.IsSuccessful())
                return new ApiResponse().ToActionResult();

            return new ApiResponse(result.Code).ToActionResult();
        }
    }
}