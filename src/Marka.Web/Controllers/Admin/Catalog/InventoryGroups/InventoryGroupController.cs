﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Marka.BusinessLogic.Interfaces.Catalog.InventoryGroups;
using Marka.Common.Exceptions;
using Marka.Common.Models.Catalog.InventoryGroups;
using Marka.Common.Models.Filters;
using Marka.Common.Responses;
using Marka.Common.Responses.Base;
using Marka.Common.Responses.Catalog.InventoryGroups;
using Marka.Common.Results;
using Marka.Web.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Marka.Web.Controllers.Admin.Catalog.InventoryGroups
{
    [Authorize(Policy = "Admin")]
    [Route("api/admin/inventory-group")]
    public class InventoryGroupController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IInventoryGroupService _inventoryGroupService;

        public InventoryGroupController(IMapper mapper, IInventoryGroupService inventoryGroupService)
        {
            _mapper = mapper;

            _inventoryGroupService = inventoryGroupService;
        }
        [HttpGet]
        public async Task<IActionResult> GetInventoryGroups()
        {
            var filter = new InventoryGroupFilter
            {
                IncludeDisabled = true,
                IncludeEmptyGroups = true
            };

            var result = await _inventoryGroupService.GetInventoryGroupsAsync(filter);

            if (result.IsSuccessful())
            {
                var response = new InventoryGroupsResponse
                {
                    Embedded = _mapper.Map<IEnumerable<InventoryGroupResponse>>(result.Value.InventoryGroups, opt => opt.AddUrlHelper(Url)),
                    //{
                    //    Categories = categories
                    //},
                    Count = result.Value.InventoryGroups.Count(),
                    Total = result.Value.Total,
                    Links = new HalLinks
                    {
                        Self = new HalLink(Url.Action("GetInventoryGroups"))
                    }
                };

                //if (skip > 0)
                //{
                //    response.Links.Prev = new HalLink(Url.Action("GetPassTypesList", new { skip = Math.Max(0, skip - take), take }));
                //}

                //if (response.Count >= take)
                //{
                //    response.Links.Next =
                //        new HalLink(Url.Action("GetPassTypesList", new { skip = Math.Max(0, skip + passTypes.Count), take }));
                //}

                return Ok(response);
            }

            return new ApiResponse(HttpStatusCode.BadRequest).ToActionResult();
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetInventoryGroup(int id)
        {
            var result = await _inventoryGroupService.GetInventoryGroupAsync(id);

            if (result.IsSuccessful())
            {
                var response = _mapper.Map<InventoryGroupResponse>(result.Value, opt => opt.AddUrlHelper(Url));

                return Ok(response);
            }

            return new ApiResponse(HttpStatusCode.BadRequest).ToActionResult();
        }

        [HttpPost]
        public async Task<IActionResult> CreateInventoryGroup([FromBody] InventoryGroupRequest request)
        {
            if (request == null)
                throw new ApiException(HttpStatusCode.BadRequest.ToString(), HttpStatusCode.BadRequest);

            if (!ModelState.IsValid)
                return BadRequest(_mapper.Map<ApiResponse>(ModelState));

            var inventoryGroup = _mapper.Map<InventoryGroup>(request);

            var result = await _inventoryGroupService.CreateInventoryGroupAsync(inventoryGroup);

            if (result.IsSuccessful())
            {
                var response = _mapper.Map<InventoryGroupResponse>(result.Value, opt => opt.AddUrlHelper(Url));

                response.Code = HttpStatusCode.Created;
                response.Message = HttpStatusCode.Created.ToString();

                return Created(Url.Action(), response);
            }

            return new ApiResponse(HttpStatusCode.BadRequest).ToActionResult();
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateInventoryGroup(int id, [FromBody] InventoryGroupRequest request)
        {
            if (request == null)
                throw new ApiException(HttpStatusCode.BadRequest.ToString(), HttpStatusCode.BadRequest);

            if (!ModelState.IsValid)
                return BadRequest(_mapper.Map<ApiResponse>(ModelState));

            var inventoryGroup = _mapper.Map<InventoryGroup>(request);

            inventoryGroup.Id = id;

            var result = await _inventoryGroupService.UpdateInventoryGroupAsync(inventoryGroup);

            if (result.IsSuccessful())
            {
                var response = _mapper.Map<InventoryGroupResponse>(result.Value, opt => opt.AddUrlHelper(Url));

                return Ok(response);
            }

            return new ApiResponse(HttpStatusCode.BadRequest).ToActionResult();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteInventoryGroup(int id)
        {
            var result = await _inventoryGroupService.DeleteInventoryGroupAsync(id);

            if (result.IsSuccessful())
                return new ApiResponse().ToActionResult();

            return new ApiResponse(HttpStatusCode.BadRequest).ToActionResult();
        }
    }
}