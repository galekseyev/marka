﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Marka.BusinessLogic.Interfaces.Catalog.InventoryGroups;
using Marka.Common.Exceptions;
using Marka.Common.Models.Catalog.InventoryGroups;
using Marka.Common.Responses;
using Marka.Common.Responses.Base;
using Marka.Common.Responses.Catalog.InventoryGroups.Inventories;
using Marka.Common.Results;
using Marka.Web.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Marka.Web.Controllers.Admin.Catalog.InventoryGroups
{
    [Authorize(Policy = "Admin")]
    [Route("api/admin/inventory-group/{id}/inventory")]
    public class InventoryGroupInventoryController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IInventoryGroupInventoryService _inventoryGroupInventoryService;

        public InventoryGroupInventoryController(IMapper mapper, IInventoryGroupInventoryService inventoryGroupInventoryService)
        {
            _mapper = mapper;
            _inventoryGroupInventoryService = inventoryGroupInventoryService;
        }

        [HttpGet]
        public async Task<IActionResult> GetInventoryGroupInventories(int id)
        {
            var result = await _inventoryGroupInventoryService.GetInventoryGroupInventoriesAsync(id);

            if (result.IsSuccessful())
            {
                var response = new InventoryGroupInventoriesResponse
                {
                    Embedded = _mapper.Map<IEnumerable<InventoryGroupInventoryResponse>>(result.Value, opt => opt.AddUrlHelper(Url)),
                    Count = result.Value.Count(),
                    Total = result.Value.Count(),
                    Links = new HalLinks
                    {
                        Self = new HalLink(Url.Action("GetInventoryGroupInventories"))
                    }
                };

                //if (skip > 0)
                //{
                //    response.Links.Prev = new HalLink(Url.Action("GetPassTypesList", new { skip = Math.Max(0, skip - take), take }));
                //}

                //if (response.Count >= take)
                //{
                //    response.Links.Next =
                //        new HalLink(Url.Action("GetPassTypesList", new { skip = Math.Max(0, skip + passTypes.Count), take }));
                //}

                return Ok(response);
            }

            return new ApiResponse(result.Code).ToActionResult();
        }


        [HttpGet("{inventoryGroupInventoryId}")]
        public async Task<IActionResult> GetInventoryGroupInventory(int id, int inventoryGroupInventoryId)
        {
            var result = await _inventoryGroupInventoryService.GetInventoryGroupInventoryAsync(inventoryGroupInventoryId);

            if (result.IsSuccessful())
            {
                var response = _mapper.Map<InventoryGroupInventoryResponse>(result.Value, opt => opt.AddUrlHelper(Url));

                return Ok(response);
            }

            return new ApiResponse(result.Code).ToActionResult();
        }

        [HttpPost]
        public async Task<IActionResult> CreateInventoryGroupInventory(int id, [FromBody]InventoryGroupInventoryRequest request)
        {
            if (request == null)
                throw new ApiException(HttpStatusCode.BadRequest.ToString(), HttpStatusCode.BadRequest);

            if (!ModelState.IsValid)
                return BadRequest(_mapper.Map<ApiResponse>(ModelState));

            var inventoryGroupInventory = _mapper.Map<InventoryGroupInventory>(request);

            inventoryGroupInventory.InventoryGroupId = id;

            var result = await _inventoryGroupInventoryService.CreateInventoryGroupInventoryAsync(inventoryGroupInventory);

            if (result.IsSuccessful())
            {
                var response = _mapper.Map<InventoryGroupInventoryResponse>(result.Value, opt => opt.AddUrlHelper(Url));

                response.Code = HttpStatusCode.Created;
                response.Message = HttpStatusCode.Created.ToString();

                return Created(Url.Action(), response);
            }

            return new ApiResponse(HttpStatusCode.BadRequest).ToActionResult();
        }

        [HttpPut("{inventoryGroupInventoryId}")]
        public async Task<IActionResult> UpdateInventoryGroupInventory(int id, int inventoryGroupInventoryId, [FromBody]InventoryGroupInventoryRequest request)
        {
            if (request == null)
                throw new ApiException(HttpStatusCode.BadRequest.ToString(), HttpStatusCode.BadRequest);

            if (!ModelState.IsValid)
                return BadRequest(_mapper.Map<ApiResponse>(ModelState));

            var inventoryGroupInventory = _mapper.Map<InventoryGroupInventory>(request);

            inventoryGroupInventory.InventoryGroupId = id;
            inventoryGroupInventory.Id = inventoryGroupInventoryId;

            var result = await _inventoryGroupInventoryService.UpdateInventoryGroupInventoryAsync(inventoryGroupInventory);

            if (result.IsSuccessful())
            {
                var response = _mapper.Map<InventoryGroupInventoryResponse>(result.Value, opt => opt.AddUrlHelper(Url));

                return Ok(response);
            }

            return new ApiResponse(result.Code).ToActionResult();
        }

        //InventoryGroupInventoryId
        [HttpDelete("{inventoryId}")]
        public async Task<IActionResult> DeleteInventoryGroupInventory(int id, int inventoryId)
        {
            var result = await _inventoryGroupInventoryService.DeleteInventoryGroupInventoryAsync(inventoryId);

            if (result.IsSuccessful())
                return new ApiResponse().ToActionResult();

            return new ApiResponse(result.Code).ToActionResult();
        }
    }
}