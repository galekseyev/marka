﻿using AutoMapper;
using Marka.BusinessLogic.Interfaces.Catalog.Attributes;
using Marka.Common.Exceptions;
using Marka.Common.Models.Catalog.Attributes;
using Marka.Common.Responses;
using Marka.Common.Responses.Base;
using Marka.Common.Responses.Catalog.Attributes;
using Marka.Common.Results;
using Marka.Web.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Marka.Web.Controllers.Admin.Catalog.Attributes
{
    [Authorize(Policy = "Admin")]
    [Route("api/admin/attribute")]
    public class AttributeController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IAttributeService _attributeService;

        public AttributeController(IMapper mapper, IAttributeService attributeService)
        {
            _mapper = mapper;
            _attributeService = attributeService;
        }

        [HttpGet]
        public async Task<IActionResult> GetAttributes()
        {
            var result = await _attributeService.GetAttributesAsync();

            if (result.IsSuccessful())
            {
                var response = new AttributesResponse
                {
                    Embedded = _mapper.Map<IEnumerable<AttributeResponse>>(result.Value, opt => opt.AddUrlHelper(Url)),
                    Count = result.Value.Count(),
                    Total = result.Value.Count(),
                    Links = new HalLinks
                    {
                        Self = new HalLink(Url.Action("GetInventoryAttributes"))
                    }
                };

                //if (skip > 0)
                //{
                //    response.Links.Prev = new HalLink(Url.Action("GetPassTypesList", new { skip = Math.Max(0, skip - take), take }));
                //}

                //if (response.Count >= take)
                //{
                //    response.Links.Next =
                //        new HalLink(Url.Action("GetPassTypesList", new { skip = Math.Max(0, skip + passTypes.Count), take }));
                //}

                return Ok(response);
            }

            return new ApiResponse(HttpStatusCode.BadRequest).ToActionResult();
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetAttribute(int id)
        {
            var result = await _attributeService.GetAttributeAsync(id);

            if (result.IsSuccessful())
            {
                var response = _mapper.Map<AttributeResponse>(result.Value, opt => opt.AddUrlHelper(Url));

                return Ok(response);
            }

            return new ApiResponse(HttpStatusCode.BadRequest).ToActionResult();
        }

        [HttpPost]
        public async Task<IActionResult> CreateAttribute([FromBody] AttributeRequest request)
        {
            if (request == null)
                throw new ApiException(HttpStatusCode.BadRequest.ToString(), HttpStatusCode.BadRequest);

            if (!ModelState.IsValid)
                return BadRequest(_mapper.Map<ApiResponse>(ModelState));

            var attribute = _mapper.Map<Attribute>(request);

            var result = await _attributeService.CreateAttributeAsync(attribute);

            if (result.IsSuccessful())
            {
                var response = _mapper.Map<AttributeResponse>(result.Value, opt => opt.AddUrlHelper(Url));

                response.Code = HttpStatusCode.Created;
                response.Message = HttpStatusCode.Created.ToString();

                return Created(Url.Action(), response);
            }

            return new ApiResponse(HttpStatusCode.BadRequest).ToActionResult();
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateAttribute(int id, [FromBody] AttributeRequest request)
        {
            if (request == null)
                throw new ApiException(HttpStatusCode.BadRequest.ToString(), HttpStatusCode.BadRequest);

            if (!ModelState.IsValid)
                return BadRequest(_mapper.Map<ApiResponse>(ModelState));

            var attribute = _mapper.Map<Attribute>(request);

            attribute.Id = id;

            var result = await _attributeService.UpdateAttributeAsync(attribute);

            if (result.IsSuccessful())
            {
                var response = _mapper.Map<AttributeResponse>(result.Value, opt => opt.AddUrlHelper(Url));

                return Ok(response);
            }

            return new ApiResponse(HttpStatusCode.BadRequest).ToActionResult();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAttribute(int id)
        {
            var result = await _attributeService.DeleteAttributeAsync(id);

            if (result.IsSuccessful())
                return new ApiResponse().ToActionResult();

            return new ApiResponse(HttpStatusCode.BadRequest).ToActionResult();
        }
    }
}
