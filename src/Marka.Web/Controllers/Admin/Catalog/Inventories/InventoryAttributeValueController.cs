﻿using AutoMapper;
using Marka.BusinessLogic.Interfaces.Catalog.Inventories;
using Marka.Common.Exceptions;
using Marka.Common.Models.Catalog.Inventories;
using Marka.Common.Responses;
using Marka.Common.Responses.Base;
using Marka.Common.Responses.Catalog.Inventories.Attributes;
using Marka.Common.Results;
using Marka.Web.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Marka.Web.Controllers.Admin.Catalog.Inventories
{
    [Authorize(Policy = "Admin")]
    [Route("api/admin/inventory/{id}/attribute/{attributeId}/value")]
    public class InventoryAttributeValueController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IInventoryAttributeValueService _inventoryAttributeValueService;
        private readonly IInventoryPictureService _inventoryPictureService;

        public InventoryAttributeValueController(IMapper mapper, IInventoryAttributeValueService inventoryAttributeValueService, IInventoryPictureService inventoryPictureService)
        {
            _mapper = mapper;
            _inventoryAttributeValueService = inventoryAttributeValueService;
            _inventoryPictureService = inventoryPictureService;
        }

        [HttpGet]
        public async Task<IActionResult> GetInventoryAttributeValues(int id, int attributeId)
        {
            var result = await _inventoryAttributeValueService.GetInventoryAttributeValuesAsync(id, attributeId);

            if (result.IsSuccessful())
            {
                var response = new InventoryAttributeValuesResponse
                {
                    Embedded = _mapper.Map<IEnumerable<InventoryAttributeValueResponse>>(result.Value, opt => opt.AddUrlHelper(Url)),
                    Count = result.Value.Count(),
                    Total = result.Value.Count(),
                    Links = new HalLinks
                    {
                        Self = new HalLink(Url.Action("GetInventoryAttributeValues"))
                    }
                };

                //if (skip > 0)
                //{
                //    response.Links.Prev = new HalLink(Url.Action("GetPassTypesList", new { skip = Math.Max(0, skip - take), take }));
                //}

                //if (response.Count >= take)
                //{
                //    response.Links.Next =
                //        new HalLink(Url.Action("GetPassTypesList", new { skip = Math.Max(0, skip + passTypes.Count), take }));
                //}

                return Ok(response);
            }

            return new ApiResponse(result.Code).ToActionResult();
        }

        [HttpGet("{attributeValueId}")]
        public async Task<IActionResult> GetInventoryAttributeValue(int id, int attributeId, int attributeValueId)
        {
            var result = await _inventoryAttributeValueService.GetInventoryAttributeValueAsync(attributeValueId);

            if (result.IsSuccessful())
            {
                var response = _mapper.Map<InventoryAttributeValueResponse>(result.Value, opt => opt.AddUrlHelper(Url));

                return Ok(response);
            }

            return new ApiResponse(result.Code).ToActionResult();
        }

        [HttpPost]
        public async Task<IActionResult> CreateInventoryAttributeValue(int id, int attributeId, [FromBody]InventoryAttributeValueRequest request)
        {
            if (request == null)
                throw new ApiException(HttpStatusCode.BadRequest.ToString(), HttpStatusCode.BadRequest);

            if (!ModelState.IsValid)
                return BadRequest(_mapper.Map<ApiResponse>(ModelState));

            var attributeValue = _mapper.Map<InventoryAttributeValue>(request);

            attributeValue.InventoryAttributeId = attributeId;

            var result = await _inventoryAttributeValueService.CreateInventoryAttributeValueAsync(attributeValue);

            if (result.IsSuccessful())
            {
                var response = _mapper.Map<InventoryAttributeValueResponse>(result.Value, opt => opt.AddUrlHelper(Url));

                response.Code = HttpStatusCode.Created;
                response.Message = HttpStatusCode.Created.ToString();

                return Created(Url.Action(), response);
            }

            return new ApiResponse(HttpStatusCode.BadRequest).ToActionResult();
        }

        [HttpPut("{attributeValueId}")]
        public async Task<IActionResult> UpdateInventoryAttributeValue(int id, int attributeId, int attributeValueId, [FromBody]InventoryAttributeValueRequest request)
        {
            if (request == null)
                throw new ApiException(HttpStatusCode.BadRequest.ToString(), HttpStatusCode.BadRequest);

            if (!ModelState.IsValid)
                return BadRequest(_mapper.Map<ApiResponse>(ModelState));

            var attribute = _mapper.Map<InventoryAttributeValue>(request);

            attribute.Id = attributeValueId;
            attribute.InventoryAttributeId = attributeId;

            var result = await _inventoryAttributeValueService.UpdateInventoryAttributeValueAsync(attribute);

            if (result.IsSuccessful())
            {
                var response = _mapper.Map<InventoryAttributeValueResponse>(result.Value, opt => opt.AddUrlHelper(Url));

                return Ok(response);
            }

            return new ApiResponse(result.Code).ToActionResult();
        }

        [HttpDelete("{attributeValueId}")]
        public async Task<IActionResult> DeleteInventoryAttributeValue(int id, int attributeId, int attributeValueId)
        {
            var result = await _inventoryAttributeValueService.DeleteInventoryAttributeValueAsync(attributeValueId);

            if (result.IsSuccessful())
                return new ApiResponse().ToActionResult();

            return new ApiResponse(result.Code).ToActionResult();
        }

    }
}
