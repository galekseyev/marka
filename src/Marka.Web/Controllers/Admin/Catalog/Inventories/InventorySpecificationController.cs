﻿using AutoMapper;
using Marka.BusinessLogic.Interfaces.Catalog.Inventories;
using Marka.Common.Exceptions;
using Marka.Common.Models.Catalog.Inventories;
using Marka.Common.Responses;
using Marka.Common.Responses.Catalog.Inventories.Specifications;
using Marka.Common.Results;
using Marka.Web.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Threading.Tasks;

namespace Marka.Web.Controllers.Admin.Catalog.Inventories
{
    [Authorize(Policy = "Admin")]
    [Route("api/admin/inventory/{id}/specification")]
    public class InventorySpecificationController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IInventorySpecificationService _inventorySpecificationService;

        public InventorySpecificationController(IMapper mapper, IInventorySpecificationService inventorySpecificationService)
        {
            _mapper = mapper;
            _inventorySpecificationService = inventorySpecificationService;
        }

        [HttpGet("{specificationId}")]
        public async Task<IActionResult> GetInventorySpecification(int id, int specificationId)
        {
            var result = await _inventorySpecificationService.GetInventorySpecificationAsync(id, specificationId);

            if (result.IsSuccessful())
            {
                var response = _mapper.Map<InventorySpecificationResponse>(result.Value, opt => opt.AddUrlHelper(Url));

                return Ok(response);
            }

            return new ApiResponse(result.Code).ToActionResult();
        }

        [HttpPost]
        public async Task<IActionResult> CreateInventorySpecification(int id, [FromBody]InventorySpecificationRequest request)
        {
            if (request == null)
                throw new ApiException(HttpStatusCode.BadRequest.ToString(), HttpStatusCode.BadRequest);

            if (!ModelState.IsValid)
                return BadRequest(_mapper.Map<ApiResponse>(ModelState));

            var inventorySpecification = _mapper.Map<InventorySpecification>(request);

            inventorySpecification.InventoryId = id;

            var result = await _inventorySpecificationService.CreateInventorySpecificationAsync(inventorySpecification);

            if (result.IsSuccessful())
            {
                var response = _mapper.Map<InventorySpecificationResponse>(result.Value, opt => opt.AddUrlHelper(Url));

                response.Code = HttpStatusCode.Created;
                response.Message = HttpStatusCode.Created.ToString();

                return Created(Url.Action(), response);
            }

            return new ApiResponse(HttpStatusCode.BadRequest).ToActionResult();
        }

        [HttpPut("{specificationId}")]
        public async Task<IActionResult> UpdateInventorySpecification(int id, int specificationId, [FromBody]InventorySpecificationRequest request)
        {
            if (request == null)
                throw new ApiException(HttpStatusCode.BadRequest.ToString(), HttpStatusCode.BadRequest);

            if (!ModelState.IsValid)
                return BadRequest(_mapper.Map<ApiResponse>(ModelState));

            var inventorySpecification = _mapper.Map<InventorySpecification>(request);

            inventorySpecification.Id = specificationId;
            inventorySpecification.InventoryId = id;

            var result = await _inventorySpecificationService.UpdateInventorySpecificationAsync(inventorySpecification);

            if (result.IsSuccessful())
            {
                var response = _mapper.Map<InventorySpecificationResponse>(result.Value, opt => opt.AddUrlHelper(Url));

                return Ok(response);
            }

            return new ApiResponse(result.Code).ToActionResult();
        }

        [HttpDelete("{specificationId}")]
        public async Task<IActionResult> DeleteInventorySpecification(int id, int specificationId)
        {
            var result = await _inventorySpecificationService.DeleteInventoryAttributeValueAsync(specificationId);

            if (result.IsSuccessful())
                return new ApiResponse().ToActionResult();

            return new ApiResponse(result.Code).ToActionResult();
        }
    }
}