﻿using AutoMapper;
using Marka.BusinessLogic.Interfaces.Catalog.Inventories;
using Marka.Common.Exceptions;
using Marka.Common.Models.Catalog.Inventories;
using Marka.Common.Models.Filters;
using Marka.Common.Responses;
using Marka.Common.Responses.Base;
using Marka.Common.Responses.Catalog.Inventories;
using Marka.Common.Results;
using Marka.Web.Constants;
using Marka.Web.Extensions;
using Marka.Web.Images;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Marka.Web.Controllers.Admin.Catalog.Inventories
{
    [Route("api/admin/inventory")]
    [Authorize(Policy = "Admin")]
    public class InventoryController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IInventoryService _inventoryService;

        public InventoryController(IMapper mapper, IImageManager imageManager, IInventoryService inventoryService)
        {
            _mapper = mapper;

            _inventoryService = inventoryService;
        }

        //[HttpGet]
        //public async Task<IActionResult> GetInventories()
        //{
        //    var result = await _inventoryService.GetInventoriesAsync();

        //    if (result.IsSuccessful())
        //    {
        //        var response = new InventoriesResponse
        //        {
        //            Embedded = _mapper.Map<IEnumerable<InventoryResponse>>(result.Value, opt => opt.AddUrlHelper(Url)),
        //            Count = result.Value.Count(),
        //            Total = result.Value.Count(),
        //            Links = new HalLinks
        //            {
        //                Self = new HalLink(Url.Action("GetInventories"))
        //            }
        //        };

        //        //if (skip > 0)
        //        //{
        //        //    response.Links.Prev = new HalLink(Url.Action("GetPassTypesList", new { skip = Math.Max(0, skip - take), take }));
        //        //}

        //        //if (response.Count >= take)
        //        //{
        //        //    response.Links.Next =
        //        //        new HalLink(Url.Action("GetPassTypesList", new { skip = Math.Max(0, skip + passTypes.Count), take }));
        //        //}

        //        return Ok(response);
        //    }

        //    return new ApiResponse(HttpStatusCode.BadRequest).ToActionResult();
        //}

        [HttpGet]
        public async Task<IActionResult> GetTranslatedInventories(InventoryFilter filter)
        {
            var code = LanguageConstants.LanguageCodes.English;

            filter.IncludeDisabled = true;
            filter.IncludeOutOfStock = true;

            var result = await _inventoryService.GetTranslatedInventoriesAsync(filter, code);

            if (result.IsSuccessful())
            {
                var response = new TranslatedInventoriesResponse
                {
                    Embedded = _mapper.Map<IEnumerable<TranslatedInventoryResponse>>(result.Value.Inventories, opt => opt.AddUrlHelper(Url)),
                    Count = result.Value.Inventories.Count(),
                    Total = result.Value.Total,
                    Links = new HalLinks
                    {
                        Self = new HalLink(Url.Action("GetTranslatedInventories"))
                    }
                };

                //if (skip > 0)
                //{
                //    response.Links.Prev = new HalLink(Url.Action("GetPassTypesList", new { skip = Math.Max(0, skip - take), take }));
                //}

                //if (response.Count >= take)
                //{
                //    response.Links.Next =
                //        new HalLink(Url.Action("GetPassTypesList", new { skip = Math.Max(0, skip + passTypes.Count), take }));
                //}

                return Ok(response);
            }

            return new ApiResponse(HttpStatusCode.BadRequest).ToActionResult();
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetInventory(int id)
        {
            var result = await _inventoryService.GetInventoryAsync(id);

            if (result.IsSuccessful())
            {
                var response = _mapper.Map<InventoryResponse>(result.Value, opt => opt.AddUrlHelper(Url));

                return Ok(response);
            }

            return new ApiResponse(HttpStatusCode.BadRequest).ToActionResult();
        }

        [HttpPost]
        public async Task<IActionResult> CreateInventory([FromBody] InventoryRequest request)
        {
            if (request == null)
                throw new ApiException(HttpStatusCode.BadRequest.ToString(), HttpStatusCode.BadRequest);

            if (!ModelState.IsValid)
                return BadRequest(_mapper.Map<ApiResponse>(ModelState));

            var inventory = _mapper.Map<Inventory>(request);

            var result = await _inventoryService.CreateInventoryAsync(inventory);

            if (result.IsSuccessful())
            {
                var response = _mapper.Map<InventoryResponse>(result.Value, opt => opt.AddUrlHelper(Url));

                response.Code = HttpStatusCode.Created;
                response.Message = HttpStatusCode.Created.ToString();

                return Created(Url.Action(), response);
            }

            return new ApiResponse(HttpStatusCode.BadRequest).ToActionResult();
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateInventory(int id, [FromBody] InventoryRequest request)
        {
            if (request == null)
                throw new ApiException(HttpStatusCode.BadRequest.ToString(), HttpStatusCode.BadRequest);

            if (!ModelState.IsValid)
                return BadRequest(_mapper.Map<ApiResponse>(ModelState));

            var inventory = _mapper.Map<Inventory>(request);

            inventory.Id = id;

            var result = await _inventoryService.UpdateInventoryAsync(inventory);

            if (result.IsSuccessful())
            {
                var response = _mapper.Map<InventoryResponse>(result.Value, opt => opt.AddUrlHelper(Url));

                return Ok(response);
            }

            return new ApiResponse(HttpStatusCode.BadRequest).ToActionResult();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteInventory(int id)

        {
            var result = await _inventoryService.DeleteInventoryAsync(id);

            if (result.IsSuccessful())
                return new ApiResponse().ToActionResult();

            return new ApiResponse(HttpStatusCode.BadRequest).ToActionResult();
        }
    }
}
