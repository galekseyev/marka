﻿using AutoMapper;
using Marka.BusinessLogic.Interfaces.Catalog.Inventories;
using Marka.Common.Exceptions;
using Marka.Common.Models.Catalog.Inventories;
using Marka.Common.Responses;
using Marka.Common.Responses.Base;
using Marka.Common.Responses.Catalog.Inventories.Pictures;
using Marka.Common.Results;
using Marka.Web.Extensions;
using Marka.Web.Images;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Marka.Web.Controllers.Admin.Catalog.Inventories
{
    [Authorize(Policy = "Admin")]
    [Route("api/admin/inventory/{id}/picture")]
    public class InventoryPictureController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IImageManager _imageManager;
        private readonly IInventoryPictureService _inventoryPictureService;

        public InventoryPictureController(IMapper mapper, IImageManager imageManager, IInventoryPictureService inventoryPictureService)
        {
            _mapper = mapper;

            _imageManager = imageManager;
            _inventoryPictureService = inventoryPictureService;
        }

        [HttpGet]
        public async Task<IActionResult> GetInventoryPictures(int id)
        {
            var result = await _inventoryPictureService.GetInventoryPicturesAsync(id);

            if (result.IsSuccessful())
            {
                var pictures = _mapper.Map<IEnumerable<InventoryPictureResponse>>(result.Value, opt => opt.AddUrlHelper(Url));

                var response = new InventoryPicturesResponse
                {
                    Embedded = pictures,
                    Count = result.Value.Count(),
                    Total = result.Value.Count(),
                    Links = new HalLinks
                    {
                        Self = new HalLink(Url.Action("GetInventoryPictures"))
                    }
                };

                //if (skip > 0)
                //{
                //    response.Links.Prev = new HalLink(Url.Action("GetPassTypesList", new { skip = Math.Max(0, skip - take), take }));
                //}

                //if (response.Count >= take)
                //{
                //    response.Links.Next =
                //        new HalLink(Url.Action("GetPassTypesList", new { skip = Math.Max(0, skip + passTypes.Count), take }));
                //}

                return Ok(response);
            }

            return new ApiResponse(HttpStatusCode.BadRequest).ToActionResult();
        }

        [HttpGet("{pictureId}")]
        public async Task<IActionResult> GetInventoryPicture(int id, int pictureId)
        {
            var result = await _inventoryPictureService.GetPictureAsync(pictureId);

            if (result.IsSuccessful())
            {
                var response = _mapper.Map<InventoryPictureResponse>(result.Value, opt => opt.AddUrlHelper(Url));

                return Ok(response);
            }

            return new ApiResponse(result.Code).ToActionResult();
        }

        [HttpPost]
        public async Task<IActionResult> CreateInventoryPicture(int id, [FromBody]InventoryPictureRequest request)
        {
            if (request == null)
                throw new ApiException(HttpStatusCode.BadRequest.ToString(), HttpStatusCode.BadRequest);

            if (!ModelState.IsValid)
                return BadRequest(_mapper.Map<ApiResponse>(ModelState));

            var picture = _mapper.Map<InventoryPicture>(request);

            var path = $"catalog/{id}";

            picture.InventoryId = id;

            picture.Thumbnail = await _imageManager.UploadToS3Async(path, request.Title, request.Thumbnail, true);
            picture.Picture = await _imageManager.UploadToS3Async(path, request.Title, request.Picture);

            var result = await _inventoryPictureService.CreatePictureAsync(picture);

            if (result.IsSuccessful())
            {
                var response = _mapper.Map<InventoryPictureResponse>(result.Value, opt => opt.AddUrlHelper(Url));

                response.Code = HttpStatusCode.Created;
                response.Message = HttpStatusCode.Created.ToString();

                return Created(Url.Action(), response);
            }

            return new ApiResponse(HttpStatusCode.BadRequest).ToActionResult();
        }

        [HttpPut("{pictureId}")]
        public async Task<IActionResult> UpdateInventoryPicture(int id, int pictureId, [FromBody]InventoryPictureRequest request)
        {
            if (request == null)
                throw new ApiException(HttpStatusCode.BadRequest.ToString(), HttpStatusCode.BadRequest);

            if (!ModelState.IsValid)
                return BadRequest(_mapper.Map<ApiResponse>(ModelState));

            var path = $"catalog/{id}";

            var picture = _mapper.Map<InventoryPicture>(request);

            picture.Id = pictureId;
            picture.InventoryId = id;

            picture.Thumbnail = await _imageManager.UploadToS3Async(path, request.Title, request.Thumbnail, true);
            picture.Picture = await _imageManager.UploadToS3Async(path, request.Title, request.Picture);

            var result = await _inventoryPictureService.UpdatePictureAsync(picture);

            if (result.IsSuccessful())
            {
                var response = _mapper.Map<InventoryPictureResponse>(result.Value, opt => opt.AddUrlHelper(Url));

                return Ok(response);
            }

            return new ApiResponse(result.Code).ToActionResult();
        }

        [HttpDelete("{pictureId}")]
        public async Task<IActionResult> DeleteInventoryPicture(int id, int pictureId)
        {
            var result = await _inventoryPictureService.DeletePictureAsync(pictureId);

            if (result.IsSuccessful())
                return new ApiResponse().ToActionResult();

            return new ApiResponse(result.Code).ToActionResult();
        }
    }
}