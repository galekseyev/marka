﻿using AutoMapper;
using Marka.BusinessLogic.Interfaces.Catalog.Inventories;
using Marka.Common.Exceptions;
using Marka.Common.Models.Catalog.Inventories;
using Marka.Common.Responses;
using Marka.Common.Responses.Base;
using Marka.Common.Responses.Catalog.Inventories.Attributes;
using Marka.Common.Results;
using Marka.Web.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Marka.Web.Controllers.Admin.Catalog.Inventories
{
    [Authorize(Policy = "Admin")]
    [Route("api/admin/inventory/{id}/attribute")]
    public class InventoryAttributeController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IInventoryAttributeService _inventoryAttributeService;

        public InventoryAttributeController(IMapper mapper, IInventoryAttributeService inventoryAttributeService)
        {
            _mapper = mapper;
            _inventoryAttributeService = inventoryAttributeService;
        }


        [HttpGet]
        public async Task<IActionResult> GetInventoryAttributes(int id)
        {
            var result = await _inventoryAttributeService.GetInventoryAttributesAsync(id);

            if (result.IsSuccessful())
            {
                var response = new InventoryAttributesResponse
                {
                    Embedded = _mapper.Map<IEnumerable<InventoryAttributeResponse>>(result.Value, opt => opt.AddUrlHelper(Url)),
                    Count = result.Value.Count(),
                    Total = result.Value.Count(),
                    Links = new HalLinks
                    {
                        Self = new HalLink(Url.Action("GetInventoryAttributes"))
                    }
                };

                //if (skip > 0)
                //{
                //    response.Links.Prev = new HalLink(Url.Action("GetPassTypesList", new { skip = Math.Max(0, skip - take), take }));
                //}

                //if (response.Count >= take)
                //{
                //    response.Links.Next =
                //        new HalLink(Url.Action("GetPassTypesList", new { skip = Math.Max(0, skip + passTypes.Count), take }));
                //}

                return Ok(response);
            }

            return new ApiResponse(result.Code).ToActionResult();
        }

        [HttpGet("{attributeId}")]
        public async Task<IActionResult> GetInventoryAttribute(int id, int attributeId)
        {
            var result = await _inventoryAttributeService.GetInventoryAttributeAsync(attributeId);

            if (result.IsSuccessful())
            {
                var response = _mapper.Map<InventoryAttributeResponse>(result.Value, opt => opt.AddUrlHelper(Url));

                return Ok(response);
            }

            return new ApiResponse(result.Code).ToActionResult();
        }

        [HttpPost]
        public async Task<IActionResult> CreateInventoryAttribute(int id, [FromBody]InventoryAttributeRequest request)
        {
            if (request == null)
                throw new ApiException(HttpStatusCode.BadRequest.ToString(), HttpStatusCode.BadRequest);

            if (!ModelState.IsValid)
                return BadRequest(_mapper.Map<ApiResponse>(ModelState));

            var attribute = _mapper.Map<InventoryAttribute>(request);

            attribute.InventoryId = id;

            var result = await _inventoryAttributeService.CreateInventoryAttributeAsync(attribute);

            if (result.IsSuccessful())
            {
                var response = _mapper.Map<InventoryAttributeResponse>(result.Value, opt => opt.AddUrlHelper(Url));

                response.Code = HttpStatusCode.Created;
                response.Message = HttpStatusCode.Created.ToString();

                return Created(Url.Action(), response);
            }

            return new ApiResponse(HttpStatusCode.BadRequest).ToActionResult();
        }

        [HttpPut("{attributeId}")]
        public async Task<IActionResult> UpdateInventoryAttribute(int id, int attributeId, [FromBody]InventoryAttributeRequest request)
        {
            if (request == null)
                throw new ApiException(HttpStatusCode.BadRequest.ToString(), HttpStatusCode.BadRequest);

            if (!ModelState.IsValid)
                return BadRequest(_mapper.Map<ApiResponse>(ModelState));

            var attribute = _mapper.Map<InventoryAttribute>(request);

            attribute.Id = attributeId;
            attribute.InventoryId = id;

            var result = await _inventoryAttributeService.UpdateInventoryAttributeAsync(attribute);

            if (result.IsSuccessful())
            {
                var response = _mapper.Map<InventoryAttributeResponse>(result.Value, opt => opt.AddUrlHelper(Url));

                return Ok(response);
            }

            return new ApiResponse(result.Code).ToActionResult();
        }

        [HttpDelete("{attributeId}")]
        public async Task<IActionResult> DeleteInventoryAttribute(int id, int attributeId)
        {
            var result = await _inventoryAttributeService.DeleteInventoryAttributeAsync(attributeId);

            if (result.IsSuccessful())
                return new ApiResponse().ToActionResult();

            return new ApiResponse(result.Code).ToActionResult();
        }
    }
}
