﻿using AutoMapper;
using Marka.BusinessLogic.Interfaces.Catalog.Categories;
using Marka.Common.Exceptions;
using Marka.Common.Models.Catalog.Categories;
using Marka.Common.Responses;
using Marka.Common.Responses.Base;
using Marka.Common.Responses.Catalog.Categories;
using Marka.Common.Results;
using Marka.Web.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Marka.Web.Controllers.Admin.Catalog.Categories
{
    [Authorize(Policy = "Admin")]
    [Route("api/admin/category")]
    public class CategoryController : Controller
    {
        private readonly IMapper _mapper;
        private readonly ICategoryService _categoryService;

        public CategoryController(IMapper mapper, ICategoryService categoryService)
        {
            _mapper = mapper;

            _categoryService = categoryService;
        }

        [HttpGet]
        public async Task<IActionResult> GetCategories()
        {
            var result = await _categoryService.GetCategoriesAsync();

            if (result.IsSuccessful())
            {
                var response = new CategoriesResponse
                {
                    Embedded = _mapper.Map<IEnumerable<CategoryResponse>>(result.Value, opt => opt.AddUrlHelper(Url)),
                    //{
                    //    Categories = categories
                    //},
                    Count = result.Value.Count(),
                    Total = result.Value.Count(),
                    Links = new HalLinks
                    {
                        Self = new HalLink(Url.Action("GetCategories"))
                    }
                };

                //if (skip > 0)
                //{
                //    response.Links.Prev = new HalLink(Url.Action("GetPassTypesList", new { skip = Math.Max(0, skip - take), take }));
                //}

                //if (response.Count >= take)
                //{
                //    response.Links.Next =
                //        new HalLink(Url.Action("GetPassTypesList", new { skip = Math.Max(0, skip + passTypes.Count), take }));
                //}

                return Ok(response);
            }

            return new ApiResponse(HttpStatusCode.BadRequest).ToActionResult();
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetCategory(int id)
        {
            var result = await _categoryService.GetCategoryAsync(id);

            if (result.IsSuccessful())
            {
                var response = _mapper.Map<CategoryResponse>(result.Value, opt => opt.AddUrlHelper(Url));

                return Ok(response);
            }

            return new ApiResponse(HttpStatusCode.BadRequest).ToActionResult();
        }

        [HttpPost]
        public async Task<IActionResult> CreateCategory([FromBody] CategoryRequest request)
        {
            if (request == null)
                throw new ApiException(HttpStatusCode.BadRequest.ToString(), HttpStatusCode.BadRequest);

            if (!ModelState.IsValid)
                return BadRequest(_mapper.Map<ApiResponse>(ModelState));

            var title = Guid.NewGuid().ToString("N");

            var category = _mapper.Map<Category>(request);

            var result = await _categoryService.CreateCategoryAsync(category);

            if (result.IsSuccessful())
            {
                var response = _mapper.Map<CategoryResponse>(result.Value, opt => opt.AddUrlHelper(Url));

                response.Code = HttpStatusCode.Created;
                response.Message = HttpStatusCode.Created.ToString();

                return Created(Url.Action(), response);
            }

            return new ApiResponse(HttpStatusCode.BadRequest).ToActionResult();
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateCategory(int id, [FromBody] CategoryRequest request)
        {
            if (request == null)
                throw new ApiException(HttpStatusCode.BadRequest.ToString(), HttpStatusCode.BadRequest);

            if (!ModelState.IsValid)
                return BadRequest(_mapper.Map<ApiResponse>(ModelState));

            var category = _mapper.Map<Category>(request);

            category.Id = id;

            var result = await _categoryService.UpdateCategoryAsync(category);

            if (result.IsSuccessful())
            {
                var response = _mapper.Map<CategoryResponse>(result.Value, opt => opt.AddUrlHelper(Url));

                return Ok(response);
            }

            return new ApiResponse(HttpStatusCode.BadRequest).ToActionResult();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCategory(int id)
        {
            var result = await _categoryService.DeleteCategoryAsync(id);

            if (result.IsSuccessful())
                return new ApiResponse().ToActionResult();

            return new ApiResponse(HttpStatusCode.BadRequest).ToActionResult();
        }
    }
}