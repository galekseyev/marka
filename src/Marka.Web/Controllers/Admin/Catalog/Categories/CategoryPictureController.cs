﻿using AutoMapper;
using Marka.BusinessLogic.Interfaces.Catalog.Categories;
using Marka.Common.Exceptions;
using Marka.Common.Models.Catalog.Categories;
using Marka.Common.Responses;
using Marka.Common.Responses.Catalog.Categories.Pictures;
using Marka.Common.Results;
using Marka.Web.Extensions;
using Marka.Web.Images;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Threading.Tasks;

namespace Marka.Web.Controllers.Admin.Catalog.Categories
{
    [Authorize(Policy = "Admin")]
    [Route("api/admin/category/{id}/picture")]
    public class CategoryPictureController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IImageManager _imageManager;
        private readonly ICategoryService _categoryService;
        private readonly ICategoryPictureService _categoryPictureService;

        public CategoryPictureController(IMapper mapper, IImageManager imageManager, ICategoryService categoryService, ICategoryPictureService categoryPictureService)
        {
            _mapper = mapper;

            _imageManager = imageManager;
            _categoryService = categoryService;
            _categoryPictureService = categoryPictureService;
        }

        [HttpGet]
        public async Task<IActionResult> GetCategoryPicture(int id)
        {
            var result = await _categoryPictureService.GetCategoryPictureAsync(id);

            if (result.IsSuccessful())
            {
                var response = _mapper.Map<CategoryPictureResponse>(result.Value, opt => opt.AddUrlHelper(Url));

                return Ok(response);
            }

            return new ApiResponse(HttpStatusCode.BadRequest).ToActionResult();
        }

        [HttpGet("{pictureId}")]
        public async Task<IActionResult> GetCategoryPicture(int id, int pictureId)
        {
            var result = await _categoryPictureService.GetPictureAsync(pictureId);

            if (result.IsSuccessful())
            {
                var response = _mapper.Map<CategoryPictureResponse>(result.Value, opt => opt.AddUrlHelper(Url));

                return Ok(response);
            }

            return new ApiResponse(result.Code).ToActionResult();
        }

        [HttpPost]
        public async Task<IActionResult> CreateCategoryPicture(int id, [FromBody]CategoryPictureRequest request)
        {
            if (request == null)
                throw new ApiException(HttpStatusCode.BadRequest.ToString(), HttpStatusCode.BadRequest);

            if (!ModelState.IsValid)
                return BadRequest(_mapper.Map<ApiResponse>(ModelState));

            var categoryResult = await _categoryService.GetCategoryAsync(id);

            if (categoryResult.IsSuccessful())
            {
                var category = categoryResult.Value;
                var picture = _mapper.Map<CategoryPicture>(request);

                var path = $"catalog/categories/{id}";

                picture.Thumbnail = await _imageManager.UploadToS3Async(path, request.Title, request.Thumbnail, true);
                picture.Picture = await _imageManager.UploadToS3Async(path, request.Title, request.Picture);

                category.Picture = picture;
                var result = await _categoryService.UpdateCategoryAsync(category);

                if (result.IsSuccessful())
                {
                    var response = _mapper.Map<CategoryPictureResponse>(result.Value.Picture, opt => opt.AddUrlHelper(Url));

                    response.Code = HttpStatusCode.Created;
                    response.Message = HttpStatusCode.Created.ToString();

                    return Created(Url.Action(), response);
                }
            }


            return new ApiResponse(HttpStatusCode.BadRequest).ToActionResult();
        }

        [HttpPut("{pictureId}")]
        public async Task<IActionResult> UpdateCategoryPicture(int id, int pictureId, [FromBody]CategoryPictureRequest request)
        {
            if (request == null)
                throw new ApiException(HttpStatusCode.BadRequest.ToString(), HttpStatusCode.BadRequest);

            if (!ModelState.IsValid)
                return BadRequest(_mapper.Map<ApiResponse>(ModelState));

            var categoryResult = await _categoryService.GetCategoryAsync(id);

            if (categoryResult.IsSuccessful())
            {
                var category = categoryResult.Value;
                var path = $"catalog/categories/{id}";

                var picture = _mapper.Map<CategoryPicture>(request);

                picture.Id = pictureId;

                picture.Thumbnail = await _imageManager.UploadToS3Async(path, request.Title, request.Thumbnail, true);
                picture.Picture = await _imageManager.UploadToS3Async(path, request.Title, request.Picture);

                category.Picture = picture;
                var result = await _categoryService.UpdateCategoryAsync(category);

                if (result.IsSuccessful())
                {
                    var response = _mapper.Map<CategoryPictureResponse>(result.Value.Picture, opt => opt.AddUrlHelper(Url));

                    return Ok(response);
                }
            }

            return new ApiResponse(HttpStatusCode.BadRequest).ToActionResult();
        }

        [HttpDelete("{pictureId}")]
        public async Task<IActionResult> DeleteCategoryPicture(int id, int pictureId)
        {
            var result = await _categoryPictureService.DeletePictureAsync(pictureId);

            if (result.IsSuccessful())
                return new ApiResponse().ToActionResult();

            return new ApiResponse(result.Code).ToActionResult();
        }
    }
}
