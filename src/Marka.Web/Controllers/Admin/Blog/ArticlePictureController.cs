﻿using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Marka.BusinessLogic.Interfaces.Blog;
using Marka.Common.Exceptions;
using Marka.Common.Models.Blog;
using Marka.Common.Responses;
using Marka.Common.Responses.Blog.Pictures;
using Marka.Common.Results;
using Marka.Web.Extensions;
using Marka.Web.Images;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace Marka.Web.Controllers.Admin.Blog
{
    [Authorize(Policy = "Admin")]
    [Route("api/admin/article/{id}/picture")]
    public class ArticlePictureController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IImageManager _imageManager;
        private readonly IArticleService _articleService;
        private readonly IArticlePictureService _articlePictureService;

        public ArticlePictureController(IMapper mapper, IImageManager imageManager, IArticleService articleService, IArticlePictureService articlePictureService)
        {
            _mapper = mapper;
            _imageManager = imageManager;
            _articleService = articleService;
            _articlePictureService = articlePictureService;
        }

        [HttpGet]
        public async Task<IActionResult> GetArticlePicture(int id)
        {
            var result = await _articlePictureService.GetArticlePictureAsync(id);

            if (result.IsSuccessful())
            {
                var response = _mapper.Map<ArticlePictureResponse>(result.Value, opt => opt.AddUrlHelper(Url));

                return Ok(response);
            }

            return new ApiResponse(HttpStatusCode.BadRequest).ToActionResult();
        }

        [HttpGet("{pictureId}")]
        public async Task<IActionResult> GetArticlePicture(int id, int pictureId)
        {
            var result = await _articlePictureService.GetPictureAsync(pictureId);

            if (result.IsSuccessful())
            {
                var response = _mapper.Map<ArticlePictureResponse>(result.Value, opt => opt.AddUrlHelper(Url));

                return Ok(response);
            }

            return new ApiResponse(result.Code).ToActionResult();
        }

        [HttpPost]
        public async Task<IActionResult> CreateArticlePicture(int id, [FromBody]ArticlePictureRequest request)
        {
            if (request == null)
                throw new ApiException(HttpStatusCode.BadRequest.ToString(), HttpStatusCode.BadRequest);

            if (!ModelState.IsValid)
                return BadRequest(_mapper.Map<ApiResponse>(ModelState));

            var articleResult = await _articleService.GetArticleAsync(id);

            if (articleResult.IsSuccessful())
            {
                var article = articleResult.Value;

                var picture = _mapper.Map<ArticlePicture>(request);

                var path = $"blog/articles/{id}";

                picture.Thumbnail = await _imageManager.UploadToS3Async(path, request.Title, request.Thumbnail, true);
                picture.Picture = await _imageManager.UploadToS3Async(path, request.Title, request.Picture);

                article.Picture = picture;

                var result = await _articleService.UpdateArticleAsync(article);

                if (result.IsSuccessful())
                {
                    var response = _mapper.Map<ArticlePictureResponse>(result.Value.Picture, opt => opt.AddUrlHelper(Url));

                    response.Code = HttpStatusCode.Created;
                    response.Message = HttpStatusCode.Created.ToString();

                    return Created(Url.Action(), response);
                }
            }

            return new ApiResponse(HttpStatusCode.BadRequest).ToActionResult();
        }

        [HttpPut("{pictureId}")]
        public async Task<IActionResult> UpdateArticlePicture(int id, int pictureId, [FromBody]ArticlePictureRequest request)
        {
            if (request == null)
                throw new ApiException(HttpStatusCode.BadRequest.ToString(), HttpStatusCode.BadRequest);

            if (!ModelState.IsValid)
                return BadRequest(_mapper.Map<ApiResponse>(ModelState));

            var articleResult = await _articleService.GetArticleAsync(id);

            if (articleResult.IsSuccessful())
            {
                var article = articleResult.Value;

                var path = $"blog/articles/{id}";
                var picture = _mapper.Map<ArticlePicture>(request);

                picture.Id = pictureId;

                picture.Thumbnail = await _imageManager.UploadToS3Async(path, request.Title, request.Thumbnail, true);
                picture.Picture = await _imageManager.UploadToS3Async(path, request.Title, request.Picture);

                article.Picture = picture;

                var result = await _articleService.UpdateArticleAsync(article);

                if (result.IsSuccessful())
                {
                    var response = _mapper.Map<ArticlePictureResponse>(result.Value.Picture, opt => opt.AddUrlHelper(Url));

                    return Ok(response);
                }
            }

            return new ApiResponse(HttpStatusCode.BadRequest).ToActionResult();
        }

        [HttpDelete("{pictureId}")]
        public async Task<IActionResult> DeleteArticlePicture(int id, int pictureId)
        {
            var result = await _articlePictureService.DeletePictureAsync(pictureId);

            if (result.IsSuccessful())
                return new ApiResponse().ToActionResult();

            return new ApiResponse(result.Code).ToActionResult();
        }
    }
}