﻿using AutoMapper;
using Marka.BusinessLogic.Interfaces.Blog;
using Marka.BusinessLogic.Interfaces.Catalog.Categories;
using Marka.Common.Exceptions;
using Marka.Common.Models.Blog;
using Marka.Common.Responses;
using Marka.Common.Responses.Base;
using Marka.Common.Responses.Blog;
using Marka.Common.Results;
using Marka.Web.Extensions;
using Marka.Web.Images;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Marka.Web.Controllers.Admin.Blog
{
    [Authorize(Policy = "Admin")]
    [Route("api/admin/article")]
    public class ArticleController: ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IArticleService _articleService;

        public ArticleController(IMapper mapper, IArticleService articleService)
        {
            _mapper = mapper;

            _articleService = articleService;
        }

        [HttpGet]
        public async Task<IActionResult> GetArticles()
        {
            var result = await _articleService.GetArticlesAsync();

            if (result.IsSuccessful())
            {
                var response = new ArticlesResponse
                {
                    Embedded = _mapper.Map<IEnumerable<ArticleResponse>>(result.Value, opt => opt.AddUrlHelper(Url)),
                    //{
                    //    Categories = categories
                    //},
                    Count = result.Value.Count(),
                    Total = result.Value.Count(),
                    Links = new HalLinks
                    {
                        Self = new HalLink(Url.Action("GetArticles"))
                    }
                };

                //if (skip > 0)
                //{
                //    response.Links.Prev = new HalLink(Url.Action("GetPassTypesList", new { skip = Math.Max(0, skip - take), take }));
                //}

                //if (response.Count >= take)
                //{
                //    response.Links.Next =
                //        new HalLink(Url.Action("GetPassTypesList", new { skip = Math.Max(0, skip + passTypes.Count), take }));
                //}

                return Ok(response);
            }

            return new ApiResponse(HttpStatusCode.BadRequest).ToActionResult();
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetArticle(int id)
        {
            var result = await _articleService.GetArticleAsync(id);

            if (result.IsSuccessful())
            {
                var response = _mapper.Map<ArticleResponse>(result.Value, opt => opt.AddUrlHelper(Url));

                return Ok(response);
            }

            return new ApiResponse(HttpStatusCode.BadRequest).ToActionResult();
        }

        [HttpPost]
        public async Task<IActionResult> CreateArticle([FromBody] ArticleRequest request)
        {
            if (request == null)
                throw new ApiException(HttpStatusCode.BadRequest.ToString(), HttpStatusCode.BadRequest);

            if (!ModelState.IsValid)
                return BadRequest(_mapper.Map<ApiResponse>(ModelState));

            var title = Guid.NewGuid().ToString("N");

            var article = _mapper.Map<Article>(request);

            var result = await _articleService.CreateArticleAsync(article);

            if (result.IsSuccessful())
            {
                var response = _mapper.Map<ArticleResponse>(result.Value, opt => opt.AddUrlHelper(Url));

                response.Code = HttpStatusCode.Created;
                response.Message = HttpStatusCode.Created.ToString();

                return Created(Url.Action(), response);
            }

            return new ApiResponse(HttpStatusCode.BadRequest).ToActionResult();
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateArticle(int id, [FromBody] ArticleRequest request)
        {
            if (request == null)
                throw new ApiException(HttpStatusCode.BadRequest.ToString(), HttpStatusCode.BadRequest);

            if (!ModelState.IsValid)
                return BadRequest(_mapper.Map<ApiResponse>(ModelState));

            var article = _mapper.Map<Article>(request);

            article.Id = id;

            var result = await _articleService.UpdateArticleAsync(article);

            if (result.IsSuccessful())
            {
                var response = _mapper.Map<ArticleResponse>(result.Value, opt => opt.AddUrlHelper(Url));

                return Ok(response);
            }

            return new ApiResponse(HttpStatusCode.BadRequest).ToActionResult();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteArticle(int id)
        {
            var result = await _articleService.DeleteArticleAsync(id);

            if (result.IsSuccessful())
                return new ApiResponse().ToActionResult();

            return new ApiResponse(HttpStatusCode.BadRequest).ToActionResult();
        }
    }
}