using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;

namespace Marka.Web.Controllers
{
    public class HomeController : Controller
    {

        private readonly IStringLocalizer<Resource> _localizer;


        public HomeController(IStringLocalizer<Resource> localizer)
        {
            _localizer = localizer;
        }

        public IActionResult Test()
        {
            return Ok(_localizer["TestKey"]);
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Error()
        {
            ViewData["RequestId"] = Activity.Current?.Id ?? HttpContext.TraceIdentifier;
            return View();
        }
    }
}
