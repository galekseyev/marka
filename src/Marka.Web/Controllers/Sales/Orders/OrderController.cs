﻿using AutoMapper;
using Marka.BusinessLogic.Interfaces;
using Marka.BusinessLogic.Interfaces.Catalog.Inventories;
using Marka.BusinessLogic.Interfaces.Orders;
using Marka.BusinessLogic.Interfaces.Payments;
using Marka.Common.Constants;
using Marka.Common.Exceptions;
using Marka.Common.Models.Account.Users;
using Marka.Common.Models.Orders;
using Marka.Common.Responses;
using Marka.Common.Responses.Orders;
using Marka.Common.Results;
using Marka.Web.Constants;
using Marka.Web.Controllers.Carts;
using Marka.Web.Core.Filters;
using Marka.Web.Core.Managers;
using Marka.Web.Extensions;
using Marka.Web.Helpers;
using Marka.Web.Options;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace Marka.Web.Controllers.Sales.Orders
{

    [Route("api/order")]
    public class OrderController : BaseCartController
    {
        private readonly IHostingEnvironment _env;
        private readonly IMapper _mapper;
        private readonly IEmailSender _emailSender;
        private readonly UserManager<User> _userManager;
        private readonly ICartManager _cartManager;
        private readonly ICartService _cartService;
        private readonly IOrderService _orderService;
        private readonly IOrderStatusService _orderStatusService;
        private readonly IPaymentTypeService _paymentTypeService;
        private readonly IPaymentStatusService _paymentStatusService;
        private readonly IInventoryService _inventoryService;

        public OrderController(
            IHostingEnvironment env,
            IOptions<CartOptions> options,
            IMapper mapper,
            IEmailSender emailSender,
            IDataProtectionProvider provider,
            UserManager<User> userManager,
            ICartManager cartManager,
            ICartService cartService,
            IOrderService orderService,
            IOrderStatusService orderStatusService,
            IPaymentTypeService paymentTypeService,
            IPaymentStatusService paymentStatusService,
            IInventoryService inventoryService
            )
            : base(options, provider, userManager, cartService)
        {
            _env = env;
            _mapper = mapper;
            _emailSender = emailSender;
            _userManager = userManager;
            _cartManager = cartManager;
            _cartService = cartService;
            _orderService = orderService;
            _orderStatusService = orderStatusService;
            _paymentStatusService = paymentStatusService;
            _paymentTypeService = paymentTypeService;
            _inventoryService = inventoryService;
        }


        [HttpPost]
        [ValidateCartItemsAsyncActionFilter]
        public async Task<IActionResult> CreateOrder([FromBody] OrderRequest request)
        {
            if (request == null)
                throw new ApiException(HttpStatusCode.BadRequest.ToString(), HttpStatusCode.BadRequest);

            if (!ModelState.IsValid)
                return BadRequest(_mapper.Map<ApiResponse>(ModelState));

            var cartId = await _cartManager.GetCartIdAsync();

            var cartResult = await _cartService.GetCartAsync(cartId);

            if (cartResult.IsSuccessful())
            {
                var orderStatusTask = _orderStatusService.GetOrderStatusByCodeAsync(OrderStatusCode.Pending);
                var paymentStatusTask = _paymentStatusService.GetPaymentStatusByCodeAsync(PaymentStatusCode.Pending);
                var paymentTypeTask = _paymentTypeService.GetPaymentTypeByCodeAsync(PaymentTypeCode.BankTransfer);

                await Task.WhenAll(orderStatusTask, paymentStatusTask, paymentTypeTask);

                if (orderStatusTask.Result.IsSuccessful() &&
                    paymentStatusTask.Result.IsSuccessful() &&
                    paymentTypeTask.Result.IsSuccessful())
                {
                    var reduceResult = await _inventoryService.ReduceInventoryQuantities(cartResult.Value);

                    if (reduceResult.IsSuccessful())
                    {
                        var order = _mapper.Map<Order>(request);

                        order.UserId = await _cartManager.GetUserIdAsync();
                        order.OrderItems = _mapper.Map<ICollection<OrderItem>>(cartResult.Value.CartItems);

                        order.OrderStatus = orderStatusTask.Result.Value;
                        order.PaymentStatus = paymentStatusTask.Result.Value;
                        order.PaymentType = paymentTypeTask.Result.Value;

                        var result = await _orderService.CreateOrderAsync(order);

                        if (result.IsSuccessful())
                        {
                            var translatedResult = await _orderService.GetTranslatedOrderAsync(order.Id, LanguageConstants.LanguageCodes.English);

                            if (translatedResult.IsSuccessful())
                            {
                                var emailTemplate = await EmailHelper.GetEmailBodyTemplate(_env.WebRootPath, EmailTemplateType.Order, EmailHelper.GetPendingEmailTemplateName(order.PaymentType.TypeCode));
                                var body = EmailHelper.ReplaceOrderEmailPlaceholders(emailTemplate, translatedResult.Value);

                                await _emailSender.SendEmailAsync(request.Email, "[M] - Order Successfully complete!", body);

                                var deleteResult = await _cartService.DeleteCartAsync(cartResult.Value.Id);

                                if (deleteResult.IsSuccessful())
                                {
                                    var response = _mapper.Map<OrderResponse>(result.Value, opt => opt.AddUrlHelper(Url));

                                    response.Code = HttpStatusCode.Created;
                                    response.Message = HttpStatusCode.Created.ToString();

                                    return Created(Url.Action(), response);
                                }
                            }
                        }
                    }
                }
            }

            return new ApiResponse(HttpStatusCode.BadRequest).ToActionResult();
        }
    }
}