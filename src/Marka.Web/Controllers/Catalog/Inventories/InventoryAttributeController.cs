﻿using AutoMapper;
using Marka.BusinessLogic.Interfaces.Catalog.Inventories;
using Marka.Common.Responses;
using Marka.Common.Responses.Base;
using Marka.Common.Responses.Catalog.Inventories.Attributes;
using Marka.Common.Results;
using Marka.Web.Constants;
using Marka.Web.Extensions;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Marka.Web.Controllers.Catalog.Inventories
{
    [Route("api/inventory/{id}/attribute")]
    public class InventoryAttributeController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IInventoryAttributeService _inventoryAttributeService;

        public InventoryAttributeController(IMapper mapper, IInventoryAttributeService inventoryAttributeService)
        {
            _mapper = mapper;
            _inventoryAttributeService = inventoryAttributeService;
        }

        [HttpGet]
        public async Task<IActionResult> GetTranslatedInventoryAttributes(int id)
        {
            var code = LanguageConstants.LanguageCodes.English;
            var result = await _inventoryAttributeService.GetTranslatedInventoryAttributesAsync(id, code);

            if (result.IsSuccessful())
            {
                var response = new TranslatedInventoryAttributesResponse
                {
                    Embedded = _mapper.Map<IEnumerable<TranslatedInventoryAttributeResponse>>(result.Value, opt => opt.AddUrlHelper(Url)),
                    Count = result.Value.Count(),
                    Total = result.Value.Count(),
                    Links = new HalLinks
                    {
                        Self = new HalLink(Url.Action("GetTranslatedInventoryAttributes"))
                    }
                };

                //if (skip > 0)
                //{
                //    response.Links.Prev = new HalLink(Url.Action("GetPassTypesList", new { skip = Math.Max(0, skip - take), take }));
                //}

                //if (response.Count >= take)
                //{
                //    response.Links.Next =
                //        new HalLink(Url.Action("GetPassTypesList", new { skip = Math.Max(0, skip + passTypes.Count), take }));
                //}

                return Ok(response);
            }

            return new ApiResponse(HttpStatusCode.BadRequest).ToActionResult();
        }
    }
}
