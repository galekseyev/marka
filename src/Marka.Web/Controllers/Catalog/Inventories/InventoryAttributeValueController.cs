﻿using AutoMapper;
using Marka.BusinessLogic.Interfaces.Catalog.Inventories;
using Marka.Common.Responses;
using Marka.Common.Responses.Base;
using Marka.Common.Responses.Catalog.Inventories.Attributes;
using Marka.Common.Results;
using Marka.Web.Constants;
using Marka.Web.Extensions;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Marka.Web.Controllers.Catalog.Inventories
{
    [Route("api/inventory/{id}/attribute/{attributeId}/value")]
    public class InventoryAttributeValueController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IInventoryAttributeValueService _inventoryAttributeValueService;

        public InventoryAttributeValueController(IMapper mapper, IInventoryAttributeValueService inventoryAttributeValueService, IInventoryPictureService inventoryPictureService)
        {
            _mapper = mapper;
            _inventoryAttributeValueService = inventoryAttributeValueService;
        }

        [HttpGet]
        public async Task<IActionResult> GetTranslatedInventoryAttributeValues(int id, int attributeId)
        {
            var code = LanguageConstants.LanguageCodes.English;
            var result = await _inventoryAttributeValueService.GetTranslatedInventoryAttributeValuesAsync(id, attributeId, code);

            if (result.IsSuccessful())
            {
                var response = new TranslatedInventoryAttributeValuesResponse
                {
                    Embedded = _mapper.Map<IEnumerable<TranslatedInventoryAttributeValueResponse>>(result.Value, opt => opt.AddUrlHelper(Url)),
                    Count = result.Value.Count(),
                    Total = result.Value.Count(),
                    Links = new HalLinks
                    {
                        Self = new HalLink(Url.Action("GetTranslatedInventoryAttributeValues"))
                    }
                };

                //if (skip > 0)
                //{
                //    response.Links.Prev = new HalLink(Url.Action("GetPassTypesList", new { skip = Math.Max(0, skip - take), take }));
                //}

                //if (response.Count >= take)
                //{
                //    response.Links.Next =
                //        new HalLink(Url.Action("GetPassTypesList", new { skip = Math.Max(0, skip + passTypes.Count), take }));
                //}

                return Ok(response);
            }

            return new ApiResponse(HttpStatusCode.BadRequest).ToActionResult();
        }
    }
}
