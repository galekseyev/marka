﻿using AutoMapper;
using Marka.BusinessLogic.Interfaces.Catalog.Inventories;
using Marka.Common.Models.Filters;
using Marka.Common.Responses;
using Marka.Common.Responses.Base;
using Marka.Common.Responses.Catalog.Inventories;
using Marka.Common.Results;
using Marka.Web.Constants;
using Marka.Web.Extensions;
using Marka.Web.Images;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Marka.Web.Controllers.Admin.Catalog
{
    [Route("api/inventory")]
    public class InventoryController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IImageManager _imageManager;
        private readonly IInventoryService _inventoryService;

        public InventoryController(IMapper mapper, IImageManager imageManager, IInventoryService inventoryService)
        {
            _mapper = mapper;

            _imageManager = imageManager;
            _inventoryService = inventoryService;
        }

        [HttpGet]
        public async Task<IActionResult> GetTranslatedInventories(InventoryFilter filter)
        {
            var code = LanguageConstants.LanguageCodes.English;
            var result = await _inventoryService.GetTranslatedInventoriesAsync(filter, code);

            if (result.IsSuccessful())
            {
                var response = new TranslatedInventoriesResponse
                {
                    Embedded = _mapper.Map<IEnumerable<TranslatedInventoryResponse>>(result.Value.Inventories, opt => opt.AddUrlHelper(Url)),
                    Count = result.Value.Inventories.Count(),
                    Total = result.Value.Total,
                    Links = new HalLinks
                    {
                        Self = new HalLink(Url.Action("GetTranslatedInventories"))
                    }
                };

                //if (skip > 0)
                //{
                //    response.Links.Prev = new HalLink(Url.Action("GetPassTypesList", new { skip = Math.Max(0, skip - take), take }));
                //}

                //if (response.Count >= take)
                //{
                //    response.Links.Next =
                //        new HalLink(Url.Action("GetPassTypesList", new { skip = Math.Max(0, skip + passTypes.Count), take }));
                //}

                return Ok(response);
            }

            return new ApiResponse(HttpStatusCode.BadRequest).ToActionResult();
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetTranslatedInventory(int id)
        {
            var code = LanguageConstants.LanguageCodes.English;

            var result = await _inventoryService.GetTranslatedInventoryAsync(id, code);

            if (result.IsSuccessful())
            {
                var response = _mapper.Map<TranslatedInventoryResponse>(result.Value, opt => opt.AddUrlHelper(Url));

                return Ok(response);
            }

            return new ApiResponse(HttpStatusCode.BadRequest).ToActionResult();
        }
    }
}