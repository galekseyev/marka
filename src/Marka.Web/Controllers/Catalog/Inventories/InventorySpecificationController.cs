﻿using AutoMapper;
using Marka.BusinessLogic.Interfaces.Catalog.Inventories;
using Marka.Common.Responses;
using Marka.Common.Responses.Base;
using Marka.Common.Responses.Catalog.Inventories.Specifications;
using Marka.Common.Results;
using Marka.Web.Constants;
using Marka.Web.Extensions;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Marka.Web.Controllers.Catalog.Inventories
{
    [Route("api/inventory/{id}/specification")]
    public class InventorySpecificationController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IInventorySpecificationService _inventorySpecificationService;

        public InventorySpecificationController(IMapper mapper, IInventorySpecificationService inventorySpecificationService, IInventoryPictureService inventoryPictureService)
        {
            _mapper = mapper;
            _inventorySpecificationService = inventorySpecificationService;
        }

        [HttpGet]
        public async Task<IActionResult> GetTranslatedInventorySpecifications(int id)
        {
            var code = LanguageConstants.LanguageCodes.English;
            var result = await _inventorySpecificationService.GetTranslatedInventorySpecificationsAsync(id, code);

            if (result.IsSuccessful())
            {
                var response = new TranslatedInventorySpecificationsResponse
                {
                    Embedded = _mapper.Map<IEnumerable<TranslatedInventorySpecificationResponse>>(result.Value, opt => opt.AddUrlHelper(Url)),
                    Count = result.Value.Count(),
                    Total = result.Value.Count(),
                    Links = new HalLinks
                    {
                        Self = new HalLink(Url.Action("GetTranslatedInventorySpecifications"))
                    }
                };

                //if (skip > 0)
                //{
                //    response.Links.Prev = new HalLink(Url.Action("GetPassTypesList", new { skip = Math.Max(0, skip - take), take }));
                //}

                //if (response.Count >= take)
                //{
                //    response.Links.Next =
                //        new HalLink(Url.Action("GetPassTypesList", new { skip = Math.Max(0, skip + passTypes.Count), take }));
                //}

                return Ok(response);
            }

            return new ApiResponse(HttpStatusCode.BadRequest).ToActionResult();
        }
    }
}
