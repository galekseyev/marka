﻿using AutoMapper;
using Marka.BusinessLogic.Interfaces.Catalog.InventoryGroups;
using Marka.Common.Models.Filters;
using Marka.Common.Responses;
using Marka.Common.Responses.Base;
using Marka.Common.Responses.Catalog.InventoryGroups;
using Marka.Common.Results;
using Marka.Web.Constants;
using Marka.Web.Extensions;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Marka.Web.Controllers.Catalog.InventoryGroups
{
    [Route("api/inventory-group")]
    public class InventoryGroupController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IInventoryGroupService _inventoryGroupService;

        public InventoryGroupController(IMapper mapper, IInventoryGroupService inventoryGroupService)
        {
            _mapper = mapper;
            _inventoryGroupService = inventoryGroupService;
        }

        [HttpGet]
        public async Task<IActionResult> GetInventoryGroups(InventoryGroupFilter filter)
        {
            var result = await _inventoryGroupService.GetInventoryGroupsAsync(filter);

            if (result.IsSuccessful())
            {
                var response = new InventoryGroupsResponse
                {
                    Embedded = _mapper.Map<IEnumerable<InventoryGroupResponse>>(result.Value.InventoryGroups, opt => opt.AddUrlHelper(Url)),
                    Count = result.Value.InventoryGroups.Count(),
                    Total = result.Value.Total,
                    Links = new HalLinks
                    {
                        Self = new HalLink(Url.Action("GetInventoryGroups"))
                    }
                };

                //if (skip > 0)
                //{
                //    response.Links.Prev = new HalLink(Url.Action("GetPassTypesList", new { skip = Math.Max(0, skip - take), take }));
                //}

                //if (response.Count >= take)
                //{
                //    response.Links.Next =
                //        new HalLink(Url.Action("GetPassTypesList", new { skip = Math.Max(0, skip + passTypes.Count), take }));
                //}

                return Ok(response);
            }

            return new ApiResponse(HttpStatusCode.BadRequest).ToActionResult();
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetTranslatedInventoryGroup(int id)
        {
            var code = LanguageConstants.LanguageCodes.English;

            var result = await _inventoryGroupService.GetTranslatedInventoryGroup(id, code);

            if (result.IsSuccessful())
            {
                var response = _mapper.Map<TranslatedInventoryGroupResponse>(result.Value, opt => opt.AddUrlHelper(Url));

                return Ok(response);
            }

            return new ApiResponse(HttpStatusCode.BadRequest).ToActionResult();
        }
    }
}
