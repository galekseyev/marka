﻿using AutoMapper;
using Marka.BusinessLogic.Interfaces.Catalog.Categories;
using Marka.Common.Responses;
using Marka.Common.Responses.Base;
using Marka.Common.Responses.Catalog.Categories;
using Marka.Common.Results;
using Marka.Web.Constants;
using Marka.Web.Extensions;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Marka.Web.Controllers.Catalog
{
    [Route("api/category")]
    public class CategoryController : Controller
    {
        private readonly IMapper _mapper;
        private readonly ICategoryService _categoryService;

        public CategoryController(IMapper mapper, ICategoryService categoryService)
        {
            _mapper = mapper;
            _categoryService = categoryService;
        }

        [HttpGet]
        public async Task<IActionResult> GetTranslatedCategories()
        {
            var code = LanguageConstants.LanguageCodes.English;
            var result = await _categoryService.GetTranslatedCategoriesAsync(code);

            if (result.IsSuccessful())
            {
                var response = new TranslatedCategoriesResponse
                {
                    Embedded = _mapper.Map<IEnumerable<TranslatedCategoryResponse>>(result.Value, opt => opt.AddUrlHelper(Url)),
                    //{
                    //    Categories = categories
                    //},
                    Count = result.Value.Count(),
                    Total = result.Value.Count(),
                    Links = new HalLinks
                    {
                        Self = new HalLink(Url.Action("GetCategories"))
                    }
                };

                //if (skip > 0)
                //{
                //    response.Links.Prev = new HalLink(Url.Action("GetPassTypesList", new { skip = Math.Max(0, skip - take), take }));
                //}

                //if (response.Count >= take)
                //{
                //    response.Links.Next =
                //        new HalLink(Url.Action("GetPassTypesList", new { skip = Math.Max(0, skip + passTypes.Count), take }));
                //}

                return Ok(response);
            }

            return new ApiResponse(HttpStatusCode.BadRequest).ToActionResult();
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetTranslatedCategory(int id)
        {
            var code = LanguageConstants.LanguageCodes.English;

            var result = await _categoryService.GetTranslatedCategoryAsync(id, code);

            if (result.IsSuccessful())
            {
                var response = _mapper.Map<CategoryResponse>(result.Value, opt => opt.AddUrlHelper(Url));

                return Ok(response);
            }

            return new ApiResponse(HttpStatusCode.BadRequest).ToActionResult();
        }
    }
}