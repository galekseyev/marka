﻿using AutoMapper;
using Marka.BusinessLogic.Interfaces.Catalog.Attributes;
using Marka.Common.Responses;
using Marka.Common.Responses.Base;
using Marka.Common.Responses.Catalog.Attributes;
using Marka.Common.Results;
using Marka.Web.Constants;
using Marka.Web.Extensions;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Marka.Web.Controllers.Admin.Catalog
{
    [Route("api/attribute")]
    public class AttributeController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IAttributeService _attributeService;

        public AttributeController(IMapper mapper, IAttributeService attributeService)
        {
            _mapper = mapper;
            _attributeService = attributeService;
        }

        [HttpGet]
        public async Task<IActionResult> GetTranslatedAttributes()
        {
            var code = LanguageConstants.LanguageCodes.English;

            var result = await _attributeService.GetTranslatedAttributesAsync(code);

            if (result.IsSuccessful())
            {
                var response = new TranslatedAttributesResponse
                {
                    Embedded = _mapper.Map<IEnumerable<TranslatedAttributeResponse>>(result.Value, opt => opt.AddUrlHelper(Url)),
                    Count = result.Value.Count(),
                    Total = result.Value.Count(),
                    Links = new HalLinks
                    {
                        Self = new HalLink(Url.Action("GetTranslatedAttributes"))
                    }
                };

                //if (skip > 0)
                //{
                //    response.Links.Prev = new HalLink(Url.Action("GetPassTypesList", new { skip = Math.Max(0, skip - take), take }));
                //}

                //if (response.Count >= take)
                //{
                //    response.Links.Next =
                //        new HalLink(Url.Action("GetPassTypesList", new { skip = Math.Max(0, skip + passTypes.Count), take }));
                //}

                return Ok(response);
            }

            return new ApiResponse(HttpStatusCode.BadRequest).ToActionResult();
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetTranslatedAttribute(int id)
        {
            var code = LanguageConstants.LanguageCodes.English;
            var result = await _attributeService.GetTranslatedAttributeAsync(id, code);

            if (result.IsSuccessful())
            {
                var response = _mapper.Map<TranslatedAttributeResponse>(result.Value, opt => opt.AddUrlHelper(Url));

                return Ok(response);
            }

            return new ApiResponse(HttpStatusCode.BadRequest).ToActionResult();
        }
    }
}