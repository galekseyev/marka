﻿using AutoMapper;
using Marka.BusinessLogic.Interfaces.Catalog.Specifications;
using Marka.Common.Responses;
using Marka.Common.Responses.Base;
using Marka.Common.Responses.Catalog.Specifications;
using Marka.Common.Results;
using Marka.Web.Constants;
using Marka.Web.Extensions;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Marka.Web.Controllers.Catalog.Specifications
{
    [Route("api/specification/{id}/value")]
    public class SpecificationValueController : Controller
    {
        private readonly IMapper _mapper;
        private readonly ISpecificationValueService _specificationValueService;

        public SpecificationValueController(IMapper mapper, ISpecificationValueService specificationValueService)
        {
            _mapper = mapper;
            _specificationValueService = specificationValueService;
        }

        [HttpGet]
        public async Task<IActionResult> GetTranslatedSpecificationValues(int id)
        {
            var code = LanguageConstants.LanguageCodes.English;

            var result = await _specificationValueService.GetTranslatedSpecificationValuesAsync(id, code);

            if (result.IsSuccessful())
            {
                var response = new TranslatedSpecificationValuesResponse
                {
                    Embedded = _mapper.Map<IEnumerable<TranslatedSpecificationValueResponse>>(result.Value, opt => opt.AddUrlHelper(Url)),
                    Count = result.Value.Count(),
                    Total = result.Value.Count(),
                    Links = new HalLinks
                    {
                        Self = new HalLink(Url.Action("GetTranslatedSpecifications"))
                    }
                };

                //if (skip > 0)
                //{
                //    response.Links.Prev = new HalLink(Url.Action("GetPassTypesList", new { skip = Math.Max(0, skip - take), take }));
                //}

                //if (response.Count >= take)
                //{
                //    response.Links.Next =
                //        new HalLink(Url.Action("GetPassTypesList", new { skip = Math.Max(0, skip + passTypes.Count), take }));
                //}

                return Ok(response);
            }

            return new ApiResponse(HttpStatusCode.BadRequest).ToActionResult();
        }
    }
}
