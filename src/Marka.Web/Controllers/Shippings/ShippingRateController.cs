﻿using AutoMapper;
using Marka.BusinessLogic.Interfaces.Shippings;
using Marka.Common.Exceptions;
using Marka.Common.Models.Shippings;
using Marka.Common.Responses;
using Marka.Common.Responses.Base;
using Marka.Common.Responses.Shippings;
using Marka.Common.Results;
using Marka.Web.Extensions;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Marka.Web.Controllers.Shippings
{
    [Route("api/shipping/{id}/rate")]
    public class ShippingRateController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IShippingRateService _shippingRateService;

        public ShippingRateController(IMapper mapper, IShippingRateService shippingRateService)
        {
            _mapper = mapper;
            _shippingRateService = shippingRateService;
        }

        [HttpGet]
        public async Task<IActionResult> GetShippingRates(int id)
        {
            var result = await _shippingRateService.GetShippingRatesAsync(id);

            if (result.IsSuccessful())
            {
                var rates = _mapper.Map<IEnumerable<ShippingRateResponse>>(result.Value, opt => opt.AddUrlHelper(Url));

                var response = new ShippingRatesResponse
                {
                    Embedded = rates,
                    Count = result.Value.Count(),
                    Total = result.Value.Count(),
                    Links = new HalLinks
                    {
                        Self = new HalLink(Url.Action("GetShippingRates"))
                    }
                };

                //if (skip > 0)
                //{
                //    response.Links.Prev = new HalLink(Url.Action("GetPassTypesList", new { skip = Math.Max(0, skip - take), take }));
                //}

                //if (response.Count >= take)
                //{
                //    response.Links.Next =
                //        new HalLink(Url.Action("GetPassTypesList", new { skip = Math.Max(0, skip + passTypes.Count), take }));
                //}

                return Ok(response);
            }

            return new ApiResponse(HttpStatusCode.BadRequest).ToActionResult();
        }

        [HttpGet("{rateId}")]
        public async Task<IActionResult> GetShippingRate(int id, int rateId)
        {
            var result = await _shippingRateService.GetShippingRateAsync(rateId);

            if (result.IsSuccessful())
            {
                var response = _mapper.Map<ShippingRateResponse>(result.Value, opt => opt.AddUrlHelper(Url));

                return Ok(response);
            }

            return new ApiResponse(result.Code).ToActionResult();
        }

        [HttpPost]
        public async Task<IActionResult> CreateShippingRate(int id, [FromBody]ShippingRateRequest request)
        {
            if (request == null)
                throw new ApiException(HttpStatusCode.BadRequest.ToString(), HttpStatusCode.BadRequest);

            if (!ModelState.IsValid)
                return BadRequest(_mapper.Map<ApiResponse>(ModelState));

            var rate = _mapper.Map<ShippingRate>(request);

            rate.ShippingMethodId = id;

            var result = await _shippingRateService.CreateShippingRateAsync(rate);

            if (result.IsSuccessful())
            {
                var response = _mapper.Map<ShippingRateResponse>(result.Value, opt => opt.AddUrlHelper(Url));

                response.Code = HttpStatusCode.Created;
                response.Message = HttpStatusCode.Created.ToString();

                return Created(Url.Action(), response);
            }

            return new ApiResponse(HttpStatusCode.BadRequest).ToActionResult();
        }

        [HttpPut("{rateId}")]
        public async Task<IActionResult> UpdateShippingRate(int id, int rateId, [FromBody]ShippingRateRequest request)
        {
            if (request == null)
                throw new ApiException(HttpStatusCode.BadRequest.ToString(), HttpStatusCode.BadRequest);

            if (!ModelState.IsValid)
                return BadRequest(_mapper.Map<ApiResponse>(ModelState));

            var rate = _mapper.Map<ShippingRate>(request);

            rate.Id = rateId;
            rate.ShippingMethodId = id;

            var result = await _shippingRateService.UpdateShippingRateAsync(rate);

            if (result.IsSuccessful())
            {
                var response = _mapper.Map<ShippingRateResponse>(result.Value, opt => opt.AddUrlHelper(Url));
                return Ok(response);
            }

            return new ApiResponse(result.Code).ToActionResult();
        }

        [HttpDelete("{rateId}")]
        public async Task<IActionResult> DeleteShippingRate(int id, int rateId)
        {
            var result = await _shippingRateService.DeleteShippingRateAsync(rateId);

            if (result.IsSuccessful())
                return new ApiResponse().ToActionResult();

            return new ApiResponse(result.Code).ToActionResult();
        }
    }
}