﻿using AutoMapper;
using Marka.BusinessLogic.Interfaces.Shippings;
using Marka.Common.Exceptions;
using Marka.Common.Models.Shippings;
using Marka.Common.Responses;
using Marka.Common.Responses.Base;
using Marka.Common.Responses.Shippings;
using Marka.Common.Results;
using Marka.Web.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Marka.Web.Controllers.Shippings
{
    [Route("api/shipping")]
    public class ShippingMethodController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IShippingMethodService _shippingMethodService;

        public ShippingMethodController(IMapper mapper, IShippingMethodService shippingMethodService)
        {
            _mapper = mapper;
            _shippingMethodService = shippingMethodService;
        }

        [HttpGet]
        public async Task<IActionResult> GetShippingMethods()
        {
            var result = await _shippingMethodService.GetShippingMethodsAsync();

            if (result.IsSuccessful())
            {
                var response = new ShippingMethodsResponse
                {
                    Embedded = _mapper.Map<IEnumerable<ShippingMethodResponse>>(result.Value, opt => opt.AddUrlHelper(Url)),
                    Count = result.Value.Count(),
                    Total = result.Value.Count(),
                    Links = new HalLinks
                    {
                        Self = new HalLink(Url.Action("GetShippingMethods"))
                    }
                };

                //if (skip > 0)
                //{
                //    response.Links.Prev = new HalLink(Url.Action("GetPassTypesList", new { skip = Math.Max(0, skip - take), take }));
                //}

                //if (response.Count >= take)
                //{
                //    response.Links.Next =
                //        new HalLink(Url.Action("GetPassTypesList", new { skip = Math.Max(0, skip + passTypes.Count), take }));
                //}

                return Ok(response);
            }

            return new ApiResponse(HttpStatusCode.BadRequest).ToActionResult();
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetShippingMethod(int id)
        {
            var result = await _shippingMethodService.GetShippingMethodAsync(id);

            if (result.IsSuccessful())
            {
                var response = _mapper.Map<ShippingMethodResponse>(result.Value, opt => opt.AddUrlHelper(Url));
           
                return Ok(response);
            }

            return new ApiResponse(HttpStatusCode.BadRequest).ToActionResult();
        }

        [HttpPost]
        [Authorize(Policy = "Admin")]
        public async Task<IActionResult> CreateShippingMethod([FromBody] ShippingMethodRequest request)
        {
            if (request == null)
                throw new ApiException(HttpStatusCode.BadRequest.ToString(), HttpStatusCode.BadRequest);

            if (!ModelState.IsValid)
                return BadRequest(_mapper.Map<ApiResponse>(ModelState));

            var shippingMethod = _mapper.Map<ShippingMethod>(request);

            var result = await _shippingMethodService.CreateShippingMethodAsync(shippingMethod);

            if (result.IsSuccessful())
            {
                var response = _mapper.Map<ShippingMethodResponse>(result.Value, opt => opt.AddUrlHelper(Url));

                response.Code = HttpStatusCode.Created;
                response.Message = HttpStatusCode.Created.ToString();

                return Created(Url.Action(), response);
            }

            return new ApiResponse(HttpStatusCode.BadRequest).ToActionResult();
        }

        [HttpPut("{id}")]
        [Authorize(Policy = "Admin")]
        public async Task<IActionResult> UpdateShippingMethod(int id, [FromBody] ShippingMethodRequest request)
        {
            if (request == null)
                throw new ApiException(HttpStatusCode.BadRequest.ToString(), HttpStatusCode.BadRequest);

            if (!ModelState.IsValid)
                return BadRequest(_mapper.Map<ApiResponse>(ModelState));

            var shippingMethod = _mapper.Map<ShippingMethod>(request);

            shippingMethod.Id = id;

            var result = await _shippingMethodService.UpdateShippingMethodAsync(shippingMethod);

            if (result.IsSuccessful())
            {
                var response = _mapper.Map<ShippingMethodResponse>(result.Value, opt => opt.AddUrlHelper(Url));

                return Ok(response);
            }

            return new ApiResponse(HttpStatusCode.BadRequest).ToActionResult();
        }

        [HttpDelete("{id}")]
        [Authorize(Policy = "Admin")]
        public async Task<IActionResult> DeleteShippingMethod(int id)

        {
            var result = await _shippingMethodService.DeleteShippingMethodAsync(id);

            if (result.IsSuccessful())
                return new ApiResponse().ToActionResult();

            return new ApiResponse(HttpStatusCode.BadRequest).ToActionResult();
        }
    }
}