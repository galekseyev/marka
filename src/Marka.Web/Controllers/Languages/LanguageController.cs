﻿using AutoMapper;
using Marka.BusinessLogic.Interfaces.Languages;
using Marka.Common.ResponseModels.Languages;
using Marka.Common.Responses;
using Marka.Common.Responses.Base;
using Marka.Common.Responses.Languages;
using Marka.Common.Results;
using Marka.Web.Extensions;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Marka.Web.Controllers.Languages
{
    [Route("api/language")]
    public class LanguageController : Controller
    {
        private readonly IMapper _mapper;
        private readonly ILanguageService _languageService;

        public LanguageController(IMapper mapper, ILanguageService languageService)
        {
            _mapper = mapper;
            _languageService = languageService;
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetLanguage(int id)
        {
            var result = await _languageService.GetLanguageAsync(id);

            if (result.IsSuccessful())
            {
                var response = _mapper.Map<LanguageResponse>(result.Value, opt => opt.AddUrlHelper(Url));

                return Ok(response);
            }

            return new ApiResponse(HttpStatusCode.BadRequest).ToActionResult();
        }

        [HttpGet]
        public async Task<IActionResult> GetLanguages()
        {
            var result = await _languageService.GetLanguagesAsync();

            if (result.IsSuccessful())
            {
                var response = new LanguagesResponse
                {
                    Embedded = _mapper.Map<IEnumerable<LanguageResponse>>(result.Value, opt => opt.AddUrlHelper(Url)),
                    Count = result.Value.Count(),
                    Total = result.Value.Count(),
                    Links = new HalLinks
                    {
                        Self = new HalLink(Url.Action("GetLanguages"))
                    }
                };

                //if (skip > 0)
                //{
                //    response.Links.Prev = new HalLink(Url.Action("GetPassTypesList", new { skip = Math.Max(0, skip - take), take }));
                //}

                //if (response.Count >= take)
                //{
                //    response.Links.Next =
                //        new HalLink(Url.Action("GetPassTypesList", new { skip = Math.Max(0, skip + passTypes.Count), take }));
                //}

                return Ok(response);
            }

            return new ApiResponse(HttpStatusCode.BadRequest).ToActionResult();
        }
    }
}
