﻿using AutoMapper;
using Marka.BusinessLogic.Interfaces;
using Marka.BusinessLogic.Interfaces.Carts;
using Marka.Common.Exceptions;
using Marka.Common.Models.Account.Users;
using Marka.Common.Models.Carts;
using Marka.Common.Responses;
using Marka.Common.Responses.Carts;
using Marka.Common.Results;
using Marka.Web.Core.Managers;
using Marka.Web.Extensions;
using Marka.Web.Options;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System.Net;
using System.Threading.Tasks;

namespace Marka.Web.Controllers.Carts
{
    [Route("api/cart/shipping")]
    public class CartShippingController : BaseCartController
    {
        private readonly IMapper _mapper;

        private readonly IDataProtector _protector;

        private readonly UserManager<User> _userManager;
        private readonly ICartManager _cartManager;

        private readonly ICartShippingService _cartShippingService;

        public CartShippingController(IOptions<CartOptions> options, IMapper mapper, IDataProtectionProvider provider, UserManager<User> userManager, ICartManager cartManager, ICartService cartService, ICartShippingService cartShippingService)
            : base(options, provider, userManager, cartService)
        {
            _mapper = mapper;

            _protector = provider.CreateProtector(options.Value.CartEncryptionPurpose);

            _userManager = userManager;
            _cartManager = cartManager;

            _cartShippingService = cartShippingService;
        }

        [HttpGet("{cartShippingId}")]
        public async Task<IActionResult> GetCartShipping(int cartShippingId)
        {
            var result = await _cartShippingService.GetCartShippingAsync(cartShippingId);

            if (result.IsSuccessful())
            {
                var response = _mapper.Map<CartShippingResponse>(result.Value, opt => opt.AddUrlHelper(Url));

                return Ok(response);
            }

            return new ApiResponse(result.Code).ToActionResult();
        }

        [HttpPost]
        public async Task<IActionResult> CreateCartShipping([FromBody]CartShippingRequest request)
        {
            if (request == null)
                throw new ApiException(HttpStatusCode.BadRequest.ToString(), HttpStatusCode.BadRequest);

            if (!ModelState.IsValid)
                return BadRequest(_mapper.Map<ApiResponse>(ModelState));

            var cartShipping = _mapper.Map<CartShipping>(request);

            var key = await _cartManager.GetCartIdAsync();

            cartShipping.CartId = key;

            var result = await _cartShippingService.CreateCartShippingAsync(cartShipping);

            if (result.IsSuccessful())
            {
                var response = _mapper.Map<CartShippingResponse>(result.Value, opt => opt.AddUrlHelper(Url));

                response.Code = HttpStatusCode.Created;
                response.Message = HttpStatusCode.Created.ToString();

                return Created(Url.Action(), response);
            }

            return new ApiResponse(HttpStatusCode.BadRequest).ToActionResult();
        }

        [HttpPut("{cartShippingId}")]
        public async Task<IActionResult> UpdateCartShipping(int cartShippingId, [FromBody]CartShippingRequest request)
        {
            if (request == null)
                throw new ApiException(HttpStatusCode.BadRequest.ToString(), HttpStatusCode.BadRequest);

            if (!ModelState.IsValid)
                return BadRequest(_mapper.Map<ApiResponse>(ModelState));

            var key = await _cartManager.GetCartIdAsync();

            var cartShipping = _mapper.Map<CartShipping>(request);

            cartShipping.Id = cartShippingId;
            cartShipping.CartId = key;

            var result = await _cartShippingService.UpdateCartShippingAsync(cartShipping);

            if (result.IsSuccessful())
            {
                var response = _mapper.Map<CartShippingResponse>(result.Value, opt => opt.AddUrlHelper(Url));

                return Ok(response);
            }

            return new ApiResponse(result.Code).ToActionResult();
        }

        [HttpDelete("{cartShippingId}")]
        public async Task<IActionResult> DeleteCartShipping(int cartShippingId)
        {
            var result = await _cartShippingService.DeleteCartShippingAsync(cartShippingId);

            if (result.IsSuccessful())
                return new ApiResponse().ToActionResult();

            return new ApiResponse(result.Code).ToActionResult();
        }
    }
}
