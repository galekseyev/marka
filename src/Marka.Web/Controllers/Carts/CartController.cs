﻿using AutoMapper;
using Marka.BusinessLogic.Interfaces;
using Marka.Common.Exceptions;
using Marka.Common.Models.Account.Users;
using Marka.Common.Models.Carts;
using Marka.Common.Responses;
using Marka.Common.Responses.Carts;
using Marka.Common.Results;
using Marka.Web.Extensions;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using System.Net;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.Extensions.Options;
using Marka.Web.Options;
using Marka.Web.Constants;
using Marka.Web.Core.Managers;

namespace Marka.Web.Controllers.Carts
{
    [Route("api/cart")]
    public class CartController : BaseCartController
    {
        private readonly IMapper _mapper;

        private readonly IDataProtector _protector;

        private readonly UserManager<User> _userManager;
        private readonly ICartManager _cartManager;

        private readonly ICartService _cartService;

        public CartController(IOptions<CartOptions> options, IMapper mapper, IDataProtectionProvider provider, UserManager<User> userManager, ICartManager cartManager, ICartService cartService)
            : base(options, provider, userManager, cartService)
        {
            _mapper = mapper;

            _protector = provider.CreateProtector(options.Value.CartEncryptionPurpose);

            _userManager = userManager;
            _cartManager = cartManager;

            _cartService = cartService;
        }

        [HttpGet]
        public async Task<IActionResult> GetTranslatedCart()
        {
            var id = await _cartManager.GetCartIdAsync();
            var userId = await _cartManager.GetUserIdAsync();

            var code = LanguageConstants.LanguageCodes.English;
            var result = await _cartService.GetTranslatedCartAsync(id, userId, code);

            if (result.IsSuccessful())
            {
                var response = _mapper.Map<TranslatedCartResponse>(result.Value, opt => opt.AddUrlHelper(Url));

                if (!User.Identity.IsAuthenticated)
                    response.Embedded.Key = _protector.Protect(result.Value.Id);

                return Ok(response);
            }

            return new ApiResponse(result.Code).ToActionResult();
        }

        [HttpPost]
        public async Task<IActionResult> CreateCart([FromBody] CartRequest request)
        {
            if (request == null)
                throw new ApiException(HttpStatusCode.BadRequest.ToString(), HttpStatusCode.BadRequest);

            if (!ModelState.IsValid)
                return BadRequest(_mapper.Map<ApiResponse>(ModelState));

            var cart = _mapper.Map<Cart>(request);

            cart.Id = await _cartManager.GetCartIdAsync();
            cart.UserId = await _cartManager.GetUserIdAsync();

            var result = await _cartService.CreateCartAsync(cart);

            if (result.IsSuccessful())
            {
                var response = _mapper.Map<CartResponse>(result.Value, opt => opt.AddUrlHelper(Url));

                if (!User.Identity.IsAuthenticated)
                    response.Embedded.Key = _protector.Protect(result.Value.Id);

                response.Code = HttpStatusCode.Created;
                response.Message = HttpStatusCode.Created.ToString();

                return Created(Url.Action(), response);
            }

            return new ApiResponse(HttpStatusCode.BadRequest).ToActionResult();
        }

        [HttpDelete]
        public async Task<IActionResult> DeleteCart()
        {
            var id = await _cartManager.GetCartIdAsync();

            var result = await _cartService.DeleteCartAsync(id);

            if (result.IsSuccessful())
                return new ApiResponse().ToActionResult();

            return new ApiResponse(HttpStatusCode.BadRequest).ToActionResult();
        }
    }
}