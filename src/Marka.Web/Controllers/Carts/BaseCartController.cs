﻿using Marka.BusinessLogic.Interfaces;
using Marka.Common.Models.Account.Users;
using Marka.Common.Results;
using Marka.Web.Options;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Marka.Web.Controllers.Carts
{
    public abstract class BaseCartController : Controller
    {
        private const string CartTokenHeader = "CartToken";

        private readonly IDataProtector _protector;
        private readonly UserManager<User> _userManager;
        private readonly ICartService _cartService;

        public BaseCartController(IOptions<CartOptions> options, IDataProtectionProvider provider, UserManager<User> userManager, ICartService cartService)
        {
            _protector = provider.CreateProtector(options.Value.CartEncryptionPurpose);

            _userManager = userManager;
            _cartService = cartService;
        }

        //protected virtual async Task<int?> GetUserIdAsync()
        //{
        //    if (User.Identity.IsAuthenticated)
        //    {
        //        var email = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
        //        var user = await _userManager.FindByEmailAsync(email);

        //        return user?.Id;
        //    }

        //    return null;
        //}

        //protected virtual string GetCartToken()
        //{
        //    return Request.Headers[CartTokenHeader].FirstOrDefault();
        //}

        //protected virtual async Task<string> GetUserCartId()
        //{
        //    var email = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;

        //    if (email != null)
        //    {
        //        var user = await _userManager.FindByEmailAsync(email);
        //        if (user != null)
        //        {
        //            var result = await _cartService.GetUserCartAsync(user.Id);

        //            if (result.IsSuccessful())
        //                return result.Value.Id;
        //        }
        //    }

        //    return null;
        //}

        //protected virtual async Task<string> GetCartIdAsync()
        //{
        //    var id = Guid.NewGuid();

        //    if (User.Identity.IsAuthenticated)
        //    {
        //        var email = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;

        //        if (email != null)
        //        {
        //            var user = await _userManager.FindByEmailAsync(email);
        //            if (user != null)
        //            {
        //                var result = await _cartService.GetUserCartAsync(user.Id);

        //                if (result.IsSuccessful())
        //                    return result.Value.Id;
        //            }
        //        }
        //    }
        //    else
        //    {
        //        var token = GetCartToken();

        //        if (!string.IsNullOrEmpty(token))
        //            Guid.TryParse(_protector.Unprotect(token), out id);
        //    }

        //    return id.ToString("N");
        //}
    }
}