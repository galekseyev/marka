﻿using AutoMapper;
using Marka.BusinessLogic.Interfaces;
using Marka.Common.Exceptions;
using Marka.Common.Models.Account.Users;
using Marka.Common.Models.Carts;
using Marka.Common.Responses;
using Marka.Common.Responses.Carts;
using Marka.Common.Results;
using Marka.Web.Constants;
using Marka.Web.Core.Filters;
using Marka.Web.Core.Managers;
using Marka.Web.Extensions;
using Marka.Web.Options;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System.Net;
using System.Threading.Tasks;

namespace Marka.Web.Controllers.Carts
{
    [Route("api/cart/item/")]
    public class CartItemController : BaseCartController
    {
        private readonly IDataProtector _protector;

        private readonly IMapper _mapper;

        private readonly ICartManager _cartManager;
        private readonly ICartService _cartService;

        public CartItemController(IOptions<CartOptions> options, IMapper mapper, IDataProtectionProvider provider, UserManager<User> userManager, ICartManager cartManager, ICartService cartService)
            : base(options, provider, userManager, cartService)
        {
            _mapper = mapper;
            _protector = provider.CreateProtector(options.Value.CartEncryptionPurpose);

            _cartManager = cartManager;
            _cartService = cartService;
        }

        [HttpPost]
        public async Task<IActionResult> AddItem([FromBody]CartItemRequest request, bool createIfMissing = false)
        {
            if (request == null)
                throw new ApiException(HttpStatusCode.BadRequest.ToString(), HttpStatusCode.BadRequest);

            if (!ModelState.IsValid)
                return BadRequest(_mapper.Map<ApiResponse>(ModelState));

            var item = _mapper.Map<CartItem>(request);

            var id = await _cartManager.GetCartIdAsync();
            var userId = await _cartManager.GetUserIdAsync();

            var result = await _cartService.AddItemAsync(id, item, createIfMissing);

            if (result.IsSuccessful())
            {
                var translatedResult = await _cartService.GetTranslatedCartAsync(id, userId, LanguageConstants.LanguageCodes.English);

                if (translatedResult.IsSuccessful())
                {
                    var response = _mapper.Map<TranslatedCartResponse>(translatedResult.Value, opt => opt.AddUrlHelper(Url));

                    return Ok(response);
                }
            }

            return new ApiResponse(result.Code).ToActionResult();
        }

        [HttpPut("{id}")]
        [ValidateCartItemsAsyncActionFilter]
        public async Task<IActionResult> UpdateItemQuantity(int id, [FromBody]int quantity)
        {
            var langCode = LanguageConstants.LanguageCodes.English;
            var cartId = await _cartManager.GetCartIdAsync();
            var userId = await _cartManager.GetUserIdAsync();

            var result = await _cartService.UpdateItemQuantityAsync(cartId, id, quantity);

            if (result.IsSuccessful())
            {
                var translatedResult = await _cartService.GetTranslatedCartAsync(cartId, userId, langCode);

                if (translatedResult.IsSuccessful())
                {
                    var response = _mapper.Map<TranslatedCartResponse>(translatedResult.Value, opt => opt.AddUrlHelper(Url));

                    return Ok(response);
                }
            }

            return new ApiResponse(result.Code).ToActionResult();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteItem(int id)
        {
            var cartId = await _cartManager.GetCartIdAsync();
            var userId = await _cartManager.GetUserIdAsync();

            var result = await _cartService.DeleteItemAsync(cartId, id);

            if (result.IsSuccessful())
            {
                var translatedResult = await _cartService.GetTranslatedCartAsync(cartId, userId, LanguageConstants.LanguageCodes.English);

                if (translatedResult.IsSuccessful())
                {
                    var response = _mapper.Map<TranslatedCartResponse>(translatedResult.Value, opt => opt.AddUrlHelper(Url));

                    return Ok(response);
                }
            }

            return new ApiResponse(result.Code).ToActionResult();
        }
    }
}