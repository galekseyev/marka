﻿using Marka.Common.Exceptions;
using Marka.Common.Responses;
using Marka.Web.Exceptions;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Net;

namespace Marka.Web.Controllers
{
    public class ErrorController : Controller
    {
        [Route("error")]
        public IActionResult Error()
        {
            var exFeature = HttpContext.Features.Get<IExceptionHandlerPathFeature>();

            if (exFeature != null)
            {
                var exception = exFeature.Error;
                var path = exFeature.Path;

                var errors = new List<string>();

                if (exception != null)
                {
                    errors.AddRange(GetErrors(exception, true));
                }

                var apiException = exception as ApiException;

                if (apiException != null)
                {
                    return apiException.ToActionResult();
                }
                else
                {
#if DEBUG
                    if (exception != null)
                        return StatusCode((int)HttpStatusCode.InternalServerError, new ApiResponse(exception.Message, HttpStatusCode.InternalServerError) { Errors = errors });
                    else
                        return StatusCode((int)HttpStatusCode.InternalServerError, new ApiResponse(HttpStatusCode.InternalServerError));
#else
                    return StatusCode((int)HttpStatusCode.InternalServerError, new ApiResponse(HttpStatusCode.InternalServerError));
#endif
                }
            }
            else
            {
                return StatusCode((int)HttpStatusCode.InternalServerError, new ApiResponse(HttpStatusCode.InternalServerError));
            }
        }

        private IEnumerable<String> GetErrors(Exception ex, bool innerOnly)
        {
            var errors = new List<String>();

            if (ex != null)
            {
                if (!innerOnly)
                {
                    errors.Add(ex.Message);
                }

                var aggEx = ex as AggregateException;

                if (aggEx != null)
                {
                    foreach (var innerEx in aggEx.InnerExceptions)
                    {
                        errors.AddRange(GetErrors(innerEx, false));
                    }
                }
                else
                {
                    errors.AddRange(GetErrors(ex.InnerException, false));
                }
            }

            return errors;
        }
    }
}
