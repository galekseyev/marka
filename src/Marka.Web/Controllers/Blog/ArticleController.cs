﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Marka.BusinessLogic.Interfaces.Blog;
using Marka.Common.Responses;
using Marka.Common.Responses.Base;
using Marka.Common.Responses.Blog;
using Marka.Common.Responses.Catalog.Categories;
using Marka.Common.Results;
using Marka.Web.Constants;
using Marka.Web.Extensions;
using Microsoft.AspNetCore.Mvc;

namespace Marka.Web.Controllers.Blog
{
    [Route("api/article")]
    public class ArticleController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IArticleService _articleService;

        public ArticleController(IMapper mapper, IArticleService articleService)
        {
            _mapper = mapper;
            _articleService = articleService;
        }

        [HttpGet]
        public async Task<IActionResult> GetTranslatedArticles()
        {
            var code = LanguageConstants.LanguageCodes.English;
            var result = await _articleService.GetTranslatedArticlesAsync(code);

            if (result.IsSuccessful())
            {
                var response = new TranslatedArticlesResponse
                {
                    Embedded = _mapper.Map<IEnumerable<TranslatedArticleResponse>>(result.Value, opt => opt.AddUrlHelper(Url)),
                    //{
                    //    Categories = categories
                    //},
                    Count = result.Value.Count(),
                    Total = result.Value.Count(),
                    Links = new HalLinks
                    {
                        Self = new HalLink(Url.Action("GetTranslatedArticles"))
                    }
                };

                //if (skip > 0)
                //{
                //    response.Links.Prev = new HalLink(Url.Action("GetPassTypesList", new { skip = Math.Max(0, skip - take), take }));
                //}

                //if (response.Count >= take)
                //{
                //    response.Links.Next =
                //        new HalLink(Url.Action("GetPassTypesList", new { skip = Math.Max(0, skip + passTypes.Count), take }));
                //}

                return Ok(response);
            }

            return new ApiResponse(HttpStatusCode.BadRequest).ToActionResult();
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetTranslatedArticle(int id)
        {
            var code = LanguageConstants.LanguageCodes.English;

            var result = await _articleService.GetTranslatedArticleAsync(id, code);

            if (result.IsSuccessful())
            {
                var response = _mapper.Map<TranslatedArticleResponse>(result.Value, opt => opt.AddUrlHelper(Url));

                return Ok(response);
            }

            return new ApiResponse(HttpStatusCode.BadRequest).ToActionResult();
        }
    }
}