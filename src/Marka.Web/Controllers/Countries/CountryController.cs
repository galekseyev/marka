﻿using AutoMapper;
using Marka.BusinessLogic.Interfaces.Countries;
using Marka.Common.Responses;
using Marka.Common.Responses.Base;
using Marka.Common.Responses.Countries;
using Marka.Common.Results;
using Marka.Web.Extensions;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Marka.Web.Controllers.Countries
{
    [Route("api/country")]
    public class CountryController : Controller
    {
        private readonly IMapper _mapper;
        private readonly ICountryService _countryService;

        public CountryController(
            IMapper mapper,
            ICountryService countryService)
        {
            _mapper = mapper;
            _countryService = countryService;
        }

        [HttpGet]
        public async Task<IActionResult> GetCountriesAsync()
        {
            var result = await _countryService.GetCountriesAsync();

            if (result.IsSuccessful())
            {
                var response = new CountriesResponse
                {
                    Embedded = _mapper.Map<IEnumerable<CountryResponse>>(result.Value, opt => opt.AddUrlHelper(Url)),
                    Count = result.Value.Count(),
                    Total = result.Value.Count(),
                    Links = new HalLinks
                    {
                        Self = new HalLink(Url.Action("GetCountriesAsync"))
                    }
                };

                return Ok(response);
            }

            return new ApiResponse(HttpStatusCode.BadRequest).ToActionResult();
        }
    }
}