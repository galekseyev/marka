﻿using AutoMapper;
using Marka.Common.Constants;
using Marka.Common.Responses;
using Marka.Common.Responses.Contact;
using Marka.Web.Emails;
using Marka.Web.Helpers;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Marka.Web.Controllers.Contact
{
    [Route("api/contact")]
    public class ContactController : Controller
    {
        private readonly IHostingEnvironment _env;
        private readonly IMapper _mapper;
        private readonly EmailOptions _emailOptions;
        private readonly IEmailSender _emailSender;

        private const string ContactUsTemplateName = "ContactUs";

        public ContactController(IHostingEnvironment env, IOptions<EmailOptions> emailOptions, IMapper mapper, IEmailSender emailSender)
        {
            _env = env;
            _mapper = mapper;
            _emailSender = emailSender;
            _emailOptions = emailOptions.Value;
        }

        [HttpPost]
        public async Task<IActionResult> SendMessage([FromBody]SendMessageRequest request)
        {
            if (!ModelState.IsValid)
                return BadRequest(_mapper.Map<ApiResponse>(ModelState));

            var subject = "[M] - Message Received";


            var emailTemplate = await EmailHelper.GetEmailBodyTemplate(_env.WebRootPath, EmailTemplateType.Contact, ContactUsTemplateName);
            var body = EmailHelper.ReplaceContactUsEmailPlaceholders(emailTemplate, request);

            await _emailSender.SendEmailAsync(_emailOptions.RecipientEmail, subject, body);

            return Ok();
        }
    }
}
