﻿using AutoMapper;
using Marka.BusinessLogic.Interfaces.Account;
using Marka.Common.Models.Account.Users;
using Marka.Common.Models.Countries;
using Marka.Common.ResponseModels.Account;
using Marka.Common.Responses;
using Marka.Common.Responses.Account;
using Marka.Common.Results;
using Marka.Web.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Marka.Web.Controllers.Account
{
    [Authorize]
    [Route("api/account/profile")]
    public class AccountProfileController : Controller
    {
        private readonly IMapper _mapper;

        private readonly UserManager<User> _userManager;
        private readonly IUserAddressService _userAddressService;

        public AccountProfileController(IMapper mapper, UserManager<User> userManager, IUserAddressService userAddressService)
        {
            _mapper = mapper;
            _userManager = userManager;
            _userAddressService = userAddressService;
        }

        [HttpGet]
        public async Task<IActionResult> GetUserProfile()
        {
            var email = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;

            if (email != null)
            {
                var user = await _userManager.FindByEmailAsync(email);

                if (user != null)
                {
                    var result = await _userAddressService.GetUserAddressAsync(user.Id);

                    if (result.IsSuccessful())
                    {
                        var userAddressModel = _mapper.Map<UserAddressModel>(result.Value);
                        var response = _mapper.Map<UserProfileResponse>(user, opt => opt.AddUrlHelper(Url));
                        response.Embedded.Address = userAddressModel;
                        return Ok(response);
                    }
                }
            }

            return new ApiResponse(HttpStatusCode.BadRequest).ToActionResult();
        }

        [HttpPost]
        public async Task<IActionResult> SaveUserProfile([FromBody]UserProfileRequest request)
        {
            if (!ModelState.IsValid)
                return BadRequest(_mapper.Map<ApiResponse>(ModelState));

            var email = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;

            if (email != null)
            {
                var user = await _userManager.FindByEmailAsync(email);

                if (user != null)
                {
                    user.FullName = request.FullName;
                    var address = _mapper.Map<UserAddress>(request);
                    address.UserId = user.Id;

                    var result = await _userManager.UpdateAsync(user);
                    var addressResult = await _userAddressService.UpdateUserAddressAsync(address);

                    if (result.Succeeded && addressResult.IsSuccessful())
                    {
                        addressResult = await _userAddressService.GetUserAddressAsync(user.Id);

                        if (addressResult.IsSuccessful())
                        {
                            var userAddressModel = _mapper.Map<UserAddressModel>(addressResult.Value);
                            var response = _mapper.Map<UserProfileResponse>(user, opt => opt.AddUrlHelper(Url));
                            response.Embedded.Address = userAddressModel;
                            return Ok(response);
                        }
                    }
                }
            }

            return new ApiResponse(HttpStatusCode.BadRequest).ToActionResult();
        }
    }
}
