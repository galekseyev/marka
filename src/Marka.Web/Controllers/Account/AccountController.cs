﻿using AutoMapper;
using Marka.BusinessLogic.Interfaces;
using Marka.Common.Constants;
using Marka.Common.Models.Account.Users;
using Marka.Common.Responses;
using Marka.Common.Responses.Account;
using Marka.Web.Auth;
using Marka.Web.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Marka.Web.Controllers
{
    [Route("api/account")]
    public class AccountController : Controller
    {
        private readonly IMapper _mapper;

        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;

        private readonly IJwtFactory _jwtFactory;
        private readonly JwtIssuerOptions _jwtOptions;

        private readonly IEmailSender _emailSender;

        private readonly ICartService _cartService;

        public AccountController(
            UserManager<User> userManager,
            SignInManager<User> signInManager,
            IMapper mapper,
            IEmailSender emailSender,
            IJwtFactory jwtFactory,
            IOptions<JwtIssuerOptions> jwtOptions,
            ICartService cartService
            )
        {
            _userManager = userManager;
            _signInManager = signInManager;

            _mapper = mapper;
            _emailSender = emailSender;

            _jwtFactory = jwtFactory;
            _jwtOptions = jwtOptions.Value;

            _cartService = cartService;
        }

        // TODO:
        // Need to implement Two Factor Authentication and Emails and SMS via WebApi.
        [HttpPost("login")]
        public async Task<IActionResult> AuthenticateUser([FromBody] LoginRequest model)
        {
            if (!ModelState.IsValid)
                return BadRequest(_mapper.Map<ApiResponse>(ModelState));

            var user = await _userManager.FindByEmailAsync(model.Email);

            if (user != null)
            {
                var result = await _signInManager.CheckPasswordSignInAsync(user, model.Password, false);

                if (result.Succeeded && !result.IsNotAllowed)
                {
                    var response = _mapper.Map<LoginResponse>(result);

                    var roles = await _userManager.GetRolesAsync(user);
                    response.Token = await _jwtFactory.GenerateEncodedToken(user.Email, roles.ToList());

                    return Ok(response);
                }
            }

            var errorResponse = new ApiResponse(HttpStatusCode.BadRequest);
            errorResponse.AddError("Invalid email or password.");

            return BadRequest(errorResponse);
        }

        [Authorize]
        public async Task<IActionResult> GetUser()
        {
            var email = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;

            if (email != null)
            {
                var user = await _userManager.FindByEmailAsync(email);

                if (user != null)
                {
                    var response = _mapper.Map<UserResponse>(user, opt => opt.AddUrlHelper(Url));

                    return Ok(response);
                }
            }

            return new ApiResponse(HttpStatusCode.BadRequest).ToActionResult();
        }

        [HttpPost("register")]
        public async Task<IActionResult> CreateUser([FromBody]RegisterRequest model)
        {
            if (!ModelState.IsValid)
                return BadRequest(_mapper.Map<ApiResponse>(ModelState));

            var user = await _userManager.FindByEmailAsync(model.Email);

            if (user != null)
            {
                var errorResponse = new ApiResponse(HttpStatusCode.BadRequest);
                errorResponse.AddError("Email is already taken.");

                return BadRequest(errorResponse);
            }

            user = new User
            {
                UserName = model.Email,
                Email = model.Email,
                CreateDate = DateTime.Now,
                UpdateDate = DateTime.Now,
            };

            var result = await _userManager.CreateAsync(user, model.Password);

            if (result.Succeeded)
            {
                result = await _userManager.AddToRoleAsync(user, RoleName.User);

                if (result.Succeeded)
                {
                    //var token = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                    //var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = token }, HttpContext.Request.Scheme);

                    //await _emailSender.SendEmailAsync(model.Email, "Confirm your account",
                    //    $"Please confirm your account by clicking this link: <a href='{callbackUrl}'>link</a>");


                    // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=532713

                    //var cart = new Cart
                    //{
                    //    Id = Guid.NewGuid().ToString("N"),
                    //    UserId = user.Id
                    //};

                    //var cartResult = await _cartService.CreateCartAsync(cart);

                    //if (cartResult.IsSuccessful())
                    //{
                    var roles = await _userManager.GetRolesAsync(user);

                    // TODO:

                    // Check do we really need user id in token as claim.
                    // Remove Expiration and use JS library to check expiration on client.
                    var response = new RegisterResponse
                    {
                        Token = await _jwtFactory.GenerateEncodedToken(user.Email, roles.ToList()),
                        Expiration = JwtIssuerOptions.ToUnixEpochDate(_jwtOptions.Expiration)
                    };

                    return Ok(response);
                    //}
                }
            }

            AddErrorsToModelState(result);

            return BadRequest(_mapper.Map<ApiResponse>(ModelState));
        }

        [HttpPost("recover")]
        public async Task<IActionResult> ForgotPassword([FromBody]RecoverPassowrdRequest model)
        {
            if (!ModelState.IsValid)
                return BadRequest(_mapper.Map<ApiResponse>(ModelState));

            var user = await _userManager.FindByEmailAsync(model.Email);

            if (user == null)
            {
                var errorResponse = new ApiResponse(HttpStatusCode.BadRequest);
                errorResponse.AddError("User with provided email is not found.");

                return BadRequest(errorResponse);
            }

            var token = await _userManager.GeneratePasswordResetTokenAsync(user);

            var tokenBase64string = Base64UrlEncoder.Encode(token);

            var callbackUrl = $"{HttpContext.Request.Scheme}://{HttpContext.Request.Host}/reset-password/{tokenBase64string}";

            await _emailSender.SendEmailAsync(model.Email, "Reset Password",
               $"Please reset your password by clicking here: <a href='{callbackUrl}'>link</a>");

            return Ok(new ApiResponse());
        }

        [HttpPost("reset")]
        public async Task<IActionResult> ResetPassword([FromBody]ChangePasswordRequest model)
        {
            if (!ModelState.IsValid)
                return BadRequest(_mapper.Map<ApiResponse>(ModelState));

            var user = await _userManager.FindByEmailAsync(model.Email);

            if (user == null)
            {
                var errorResponse = new ApiResponse(HttpStatusCode.BadRequest);
                errorResponse.AddError("User with provided email is not found.");

                return BadRequest(errorResponse);
            }

            var token = Base64UrlEncoder.Decode(model.Token);

            var result = await _userManager.ResetPasswordAsync(user, token, model.Password);

            if (result.Succeeded)
                return Ok(new ApiResponse());

            AddErrorsToModelState(result);

            return BadRequest(_mapper.Map<ApiResponse>(ModelState));
        }

        [HttpPost("validate-email")]
        public async Task<IActionResult> ValidateEmail([FromBody] ValidateUniqueEmailRequest model)
        {
            var isUnique = false;

            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByEmailAsync(model.Email);

                if (user == null)
                    isUnique = true;

            }

            return Ok(isUnique);
        }

        private void AddErrorsToModelState(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error.Description);
            }
        }
    }
}