﻿using AutoMapper;
using Marka.BusinessLogic.Interfaces.Orders;
using Marka.Common.Models.Account.Users;
using Marka.Common.Responses;
using Marka.Common.Responses.Base;
using Marka.Common.Responses.Orders;
using Marka.Common.Results;
using Marka.Web.Constants;
using Marka.Web.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Marka.Web.Controllers.Account
{
    [Authorize]
    [Route("api/account/order")]
    public class AccountOrderController : Controller
    {
        private readonly IMapper _mapper;

        private readonly UserManager<User> _userManager;
        private readonly IOrderService _orderService;

        public AccountOrderController(IMapper mapper, UserManager<User> userManager, IOrderService orderService)
        {
            _mapper = mapper;
            _userManager = userManager;
            _orderService = orderService;
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetUserOrder(int id)
        {
            var email = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;

            if (email != null)
            {
                var user = await _userManager.FindByEmailAsync(email);

                if (user != null)
                {
                    var result = await _orderService.GetUserTranslatedOrderAsync(id, user.Id, LanguageConstants.LanguageCodes.English);

                    if (result.IsSuccessful())
                    {
                        var response = _mapper.Map<TranslatedOrderResponse>(result.Value, opt => opt.AddUrlHelper(Url));

                        return Ok(response);
                    }
                }
            }

            return new ApiResponse(HttpStatusCode.BadRequest).ToActionResult();
        }


        [HttpGet]
        public async Task<IActionResult> GetUserOrders()
        {
            var email = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;

            if (email != null)
            {
                var user = await _userManager.FindByEmailAsync(email);
                if (user != null)
                {
                    var result = await _orderService.GetUserOrdersAsync(user.Id);

                    if (result.IsSuccessful())
                    {
                        var response = new OrdersResponse
                        {
                            Embedded = _mapper.Map<IEnumerable<OrderResponse>>(result.Value, opt => opt.AddUrlHelper(Url)),
                            Count = result.Value.Count(),
                            Total = result.Value.Count(),
                            Links = new HalLinks
                            {
                                Self = new HalLink(Url.Action("GetInventories"))
                            }
                        };

                        return Ok(response);
                    }
                }
            }

            return new ApiResponse(HttpStatusCode.BadRequest).ToActionResult();
        }
    }
}