﻿using Marka.DataLayer.Interfaces.Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Marka.DataLayer.Repositories
{
    public abstract class BaseRepository<T> where T : class
    {
        protected IDataContext DataContext;
        protected readonly DbSet<T> Dbset;

        protected BaseRepository(IDataContext dataContext)
        {
            this.DataContext = dataContext;
            Dbset = dataContext.Set<T>();
        }

        public virtual async Task AddAsync(T entity)
        {
            await Dbset.AddAsync(entity);
            await DataContext.SaveChangesAsync();
        }

        public virtual async Task AddRangeAsync(IEnumerable<T> entities)
        {
            await Dbset.AddRangeAsync(entities);
            await DataContext.SaveChangesAsync();
        }

        public virtual async Task UpdateAsync(T entity)
        {
            //Dbset.Attach(entity);
            Dbset.Update(entity);
            //DataContext.Entry(entity).State = EntityState.Modified;
            await DataContext.SaveChangesAsync();
        }

        public virtual async Task UpdateManyAsync(IEnumerable<T> entities)
        {
            foreach (var entity in entities)
            {
                Dbset.Update(entity);
            }

            await DataContext.SaveChangesAsync();
        }

        public virtual async Task DeleteAsync(T entity)
        {
            Dbset.Remove(entity);

            await DataContext.SaveChangesAsync();
        }

        public virtual async Task DeleteAsync(Expression<Func<T, bool>> where)
        {
            IEnumerable<T> objects = Dbset.Where<T>(where).AsEnumerable();

            foreach (T obj in objects)
                Dbset.Remove(obj);

            await DataContext.SaveChangesAsync();
        }

        public virtual async Task<T> GetByIdAsync(int id)
        {
            return await Dbset.FindAsync(id);
        }

        public virtual async Task<T> GetByIdAsync(string id)
        {
            return await Dbset.FindAsync(id);
        }

        public virtual async Task<T> GetFirstAsync(Expression<Func<T, bool>> where)
        {
            return await Dbset.FirstOrDefaultAsync(where);
        }

        public virtual async Task<IEnumerable<T>> GetManyAsync(Expression<Func<T, bool>> where)
        {
            return await Dbset.Where(where).ToListAsync();
        }

        public virtual async Task<IEnumerable<T>> GetAllAsync()
        {
            return await Dbset.ToListAsync();
        }
    }
}
