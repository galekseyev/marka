﻿using Marka.Common.Models.Catalog.InventoryGroups;
using Marka.Common.Models.Filters;
using Marka.DataLayer.Interfaces.Domain;
using Marka.DataLayer.Interfaces.Repositories.Catalog.InventoryGroups;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Marka.DataLayer.Repositories.Catalog.InventoryGroups
{
    public class InventoryGroupRepository : BaseRepository<InventoryGroup>, IInventoryGroupRepository
    {
        public InventoryGroupRepository(IDataContext dataContext)
         : base(dataContext) { }

        public async Task<InventoryGroup> GetInventoryGroupAsync(int id)
        {
            return await DataContext.InventoryGroups
                .Where(c => c.Id == id)
                .Include(c => c.Picture)
                .FirstOrDefaultAsync();
        }

        public async Task<FilteredInventoryGroups> GetInventoryGroupsAsync(InventoryGroupFilter filter)
        {
            if (filter == null)
                throw new ArgumentNullException(nameof(filter));

            var query = DataContext.InventoryGroups
                .Where(c =>
                    (filter.IncludeDisabled || !c.Disabled) &&
                    c.InventoryGroupInventories.Any(x =>
                        (filter.IncludeDisabled || !x.Inventory.Disabled) &&
                        (filter.IncludeEmptyGroups || x.Inventory.Quantity > 0)))
                .OrderBy(c => c.DisplayOrder)
                .Include(c => c.Picture)
                .AsQueryable();

            var total = query.Count();

            return new FilteredInventoryGroups
            {
                Total = total,
                InventoryGroups = await query.ToListAsync()
            };
        }

        public async Task<TranslatedInventoryGroup> GetTranslatedInventoryGroup(int id, string langCode)
        {
            var translated = await DataContext.InventoryGroups
                .Include(c => c.Picture)
                .Include(c => c.InventoryGroupInventories)
                .ThenInclude(c => c.Inventory)
                .ThenInclude(c => c.Pictures)
                .Include(c => c.InventoryGroupInventories)
                .ThenInclude(c => c.Inventory)
                .ThenInclude(c => c.Translations)
                .ThenInclude(c => c.Language)
                .Where(c => c.Id == id && !c.Disabled)
                .Select(c => new
                {
                    InventoryGroup = c,
                    InventoryGroupsInventories = c.InventoryGroupInventories
                        .Where(x => !x.Disabled && !x.Inventory.Disabled && x.Inventory.Quantity > 0)
                        .OrderBy(x => x.DisplayOrder)
                        .Select(x => new
                        {
                            InventoryGroupInventory = x,
                            InventoryTranslation = x.Inventory.Translations.FirstOrDefault(o => o.Language.LanguageCode == langCode)
                        })
                })
                .FirstOrDefaultAsync();

            if (translated == null)
                return null;

            return new TranslatedInventoryGroup
            {
                Id = translated.InventoryGroup.Id,
                Disabled = translated.InventoryGroup.Disabled,
                DisplayOrder = translated.InventoryGroup.DisplayOrder,
                Picture = translated.InventoryGroup.Picture,
                Inventories = translated.InventoryGroupsInventories
                    .Where(x => !x.InventoryGroupInventory.Disabled)
                    .Select(x => new TranslatedInventoryGroupInventory
                    {
                        Id = x.InventoryGroupInventory.Id,
                        Disabled = x.InventoryGroupInventory.Disabled,
                        DisplayOrder = x.InventoryGroupInventory.DisplayOrder,
                        InventoryId = x.InventoryGroupInventory.InventoryId,
                        Title = x.InventoryTranslation.Title,
                        Picture = x.InventoryGroupInventory.Inventory.Pictures.FirstOrDefault()
                    }).ToList()
            };
        }
    }
}