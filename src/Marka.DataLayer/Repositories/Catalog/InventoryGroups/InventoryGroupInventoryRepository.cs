﻿using Marka.Common.Models.Catalog.InventoryGroups;
using Marka.DataLayer.Interfaces.Domain;
using Marka.DataLayer.Interfaces.Repositories.Catalog.InventoryGroups;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Marka.DataLayer.Repositories.Catalog.InventoryGroups
{
    public class InventoryGroupInventoryRepository : BaseRepository<InventoryGroupInventory>, IInventoryGroupInventoryRepository
    {
        public InventoryGroupInventoryRepository(IDataContext dataContext)
        : base(dataContext) { }

        public async Task<IEnumerable<TranslatedInventoryGroupInventory>> GetTranslatedInventoryGroupInventoriesAsync(int id, string langCode)
        {
            var translated = DataContext.InventoryGroupInventories
                .Include(c => c.Inventory)
                .ThenInclude(c => c.Translations)
                .ThenInclude(c => c.Language)
                .Include(c => c.Inventory)
                .ThenInclude(c => c.Pictures)
                .Where(c => c.InventoryGroupId == id)
                .Select(c => new
                {
                    Value = c,
                    InventoryTranslation = c.Inventory.Translations.FirstOrDefault(x => x.Language.LanguageCode == langCode)
                });

            return await translated.Select(c => new TranslatedInventoryGroupInventory
            {
                Id = c.Value.Id,
                Title = c.InventoryTranslation.Title,
                Disabled = c.Value.Disabled,
                DisplayOrder = c.Value.DisplayOrder,
                Picture = c.Value.Inventory.Pictures.FirstOrDefault()
            }).ToListAsync();
        }
    }
}