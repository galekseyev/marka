﻿using Marka.Common.Models.Catalog.InventoryGroups;
using Marka.DataLayer.Interfaces.Domain;
using Marka.DataLayer.Interfaces.Repositories.Catalog.InventoryGroups;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Marka.DataLayer.Repositories.Catalog.InventoryGroups
{
    public class InventoryGroupPictureRepository : BaseRepository<InventoryGroupPicture>, IInventoryGroupPictureRepository
    {
        public InventoryGroupPictureRepository(IDataContext dataContext)
      : base(dataContext) { }
    }
}