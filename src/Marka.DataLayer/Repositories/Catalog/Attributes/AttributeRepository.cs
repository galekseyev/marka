﻿using Marka.Common.Models.Catalog.Attributes;
using Marka.DataLayer.Interfaces.Domain;
using Marka.DataLayer.Interfaces.Repositories.Catalog.Attributes;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Marka.DataLayer.Repositories.Catalog.Attributes
{
    public class AttributeRepository : BaseRepository<Attribute>, IAttributeRepository
    {
        public AttributeRepository(IDataContext dataContext)
            : base(dataContext) { }

        public async Task<Attribute> GetAttributeAsync(int id)
        {
            return await DataContext.Attributes
                .Include(c => c.Translations)
                .FirstOrDefaultAsync(c => c.Id == id);
        }

        public async Task<IEnumerable<Attribute>> GetAttributesAsync()
        {
            return await DataContext.Attributes
                .Include(c => c.Translations)
                .ToListAsync();
        }

        public async Task<TranslatedAttribute> GetTranslatedAttributeAsync(int id, string langCode)
        {
            var translated = await DataContext.Attributes
                .Include(c => c.Translations)
                .ThenInclude(c => c.Language)
                .Where(c => c.Id == id)
                .Select(c => new
                {
                    Attribute = c,
                    Translation = c.Translations.FirstOrDefault(x => x.Language.LanguageCode == langCode)
                }).FirstOrDefaultAsync();

            if (translated == null)
                return null;

            return new TranslatedAttribute
            {
                Id = translated.Attribute.Id,
                Title = translated.Translation.Title,
                Description = translated.Translation.Description,
            };
        }

        public async Task<IEnumerable<TranslatedAttribute>> GetTranslatedAttributesAsync(string langCode)
        {
            var translatedList = DataContext.Attributes
                .Include(c => c.Translations)
                .ThenInclude(c => c.Language)
                .Select(c => new
                {
                    Attribute = c,
                    Translation = c.Translations.FirstOrDefault(x => x.Language.LanguageCode == langCode)
                });

            return await translatedList.Select(c => new TranslatedAttribute
            {
                Id = c.Attribute.Id,
                Title = c.Translation.Title,
                Description = c.Translation.Description,
            }).ToListAsync();
        }
    }
}