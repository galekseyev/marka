﻿using Marka.Common.Models.Catalog.Inventories;
using Marka.DataLayer.Interfaces.Domain;
using Marka.DataLayer.Interfaces.Repositories.Catalog.Inventories;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Marka.DataLayer.Repositories.Catalog.Inventories
{
    public class InventoryPictureRepository : BaseRepository<InventoryPicture>, IInventoryPictureRepository
    {
        public InventoryPictureRepository(IDataContext dataContext)
         : base(dataContext) { }

        public async Task<IEnumerable<InventoryPicture>> GetInventoryPicturesAsync(int id)
        {
            return await DataContext.InventoryPictures
                .Where(c=>c.InventoryId == id)
                .ToListAsync();
        }
    }
}