﻿using Marka.Common.Models.Catalog.Attributes;
using Marka.Common.Models.Catalog.Inventories;
using Marka.DataLayer.Interfaces.Domain;
using Marka.DataLayer.Interfaces.Repositories.Catalog.Inventories;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Marka.DataLayer.Repositories.Catalog.Inventories
{
    public class InventoryAttributeRepository : BaseRepository<InventoryAttribute>, IInventoryAttributeRepository
    {
        public InventoryAttributeRepository(IDataContext dataContext)
         : base(dataContext) { }

        public async Task<IEnumerable<InventoryAttribute>> GetInventoryAttributesAsync(long id)
        {
            return await DataContext.InventoryAttributes
                .Where(c => c.InventoryId == id)
                .Include(c => c.Attribute)
                .ToListAsync();
        }

        public async Task<InventoryAttribute> GetInventoryAttributeAsync(int id)
        {
            return await DataContext.InventoryAttributes
                .Where(c => c.Id == id)
                .Include(c => c.Values)
                .ThenInclude(x => x.Picture)
                .FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<TranslatedInventoryAttribute>> GetTranslatedInventoryAttributesAsync(int id, string langCode)
        {
            var translatedList = DataContext.InventoryAttributes
                .Include(c => c.Attribute)
                .ThenInclude(c => c.Translations)
                .ThenInclude(c => c.Language)
                .Where(c => c.InventoryId == id)
                .Select(c => new
                {
                    InventoryAttribute = c,
                    AttributeTranslation = c.Attribute.Translations.FirstOrDefault(x => x.Language.LanguageCode == langCode)
                });

            return await translatedList.Select(c => new TranslatedInventoryAttribute
            {
                Id = c.InventoryAttribute.Id,
                IsRequired = c.InventoryAttribute.IsRequired,
                Disabled = c.InventoryAttribute.Disabled,
                ControlType = c.InventoryAttribute.ControlType,
                DisplayOrder = c.InventoryAttribute.DisplayOrder,
                Attribute = new TranslatedAttribute
                {
                    Id = c.AttributeTranslation.AttributeId,
                    Title = c.AttributeTranslation.Title,
                    Description = c.AttributeTranslation.Description
                }
            }).ToListAsync();
        }
    }
}