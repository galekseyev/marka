﻿using Marka.Common.Models.Catalog.Inventories;
using Marka.DataLayer.Interfaces.Domain;
using Marka.DataLayer.Interfaces.Repositories.Catalog.Inventories;
using Microsoft.EntityFrameworkCore;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Marka.DataLayer.Repositories.Catalog.Inventories
{
    public class InventoryAttributeValueRepository : BaseRepository<InventoryAttributeValue>, IInventoryAttributeValueRepository
    {
        public InventoryAttributeValueRepository(IDataContext dataContext)
         : base(dataContext) { }

        public async Task<IEnumerable<InventoryAttributeValue>> GetInventoryAttributeValuesAsync(int id, int attributeId)
        {
            return await DataContext.InventoryAttributeValues
                .Where(c => c.InventoryAttribute.InventoryId == id && c.InventoryAttributeId == attributeId)
                .Include(c => c.Picture)
                .Include(c => c.Translations)
                .ToListAsync();
        }

        public async Task<IEnumerable<TranslatedInventoryAttributeValue>> GetTranslatedInventoryAttributeValuesAsync(int id, int attributeId, string langCode)
        {
            var translated = DataContext.InventoryAttributeValues
                .Include(c => c.Picture)
                .Include(c => c.Translations)
                .ThenInclude(c => c.Language)
                .Where(c => c.InventoryAttribute.InventoryId == id && c.InventoryAttributeId == attributeId)
                .Select(c => new
                {
                    Value = c,
                    Translation = c.Translations.FirstOrDefault(x => x.Language.LanguageCode == langCode)
                });

            return await translated.Select(c => new TranslatedInventoryAttributeValue
            {
                Id = c.Value.Id,
                Title = c.Translation.Title,
                Disabled = c.Value.Disabled,
                DisplayOrder = c.Value.DisplayOrder,
                Picture = c.Value.Picture
            }).ToListAsync();
        }
    }
}