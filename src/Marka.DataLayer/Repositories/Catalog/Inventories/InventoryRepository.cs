﻿
using Marka.Common.Models.Catalog.Attributes;
using Marka.Common.Models.Catalog.Inventories;
using Marka.Common.Models.Catalog.Specifications;
using Marka.Common.Models.Filters;
using Marka.Common.Models.Orders;
using Marka.DataLayer.Interfaces.Domain;
using Marka.DataLayer.Interfaces.Repositories.Catalog.Inventories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Marka.DataLayer.Repositories.Catalog.Inventories
{
    public class InventoryRepository : BaseRepository<Inventory>, IInventoryRepository
    {
        public InventoryRepository(IDataContext dataContext)
         : base(dataContext) { }


        public async Task<Inventory> GetInventoryAsync(int id)
        {
            return await DataContext.Inventories
                .Where(c => c.Id == id)
                .Include(c => c.Translations)
                .FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<Inventory>> GetInventoriesAsync()
        {
            return await DataContext.Inventories
                .Include(c => c.Translations)
                .ToListAsync();
        }

        public async Task<TranslatedInventory> GetTranslatedInventoryAsync(int id, string langCode)
        {
            var translated = await DataContext.Inventories
                .Where(c => c.Id == id && !c.Disabled)
                .Include(c => c.Translations)
                .ThenInclude(c => c.Language)
                .Include(c => c.Pictures)
                .Include(c => c.Attributes)
                .ThenInclude(x => x.Attribute)
                .Include(c => c.Attributes)
                .ThenInclude(x => x.Values)
                .Include(c => c.Specifications)
                .ThenInclude(c => c.Specification)
                .Include(c => c.Specifications)
                .ThenInclude(c => c.SpecificationValue)
                .Select(c => new
                {
                    Inventory = c,
                    Translation = c.Translations.FirstOrDefault(x => x.Language.LanguageCode == langCode),
                    InventoryAttributes = c.Attributes
                        .Where(x => !x.Disabled)
                        .OrderBy(x => x.DisplayOrder)
                        .Select(x => new
                        {
                            InventoryAttribute = x,
                            Attribute = x.Attribute,
                            AttributeTranslation = x.Attribute.Translations.FirstOrDefault(m => m.Language.LanguageCode == langCode),
                            AttributeValues = x.Values
                                .Where(m => !m.Disabled)
                                .OrderBy(m => m.DisplayOrder)
                                .Select(m => new
                                {
                                    AttributeValue = m,
                                    AttributeValueTranslation = m.Translations.FirstOrDefault(n => n.Language.LanguageCode == langCode)
                                })
                        }),

                    InventorySpecifications = c.Specifications
                        .Where(x => !x.Disabled && !x.Hidden)
                        .OrderBy(x => x.DisplayOrder)
                        .Select(x => new
                        {
                            InventorySpecification = x,
                            SpecificationTranslation = x.Specification.Translations.FirstOrDefault(m => m.Language.LanguageCode == langCode),
                            SpecificationValueTranslation = x.SpecificationValue.Translations.FirstOrDefault(m => m.Language.LanguageCode == langCode)
                        })
                }).FirstOrDefaultAsync();

            if (translated == null)
                return null;

            return new TranslatedInventory
            {
                Id = translated.Inventory.Id,
                Disabled = translated.Inventory.Disabled,
                Title = translated.Translation.Title,
                Description = translated.Translation.Description,
                OriginalPrice = translated.Inventory.OriginalPrice,
                Price = translated.Inventory.Price,
                Quantity = translated.Inventory.Quantity,
                Pictures = translated.Inventory.Pictures,
                Attributes = translated.InventoryAttributes.Select(c => new TranslatedInventoryAttribute
                {
                    Id = c.InventoryAttribute.Id,
                    IsRequired = c.InventoryAttribute.IsRequired,
                    Disabled = c.InventoryAttribute.Disabled,
                    ControlType = c.InventoryAttribute.ControlType,
                    DisplayOrder = c.InventoryAttribute.DisplayOrder,
                    Attribute = new TranslatedAttribute
                    {
                        Id = c.Attribute.Id,
                        Title = c.AttributeTranslation.Title,
                        Description = c.AttributeTranslation.Description
                    },
                    Values = c.AttributeValues.Select(x => new TranslatedInventoryAttributeValue
                    {
                        Id = x.AttributeValue.Id,
                        Disabled = x.AttributeValue.Disabled,
                        DisplayOrder = x.AttributeValue.DisplayOrder,
                        Picture = x.AttributeValue.Picture,
                        Title = x.AttributeValueTranslation.Title
                    }).ToList()

                }).ToList(),
                Specifications = translated.InventorySpecifications.Select(c => new TranslatedInventorySpecification
                {
                    Id = c.InventorySpecification.Id,
                    Hidden = c.InventorySpecification.Hidden,
                    Disabled = c.InventorySpecification.Disabled,
                    DisplayOrder = c.InventorySpecification.DisplayOrder,
                    Specification = new TranslatedSpecification
                    {
                        Id = c.InventorySpecification.Specification.Id,
                        Title = c.SpecificationTranslation.Title,
                        Description = c.SpecificationTranslation.Description
                    },
                    SpecificationValue = new TranslatedSpecificationValue
                    {
                        Id = c.InventorySpecification.SpecificationValue.Id,
                        DisplayOrder = c.InventorySpecification.SpecificationValue.DisplayOrder,
                        Disabled = c.InventorySpecification.SpecificationValue.Disabled,
                        Title = c.SpecificationValueTranslation.Title,
                    }
                }).ToList()
            };
        }

        public async Task<TranslatedInventories> GetTranslatedInventoriesAsync(InventoryFilter filter, string langCode)
        {
            if (filter == null)
                throw new ArgumentNullException(nameof(filter));

            var query = DataContext.Inventories
                .Where(c =>
                    (filter.IncludeDisabled || !c.Disabled) &&
                    (filter.IncludeOutOfStock || c.Quantity > 0))
                .Include(c => c.Translations)
                .ThenInclude(c => c.Language)
                .Include(c => c.Pictures)
                .Include(c => c.Specifications)
                .AsQueryable();

            if (filter.CategoryId.HasValue)
                query = query.Where(c => c.CategoryId == filter.CategoryId.Value);

            if (!string.IsNullOrEmpty(filter.SpecificationValueIds))
            {
                var ids = filter.SpecificationValueIds.Split(',').Select(c => int.Parse(c));

                query = query.Where(c => c.Specifications.Any(x => ids.Contains(x.SpecificationValueId)));
            }

            var total = query.Count();

            if (filter.Skip.HasValue)
                query = query.Skip(filter.Skip.Value);

            if (filter.Take.HasValue)
                query = query.Take(filter.Take.Value);

            var translatedList = query.Select(c => new
            {
                Inventory = c,
                Translation = c.Translations.FirstOrDefault(x => x.Language.LanguageCode == langCode)
            });

            var totalInventories = new TranslatedInventories
            {
                Total = total,
                Inventories = await translatedList.Select(c => new TranslatedInventory
                {
                    Id = c.Inventory.Id,
                    Title = c.Translation.Title,
                    Description = c.Translation.Description,
                    OriginalPrice = c.Inventory.OriginalPrice,
                    Price = c.Inventory.Price,
                    Quantity = c.Inventory.Quantity,
                    Pictures = c.Inventory.Pictures
                }).ToListAsync()
            };

            return totalInventories;
        }
    }
}