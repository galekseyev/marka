﻿using Marka.Common.Models.Catalog.Inventories;
using Marka.Common.Models.Catalog.Specifications;
using Marka.DataLayer.Interfaces.Domain;
using Marka.DataLayer.Interfaces.Repositories.Catalog.Inventories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Marka.DataLayer.Repositories.Catalog.Inventories
{
    public class InventorySpecificationRepository : BaseRepository<InventorySpecification>, IInventorySpecificationRepository
    {
        public InventorySpecificationRepository(IDataContext dataContext)
            : base(dataContext) { }

        public async Task<IEnumerable<TranslatedInventorySpecification>> GetTranslatedInventorySpecificationsAsync(int id, string langCode)
        {
            var translated = DataContext.InventorySpecifications
                .Include(c => c.Specification)
                .ThenInclude(c => c.Translations)
                .ThenInclude(c => c.Language)
                .Include(c => c.SpecificationValue)
                .ThenInclude(c => c.Translations)
                .ThenInclude(c => c.Language)
                .Where(c => c.InventoryId == id)
                .Select(c => new
                {
                    InventorySpecification = c,
                    SpecificationTranslation = c.Specification.Translations.FirstOrDefault(x => x.Language.LanguageCode == langCode),
                    SpecificationValueTranslation = c.SpecificationValue.Translations.FirstOrDefault(x => x.Language.LanguageCode == langCode)
                });

            return await translated.Select(c => new TranslatedInventorySpecification
            {
                Id = c.InventorySpecification.Id,
                Disabled = c.InventorySpecification.Disabled,
                DisplayOrder = c.InventorySpecification.DisplayOrder,
                Hidden = c.InventorySpecification.Hidden,
                Specification = new TranslatedSpecification
                {
                    Id = c.InventorySpecification.Specification.Id,
                    Title = c.SpecificationTranslation.Title,
                    Description = c.SpecificationTranslation.Description,
                },
                SpecificationValue = new TranslatedSpecificationValue
                {
                    Id = c.SpecificationValueTranslation.SpecificationValueId,
                    Title = c.SpecificationValueTranslation.Title,
                    Disabled = c.InventorySpecification.SpecificationValue.Disabled,
                    DisplayOrder = c.InventorySpecification.SpecificationValue.DisplayOrder
                }
            }).ToListAsync();
        }
    }
}
