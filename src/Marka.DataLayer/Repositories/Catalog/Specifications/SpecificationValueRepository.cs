﻿using Marka.Common.Models.Catalog.Specifications;
using Marka.DataLayer.Interfaces.Domain;
using Marka.DataLayer.Interfaces.Repositories.Catalog.Specifications;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Marka.DataLayer.Repositories.Catalog.Specifications
{
    public class SpecificationValueRepository : BaseRepository<SpecificationValue>, ISpecificationValueRepository
    {
        public SpecificationValueRepository(IDataContext dataContext)
        : base(dataContext) { }

        public async Task<IEnumerable<SpecificationValue>> GetSpecificationValuesAsync(int id)
        {
            return await DataContext.SpecificationValues
                .Where(c => c.SpecificationId == id)
                .Include(c => c.Translations)
                .ToListAsync();
        }

        public async Task<IEnumerable<TranslatedSpecificationValue>> GetTranslatedSpecificationValuesAsync(int id, string langCode)
        {
            var translated = DataContext.SpecificationValues
                .Include(c => c.Translations)
                .ThenInclude(c => c.Language)
                .Where(c => c.SpecificationId == id)
                .Select(c => new
                {
                    Value = c,
                    Translation = c.Translations.FirstOrDefault(x => x.Language.LanguageCode == langCode)
                });

            return await translated.Select(c => new TranslatedSpecificationValue
            {
                Id = c.Value.Id,
                Title = c.Translation.Title,
                Disabled = c.Value.Disabled,
                DisplayOrder = c.Value.DisplayOrder,
            }).ToListAsync();
        }
    }
}