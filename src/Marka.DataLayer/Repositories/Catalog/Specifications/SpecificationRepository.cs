﻿using Marka.Common.Models.Catalog.Specifications;
using Marka.DataLayer.Interfaces.Domain;
using Marka.DataLayer.Interfaces.Repositories.Catalog.Specifications;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Marka.DataLayer.Repositories.Catalog.Specifications
{
    public class SpecificationRepository : BaseRepository<Specification>, ISpecificationRepository
    {
        public SpecificationRepository(IDataContext dataContext)
            : base(dataContext) { }

        public async Task<Specification> GetSpecificationAsync(int id)
        {
            return await DataContext.Specifications
                .Include(c => c.Translations)
                .FirstOrDefaultAsync(c => c.Id == id);
        }

        public async Task<IEnumerable<Specification>> GetSpecificationsAsync()
        {
            return await DataContext.Specifications
                .Include(c => c.Translations)
                .ToListAsync();
        }

        public async Task<TranslatedSpecification> GetTranslatedSpecificationAsync(int id, string langCode)
        {
            var translated = await DataContext.Specifications
                .Include(c => c.Translations)
                .ThenInclude(c => c.Language)
                .Where(c => c.Id == id)
                .Select(c => new
                {
                    Specification = c,
                    Translation = c.Translations.FirstOrDefault(x => x.Language.LanguageCode == langCode)
                }).FirstOrDefaultAsync();

            if (translated == null)
                return null;

            return new TranslatedSpecification
            {
                Id = translated.Specification.Id,
                Title = translated.Translation.Title,
                Description = translated.Translation.Description,
            };
        }

        public async Task<IEnumerable<TranslatedSpecification>> GetTranslatedSpecificationsAsync(string langCode)
        {
            var translatedList = DataContext.Specifications
                .Include(c => c.Translations)
                .ThenInclude(c => c.Language)
                .Include(c => c.Values)
                .ThenInclude(c => c.Translations)
                .ThenInclude(c => c.Language)
                .Select(c => new
                {
                    Specification = c,
                    Translation = c.Translations.FirstOrDefault(x => x.Language.LanguageCode == langCode),
                    Values = c.Values
                        .Where(x => !x.Disabled)
                        .OrderBy(x => x.DisplayOrder)
                        .Select(x => new
                        {
                            Value = x,
                            ValueTranslation = x.Translations.FirstOrDefault(m => m.Language.LanguageCode == langCode)
                        })
                });

            return await translatedList.Select(c => new TranslatedSpecification
            {
                Id = c.Specification.Id,
                Title = c.Translation.Title,
                Description = c.Translation.Description,
                Values = c.Values.Select(x => new TranslatedSpecificationValue
                {
                    Id = x.Value.Id,
                    DisplayOrder = x.Value.DisplayOrder,
                    Disabled = x.Value.Disabled,
                    Title = x.ValueTranslation.Title
                }).ToList()
            }).ToListAsync();
        }
    }
}
