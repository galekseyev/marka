﻿using Marka.Common.Models.Catalog.Categories;
using Marka.DataLayer.Interfaces.Domain;
using Marka.DataLayer.Interfaces.Repositories.Catalog.Categories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Marka.DataLayer.Repositories.Catalog.Categories
{
    public class CategoryPictureRepository : BaseRepository<CategoryPicture>, ICategoryPictureRepository
    {
        public CategoryPictureRepository(IDataContext dataContext)
         : base(dataContext) { }

        public async Task<CategoryPicture> GetCategoryPictureAsync(int id)
        {
            return await DataContext.CategoryPictures
                .FirstOrDefaultAsync(c => c.Category.Id == id);
        }
    }
}
