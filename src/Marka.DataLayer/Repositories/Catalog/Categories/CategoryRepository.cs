﻿using Marka.Common.Models.Catalog.Categories;
using Marka.DataLayer.Interfaces.Domain;
using Marka.DataLayer.Interfaces.Repositories.Catalog.Categories;
using Microsoft.EntityFrameworkCore;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Marka.DataLayer.Repositories.Catalog.Categories
{
    public class CategoryRepository : BaseRepository<Category>, ICategoryRepository
    {
        public CategoryRepository(IDataContext dataContext)
            : base(dataContext) { }

        public async Task<Category> GetCategoryAsync(int id)
        {
            return await DataContext.Categories
                .Include(c => c.Translations)
                .FirstOrDefaultAsync(c => c.Id == id);
        }

        public async Task<IEnumerable<Category>> GetCategoriesAsync()
        {
            return await DataContext.Categories
                .Include(c => c.Translations)
                .ToListAsync();
        }

        public async Task<TranslatedCategory> GetTranslatedCategoryAsync(int id, string langCode)
        {
            var translated = await DataContext.Categories
                .Include(c => c.Translations)
                .ThenInclude(c => c.Language)
                .Where(c => c.Id == id)
                .Select(c => new
                {
                    Category = c,
                    Translation = c.Translations.FirstOrDefault(x => x.Language.LanguageCode == langCode)
                }).FirstOrDefaultAsync();

            if (translated == null)
                return null;

            return new TranslatedCategory
            {
                Id = translated.Category.Id,
                Title = translated.Translation.Title,
                Description = translated.Translation.Description,
            };
        }

        public async Task<IEnumerable<TranslatedCategory>> GetTranslatedCategoriesAsync(string langCode)
        {
            var translatedList = DataContext.Categories
                .Include(c => c.Translations)
                .ThenInclude(c => c.Language)
                .Select(c => new
                {
                    Category = c,
                    Translation = c.Translations.FirstOrDefault(x => x.Language.LanguageCode == langCode)
                });

            return await translatedList.Select(c => new TranslatedCategory
            {
                Id = c.Category.Id,
                Title = c.Translation.Title,
                Description = c.Translation.Description,
            }).ToListAsync();
        }
    }
}