﻿using Marka.Common.Models.Carts;
using Marka.DataLayer.Interfaces.Domain;
using Marka.DataLayer.Interfaces.Repositories.Carts;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace Marka.DataLayer.Repositories.Carts
{
    public class CartRepository : BaseRepository<Cart>, ICartRepository
    {
        public CartRepository(IDataContext dataContext)
            : base(dataContext) { }

        public async Task<Cart> GetCartAsync(string id)
        {
            return await DataContext.Carts
                .Include(c => c.CartItems)
                .ThenInclude(c => c.Inventory)
                .Include(c => c.CartItems)
                .ThenInclude(c => c.CartItemAttributesValuesXref)
                .Where(c => c.Id == id)
                .FirstOrDefaultAsync();
        }

        public async Task<TranslatedCart> GetTranslatedCartAsync(string id, string langCode)
        {
            var translated = await DataContext.Carts
                .Include(c => c.CartItems)
                .ThenInclude(c => c.Inventory)
                .ThenInclude(c => c.Translations)
                .ThenInclude(c => c.Inventory)
                .ThenInclude(c => c.Pictures)
                .Include(c => c.CartItems)
                .ThenInclude(c => c.CartItemAttributesValuesXref)
                .ThenInclude(c => c.InventoryAttribute)
                .ThenInclude(c => c.Attribute)
                .ThenInclude(c => c.Translations)
                .Include(c => c.CartItems)
                .ThenInclude(c => c.CartItemAttributesValuesXref)
                .ThenInclude(c => c.InventoryAttributeValue)
                .ThenInclude(c => c.Translations)
                .Where(c => c.Id == id)
                .Select(c => new
                {
                    Cart = c,
                    CartItems = c.CartItems.Select(x => new
                    {
                        CartItem = x,
                        Inventory = x.Inventory,
                        InventoryTranslation = x.Inventory.Translations.FirstOrDefault(m => m.Language.LanguageCode == langCode),
                        AttributesValuesXref = x.CartItemAttributesValuesXref.Select(m => new
                        {
                            Xref = m,
                            InventoryAttribute = m.InventoryAttribute,
                            AttributeTranslation = m.InventoryAttribute.Attribute.Translations.FirstOrDefault(n => n.Language.LanguageCode == langCode),
                            AttributeValue = m.InventoryAttributeValue,
                            AttributeValueTranslation = m.InventoryAttributeValue.Translations.FirstOrDefault(n => n.Language.LanguageCode == langCode)
                        })
                    })
                })
                .FirstOrDefaultAsync();

            if (translated == null)
                return null;

            return new TranslatedCart
            {
                Id = translated.Cart.Id,
                CreateDate = translated.Cart.CreateDate,
                UpdateDate = translated.Cart.UpdateDate,
                CartItems = translated.CartItems.Select(c => new TranslatedCartItem
                {
                    Id = c.CartItem.Id,
                    Title = c.InventoryTranslation.Title,
                    Description = c.InventoryTranslation.Description,
                    Price = c.Inventory.Price,
                    OriginalPrice = c.Inventory.OriginalPrice,
                    Quantity = c.CartItem.Quantity,
                    InventoryQuantity = c.Inventory.Quantity,
                    Picture = c.Inventory.Pictures.FirstOrDefault(),
                    CartItemAttributesValuesXref = c.AttributesValuesXref.Select(x => new TranslatedCartItemAttributesValuesXref
                    {
                        Id = x.Xref.Id,
                        InventoryAttributeId = x.InventoryAttribute.Id,
                        InventoryAttributeTitle = x.AttributeTranslation.Title,
                        InventoryAttributeValueId = x.AttributeValue.Id,
                        InventoryAttributeValueTitle = x.AttributeValueTranslation.Title

                    }).ToList()

                }).ToList()
            };
        }
    }
}