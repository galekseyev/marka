﻿using Marka.Common.Models.Carts;
using Marka.DataLayer.Interfaces.Domain;
using Marka.DataLayer.Interfaces.Repositories.Carts;

namespace Marka.DataLayer.Repositories.Carts
{
    public class CartShippingRepository : BaseRepository<CartShipping>, ICartShippingRepository
    {
        public CartShippingRepository(IDataContext dataContext)
            : base(dataContext) { }
    }
}