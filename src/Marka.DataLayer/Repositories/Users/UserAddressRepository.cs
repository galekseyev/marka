﻿using Marka.Common.Models.Account.Users;
using Marka.DataLayer.Interfaces.Domain;
using Marka.DataLayer.Interfaces.Repositories.Users;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Marka.DataLayer.Repositories.Users
{
    public class UserAddressRepository : BaseRepository<UserAddress>, IUserAddressRepository
    {
        public UserAddressRepository(IDataContext dataContext)
            : base(dataContext) { }

        public async Task<UserAddress> GetUserAddressAsync(int userId)
        {
            return await DataContext.UserAddresses
                .Where(c => c.UserId == userId)
                .Include(c => c.Country)
                .FirstOrDefaultAsync();
        }
    }
}