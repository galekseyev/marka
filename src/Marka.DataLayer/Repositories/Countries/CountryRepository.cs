﻿using Marka.Common.Models.Countries;
using Marka.DataLayer.Interfaces.Domain;
using Marka.DataLayer.Interfaces.Repositories.Countries;
using System;
using System.Collections.Generic;
using System.Text;

namespace Marka.DataLayer.Repositories.Countries
{
    public class CountryRepository : BaseRepository<Country>, ICountryRepository
    {
        public CountryRepository(IDataContext dataContext)
            : base(dataContext) { }
    }
}