﻿using Marka.Common.Models.Orders;
using Marka.DataLayer.Interfaces.Domain;
using Marka.DataLayer.Interfaces.Repositories.Orders;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Marka.DataLayer.Repositories.Orders
{
    public class OrderRepository : BaseRepository<Order>, IOrderRepository
    {
        public OrderRepository(IDataContext dataContext)
        : base(dataContext) { }

        public async Task<Order> GetOrderAsync(int id)
        {
            return await DataContext.Orders
                .Where(c => c.Id == id)
                .Include(c => c.OrderStatus)
                .Include(c => c.PaymentType)
                .Include(c => c.PaymentStatus)
                .FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<Order>> GetOrdersAsync()
        {
            return await DataContext.Orders
                .Include(c => c.OrderStatus)
                .OrderByDescending(c => c.CreateDate)
                .ToListAsync();
        }

        public async Task<IEnumerable<Order>> GetUserOrdersAsync(int userId)
        {
            return await DataContext.Orders
                .Include(c => c.OrderStatus)
                .Where(c => c.UserId == userId)
                .OrderByDescending(c => c.CreateDate)
                .ToListAsync();
        }

        public async Task<TranslatedOrder> GetTranslatedOrderAsync(int id, string langCode)
        {
            return await GetTranslatedOrderAsync(c => c.Id == id, langCode);
        }

        public async Task<TranslatedOrder> GetUserTranslatedOrderAsync(int id, int userId, string langCode)
        {
            return await GetTranslatedOrderAsync(c => c.Id == id && c.UserId == userId, langCode);
        }

        private async Task<TranslatedOrder> GetTranslatedOrderAsync(Expression<Func<Order, bool>> where, string langCode)
        {
            var translated = await DataContext.Orders
                .Include(c => c.Shipment)
                .ThenInclude(c => c.Country)
                .Include(c => c.OrderStatus)
                .Include(c => c.PaymentType)
                .Include(c => c.PaymentStatus)
                .Include(c => c.OrderItems)
                .ThenInclude(c => c.Inventory)
                .ThenInclude(c => c.Translations)
                .ThenInclude(c => c.Inventory)
                .ThenInclude(c => c.Pictures)
                .Include(c => c.OrderItems)
                .ThenInclude(c => c.OrderItemAttributesValuesXref)
                .ThenInclude(c => c.InventoryAttribute)
                .ThenInclude(c => c.Attribute)
                .ThenInclude(c => c.Translations)
                .Include(c => c.OrderItems)
                .ThenInclude(c => c.OrderItemAttributesValuesXref)
                .ThenInclude(c => c.InventoryAttributeValue)
                .ThenInclude(c => c.Translations)
                .Where(where)
                .Select(c => new
                {
                    Order = c,
                    OrderItems = c.OrderItems.Select(x => new
                    {
                        OrderItem = x,
                        Inventory = x.Inventory,
                        InventoryTranslation = x.Inventory.Translations.FirstOrDefault(m => m.Language.LanguageCode == langCode),
                        AttributesValuesXref = x.OrderItemAttributesValuesXref.Select(m => new
                        {
                            Xref = m,
                            InventoryAttribute = m.InventoryAttribute,
                            AttributeTranslation = m.InventoryAttribute.Attribute.Translations.FirstOrDefault(n => n.Language.LanguageCode == langCode),
                            AttributeValue = m.InventoryAttributeValue,
                            AttributeValueTranslation = m.InventoryAttributeValue.Translations.FirstOrDefault(n => n.Language.LanguageCode == langCode)
                        })
                    }),
                    Shipment = c.Shipment
                })
                .FirstOrDefaultAsync();

            if (translated == null)
                return null;

            return new TranslatedOrder
            {
                Id = translated.Order.Id,
                OrderNumber = translated.Order.OrderNumber,
                OrderStatus = translated.Order.OrderStatus,
                PaymentType = translated.Order.PaymentType,
                PaymentStatus = translated.Order.PaymentStatus,
                ShippingPrice = translated.Order.ShippingPrice,
                SubTotalPrice = translated.Order.SubTotalPrice,
                TotalPrice = translated.Order.TotalPrice,
                CreateDate = translated.Order.CreateDate,
                UpdateDate = translated.Order.UpdateDate,
                Shipment = translated.Shipment,
                OrderItems = translated.OrderItems.Select(c => new TranslatedOrderItem
                {
                    Id = c.OrderItem.Id,
                    Title = c.InventoryTranslation.Title,
                    Description = c.InventoryTranslation.Description,
                    Price = c.OrderItem.Price,
                    Quantity = c.OrderItem.Quantity,
                    Picture = c.Inventory.Pictures.FirstOrDefault(),
                    OrderItemAttributesValuesXref = c.AttributesValuesXref.Select(x => new TranslatedOrderItemAttributesValuesXref
                    {
                        Id = x.Xref.Id,
                        InventoryAttributeId = x.InventoryAttribute.Id,
                        InventoryAttributeTitle = x.AttributeTranslation.Title,
                        InventoryAttributeValueId = x.AttributeValue.Id,
                        InventoryAttributeValueTitle = x.AttributeValueTranslation.Title

                    }).ToList()

                }).ToList()
            };
        }

    }
}