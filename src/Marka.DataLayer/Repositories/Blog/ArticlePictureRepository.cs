﻿using Marka.Common.Models.Blog;
using Marka.DataLayer.Interfaces.Domain;
using Marka.DataLayer.Interfaces.Repositories.Blog;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Marka.DataLayer.Repositories.Blog
{
    public class ArticlePictureRepository : BaseRepository<ArticlePicture>, IArticlePictureRepository
    {
        public ArticlePictureRepository(IDataContext dataContext)
         : base(dataContext) { }

        public async Task<ArticlePicture> GetArticlePictureAsync(int id)
        {
            return await DataContext.ArticlePictures
                .FirstOrDefaultAsync(c => c.Article.Id == id);
        }
    }
}