﻿using Marka.Common.Models.Blog;
using Marka.Common.Models.Catalog.Categories;
using Marka.DataLayer.Interfaces.Domain;
using Marka.DataLayer.Interfaces.Repositories.Blog;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Marka.DataLayer.Repositories.Blog
{
    public class ArticleRepository : BaseRepository<Article>, IArticleRepository
    {
        public ArticleRepository(IDataContext dataContext)
           : base(dataContext) { }

        public async Task<Article> GetArticleAsync(int id)
        {
            return await DataContext.Articles
                .Include(c => c.Translations)
                .FirstOrDefaultAsync(c => c.Id == id);
        }

        public async Task<IEnumerable<Article>> GetArticlesAsync()
        {
            return await DataContext.Articles
                .Include(c => c.Translations)
                .ToListAsync();
        }

        public async Task<TranslatedArticle> GetTranslatedArticleAsync(int id, string langCode)
        {
            var translated = await DataContext.Articles
                .Include(c => c.Picture)
                .Include(c => c.Translations)
                .ThenInclude(c => c.Language)
                .Where(c => c.Id == id)
                .Select(c => new
                {
                    Article = c,
                    Translation = c.Translations.FirstOrDefault(x => x.Language.LanguageCode == langCode)
                }).FirstOrDefaultAsync();

            if (translated == null)
                return null;

            return new TranslatedArticle
            {
                Id = translated.Article.Id,
                Title = translated.Translation.Title,
                Description = translated.Translation.Description,
                Picture = translated.Article.Picture,
                CreateDate = translated.Article.CreateDate,
                UpdateDate = translated.Article.UpdateDate
            };
        }

        public async Task<IEnumerable<TranslatedArticle>> GetTranslatedArticlesAsync(string langCode)
        {
            var translatedList = DataContext.Articles
                .Include(c => c.Picture)
                .Include(c => c.Translations)
                .ThenInclude(c => c.Language)
                .Select(c => new
                {
                    Article = c,
                    Translation = c.Translations.FirstOrDefault(x => x.Language.LanguageCode == langCode)
                });

            return await translatedList.Select(c => new TranslatedArticle
            {
                Id = c.Article.Id,
                Title = c.Translation.Title,
                Description = c.Translation.Description,
                Picture = c.Article.Picture,
                CreateDate = c.Article.CreateDate,
                UpdateDate = c.Article.UpdateDate

            }).ToListAsync();
        }
    }
}