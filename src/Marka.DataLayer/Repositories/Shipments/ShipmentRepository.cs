﻿using Marka.Common.Models.Orders;
using Marka.Common.Models.Shipments;
using Marka.DataLayer.Interfaces.Domain;
using Marka.DataLayer.Interfaces.Repositories.Shipments;

namespace Marka.DataLayer.Repositories.Shipments
{
    public class ShipmentRepository : BaseRepository<Shipment>, IShipmentRepository
    {
        public ShipmentRepository(IDataContext dataContext)
        : base(dataContext) { }
    }
}