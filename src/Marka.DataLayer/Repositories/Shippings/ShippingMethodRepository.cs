﻿using Marka.Common.Models.Shippings;
using Marka.DataLayer.Interfaces.Domain;
using Marka.DataLayer.Interfaces.Repositories.Shippings;

namespace Marka.DataLayer.Repositories.Shippings
{
    public class ShippingMethodRepository : BaseRepository<ShippingMethod>, IShippingMethodRepository
    {
        public ShippingMethodRepository(IDataContext dataContext)
           : base(dataContext) { }
    }
}