﻿using Marka.Common.Models.Shippings;
using Marka.DataLayer.Interfaces.Domain;
using Marka.DataLayer.Interfaces.Repositories.Shippings;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Marka.DataLayer.Repositories.Shippings
{
    public class ShippingRateRepository : BaseRepository<ShippingRate>, IShippingRateRepository
    {
        public ShippingRateRepository(IDataContext dataContext)
           : base(dataContext) { }

        public async Task<IEnumerable<ShippingRate>> GetShippingRatesAsync(int id)
        {
            return null;
            //return await DataContext.ShippingRates
            //    .Where(c => c.ShippingMethodId == id)
            //    .ToListAsync();
        }
    }
}