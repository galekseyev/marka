﻿using Marka.Common.Models.Languages;
using Marka.DataLayer.Interfaces.Domain;
using Marka.DataLayer.Interfaces.Repositories.Languages;
using System;
using System.Collections.Generic;
using System.Text;

namespace Marka.DataLayer.Repositories.Languages
{
    public class LanguageRepository : BaseRepository<Language>, ILanguageRepository
    {
        public LanguageRepository(IDataContext dataContext)
            : base(dataContext) { }
    }
}
