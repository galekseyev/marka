﻿using Marka.Common.Models.Payments;
using Marka.DataLayer.Interfaces.Domain;
using Marka.DataLayer.Interfaces.Repositories.Payments;
using System;
using System.Collections.Generic;
using System.Text;

namespace Marka.DataLayer.Repositories.Payments
{
    public class PaymentTypeRepository : BaseRepository<PaymentType>, IPaymentTypeRepository
    {
        public PaymentTypeRepository(IDataContext dataContext)
            : base(dataContext) { }
    }
}