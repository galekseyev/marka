﻿using Marka.Common.Models.Payments;
using Marka.DataLayer.Interfaces.Domain;
using Marka.DataLayer.Interfaces.Repositories.Payments;
using System;
using System.Collections.Generic;
using System.Text;

namespace Marka.DataLayer.Repositories.Payments
{
    public class PaymentStatusRepository : BaseRepository<PaymentStatus>, IPaymentStatusRepository
    {
        public PaymentStatusRepository(IDataContext dataContext)
            : base(dataContext) { }
    }
}