﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Marka.DataLayer.Interfaces.Domain;
using Marka.Common.Models.Account.Users;
using Marka.Common.Models.Account.Roles;
using Marka.Common.Models.Catalog.Categories;
using Marka.Common.Models.Catalog.Inventories;
using Marka.Common.Models.Carts;
using Marka.Common.Models.Shippings;
using Marka.Common.Models.Orders;
using Marka.Common.Models.Languages;
using Marka.Common.Models.Catalog.Attributes;
using Marka.Common.Models.Catalog.Specifications;
using Marka.Common.Models.Catalog.InventoryGroups;
using Marka.Common.Models.Blog;
using Marka.Common.Models.Payments;
using Marka.Common.Models.Shipments;
using Marka.Common.Models.Countries;

namespace Marka.DataLayer.Domain
{
    public class DataContext : IdentityDbContext<User, Role, int>, IDataContext, IDisposable
    {
        public DataContext(DbContextOptions options) : base(options) { }

        //User
        public DbSet<UserAddress> UserAddresses { get; set; }

        //Languages
        public DbSet<Language> Languages { get; set; }

        //Countries
        public DbSet<Country> Countries { get; set; }
        public DbSet<CountryState> CountryStates { get; set; }

        //Catalog - Categories
        public DbSet<Category> Categories { get; set; }
        public DbSet<CategoryTranslation> CategoryTranslations { get; set; }
        public DbSet<CategoryPicture> CategoryPictures { get; set; }

        //Catalog - Attributes
        public DbSet<Common.Models.Catalog.Attributes.Attribute> Attributes { get; set; }
        public DbSet<AttributeTranslation> AttributeTranslations { get; set; }

        //Catalog - Specifications
        public DbSet<Specification> Specifications { get; set; }
        public DbSet<SpecificationTranslation> SpecificationTranslations { get; set; }
        public DbSet<SpecificationValue> SpecificationValues { get; set; }
        public DbSet<SpecificationValueTranslation> SpecificationValueTranslations { get; set; }

        //Catalog - Inventories
        public DbSet<Inventory> Inventories { get; set; }
        public DbSet<InventoryTranslation> InventoryTranslations { get; set; }
        public DbSet<InventoryPicture> InventoryPictures { get; set; }
        public DbSet<InventoryAttribute> InventoryAttributes { get; set; }
        public DbSet<InventoryAttributeValue> InventoryAttributeValues { get; set; }
        public DbSet<InventoryAttributeValueTranslation> InventoryAttributeValueTranslations { get; set; }
        public DbSet<InventorySpecification> InventorySpecifications { get; set; }

        //Catalog - InventoryGroups
        public DbSet<InventoryGroup> InventoryGroups { get; set; }
        public DbSet<InventoryGroupPicture> InventoryGroupPictures { get; set; }
        public DbSet<InventoryGroupInventory> InventoryGroupInventories { get; set; }

        //ShoppingCart
        public DbSet<Cart> Carts { get; set; }
        public DbSet<CartItem> CartItems { get; set; }
        public DbSet<CartItemAttributesValuesXref> CartItemAttributesValuesXrefs { get; set; }

        //Order
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderStatus> OrderStatus { get; set; }
        public DbSet<OrderItem> OrderItems { get; set; }
        public DbSet<OrderItemAttributesValuesXref> OrderItemAttributesValuesXrefs { get; set; }

        //Shipment
        public DbSet<Shipment> Shipments { get; set; }

        //Shipping
        //public DbSet<ShippingMethod> ShippingMethods { get; set; }
        //public DbSet<ShippingRate> ShippingRates { get; set; }

        //Payment
        public DbSet<PaymentType> PaymentType { get; set; }
        public DbSet<PaymentStatus> PaymentStatus { get; set; }

        //Article
        public DbSet<Article> Articles { get; set; }
        public DbSet<ArticleTranslation> ArticleTranslations { get; set; }
        public DbSet<ArticlePicture> ArticlePictures { get; set; }

        //Initialization
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            #region Identity Table Names

            modelBuilder.Entity<User>().ToTable("Users");
            modelBuilder.Entity<Role>().ToTable("Roles");
            modelBuilder.Entity<IdentityUserClaim<int>>().ToTable("UserClaims");
            modelBuilder.Entity<IdentityUserLogin<int>>().ToTable("UserLogins");
            modelBuilder.Entity<IdentityUserRole<int>>().ToTable("UserRoles");
            modelBuilder.Entity<IdentityRoleClaim<int>>().ToTable("RoleClaims");
            modelBuilder.Entity<IdentityUserToken<int>>().ToTable("UserTokens");

            #endregion

            #region Users

            modelBuilder.Entity<User>()
                .HasMany(e => e.Claims)
                .WithOne()
                .HasForeignKey(e => e.UserId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Logins)
                .WithOne()
                .HasForeignKey(e => e.UserId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Roles)
                .WithOne()
                .HasForeignKey(e => e.UserId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Cascade);

            #endregion

            #region Catalog

            modelBuilder.Entity<Category>()
                .HasMany(e => e.Translations)
                .WithOne(e => e.Category)
                .HasForeignKey(e => e.CategoryId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Category>()
                .HasOne(e => e.Picture)
                .WithOne(e => e.Category)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Common.Models.Catalog.Attributes.Attribute>()
                .HasMany(e => e.Translations)
                .WithOne(e => e.Attribute)
                .HasForeignKey(e => e.AttributeId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Specification>()
                .HasMany(e => e.Translations)
                .WithOne(e => e.Specification)
                .HasForeignKey(e => e.SpecificationId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Specification>()
                .HasMany(e => e.Values)
                .WithOne(e => e.Specification)
                .HasForeignKey(e => e.SpecificationId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<SpecificationValue>()
                .HasMany(e => e.Translations)
                .WithOne(e => e.SpecificationValue)
                .HasForeignKey(e => e.SpecificationValueId)
                .OnDelete(DeleteBehavior.Cascade);

            #endregion

            #region Inventory

            modelBuilder.Entity<Inventory>()
                .HasMany(e => e.Translations)
                .WithOne(e => e.Inventory)
                .HasForeignKey(e => e.InventoryId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Inventory>()
                .HasMany(e => e.Attributes)
                .WithOne(e => e.Inventory)
                .HasForeignKey(e => e.InventoryId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Inventory>()
                .HasMany(e => e.Pictures)
                .WithOne(e => e.Inventory)
                .HasForeignKey(e => e.InventoryId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<InventoryAttribute>()
                .HasMany(e => e.Values)
                .WithOne(e => e.InventoryAttribute)
                .HasForeignKey(e => e.InventoryAttributeId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<InventoryAttributeValue>()
                .HasMany(e => e.Translations)
                .WithOne(e => e.InventoryAttributeValue)
                .HasForeignKey(e => e.InventoryAttributeValueId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Inventory>()
                .HasMany(e => e.Specifications)
                .WithOne(e => e.Inventory)
                .HasForeignKey(e => e.InventoryId)
                .OnDelete(DeleteBehavior.Cascade);

            #endregion

            #region InventoryGroups

            modelBuilder.Entity<InventoryGroup>()
               .HasOne(e => e.Picture)
               .WithOne(e => e.InventoryGroup)
               .OnDelete(DeleteBehavior.Cascade);


            modelBuilder.Entity<InventoryGroup>()
                .HasMany(e => e.InventoryGroupInventories)
                .WithOne(e => e.InventoryGroup)
                .HasForeignKey(e => e.InventoryGroupId)
                .OnDelete(DeleteBehavior.Cascade);

            #endregion

            #region Carts

            modelBuilder.Entity<Cart>()
                .HasMany(e => e.CartItems)
                .WithOne(e => e.Cart)
                .HasForeignKey(e => e.CartId)
                .OnDelete(DeleteBehavior.Cascade);

            //modelBuilder.Entity<Cart>()
            //   .HasOne(e => e.CartShipping)
            //   .WithOne(e => e.Cart)
            //   .HasForeignKey<CartShipping>(e => e.CartId)
            //   .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<CartItem>()
                .HasMany(e => e.CartItemAttributesValuesXref)
                .WithOne(e => e.CartItem)
                .HasForeignKey(e => e.CartItemId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Cascade);

            #endregion

            #region Orders

            modelBuilder.Entity<Order>()
                .HasMany(e => e.OrderItems)
                .WithOne(e => e.Order)
                .HasForeignKey(e => e.OrderId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Order>()
                .HasOne(e => e.Shipment)
                .WithOne(e => e.Order)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Order>()
                .HasIndex(e => e.OrderNumber)
                .IsUnique();

            modelBuilder.Entity<OrderItem>()
                .HasMany(e => e.OrderItemAttributesValuesXref)
                .WithOne(e => e.OrderItem)
                .HasForeignKey(e => e.OrderItemId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Cascade);

            #endregion

            #region Blog

            modelBuilder.Entity<Article>()
                .HasMany(e => e.Translations)
                .WithOne(e => e.Article)
                .HasForeignKey(e => e.ArticleId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Article>()
               .HasOne(e => e.Picture)
               .WithOne(e => e.Article)
               .OnDelete(DeleteBehavior.Cascade);

            #endregion

            //Shipping
            //modelBuilder.Entity<ShippingMethod>()
            //    .HasMany(e => e.ShippingRates)
            //    .WithOne(e => e.ShippingMethod)
            //    .HasForeignKey(e => e.ShippingMethodId)
            //    .IsRequired()
            //    .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
