﻿using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Marka.Common.Models.Account.Users;
using Marka.Common.Models.Account.Roles;
using Marka.Common.Models.Catalog.Attributes;
using Marka.Common.Models.Catalog.Categories;
using Marka.Common.Models.Catalog.Inventories;
using Marka.Common.Models.Carts;
using Marka.Common.Models.Shippings;
using Marka.Common.Models.Orders;
using Marka.Common.Models.Languages;
using Marka.Common.Models.Catalog.Specifications;
using Marka.Common.Models.Catalog.InventoryGroups;
using Marka.Common.Models.Blog;
using Marka.Common.Models.Payments;
using Marka.Common.Models.Shipments;
using Marka.Common.Models.Countries;

namespace Marka.DataLayer.Interfaces.Domain
{
    public interface IDataContext
    {
        //User
        DbSet<UserAddress> UserAddresses { get; set; }

        //Languages 
        DbSet<Language> Languages { get; set; }

        //Countries
        DbSet<Country> Countries { get; set; }
        DbSet<CountryState> CountryStates { get; set; }

        //Account
        DbSet<User> Users { get; set; }
        DbSet<Role> Roles { get; set; }

        //Catalog - Categories
        DbSet<Category> Categories { get; set; }
        DbSet<CategoryTranslation> CategoryTranslations { get; set; }
        DbSet<CategoryPicture> CategoryPictures { get; set; }

        //Catalog - Attributes
        DbSet<Attribute> Attributes { get; set; }
        DbSet<AttributeTranslation> AttributeTranslations { get; set; }

        //Catalog - Specifications
        DbSet<Specification> Specifications { get; set; }
        DbSet<SpecificationTranslation> SpecificationTranslations { get; set; }
        DbSet<SpecificationValue> SpecificationValues { get; set; }
        DbSet<SpecificationValueTranslation> SpecificationValueTranslations { get; set; }

        //Catalog - Inventories
        DbSet<Inventory> Inventories { get; set; }
        DbSet<InventoryTranslation> InventoryTranslations { get; set; }
        DbSet<InventoryPicture> InventoryPictures { get; set; }
        DbSet<InventoryAttribute> InventoryAttributes { get; set; }
        DbSet<InventoryAttributeValue> InventoryAttributeValues { get; set; }
        DbSet<InventoryAttributeValueTranslation> InventoryAttributeValueTranslations { get; set; }
        DbSet<InventorySpecification> InventorySpecifications { get; set; }

        //Catalog - InventoryGroups
        DbSet<InventoryGroup> InventoryGroups { get; set; }
        DbSet<InventoryGroupPicture> InventoryGroupPictures { get; set; }
        DbSet<InventoryGroupInventory> InventoryGroupInventories { get; set; }

        //Shopping Cart
        DbSet<Cart> Carts { get; set; }
        DbSet<CartItem> CartItems { get; set; }
        DbSet<CartItemAttributesValuesXref> CartItemAttributesValuesXrefs { get; set; }

        //Order
        DbSet<Order> Orders { get; set; }
        DbSet<OrderStatus> OrderStatus { get; set; }
        DbSet<OrderItem> OrderItems { get; set; }
        DbSet<OrderItemAttributesValuesXref> OrderItemAttributesValuesXrefs { get; set; }

        //Shipment
        DbSet<Shipment> Shipments { get; set; }

        //Shipping - Add by country
        //DbSet<ShippingMethod> ShippingMethods { get; set; }
        //DbSet<ShippingRate> ShippingRates { get; set; }

        //Payment
        DbSet<PaymentType> PaymentType { get; set; }
        DbSet<PaymentStatus> PaymentStatus { get; set; }

        //Article
        DbSet<Article> Articles { get; set; }
        DbSet<ArticleTranslation> ArticleTranslations { get; set; }
        DbSet<ArticlePicture> ArticlePictures { get; set; }



        DatabaseFacade Database { get; }

        DbSet<TEntity> Set<TEntity>() where TEntity : class;
        EntityEntry<TEntity> Entry<TEntity>(TEntity entity) where TEntity : class;

        int SaveChanges();
        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken));
    }
}