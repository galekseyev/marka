﻿using Marka.Common.Models.Blog;
using Marka.DataLayer.Interfaces.Repositories.Base;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Marka.DataLayer.Interfaces.Repositories.Blog
{
    public interface IArticlePictureRepository : IBaseRepository<ArticlePicture>
    {
        Task<ArticlePicture> GetArticlePictureAsync(int id);
    }
}