﻿using Marka.Common.Models.Blog;
using Marka.DataLayer.Interfaces.Repositories.Base;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Marka.DataLayer.Interfaces.Repositories.Blog
{
    public interface IArticleRepository : IBaseRepository<Article>
    {
        Task<Article> GetArticleAsync(int id);
        Task<IEnumerable<Article>> GetArticlesAsync();

        Task<IEnumerable<TranslatedArticle>> GetTranslatedArticlesAsync(string langCode);
        Task<TranslatedArticle> GetTranslatedArticleAsync(int id, string langCode);
    }
}