﻿using Marka.Common.Models.Carts;
using Marka.DataLayer.Interfaces.Repositories.Base;
using System.Threading.Tasks;

namespace Marka.DataLayer.Interfaces.Repositories.Carts
{
    public interface ICartRepository : IBaseRepository<Cart>
    {
        Task<Cart> GetCartAsync(string id);
        Task<TranslatedCart> GetTranslatedCartAsync(string id, string langCode);
    }
}