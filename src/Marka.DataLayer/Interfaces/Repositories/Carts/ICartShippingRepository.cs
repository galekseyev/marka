﻿using Marka.Common.Models.Carts;
using Marka.DataLayer.Interfaces.Repositories.Base;

namespace Marka.DataLayer.Interfaces.Repositories.Carts
{
    public interface ICartShippingRepository : IBaseRepository<CartShipping>
    {
    }
}