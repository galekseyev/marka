﻿using Marka.Common.Models.Countries;
using Marka.DataLayer.Interfaces.Repositories.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace Marka.DataLayer.Interfaces.Repositories.Countries
{
    public interface ICountryRepository : IBaseRepository<Country>
    {
    }
}