﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Marka.DataLayer.Interfaces.Repositories.Base
{
    public interface IBaseRepository<T> where T : class
    {
        Task AddAsync(T entity);
        Task AddRangeAsync(IEnumerable<T> entity);

        Task UpdateAsync(T entity);
        Task UpdateManyAsync(IEnumerable<T> entities);

        Task DeleteAsync(T entity);
        Task DeleteAsync(Expression<Func<T, bool>> where);

        Task<T> GetByIdAsync(int id);
        Task<T> GetByIdAsync(string id);

        Task<T> GetFirstAsync(Expression<Func<T, bool>> where);

        Task<IEnumerable<T>> GetManyAsync(Expression<Func<T, bool>> where);
        Task<IEnumerable<T>> GetAllAsync();
    }
}
