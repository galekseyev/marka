﻿using Marka.Common.Models.Shippings;
using Marka.DataLayer.Interfaces.Repositories.Base;

namespace Marka.DataLayer.Interfaces.Repositories.Shippings
{
    public interface IShippingMethodRepository : IBaseRepository<ShippingMethod>
    {
    }
}
