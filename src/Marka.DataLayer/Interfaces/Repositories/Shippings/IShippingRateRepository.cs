﻿using Marka.Common.Models.Shippings;
using Marka.DataLayer.Interfaces.Repositories.Base;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Marka.DataLayer.Interfaces.Repositories.Shippings
{
    public interface IShippingRateRepository : IBaseRepository<ShippingRate>
    {
        Task<IEnumerable<ShippingRate>> GetShippingRatesAsync(int id);
    }
}