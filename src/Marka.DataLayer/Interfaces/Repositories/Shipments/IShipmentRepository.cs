﻿using Marka.Common.Models.Orders;
using Marka.Common.Models.Shipments;
using Marka.DataLayer.Interfaces.Repositories.Base;

namespace Marka.DataLayer.Interfaces.Repositories.Shipments
{
    public interface IShipmentRepository : IBaseRepository<Shipment>
    {
    }
}
