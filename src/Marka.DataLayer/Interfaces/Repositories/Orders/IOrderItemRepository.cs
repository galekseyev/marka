﻿using Marka.Common.Models.Orders;
using Marka.DataLayer.Interfaces.Repositories.Base;

namespace Marka.DataLayer.Interfaces.Repositories.Orders
{
    public interface IOrderItemRepository : IBaseRepository<OrderItem>
    {
    }
}
