﻿using Marka.Common.Models.Orders;
using Marka.DataLayer.Interfaces.Repositories.Base;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Marka.DataLayer.Interfaces.Repositories.Orders
{
    public interface IOrderStatusRepository : IBaseRepository<OrderStatus>
    {
    }
}