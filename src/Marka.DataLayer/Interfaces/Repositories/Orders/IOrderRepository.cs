﻿using Marka.Common.Models.Orders;
using Marka.DataLayer.Interfaces.Repositories.Base;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Marka.DataLayer.Interfaces.Repositories.Orders
{
    public interface IOrderRepository : IBaseRepository<Order>
    {
        Task<Order> GetOrderAsync(int id);
        Task<IEnumerable<Order>> GetOrdersAsync();
        Task<IEnumerable<Order>> GetUserOrdersAsync(int userId);

        Task<TranslatedOrder> GetTranslatedOrderAsync(int id, string langCode);
        Task<TranslatedOrder> GetUserTranslatedOrderAsync(int id, int userId, string langCode);
    }
}