﻿using Marka.Common.Models.Languages;
using Marka.DataLayer.Interfaces.Repositories.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace Marka.DataLayer.Interfaces.Repositories.Languages
{
    public interface ILanguageRepository : IBaseRepository<Language>
    {
    }
}