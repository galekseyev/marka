﻿using Marka.Common.Models.Catalog.Categories;
using Marka.DataLayer.Interfaces.Repositories.Base;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Marka.DataLayer.Interfaces.Repositories.Catalog.Categories
{
    public interface ICategoryPictureRepository : IBaseRepository<CategoryPicture>
    {
        Task<CategoryPicture> GetCategoryPictureAsync(int id);
    }
}