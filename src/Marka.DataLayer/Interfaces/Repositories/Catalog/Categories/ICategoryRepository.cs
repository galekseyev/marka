﻿using Marka.Common.Models.Catalog.Categories;
using Marka.DataLayer.Interfaces.Repositories.Base;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Marka.DataLayer.Interfaces.Repositories.Catalog.Categories
{
    public interface ICategoryRepository : IBaseRepository<Category>
    {
        Task<IEnumerable<Category>> GetCategoriesAsync();
        Task<Category> GetCategoryAsync(int id);
        Task<IEnumerable<TranslatedCategory>> GetTranslatedCategoriesAsync(string langCode);
        Task<TranslatedCategory> GetTranslatedCategoryAsync(int id, string langCode);
    }
}