﻿using Marka.Common.Models.Catalog.Specifications;
using Marka.DataLayer.Interfaces.Repositories.Base;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Marka.DataLayer.Interfaces.Repositories.Catalog.Specifications
{
    public interface ISpecificationRepository : IBaseRepository<Specification>
    {
        Task<IEnumerable<Specification>> GetSpecificationsAsync();
        Task<Specification> GetSpecificationAsync(int id);
        Task<IEnumerable<TranslatedSpecification>> GetTranslatedSpecificationsAsync(string langCode);
        Task<TranslatedSpecification> GetTranslatedSpecificationAsync(int id, string langCode);
    }
}