﻿using Marka.Common.Models.Catalog.Specifications;
using Marka.DataLayer.Interfaces.Repositories.Base;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Marka.DataLayer.Interfaces.Repositories.Catalog.Specifications
{
    public interface ISpecificationValueRepository : IBaseRepository<SpecificationValue>
    {
        Task<IEnumerable<SpecificationValue>> GetSpecificationValuesAsync(int id);

        Task<IEnumerable<TranslatedSpecificationValue>> GetTranslatedSpecificationValuesAsync(int id, string langCode);
    }
}
