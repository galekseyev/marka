﻿using Marka.Common.Models.Catalog.Inventories;
using Marka.Common.Models.Catalog.Specifications;
using Marka.DataLayer.Interfaces.Repositories.Base;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Marka.DataLayer.Interfaces.Repositories.Catalog.Inventories
{
    public interface IInventorySpecificationRepository : IBaseRepository<InventorySpecification>
    {
        Task<IEnumerable<TranslatedInventorySpecification>> GetTranslatedInventorySpecificationsAsync(int id, string langCode);
    }
}