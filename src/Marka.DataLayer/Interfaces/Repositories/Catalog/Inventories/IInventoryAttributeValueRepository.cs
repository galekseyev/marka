﻿using Marka.Common.Models.Catalog.Inventories;
using Marka.DataLayer.Interfaces.Repositories.Base;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Marka.DataLayer.Interfaces.Repositories.Catalog.Inventories
{
    public interface IInventoryAttributeValueRepository : IBaseRepository<InventoryAttributeValue>
    {
        Task<IEnumerable<InventoryAttributeValue>> GetInventoryAttributeValuesAsync(int id, int attributeId);

        Task<IEnumerable<TranslatedInventoryAttributeValue>> GetTranslatedInventoryAttributeValuesAsync(int id, int attributeId, string langCode);
    }
}