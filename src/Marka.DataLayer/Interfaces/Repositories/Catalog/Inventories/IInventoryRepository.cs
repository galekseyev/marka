﻿using Marka.Common.Models.Catalog.Inventories;
using Marka.Common.Models.Filters;
using Marka.DataLayer.Interfaces.Repositories.Base;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Marka.DataLayer.Interfaces.Repositories.Catalog.Inventories
{
    public interface IInventoryRepository : IBaseRepository<Inventory>
    {
        Task<Inventory> GetInventoryAsync(int id);
        Task<IEnumerable<Inventory>> GetInventoriesAsync();


        Task<TranslatedInventories> GetTranslatedInventoriesAsync(InventoryFilter filter, string langCode);
        Task<TranslatedInventory> GetTranslatedInventoryAsync(int id, string langCode);
    }
}