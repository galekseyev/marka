﻿using Marka.Common.Models.Catalog.Inventories;
using Marka.DataLayer.Interfaces.Repositories.Base;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Marka.DataLayer.Interfaces.Repositories.Catalog.Inventories
{
    public interface IInventoryAttributeRepository : IBaseRepository<InventoryAttribute>
    {
        /// <summary>
        /// Gets inventory attributes collection by inventory id.
        /// </summary>
        /// <param name="id">Inventory id</param>
        /// <returns>Returns inventory attributes collection.</returns>
        Task<IEnumerable<InventoryAttribute>> GetInventoryAttributesAsync(long id);

        /// <summary>
        /// Gets inventory attribute with all related data by inventory attribute id.
        /// </summary>
        /// <param name="id">Inventory attribute id</param>
        /// <returns>Returns inventory attribute</returns>
        Task<InventoryAttribute> GetInventoryAttributeAsync(int id);


        Task<IEnumerable<TranslatedInventoryAttribute>> GetTranslatedInventoryAttributesAsync(int id, string langCode);
    }
}