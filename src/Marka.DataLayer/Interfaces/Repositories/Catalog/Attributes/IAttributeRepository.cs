﻿using Marka.Common.Models.Catalog.Attributes;
using Marka.DataLayer.Interfaces.Repositories.Base;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Marka.DataLayer.Interfaces.Repositories.Catalog.Attributes
{
    public interface IAttributeRepository : IBaseRepository<Attribute>
    {
        Task<IEnumerable<Attribute>> GetAttributesAsync();
        Task<Attribute> GetAttributeAsync(int id);
        Task<IEnumerable<TranslatedAttribute>> GetTranslatedAttributesAsync(string langCode);
        Task<TranslatedAttribute> GetTranslatedAttributeAsync(int id, string langCode);
    }
}