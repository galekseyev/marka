﻿using Marka.Common.Models.Catalog.InventoryGroups;
using Marka.Common.Models.Filters;
using Marka.DataLayer.Interfaces.Repositories.Base;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Marka.DataLayer.Interfaces.Repositories.Catalog.InventoryGroups
{
    public interface IInventoryGroupRepository : IBaseRepository<InventoryGroup>
    {
        Task<InventoryGroup> GetInventoryGroupAsync(int id);
        Task<FilteredInventoryGroups> GetInventoryGroupsAsync(InventoryGroupFilter filter);
        Task<TranslatedInventoryGroup> GetTranslatedInventoryGroup(int id, string langCode);
    }
}