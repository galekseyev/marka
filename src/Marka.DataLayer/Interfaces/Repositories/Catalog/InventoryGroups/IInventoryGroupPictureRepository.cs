﻿using Marka.Common.Models.Catalog.InventoryGroups;
using Marka.DataLayer.Interfaces.Repositories.Base;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Marka.DataLayer.Interfaces.Repositories.Catalog.InventoryGroups
{
    public interface IInventoryGroupPictureRepository : IBaseRepository<InventoryGroupPicture>
    {
    }
}