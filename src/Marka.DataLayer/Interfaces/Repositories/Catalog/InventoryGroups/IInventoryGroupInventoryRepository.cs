﻿using Marka.Common.Models.Catalog.InventoryGroups;
using Marka.DataLayer.Interfaces.Repositories.Base;
using Marka.DataLayer.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Marka.DataLayer.Interfaces.Repositories.Catalog.InventoryGroups
{
    public interface IInventoryGroupInventoryRepository : IBaseRepository<InventoryGroupInventory>
    {
        Task<IEnumerable<TranslatedInventoryGroupInventory>> GetTranslatedInventoryGroupInventoriesAsync(int id, string langCode);
    }
}