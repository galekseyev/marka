﻿using Marka.Common.Models.Account.Users;
using Marka.DataLayer.Interfaces.Repositories.Base;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Marka.DataLayer.Interfaces.Repositories.Users
{
    public interface IUserAddressRepository : IBaseRepository<UserAddress>
    {
        Task<UserAddress> GetUserAddressAsync(int userId);
    }
}