﻿using Marka.Common.Models.Payments;
using Marka.DataLayer.Interfaces.Repositories.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace Marka.DataLayer.Interfaces.Repositories.Payments
{
    public interface IPaymentTypeRepository : IBaseRepository<PaymentType>
    {

    }
}